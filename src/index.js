import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux'
import Amplify from "aws-amplify"
import configAws from './app/state/utils/envAws'
import { unregister } from './serviceWorker'
import Routes from './app/routes/'
import store from './app/state/utils/createStore'
import {
  // ToastConsumer,
  ToastProvider,
  // withToastManager,
} from 'react-toast-notifications'
// Element language settings
import { i18n } from 'element-react'
import locale from 'element-react/src/locale/lang/es'

// ESTILOS
import 'react-table/react-table.css'
//Stilos de element
import 'element-theme-default'
import '@fortawesome/fontawesome-free/css/all.min.css'
import 'bootstrap-css-only/css/bootstrap.min.css'
import 'mdbreact/dist/css/mdb.css'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import './assets/index.css'
//DatePicker
import 'react-datepicker/dist/react-datepicker.css'

window._log = (message) => {
  console.log(`%c ${'='.repeat(50)}`, 'font-size: 16px; background-color: yellow; color: red;')
  console.log(message)
  console.log(`%c ${'='.repeat(50)}`, 'font-size: 16px; background-color: black; color: red;')
}

const AUTH_WITHOUT_FEDERATION_CONFIG = {
  userPoolId: configAws.cognito.USER_POOL_ID,
  identityPoolId: configAws.cognito.IDENTITY_POOL_ID,
  userPoolWebClientId: configAws.cognito.APP_CLIENT_ID,
}

const AUTH_WITH_FEDERATION_CONFIG = {
  identityPoolId: configAws.cognito.FEDERATED_IDENTITY_POOL_ID,
  userPoolId: configAws.cognito.FEDERATED_USER_POOL_ID,
  userPoolWebClientId: configAws.cognito.FEDERATED_APP_CLIENT_ID,
  oauth: {
    domain: process.env.NODE_ENV === 'development' ? configAws.LOCAL_VARIABLES.REACT_APP_OAUTH_DOMAIN : process.env.REACT_APP_OAUTH_DOMAIN,
    scope: ['phone', 'email', 'profile', 'openid', 'aws.cognito.signin.user.admin'],
    redirectSignIn: `${process.env.NODE_ENV === 'development' ? configAws.LOCAL_VARIABLES.REACT_APP_OAUTH_REDIRECT_SIGN_IN : process.env.REACT_APP_OAUTH_REDIRECT_SIGN_IN}`,
    redirectSignOut: `${process.env.NODE_ENV === 'development' ? configAws.LOCAL_VARIABLES.REACT_APP_OAUTH_REDIRECT_SIGN_OUT : process.env.REACT_APP_OAUTH_REDIRECT_SIGN_OUT}`,
    /**
     * 'code' / 'token', note that REFRESH token will only be
     * generated when the responseType is 'code'
     */
    responseType: 'token',
  },
}
console.log("Hello", process.env.REACT_APP_API_URL)
console.log("Este es mi NODE_ENV:", process.env.NODE_ENV)

Amplify.configure({
  Auth: {
    mandatorySignIn: true,
    region: configAws.cognito.REGION,
    // ...AUTH_WITHOUT_FEDERATION_CONFIG,
    ...AUTH_WITH_FEDERATION_CONFIG
  },
  Storage: {
    region: configAws.s3.REGION,
    bucket: configAws.s3.BUCKET,
    identityPoolId: configAws.cognito.FEDERATED_IDENTITY_POOL_ID
  },
  API: {
    endpoints: [
      {
        name: configAws.apiGateway.NAME,
        endpoint: configAws.apiGateway.URL,
        region: configAws.apiGateway.REGION
      },
    ]
  }
});


unregister();
i18n.use(locale);

render(
  <Provider store={store}>
    <ToastProvider>
      <Routes />
    </ToastProvider>
  </Provider>,
  document.getElementById('root'),
);

unregister();

