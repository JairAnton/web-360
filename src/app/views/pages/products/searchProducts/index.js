import React, { Component, useState, useMemo, useEffect } from 'react'
import { Animated } from "react-animated-css";

import Select from 'react-select'
import { Button } from 'element-react';
import { constants as CONSTANTS } from 'state/utils/constants'
import ProductsNavDetalle from './productsNavDetalle';
import { policyOperations } from 'state/ducks/policy';
import { Layout } from 'element-react'

import { Alert } from 'element-react'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { withToastManager } from 'react-toast-notifications'

import { MDBCard, MDBCardHeader, MDBCardBody, MDBRow } from 'mdbreact';

import { salesforceOperations } from 'state/ducks/salesforce'
import { sinisterOperations } from 'state/ducks/sinister'
import { operationOperations } from 'state/ducks/operation'
import { personOperations, personLoaderKeys } from 'state/ducks/person'
import { generalOperations } from 'state/ducks/general'
import PubSub from 'state/utils/PubSub'


const TIPOS_DOC=[
    {
        label: 'RUC',
        value: '1'
      },
      {
        label: 'DNI',
        value: '2'
      },
      {
        label: 'PASAPORTE',
        value: '3'
      },
      {
        label: 'C.E.',
        value: '4'
      },
    
]
const ESTADOS_POLIZAS = [
    {
      label: 'Vigentes',
      value: 'PV'
    },
    {
      label: 'No-vigentes',
      value: 'PN'
    },
    {
      label: 'Futuros',
      value: 'PR'
    },
    {
      label: 'Anulados',
      value: 'PA'
    },
    {
      label: 'Todos',
      value: ''
    },
  ]


const PN_POLICY_COLUMNS = [
    {
      label: "Ramo",
      prop: "ramo",
      minWidth: 150
    },
    {
      label: "Producto",
      prop: "desProducto",
      minWidth: 170
    },
    {
      label: "Estado\nPóliza",
      prop: "estadoPoliza",
      minWidth: 100
    },
    {
      label: "Estado\nCertificado",
      prop: "estadoCertificado",
      minWidth: 115
    },
    {
      label: "Cód.\nProducto",
      prop: "codProdAcselx",
      minWidth: 90
    },
    {
      label: "N° Póliza",
      prop: "numPoliza",
      minWidth: 105
    },
    {
      label: "Fecha Inicio\nPóliza",
      prop: "fecIniVigencia",
      width: 100,
      render: (row, column, _index) => (
        <span>
          {
              //changeFormatDate(row[column.prop], '-', '/')
          }
        </span>
      ),
    },
    {
      label: "Fecha Fin\nPóliza",
      prop: "fecFinVigencia",
      width: 100,
      render: (row, column, _index) => (
        <span>
          {
              //changeFormatDate(row[column.prop], '-', '/')
            }
        </span>
      ),
    },
    {
      label: "N° Certificado",
      prop: "numCertificado",
      minWidth: 140
    },
    {
      label: 'Corredor',
      prop: 'nomBroker',
      minWidth: 190
    },
    {
      label: "Fecha Exclusión\nAsegurado",
      prop: "fecExcluAsegurado",
      width: 150
    },
  ]
  
const PJ_POLICY_COLUMNS = [
    {
      label: "Ramo",
      prop: "ramo",
      minWidth: 150
    },
    {
      label: "Producto",
      prop: "desProducto",
      minWidth: 150
    },
    {
      label: "Estado Póliza",
      prop: "estadoPoliza",
      minWidth: 150
    },
    {
      label: "Cód Producto",
      prop: "codProducto",
      minWidth: 120
    },
    {
      label: "Póliza",
      prop: "numPoliza",
      minWidth: 150
    },
  
    {
      label: "Fecha Inicio",
      prop: "fecIniVigencia",
      minWidth: 100,
      render: (row, column, _index) => (
        <span>
          {
              //changeFormatDate(row[column.prop], '-', '/')
              }
        </span>
      ),
    },
    {
      label: "Fecha Fin",
      prop: "fecFinVigencia",
      minWidth: 100,
      render: (row, column, _index) => (
        <span>
          {
              //changeFormatDate(row[column.prop], '-', '/')
              }
        </span>
      ),
    },
    {
      label: 'Corredor',
      prop: 'nomBroker',
      minWidth: 170
    },
]





class SearchProducts extends React.Component {
    
  constructor(props){
    super(props)

    this.state = {
      "contador":4,
      "search": false,
      "body":{
        "ideTercero": "",
        "codExterno": "",
        "codAfiliado": "",
        "numPlaca": "", 	
        "numMotor": "",
        "nombreTitular": "",
        "estado": "",
        "tipoDocumento": "",
        "numDocumento": "",
        "ramo":"",
        "numPoliza":""
      }
    }


  }
  

  componentDidMount() {
    const { toastManager, policyOperations, salesforceOperations, sinisterOperations, operationOperations } = this.props
    
    // auditoria
    let requestLogging = {
      "path": window.location.href,
      "action": "PAGE_POLIZAS"
    }
    operationOperations.auditLogging(requestLogging)

    policyOperations.resetDataPolicy()
    salesforceOperations.resetDataSalesforce()
    sinisterOperations.resetDataSinister()
    operationOperations.resetDataOperation()

    this.getDataCombo()
  } 

  
  getColumnsTableProduct = () => {
    const { personType } = this.props
    if (personType === CONSTANTS.PN)
      return PN_POLICY_COLUMNS
    else
      return PJ_POLICY_COLUMNS
  }
  
  validateForm = () => {
    const body = this.state.body
    if(body.tipoDocumento !== ''  && body.numDocumento.length === 0){
      PubSub.publish('WARNING_SEARCH_THIRD', 'Debe llenar el número de documento.');
      return false
    }
    if(body.tipoDocumento === '1' && body.numDocumento.length !== 11)
    {
      PubSub.publish('WARNING_SEARCH_THIRD', 'Debe llenar el número de documento válido.');
      return false;
    }  
    if(body.tipoDocumento === '2' && body.numDocumento.length !== 8)
    {
      PubSub.publish('WARNING_SEARCH_THIRD', 'Debe llenar el número de documento válido.');
      return false;
    }
    if(body.tipoDocumento === '3' && body.numDocumento.length === 0)
    {
      PubSub.publish('WARNING_SEARCH_THIRD', 'Debe llenar el número de documento válido.');
      return false;
    }
    if(body.tipoDocumento === '4' && body.numDocumento.length === 0)
    {
      PubSub.publish('WARNING_SEARCH_THIRD', 'Debe llenar el número de documento válido.');
      return false;
    }

    if(body.ramo !== '' && body.numPoliza.length === 0)
    {
      
      PubSub.publish('WARNING_SEARCH_THIRD', 'Debe llenar el número de poliza.');
      return false;
    }
    return true
  }
  
  LimpiarFormulario = () =>{
    
    this.setState({
      body:{
        "ideTercero": "",
        "codExterno": "",
        "codAfiliado": "",
        "numPlaca": "", 	
        "numMotor": "",
        "nombreTitular": "",
        "estado": "",
        "tipoDocumento": "",
        "numDocumento": "",
        "ramo":"",
        "numPoliza":""
      }
    })
  }

  

searchPoliza () {
  


  if(this.validateForm()){
    const body = this.state.body
      let requestBody = {
        "filter": {
          "ideTercero": "",
          "codExterno": "",
          "codAfiliado": "",
          "numPlaca": body.numPlaca ? body.numPlaca : '',
          "numMotor": body.numMotor ? body.numMotor : '',
          "estado": body.estado ? body.estado : '',
          "tipoDocumento": body.tipoDocumento ? body.tipoDocumento : '',
          "numDocumento": body.numDocumento ? body.numDocumento : '',
          "ramo": body.ramo ? body.ramo : '',
          "numPoliza": body.numPoliza ? body.numPoliza : ''
        },
        "pagination": {
            "filasPorPag": 10,
            "pagActual": 1
        }
      }

    const {policyOperations} = this.props
    policyOperations.policyList(requestBody)
    
    // auditoria
    let requestLogging = {
      "path": window.location.href,
      "action": "POLICY_LIST"
    }
    policyOperations.auditLogging(requestLogging)
    this.setState({
      search:false
    })
    }
}

handleChange = (evnt) => {
  
  let cutNumDoc = false
  let long = 15;
  const { body } = this.state
  let {name, value} = evnt.target

  if (name === "numDocumento") {
    if (body.tipoDocumento === '1') {
      long = 11
      cutNumDoc = value.length > long
    } 
    else if (body.tipoDocumento === '2') {
      long = 8
      cutNumDoc = value.length > long
    }
    else if (body.tipoDocumento === '') {
      long = 0
      cutNumDoc = value.length > long
    }
    else if (body.tipoDocumento === '3') {
      long = 20
      cutNumDoc = value.length > long
    }
    else if (body.tipoDocumento === '4') {
      long = 20
      cutNumDoc = value.length > long
    }
    if (cutNumDoc) {
      value = value.slice(0, long)
    }
  }
  if(name === "numPlaca"){
    long = 6
    cutNumDoc = value.length > long

    if (cutNumDoc) {
      value = value.slice(0, long)
    }
  }
  if(name === "numMotor"){
    long = 20
    cutNumDoc = value.length > long

    if (cutNumDoc) {
      value = value.slice(0, long)
    }
  }
  if(name === "numPoliza"){
    long = 10
    cutNumDoc = value.length > long

    if (cutNumDoc) {
      value = value.slice(0, long)
    }
  }

  if (name === "tipoDocumento") body.numDocumento = ''

  this.setState({
    body: {
      ...body,
      [name]: value.toUpperCase()
    }
  })
}

getDataCombo = () =>{
  let requestBody = {
    "request": {
      "trace": {
        "serviceId":"SERV01",
        "consumerId":"COS01",
        "moduleId": "MOD01",
        "channelCode": "WEB",
        "traceId": "TRACEID01",
        "timestamp": "2019-01-24 10:00:00",
        "identity": {
          "userId": "USER01",
          "deviceId": "DEV01",
          "host": "HOST01"
        }
      },
      "payload": {
      }
    }
  }
  const {policyOperations} = this.props
  policyOperations.policyListRamos(requestBody)
  
  // auditoria
  let requestLogging = {
    "path": window.location.href,
    "action": "POLICY_LIST_RAMOS"
  }
  policyOperations.auditLogging(requestLogging)

}

selectPolicy = async (selected) => {
  const { policyOperations } = this.props
  const requestBody = {
    core: '',
    codProducto: '',
    codProdAcselx: '',
    numPoliza: '',
    numCertificado: '',
    codAfiliado: '',
    codCliente: '',
    codPlan: '',
    idePoliza: '',
  }
  requestBody.core = selected.core
  requestBody.codProducto = selected.codProducto ? selected.codProducto.toString() : ""
  requestBody.codProdAcselx = selected.codProdAcselx ? selected.codProdAcselx.toString() : ""
  requestBody.numPoliza = selected.numPoliza ? selected.numPoliza.toString() : ""
  requestBody.numCertificado = selected.numCertificado ? selected.numCertificado.toString() : ""
  requestBody.codAfiliado = selected.codAfiliado ? selected.codAfiliado.toString() : ""
  requestBody.codCliente = selected.codCliente ? selected.codCliente.toString() : ""
  requestBody.codPlan = selected.codPlan ? selected.codPlan.toString() : ""
  requestBody.idePoliza = selected.idePoliza ? selected.idePoliza.toString() : ""

  policyOperations.policySelect(selected)
  let requestLogging = {
    "path": window.location.href,
    "action": "POLICY_SELECT"
  }
  policyOperations.auditLogging(requestLogging)
  {/* Enviar endpoint auditoria*/ }
  setTimeout(function () {
    policyOperations.policyDetail(requestBody)
    let requestLogging = {
      "path": window.location.href,
      "action": "POLICY_DETAIL"
    }
    policyOperations.auditLogging(requestLogging)
    {/* Enviar endpoint auditoria*/ }
  }, 500)
  // auditoria
  
}
getListPolicy = async (filasPorPag = 10, pagActual = 1, rbProductosValue = 'PV') => {
  const { person: { select }, policyOperations, } = this.props
  let requestBody = {
    filter: {
      ideTercero: (select.ideTercero) ? select.ideTercero.toString() : "",
      codExterno: (select.codExterno) ? select.codExterno.toString() : "",
      codAfiliado: (select.codAfiliado) ? select.codAfiliado.toString() : "",
      numPlaca: "",
      numMotor: "",
      nombreTitular: "",
      estado: rbProductosValue,
      tipoDocumento: (select.codTipoDocumento) ? select.codTipoDocumento.toString() : "",
      numDocumento: (select.numDocumento) ? select.numDocumento.toString() : "",
    },
    pagination: {
      filasPorPag,
      pagActual,
    }
  }

  policyOperations.policyList(requestBody)
  // auditoria
  let requestLogging = {
    "path": window.location.href,
    "action": "POLICY_LIST"
  }
  policyOperations.auditLogging(requestLogging)
}



  render(){
    const { policy, policyList, person, policyL } = this.props

    var loadCombo =  () => {  
      if(policy){
        var response = policy.dataList.map((item)=> (
            {
              'value':item.desRamo,
              'label':item.desRamo
            }
          )
        ).sort((a, b) => {
            if (a.value > b.value) {
              return 1
            }
            if (b.value > a.value) {
              return -1
            }
            return 0
          })
        return response
      } 
      else{
        return []
      }
    } 


    return (
      <div>
        <Animated animationIn="bounceInLeft" animationOut="bounceOutLeft" animationInDuration={2000} animationOutDuration={2000} isVisible={true}
            style={{
              "position": "relative",
              "zIndex": "0"
            }}
          >
            <div className="row">
              <div className="col-lg">
                <MDBCard>
                  <MDBCardHeader color="lighten-1"
                    style={{
                      "backgroundColor": "#E52E2D"
                    }}
                  >Búsqueda de pólizas</MDBCardHeader>
                  <MDBCardBody >
                    <form  >
                      <div className="row" >
                        <div className="col-lg-4" >
                          <div className="form-group row" >
                            <label className="col-sm-5 col-form-label">Ramo:</label>
                            <div className="col-sm-7" >
                              <Select placeholder="Seleccionar" 
                                name="ramo"
                                options={loadCombo()}
                                onChange={(val) => {
                                  this.handleChange({
                                    target: {
                                      name: 'ramo',
                                      value: val ? val.value : ''
                                    }
                                  })
                                }}
                                isClearable
                              />
                            </div>
                          </div>
                        </div>
                        <div className="col-lg-4">
                          <div className="form-group row">
                            <label className="col-sm-5 col-form-label">N° de poliza:</label>
                            <div className="col-sm-7" >
                                <input
                                  className="form-control"
                                  type="text"
                                  name="numPoliza"
                                  onChange={this.handleChange}
                                  value={this.state.body.numPoliza}
                                />
                            </div>
                          </div>
                        </div>
                        <div className="col-lg-4">
                          <div className="form-group row">
                            <label className="col-sm-5 col-form-label">Estado:</label>
                            <div className="col-sm-7" >
                              <Select placeholder="Seleccionar"
                                name="estado"
                                options={ESTADOS_POLIZAS}
                                onChange={(val) => {
                                    this.handleChange({
                                      target: {
                                        name: 'estado',
                                        value: val ? val.value : ''
                                      }
                                    })
                                }}
                                isClearable
                              />
                            </div>
                          </div>
                        </div>
                        
                      </div>
  
                      <div className="row">
                        
                        <div className="col-lg-4">
                          <div className="form-group row">
                            <label className="col-sm-5 col-form-label">Placa:</label>
                            <div className="col-sm-7" >
                                <input
                                  className="form-control"
                                  type="text"
                                  name="numPlaca"
                                  onChange={this.handleChange}
                                  value={this.state.body.numPlaca}
                                />
                            </div>
                          </div>
                        </div>
                        <div className="col-lg-4">
                          <div className="form-group row">
                            <label className="col-sm-5 col-form-label">Motor:</label>
                            <div className="col-sm-7" >
                              <input
                                className="form-control"
                                type="text"
                                name="numMotor"
                                onChange={this.handleChange}
                                value={this.state.body.numMotor}
                              />
                            </div>
                          </div>
                        </div>
                        
                      </div>
  
                      <div className="row">
                      <div className="col-lg-4">
                          <div className="form-group row">
                            <label className="col-sm-5 col-form-label">T. Documento:</label>
                            <div className="col-sm-7" >
                            <Select placeholder="Seleccionar"
                                name="tipoDocumento"
                                options={TIPOS_DOC}
                                onChange={(val) => {
                                    this.handleChange({
                                      target: {
                                        name: 'tipoDocumento',
                                        value: val ? val.value : ''
                                      }
                                    })
                                }}
                                
                                isClearable
                              />
                            </div>
                          </div>
                        </div>
                        <div className="col-lg-4">
                          <div className="form-group row">
                            <label className="col-sm-5 col-form-label">N° documento:</label>
                            <div className="col-sm-7" >
                              <input
                                className="form-control"
                                type="text"
                                name="numDocumento"
                                onChange={this.handleChange}
                                value={this.state.body.numDocumento}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </form>
                  </MDBCardBody>
                </MDBCard>
              </div>
            </div>
          </Animated>
          <br />
          <Animated animationIn="pulse" animationOut="tada" animationInDuration={1000} animationOutDuration={1000} isVisible={true}>
            <div className="row justify-content-md-center">
              <div className="col-2">
                <Button
                  type="danger"
                  onClick={() => {
                    this.searchPoliza()
                  }}
                >
                  <span className="icon text-white-50">
                    <i className="fas fa-search"></i>
                  </span>
                  <span className="text">{"  Buscar"}</span>
                </Button>
              </div>
              <div className="col-2">
                <Button
                  type="danger"
                  onClick={()=>{
                    this.LimpiarFormulario()
                    }
                  }
                >
                  <span className="icon text-white-50">
                    <i className="fas fa-eraser"></i>
                  </span>
                  <span className="text">{"  Limpiar"}</span>
                </Button>
              </div>
            </div>
          </Animated>
  
          <div style={{paddingTop:'60px'}}>
            <ProductsNavDetalle
                productsFA={policyList}
                selectPolicy={this.selectPolicy}
                getPolicyList={this.getListPolicy}
                personType={"PJ"}
                policy={policyL}
                newSelected={this.state.search}
                  />
          </div>
            <br />
          <MDBRow center>
            <div className="col-lg-12" style={{ cursor: 'pointer' }}>
                
              </div>
          </MDBRow>
          <div
            style={{ width: '10px', height: '10px' }}
            id="searchCase_scrollable_div"
          /> 
      </div>
    )
  } 
}


const mapStateToProps = state => ({
  user: state.user,
  person: state.person,
  parameters: state.general.parameters,
  loader: state.shared.loader,
  policy: state.policy.listRamos,
  policyList: state.policy.dataList,
  policyL: state.policy

  //userFrequents:state.general.userFrequents
});

const mapDispatchToProps = (dispatch) => ({
  policyOperations: bindActionCreators(policyOperations, dispatch),
  personOperations: bindActionCreators(personOperations, dispatch),
  salesforceOperations: bindActionCreators(salesforceOperations, dispatch),
  sinisterOperations: bindActionCreators(sinisterOperations, dispatch),
  generalOperations: bindActionCreators(generalOperations, dispatch),
  operationOperations: bindActionCreators(operationOperations, dispatch)
});

export default connect(mapStateToProps,mapDispatchToProps)(withToastManager(SearchProducts));