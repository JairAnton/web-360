//@ts-check
import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import _ from 'lodash'
// import ScrollableAnchor, { goToAnchor } from 'react-scrollable-anchor'
import { withToastManager } from 'react-toast-notifications';

// import ModalProductCanceled from './productsNav/modalProductCanceled';
// import ModalProductRenovate from './productsNav/modalProductRenovate'

import DetailProductNav from 'views/pages/third/detailThird/detailNav/productsNav/detailProductNav'

import { policyOperations, policyLoaderKeys } from 'state/ducks/policy'
import PubSub from 'state/utils/PubSub'
import ElementTable from 'components/table/elementTable'
import { constants as CONSTANTS } from 'state/utils/constants'


import { changeFormatDate } from 'state/utils/functionUtils'
import { stat } from 'fs';
// import { select } from 'redux-saga/effects';

const PN_POLICY_COLUMNS = [
  {
    label: "Ramo",
    prop: "ramo",
    minWidth: 150
  },
  {
    label: "Producto",
    prop: "desProducto",
    minWidth: 170
  },
  {
    label: "Estado\nPóliza",
    prop: "estadoPoliza",
    minWidth: 100
  },
  {
    label: "Estado\nCertificado",
    prop: "estadoCertificado",
    minWidth: 95
  },
  {
    label: "Cód.\nProducto",
    prop: "codProdAcselx",
    minWidth: 70
  },
  {
    label: "N° Póliza",
    prop: "numPoliza",
    minWidth: 105
  },
  {
    label: "Fecha Inicio\nPóliza",
    prop: "fecIniVigencia",
    width: 100,
    render: (row, column, _index) => (
      <span>
        {changeFormatDate(row[column.prop], '-', '/')}
      </span>
    ),
  },
  {
    label: "Fecha Fin\nPóliza",
    prop: "fecFinVigencia",
    width: 100,
    render: (row, column, _index) => (
      <span>
        {changeFormatDate(row[column.prop], '-', '/')}
      </span>
    ),
  },
  {
    label: "N° Certificado",
    prop: "numCertificado",
    minWidth: 100
  },
  {
    label: 'Corredor',
    prop: 'nomBroker',
    minWidth: 190
  },
  {
    label: "Fecha Exclusión\nAsegurado",
    prop: "fecExcluAsegurado",
    width: 150
  },
]

const PJ_POLICY_COLUMNS = [
  {
    label: "Ramo",
    prop: "ramo",
    minWidth: 150
  },
  {
    label: "Producto",
    prop: "desProducto",
    minWidth: 150
  },
  {
    label: "Estado Póliza",
    prop: "estadoPoliza",
    minWidth: 150
  },
  {
    label: "Cód Producto",
    prop: "codProducto",
    minWidth: 120
  },
  {
    label: "Póliza",
    prop: "numPoliza",
    minWidth: 150
  },

  {
    label: "Fecha Inicio",
    prop: "fecIniVigencia",
    minWidth: 100,
    render: (row, column, _index) => (
      <span>
        {changeFormatDate(row[column.prop], '-', '/')}
      </span>
    ),
  },
  {
    label: "Fecha Fin",
    prop: "fecFinVigencia",
    minWidth: 100,
    render: (row, column, _index) => (
      <span>
        {changeFormatDate(row[column.prop], '-', '/')}
      </span>
    ),
  },
  {
    label: 'Corredor',
    prop: 'nomBroker',
    minWidth: 170
  },
]

class SearchProducts  extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rbProductos: 'rbProductosVigentes',
      rbProductosValue: 'PV',
      dataSelect: false,
      dataList: [],
      loaderTableProducts: this.props.loaderProducts,
      nameTabKey: 'GeneralData',
      page: 1,
      rowPage: 10,
      typePerson: "PJ",
    }
  }

  componentDidMount() {
    const { toastManager, getPolicyList, productsFA, newSelected } = this.props;
    this.setState({
      dataSelect: newSelected
    })

    PubSub.subscribe('ERR_POLICY_CERTIFICATE_BENEFICIARIES_LIST', (message) => {
      console.log('ERR_POLICY_CERTIFICATE_BENEFICIARIES_LIST');
      toastManager.add(message, {
        autoDismissTimeout: 8000,
        appearance: 'error',
        autoDismiss: true,
        pauseOnHover: true,
      })
    })

    PubSub.subscribe('ERR_POLICY_CERTIFICATE_INSURED_LIST', (message) => {
      console.log('ERR_POLICY_CERTIFICATE_INSURED_LIST');
      toastManager.add(message, {
        autoDismissTimeout: 8000,
        appearance: 'error',
        autoDismiss: true,
        pauseOnHover: true,
      })
    })

    PubSub.subscribe('ERR_POLICY_STATE_ACCOUNT', (message) => {
      console.log('ERR_POLICY_STATE_ACCOUNT');
      toastManager.add(message, {
        autoDismissTimeout: 8000,
        appearance: 'error',
        autoDismiss: true,
        pauseOnHover: true,
      })
    })

    if (productsFA && productsFA.callService) {
      getPolicyList()
      return
    }

    if (productsFA && productsFA.select) {
      this.setState({
        dataList: new Array(productsFA.select)
      }, () => {
        this.selectRowTable(productsFA.select)
      })
      return
    }

    if (!productsFA) {
      getPolicyList()
    }

  }

  componentDidUpdate(prevProps) {
    
    const {newSelected } = this.props;
    const {
      dataList: dlPol1
    } = this.props.policy

    const {
      dataList: dlPol2
    } = prevProps.policy

    if (JSON.stringify(dlPol1) !== JSON.stringify(dlPol2)) {
      this.setState({
        dataList: dlPol1,

        dataSelect: newSelected
      })
    }
  }

  getListPolicyBenefiaries = () => {
    let {
      policy,
      person,
      policyOperations
    } = this.props

    let select = policy.select

    let requestBody = {
      "core": (select && !_.isNil(select.core)) ? select.core : "",
      "codExterno": !_.isNil(person.detail.codExterno) ? person.detail.codExterno.toString() : "",
      "codProducto": (select && !_.isNil(select.codProducto)) ? select.codProducto.toString() : "",
      "numPoliza": (select && !_.isNil(select.numPoliza)) ? select.numPoliza.toString() : "",
      "numCertificado": (select && !_.isNil(select.numCertificado)) ? select.numCertificado.toString() : ""
    }

    policyOperations.policyCertificateBeneficiariesList(requestBody)
    
    // auditoria
    let requestLogging = {
      "path": window.location.href,
      "action": "POLICY_CERTIFICATE_BENEFICIARIES_LIST"
    }
    policyOperations.auditLogging(requestLogging)
  }

  getListPolicyInsured = () => {
    const { policy, person, policyOperations } = this.props

    const requestBody = {
      "core": (policy && policy.select && !_.isNil(policy.select.core)) ? policy.select.core : "",
      "codProducto": (policy && policy.select && !_.isNil(policy.select.codProducto)) ? policy.select.codProducto.toString() : "",
      "numPoliza": (policy && policy.select && !_.isNil(policy.select.numPoliza)) ? policy.select.numPoliza.toString() : "",
      "numCertificado": (policy && policy.select && !_.isNil(policy.select.numCertificado)) ? policy.select.numCertificado.toString() : "",
      "codAfiliado": !_.isNil(person.detail.codAfiliado) ? person.detail.codAfiliado.toString() : "",
      "codExterno": !_.isNil(person.select.codExterno) ? person.select.codExterno.toString() : "",
    }

    policyOperations.policyCertificateInsuredList(requestBody)
    
    // auditoria
    let requestLogging = {
      "path": window.location.href,
      "action": "POLICY_CERTIFICATE_INSURED_LIST"
    }
    policyOperations.auditLogging(requestLogging)
  }
  getListPolicy = async (filasPorPag = 10, pagActual = 1, rbProductosValue = '') => {
    const { person: { select }, policyOperations, } = this.props
    let requestBody = {
      filter: {
        ideTercero: (select.ideTercero) ? select.ideTercero.toString() : "",
        codExterno: (select.codExterno) ? select.codExterno.toString() : "",
        codAfiliado: (select.codAfiliado) ? select.codAfiliado.toString() : "",
        numPlaca: "",
        numMotor: "",
        nombreTitular: "",
        estado: rbProductosValue,
        tipoDocumento: (select.codTipoDocumento) ? select.codTipoDocumento.toString() : "",
        numDocumento: (select.numDocumento) ? select.numDocumento.toString() : "",
      },
      pagination: {
        filasPorPag,
        pagActual,
      }
    }
    policyOperations.policyList(requestBody)
    
    // auditoria
    let requestLogging = {
      "path": window.location.href,
      "action": "POLICY_LIST"
    }
    policyOperations.auditLogging(requestLogging)
  }


  getPolicyCertificateList = (page, rowPage) => {

    const { policy, policyOperations } = this.props
    const requestBody = {
      "filter": {
        "idePol": (policy && policy.select.idePoliza) ? policy.select.idePoliza.toString() : "",
      },
      "pagination": {
        "filasPorPag": rowPage,
        "pagActual": page,
      },
      // "idePol" : '7100553'// (policy && policy.select.idePol) ? policy.select.idePol.toString() : "",
    }

    policyOperations.policyCertificateList(requestBody)
    
    // auditoria
    let requestLogging = {
      "path": window.location.href,
      "action": "POLICY_CERTIFICATE_LIST"
    }
    policyOperations.auditLogging(requestLogging)
  }

  getStateAccount = () => {
    const { policy, policyOperations } = this.props
    const select = policy.select
    if (select) {
      const requestBody = {
        "core": !_.isNil(select.core) ? select.core : "",
        "numPoliza": !_.isNil(select.numPoliza) ? select.numPoliza.toString() : "",
        "numCertificado": !_.isNil(select.numCertificado) ? select.numCertificado.toString().trim() : "",
        "codProdAcselx": !_.isNil(select.codProdAcselx) ? select.codProdAcselx.toString() : ""
      }
      policyOperations.policyStateAccount(requestBody)
      
      // auditoria
      let requestLogging = {
        "path": window.location.href,
        "action": "POLICY_STATE_ACCOUNT"
      }
      policyOperations.auditLogging(requestLogging)
    } else {
      console.log('getStateAccount -> Error (no value in `select` variable)')
    }
  }

  getColumnsTableProduct = () => {
    const { personType } = this.props
    if (personType === CONSTANTS.PN)
      return PN_POLICY_COLUMNS
    else
      return PJ_POLICY_COLUMNS
  }

  getDataRowsTableProduct = () => this.props.policy.dataList;

  changeTabKey = (nameTab) => {
    this.setState({
      nameTabKey: nameTab
    })
  }

  //Cuando haces click al fila de la tabla
  selectRowTable = selected => {
    if(this.state.dataSelect === true){
      this.setState({
        dataSelect: false
      })
    }
    this.setState({
      nameTabKey: 'GeneralData',
      dataSelect: true
    }, () => {
      //TODO: mandar obj
      //this.props.selectPolicy(selected)
      const { policyOperations } = this.props
      const {dataList} = this.props
      const requestBody = {
        core: '',
        codProducto: '',
        codProdAcselx: '',
        numPoliza: '',
        numCertificado: '',
        codAfiliado: '',
        codCliente: '',
        codPlan: '',
        idePoliza: '',
      }
      requestBody.core = selected.core
      requestBody.codProducto = selected.codProducto ? selected.codProducto.toString() : ""
      requestBody.codProdAcselx = selected.codProdAcselx ? selected.codProdAcselx.toString() : ""
      requestBody.numPoliza = selected.numPoliza ? selected.numPoliza.toString() : ""
      requestBody.numCertificado = selected.numCertificado ? selected.numCertificado.toString() : ""
      requestBody.codAfiliado = selected.codAfiliado ? selected.codAfiliado.toString() : ""
      requestBody.codCliente = selected.codCliente ? selected.codCliente.toString() : ""
      requestBody.codPlan = selected.codPlan ? selected.codPlan.toString() : ""
      requestBody.idePoliza = selected.idePoliza ? selected.idePoliza.toString() : ""

      policyOperations.policySelect(selected)

      let requestLogging = {
        "path": window.location.href,
        "action": "POLICY_SELECT"
      }
      policyOperations.auditLogging(requestLogging)
      {/* Enviar endpoint auditoria*/ }
      setTimeout(function () {
        policyOperations.policyDetail(requestBody)
        let requestLogging = {
          "path": window.location.href,
          "action": "POLICY_DETAIL"
        }
        policyOperations.auditLogging(requestLogging)
        {/* Enviar endpoint auditoria*/ }
      }, 500)
      this.scrollToRef()
      // auditoria
      
      // goToAnchor('detailProductNav', false)
    })
    
    if(selected.tipoPersonaContratante === "N"){
      this.setState({
        typePerson: "PN",
      })
    }
    if(selected.tipoPersonaContratante === "J"){
      this.setState({
        typePerson: "PJ",
      })
    }
  }

  changePage = numPage => {
    this.setState({
      page: numPage,
      dataSelect: false
    }, () => {
      const { getPolicyList } = this.props
      const { page, rowPage, rbProductosValue } = this.state

      getPolicyList(rowPage, page, rbProductosValue)
    })
  }

  changeRowPage = sizePage => {
    this.setState({
      rowPage: sizePage,
      dataSelect: false
    }, () => {
      const { getPolicyList } = this.props
      const { page, rowPage, rbProductosValue } = this.state
      getPolicyList(rowPage, page, rbProductosValue)
    })
  }

  scrollToRef = () => {
    document.getElementById('my_scrollable_div').scrollIntoView({ behavior: 'smooth' })
  }

  render() {
    const { loaderVisible, policy, pagination } = this.props
    const { nameTabKey, dataSelect, rbProductos, dataList } = this.state
    const {typePerson} = this.state
    const { page,  rowPage} = this.state
    return (
      <div className="row">
        <div className="col-lg">
          <form>
            {/* <div className="col-lg">
              <div className="row">
                <p>
                  <a href="" data-toggle="modal" data-target=".bd-modal-product-renovate-xl">
                    Productos por Renovar
                  </a>
                  <a href="#" data-toggle="modal" data-target=".bd-modal-product-canceled-xl">
                    Productos Anulados
                  </a>
                </p>
              </div>

              <ModalProductCanceled />
              <ModalProductRenovate />
            </div> */}
            
          </form>
          <br />
        </div>
        <div className="col-lg-12" style={{ cursor: 'pointer' }}>
          <ElementTable
            columns={PJ_POLICY_COLUMNS}
            data={dataList}
            onChangeSelect={selected => {
              this.selectRowTable(selected)
            }}
            page={page}
            rowPage={rowPage}
            onChangePage={this.changePage}
            onChangeRowPage={this.changeRowPage}
            loader={loaderVisible}
          />
        </div>
        <div className="col-lg">
          <div
            style={{ width: '10px', height: '10px' }}
            id="my_scrollable_div"
          />
          {dataSelect  && !loaderVisible &&
            //@ts-ignore
            <DetailProductNav
              getListPolicyBenefiaries={this.getListPolicyBenefiaries}
              getListPolicyInsured={this.getListPolicyInsured}
              getStateAccount={this.getStateAccount}
              getPolicyCertificateList={this.getPolicyCertificateList}
              changeTabKey={this.changeTabKey}
              nameTabKey={nameTabKey}
              personType={typePerson}
              newSelected={true}
            />}
        </div>
      </div>
    )
  }
} 


const mapStateToProps = (state) => ({
  person: state.person,
  pagination: state.policy,
  policy: state.policy,
  loaderVisible: state.shared.loader.includes(policyLoaderKeys.POLICY_LIST),
})

const mapDispatchToProps = (dispatch) => ({
  policyOperations: bindActionCreators(policyOperations, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(withToastManager(SearchProducts ))