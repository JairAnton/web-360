import React, { Component } from 'react'
import { Route, Switch } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { routes } from 'state/utils/routes'
import { MDBIcon } from 'mdbreact'
//index
import Footer from 'components/layout/footerMB'
import SideMenu, { SIDEBAR_WIDTH } from 'components/layout/sideMenu'


import { salesforceOperations } from 'state/ducks/salesforce'
import { personOperations } from 'state/ducks/person'
import { generalOperations } from 'state/ducks/general'
import UserFrequent from 'components/layout/userFrequent'

import SearchProducts from './searchProducts'


class ProductsPo extends Component {
  state = {
    menuOpened: true,
  }
  
  handleClick = (fromSidemenu) => {
    if (!fromSidemenu) {
      this.props.generalOperations.setSidemenuStatus()
      localStorage.setItem('menuOpened', `${!this.props.menuOpened}`)
    } else {
      this.props.menuOpened && this.props.generalOperations.setSidemenuStatus()
      this.props.menuOpened && localStorage.setItem('menuOpened', `${!this.props.menuOpened}`)
    }
  }
  render(){
      return(
          <div style={{ width: '100%', minHeight: '100%', }}>
              <SideMenu
                //marginLeft={this.state.menuOpened ? '0px' : `-${SIDEBAR_WIDTH}`}
                marginLeft={this.props.menuOpened ? '0px' : '-180px'}
                toggle={(toggleStatus) => this.handleClick(toggleStatus)}
                isOpenMenu={this.props.menuOpened} />
             
              {
                <UserFrequent/>
              }
        
            <div
                style={{
                    // backgroundColor: 'white',
                    marginLeft: this.props.menuOpened ? SIDEBAR_WIDTH : '50px',
                    //marginLeft: this.state.menuOpened ? SIDEBAR_WIDTH : '0px',
                    transition: 'margin .5s',
                    height: '100vh',
                    overflow: 'auto',
                    minHeight:'100vh'
                }}
                >
                <div
                    className="container-fluid"
                    style={{
                    backgroundColor: 'white',
                    paddingBottom: '30px',
                    }} >
                    <br/>  
                    <Switch>
                    <Route exact path={routes.POLIZAS} component={SearchProducts} />
                    </Switch>
                </div>
                <Footer />
            </div>
        </div>
      )
  }
  
}

const mapStateToProps = (state) => ({
    menuOpened: state.general.sideMenuOpened
  })
  
  const mapDispatchToProps = (dispatch) => ({
    personOperations: bindActionCreators(personOperations, dispatch),
    salesforceOperations: bindActionCreators(salesforceOperations, dispatch),
    generalOperations: bindActionCreators(generalOperations, dispatch)
  });
  
  export default connect(mapStateToProps, mapDispatchToProps)(ProductsPo);