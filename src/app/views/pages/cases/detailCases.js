import React, {Component} from 'react'
import { MDBCard, MDBCardBody, MDBCardTitle, MDBCardText, MDBCardHeader, MDBBtn, MDBContainer,
    MDBInput,
    MDBCollapse,
    MDBBreadcrumb,
    MDBBreadcrumbItem,
    Button
} from "mdbreact";
import styled from 'styled-components'

import {
    DataGeneral,
    InformationContact,
    Record
} from './detailCases/index'

const DivHeaderNav = styled.div`
    button{
        text-align: center;
        border-radius: 5px;
        background-color: white !important;;
        color: black;
        width: 100%;
    }
    button:hover{
      background-color: red !important;
      color: white;
    }
    button.active{
      background-color: red !important;
      color: white;
    } 
`
class DetailCases extends Component  {
    constructor(props) {
        super(props);
        
        this.state = {
          numTabCase: 1
        }

        this.selectNumTabCase = this.selectNumTabCase.bind(this);
    }

    selectNumTabCase = (numTab) => {
      this.setState({
        numTabCase: numTab
      });
    }

    render() {
      const {
        numTabCase
      } = this.state;

      return(
        <div>
          <MDBCard>
            <MDBCardHeader 
                color="lighten-1"
                tag="h3"
                style={{
                    "backgroundColor":"#E52E2D"
                }}    
            >
                Caso: <strong>2345298374</strong>
            </MDBCardHeader>
            <MDBCardBody>
              <div className="row">
                  <div className="col-lg">
                      <MDBInput value=" " disabled label="Tratamiento" size="sm"/>                            
                  </div>
                  <div className="col-lg">
                      <MDBInput value=" " disabled label="Prioridad" size="sm"/>                            
                  </div>
                  <div className="col-lg">
                      <MDBInput value=" " disabled label="Estado" size="sm"/>                            
                  </div>
              </div>
            </MDBCardBody>
          </MDBCard>
          <br/>
          <MDBCard>
            <MDBCardHeader>
              <DivHeaderNav>
                <div className="row">
                  <div className="col-lg">
                      <button
                        type="button"
                        className={"btn "+ (numTabCase === 1 ? 'active': '')}
                        onClick={() => this.selectNumTabCase(1)}
                      >Datos Generales</button>
                  </div>
                  <div className="col-lg">
                      <button
                        type="button"
                        className={"btn "+ (numTabCase === 2 ? 'active': '')}
                        onClick={() => this.selectNumTabCase(2)}
                      >Datos de Contacto</button>
                  </div>
                  <div className="col-lg">
                      <button
                        type="button"
                        className={"btn "+ (numTabCase === 3 ? 'active': '')}
                        onClick={() => this.selectNumTabCase(3)}
                      >Historial</button>
                  </div>
                </div>
              </DivHeaderNav>
            </MDBCardHeader>
            <MDBCardBody>
              {
                (() => {
                  switch(numTabCase){
                    case 1:
                      return (<DataGeneral/>);
                    case 2:
                      return <Record/>;
                    case 3:
                      return <InformationContact/>;
                    default:
                      return (
                        <div>
                          Error!!.. Llame a su backOffice.
                        </div>
                      );
                  }
                })()
              }
            </MDBCardBody>
          </MDBCard>
        </div>
      );
    }
}

export default DetailCases;