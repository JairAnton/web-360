import React, { Component } from 'react';
import {
  MDBModal,
  MDBModalHeader,
  MDBModalBody,
  MDBModalFooter,
  MDBBtn,
  MDBRow
} from 'mdbreact'
import { bindActionCreators } from 'redux'
import { HashLink as Link } from 'react-router-hash-link'
import { connect } from 'react-redux'
import { personOperations, personLoaderKeys } from 'state/ducks/person'
import ElementTable from 'components/table/elementTable';
import { changeFormatDate, } from 'state/utils/functionUtils'

const dataSelectPN = [
  {
    "usuCreacion": "MIGRACION",
    "fecCreacion": "2012-04-19 03:12:37",
    "usuModif": "DS_SASPRI_WEB",
    "fecModif": "2018-12-28 14:28:43",
    "idePar": "7348",
    "codExterno": "2",
    "ideTipPar": "TER_TIPODOCUMENTO",
    "ideTipParPadre": null,
    "codigo": "2",
    "abreviatura": "DNI            ",
    "descripcion": "DOC. NACIONAL IDENTIDAD",
    "descripcion2": null,
    "indActivo": "S",
    "masterDetail": null,
    "refmigracionrs": "2",
    "codigoN": 2,
    "codigoC": "2"
  },
  {
    "usuCreacion": "DS_CNTWEB",
    "fecCreacion": "2012-05-28 15:15:06",
    "usuModif": "DS_CNTWEB",
    "fecModif": "2017-07-11 15:39:23",
    "idePar": "7435",
    "codExterno": "4",
    "ideTipPar": "TER_TIPODOCUMENTO",
    "ideTipParPadre": null,
    "codigo": "4",
    "abreviatura": "C.E.",
    "descripcion": "CARNET DE EXTRANJERIA",
    "descripcion2": "C.E.",
    "indActivo": "S",
    "masterDetail": null,
    "refmigracionrs": "4",
    "codigoN": 4,
    "codigoC": "4"
  },
  {
    "usuCreacion": "DS_CNTWEB",
    "fecCreacion": "2012-05-28 15:14:15",
    "usuModif": "DS_CNTWEB",
    "fecModif": "2017-07-11 15:39:23",
    "idePar": "7434",
    "codExterno": "6",
    "ideTipPar": "TER_TIPODOCUMENTO",
    "ideTipParPadre": null,
    "codigo": "3",
    "abreviatura": "PASAPORTE",
    "descripcion": "PASAPORTE",
    "descripcion2": "PASAPORTE",
    "indActivo": "S",
    "masterDetail": null,
    "refmigracionrs": "6",
    "codigoN": 3,
    "codigoC": "3"
  },
  {
    "usuCreacion": "MIGRACION",
    "fecCreacion": "2012-04-19 03:12:37",
    "usuModif": "DS_SASPRI_WEB",
    "fecModif": "2018-12-14 11:31:17",
    "idePar": "7349",
    "codExterno": "1",
    "ideTipPar": "TER_TIPODOCUMENTO",
    "ideTipParPadre": null,
    "codigo": "1",
    "abreviatura": "RUC            ",
    "descripcion": "REG. UNICO CONTRIBUY.",
    "descripcion2": null,
    "indActivo": "S",
    "masterDetail": null,
    "refmigracionrs": "1",
    "codigoN": 1,
    "codigoC": "1"
  }
]

const dataSelectPJ = [
  {
    "usuCreacion": "MIGRACION",
    "fecCreacion": "2012-04-19 03:12:37",
    "usuModif": "DS_SASPRI_WEB",
    "fecModif": "2018-12-14 11:31:17",
    "idePar": "7349",
    "codExterno": "1",
    "ideTipPar": "TER_TIPODOCUMENTO",
    "ideTipParPadre": null,
    "codigo": "1",
    "abreviatura": "RUC            ",
    "descripcion": "REG. UNICO CONTRIBUY.",
    "descripcion2": null,
    "indActivo": "S",
    "masterDetail": null,
    "refmigracionrs": "1",
    "codigoN": 1,
    "codigoC": "1"
  }
]

class ModalUserSearch extends Component {
  state = {
    checkPN: true,
    checkPJ: false,
    selectTD: '2',
    dataSelectTD: dataSelectPN,
    body: {
      'tipoTercero': 'N',//J juridico
      'tipoDocumento': '2',
      //'numDocumento': '41304548', // TODO: remover 
      'numDocumento': '', // TODO: remover 
      'nombre': '', //solo N
      'apePaterno': '',
      'apeMaterno': '',
      'nombreComercial': '',
      'codExterno': '',
      'razonSocial': '',
    },
    page: 1,
    rowPage: 10,
  }

  handleChangeCheck = (evt) => {
    const {
      name,
      value
    } = evt.target

    let body = this.state.body

    if (value === 'N') {
      body.numDocumento = ''
      body.nombre = ''
      body.apeMaterno = ''
      body.apePaterno = ''
    }

    if (value === 'J') {
      body.numDocumento = ''
      body.nombreComercial = ''
      body.razonSocial = ''
    }

    this.setState({
      selectTD: (value === 'J') ? '1' : '2',
      dataSelectTD: (value === 'N') ? dataSelectPN : dataSelectPJ,
      body: {
        ...body,
        tipoTercero: value,
        tipoDocumento: (value === 'J') ? '1' : '2'
      }
    })
  }

  handleChange = (evt) => {
    const {
      name,
      value
    } = evt.target
    const body = this.state.body
    const selectTD = this.state.selectTD

    this.setState({
      selectTD: (name === "tipoDocumento") ? value : selectTD,
      body: {
        ...body,
        [name]: value
      }
    })
  }

  selectContact = (row) => {
    const { toggleModalOpen, onSelectContact } = this.props
    const {
      apeMaterno,
      apePaterno,
      nombre,
      codExterno
    } = row
    const nomCompleto = `${nombre} ${apePaterno} ${apeMaterno}`

    onSelectContact(codExterno, nomCompleto)
  }

  getColumnsTableSearchThird = () => {
    const personType = this.state.checkPJ ? 'PJ' : 'PN'
    return [
      {
        label: "Tipo Documento",
        prop: "tipoDocumento",
        minWidth: 160,
        personTypes: ['PN', 'PJ'],
      },
      {
        label: "N° documento",
        prop: "numDocumento",
        minWidth: 150,
        render: (row, column, _index) => (
          <strong style={{ paddingTop: 15, paddingBottom: 15, color: 'dodgerblue' }}>
            <strong>
              <Link to="#" onClick={() => this.selectContact(row)}>
                <u>{row[column.prop]}</u>
              </Link>
            </strong>
          </strong>
        ),
        personTypes: ['PN', 'PJ'],
      },
      {
        label: "Razón social",
        prop: "razonSocial",
        minWidth: 130,
        render: (row, column, _index) => (
          <div style={{ paddingTop: 15, paddingBottom: 15 }}>
            <span>{row[column.prop]}</span>
          </div>
        ),
        personTypes: ['PJ'],
      },
      {
        label: "Nombres",
        prop: "nombre",
        minWidth: 160,
        personTypes: ['PN'],
      },
      {
        label: "Nombre Comercial",
        prop: "nombre",
        minWidth: 160,
        personTypes: ['PJ'],
      },
      {
        label: "Apellido Paterno",
        prop: "apePaterno",
        minWidth: 160,
        personTypes: ['PN'],
      },
      {
        label: "Apellido Materno",
        prop: "apeMaterno",
        minWidth: 170,
        personTypes: ['PN'],
      },
      {
        label: `Fecha Nacimiento`,
        prop: "fecNacimiento",
        minWidth: 150,
        personTypes: ['PN'],
      },
      {
        label: "Estado Civil",
        prop: "estadoCivil",
        minWidth: 130,
        personTypes: ['PN'],
      },
      {
        label: "Tratamiento",
        prop: "tratamiento",
        minWidth: 150,
        personTypes: ['PN', 'PJ'],
      },
      {
        label: "Ley de Datos",
        prop: "leyProteccionDatos",
        minWidth: 150,
        personTypes: ['PN', 'PJ'],
      },
      {
        label: "Código AcselX",
        prop: "codigoExterno",
        minWidth: 150,
        personTypes: ['PN', 'PJ'],
      }
    ].filter(item => item.personTypes.includes(personType))
  }

  getDataRowsTableSearchThird = () => {
    const { page, rowPage } = this.state
    const { search: { dataList } } = this.props.person

    if (dataList)
      return dataList.map((item, index) => ({
        ...item,
        "id": ((page - 1) * rowPage + index + 1),
        "tipoDocumento": item.desTipoDocumento,
        "fecNacimiento": changeFormatDate(item.fecNacimiento, "-", "/"),//Cambio del formato fecha
      }))
    return []
  }

  changePageTercero = (currentPage) => {
    this.setState({
      page: currentPage
    }, () => {
      this.buscarTercero();
    });
  }

  changeRowsPerPageTercero = (numberOfRows) => {
    this.setState({
      rowPage: numberOfRows
    }, () => {
      this.buscarTercero();
    });
  }

  validateForm = () => {
    const body = this.state.body
    if (body.tipoTercero === 'N') {
      if (body.numDocumento.replace(/ /gi, '').length > 0 || body.nombre.replace(/ /gi, '').length > 0 || body.apeMaterno.replace(/ /gi, '').length > 0 || body.apePaterno.replace(/ /gi, '').length > 0)
        return true

      // PubSub.publish('WARNING_LOGIN', 'Debe llenar los campos vacios.');
    } else {
      if (body.numDocumento.replace(/ /gi, '').length > 0 || body.nombreComercial.replace(/ /gi, '').length > 0 || body.razonSocial.replace(/ /gi, '').length > 0)
        return true

      // PubSub.publish('WARNING_LOGIN', 'Debe llenar los campos vacios.');
    }

    return false
  }

  buscarTercero = () => {
    const {
      personOperations
    } = this.props

    const { body, page, rowPage } = this.state

    if (this.validateForm()) {
      let requestBody = {
        filter: body,
        "pagination": {
          "filasPorPag": rowPage,
          "pagActual": page
        }
      }
      personOperations.personSearch(requestBody)
      // auditoria
      let requestLogging = {
        "path": window.location.href,
        "action": "PERSON_SEARCH"
      }
      personOperations.auditLogging(requestLogging)
    }
  }

  render() {
    const {
      dataSelectTD,
      page,
      rowPage
    } = this.state

    const {
      openModal,
      toggleModalOpen,
      loaderVisible,
      person: {
        search: {
          pagination
        }
      }
    } = this.props

    return (
      <MDBModal isOpen={openModal} toggle={() => toggleModalOpen()} fullHeight position="top">
        <MDBModalHeader toggle={() => toggleModalOpen()}>
          Busqueda de Contacto
      </MDBModalHeader>
        <MDBModalBody>
          <form>
            <div className="row">
              <div className="col-sm">
                Criterios de Búsqueda:
                      </div>
              <div className="form-check col-sm">
                <input
                  className="form-check-input"
                  type="radio"
                  name="tipoTercero"
                  id="exampleRadios1"
                  value="N"
                  defaultChecked
                  onClick={(evn) => this.handleChangeCheck(evn)}
                />
                <label className="form-check-label" htmlFor="exampleRadios1">
                  Persona natural
                        </label>
              </div>
              <div className="form-check col-sm">
                <input
                  className="form-check-input"
                  type="radio"
                  name="tipoTercero"
                  id="exampleRadios2"
                  value="J"
                  onClick={(evn) => this.handleChangeCheck(evn)}
                />
                <label className="form-check-label" htmlFor="exampleRadios2">
                  Persona jurídica
                        </label>
              </div>
            </div>
            <hr />
            <div className="row">
              <div className="col-lg-6">
                <div className="form-group row">
                  <label className="col-sm-4 col-form-label">Tipo documento:</label>
                  <select className="custom-select col-sm-7"
                    name="tipoDocumento"
                    value={this.state.selectTD}
                    onChange={this.handleChange}
                  >
                    {
                      dataSelectTD &&
                      dataSelectTD.length > 0 &&
                      dataSelectTD.map((val, key) => {
                        return (<option value={val.codigoN} key={key}>{val.abreviatura}</option>)
                      })
                    }
                  </select>
                </div>
              </div>
              <div className="col-lg-6">
                <div className="form-group row">
                  <label className="col-sm-4 col-form-label">Número documento:</label>
                  <input
                    className="col-sm-7 form-control"
                    type="number"
                    name="numDocumento"
                    value={this.state.body.numDocumento}
                    onChange={this.handleChange}
                  // onKeyDown={this.keyPress}
                  />
                </div>
              </div>
            </div>
            {
              (this.state.body.tipoTercero === 'N') &&
              <div>
                <div className="row">
                  <div className="col-lg-6">
                    <div className="form-group row">
                      <label className="col-sm-4 col-form-label">Apellido paterno:</label>
                      <input
                        className="col-sm-7 form-control"
                        type="text"
                        name="apePaterno"
                        value={this.state.body.apePaterno}
                        onChange={this.handleChange}
                      // onKeyDown={this.keyPress}
                      />
                    </div>
                  </div>
                  <div className="col-lg-6">
                    <div className="form-group row">
                      <label className="col-sm-4 col-form-label">Apellido materno:</label>
                      <input
                        className="col-sm-7 form-control"
                        type="text"
                        name="apeMaterno"
                        value={this.state.body.apeMaterno}
                        onChange={this.handleChange}
                      // onKeyDown={this.keyPress}
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-lg-6">
                    <div className="form-group row">
                      <label className="col-sm-4 col-form-label">Nombres:</label>
                      <input
                        className="col-sm-7 form-control"
                        type="text"
                        name="nombre"
                        value={this.state.body.nombre}
                        onChange={this.handleChange}
                      // onKeyDown={this.keyPress}
                      />
                    </div>
                  </div>
                </div>
              </div>
            }
            {
              (this.state.body.tipoTercero === 'J') &&
              <div>
                <div className="row">
                  <div className="col-lg-6">
                    <div className="form-group row">
                      <label className="col-sm-4 col-form-label">Razón social:</label>
                      <input
                        className="col-sm-7 form-control"
                        type="text"
                        name="razonSocial"
                        value={this.state.body.razonSocial}
                        onChange={this.handleChange}
                      // onKeyDown={this.keyPress}
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-lg-6">
                    <div className="form-group row">
                      <label className="col-sm-4 col-form-label">Nombre comercial:</label>
                      <input
                        className="col-sm-7 form-control"
                        type="text"
                        name="nombreComercial"
                        value={this.state.body.nombreComercial}
                        onChange={this.handleChange}
                      // onKeyDown={this.keyPress}
                      />
                    </div>
                  </div>
                </div>
              </div>
            }
          </form>
          <hr />
          <div className="row justify-content-md-center">
            <div className="col-2">
              <button
                className="btn btn-danger btn-icon-split"
                onClick={this.buscarTercero}
              >
                <span className="icon text-white-50">
                  <i className="fas fa-search" style={{ color: 'white' }} />
                </span>
                <span className="text">
                  {'    Buscar'}
                </span>
              </button>
            </div>
            <div className="col-2">
              <button
                className="btn btn-danger btn-icon-split"
              // onClick={this.limpiarFormTercero}
              >
                <span className="icon text-white-50">
                  <i className="fas fa-eraser" style={{ color: 'white' }} />
                </span>
                <span className="text">
                  {'    Limpiar'}
                </span>
              </button>
            </div>
          </div>
          <hr />

          <MDBRow center>
            <div className="col-lg-12">
              <ElementTable
                data={this.getDataRowsTableSearchThird()}
                columns={this.getColumnsTableSearchThird()}
                onChangePage={this.changePageTercero}
                onChangeRowPage={this.changeRowsPerPageTercero}
                pagination={pagination}
                onChangeSelect={() => { }}
                loader={loaderVisible}
                page={page}
                rowPage={rowPage}
              />
            </div>
          </MDBRow>
        </MDBModalBody>
        <MDBModalFooter>
          <MDBBtn color="danger" onClick={() => toggleModalOpen()}
            style={{
              'color': 'white'
            }}
          >Cerrar</MDBBtn>
        </MDBModalFooter>
      </MDBModal>
    )
  }
}



const mapStateToProps = state => ({
  person: state.person,
  loaderVisible: state.shared.loader.includes(personLoaderKeys.PERSON_SEARCH),
});

const mapDispatchToProps = (dispatch) => ({
  personOperations: bindActionCreators(personOperations, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(ModalUserSearch);