import React, { Component, useState, useMemo, useEffect } from 'react'
import { Animated } from "react-animated-css";
import {
  MDBCard,
  MDBCardBody,
  MDBCardTitle,
  MDBCardText,
  MDBCardHeader,
  MDBBtn,
  MDBCollapse,
  MDBRow,
  MDBIcon,
  MDBContainer
} from "mdbreact";
import styled from 'styled-components';

import moment from 'moment'
// import DatePicker from "react-datepicker";
import { bindActionCreators } from 'redux';
import { connect, useSelector, useDispatch } from 'react-redux';
import { withToastManager } from 'react-toast-notifications'
import PubSub from 'state/utils/PubSub'
import Select from 'react-select'

import ElementTable from 'components/table/elementTable';
import loaderKeys from 'state/ducks/salesforce/loaderKeys';
import { salesforceOperations } from 'state/ducks/salesforce'
import { changeFormatDate, removeSeconds } from 'state/utils/functionUtils'

import ModalUserSearch from './modalUserSearch'
import DetailCases from '../../third/detailThird/detailNav/casesNav/detailCase/detailCase'
import { Button, DatePicker } from 'element-react';

const DivPickerInput = styled.div`
    input{
        padding: .5em 1em;
        border: 1px solid #ddd;
        font-size: 1em;
        font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Helvetica,Arial,sans-serif;
        text-align: center;
        font-weight: 400;
        border-radius: 5px;
    }
`

const TYPE_REGISTER_CASE = [
  { value: 'F1_AsistenciaHogar', label: 'Asistencia al hogar' },
  { value: 'F1_AsistenciaViajes', label: 'Asistencia en viajes' },
  { value: 'F1_AsistenciaFunerariaSepelio', label: 'Asistencia funeraria sepelio' },
  { value: 'F1_AtencionProgamada', label: 'Atención programada' },
  { value: 'F1_ChoferReemplazo', label: 'Chofer de reemplazo' },
  { value: 'F1_CobranzaRenovacion', label: 'Cobranza - Renovación' },
  { value: 'F1_DeliveryMedicinas', label: 'Delivery de medicinas' },
  { value: 'F1_EmergenciaRiesgosGeneralesAjustador', label: 'Emergencia riesgos generales - Ajustador' },
  { value: 'F1_EmergenciaVehicular', label: 'Emergencia vehicular' },
  { value: 'F1_GruaAuxilioMecanico', label: 'Grúa y / o auxilio mecánico' },
  { value: 'F1_MedicoDomicilio', label: 'Médico a domicilio' },
  { value: 'F1_OrientacionMedicaTelefonicaVirtual', label: 'Orientación médica(Telefónica / Virtual)' },
  { value: 'F1_Reclamaciones_CNT', label: 'Reclamaciones CNT' },
  { value: 'F1_ReclamacionEPS', label: 'Reclamación EPS' },
  { value: 'F1_ReclamacionLegal', label: 'Reclamación Legal' },
  { value: 'F1_ReclamacionSeguros', label: 'Reclamación de seguros' },
  { value: 'F1_Referencia', label: 'Referencia' },
  { value: 'F1_Renovacion', label: 'Renovación' },
  { value: 'F1_RequerimientoOperaciones', label: 'Requerimiento u operaciones' },
  { value: 'F1_RequerimientosOperacionesCNT', label: 'Requerimientos u operaciones CNT' },
  { value: 'F1_Retencion', label: 'Retención' },
  { value: 'F1_SiniestroSOAT', label: 'Siniestro SOAT' },
  { value: 'F1_UnidadMedicaEmergencia', label: 'Unidad médica emergencia' },
]

const COLUMNS = [
  {
    label: "N° Caso",
    prop: "numCasoSf",
    minWidth: 55
  },
  {
    label: "Fecha/Hora Apertura",
    prop: "fechaHoraApertura",
    minWidth: 100,
    render: (row, column, idx) => (
      <span>
        {removeSeconds(changeFormatDate(row[column.prop], '-', '/').split('.')[0])}
      </span>
    ),
  },
  {
    label: "Tipo de Registro del Caso",
    prop: "descripcionTipoCasoSf",
    minWidth: 100,
  },
  {
    label: "Tipo",
    prop: "tipo",
    minWidth: 110,
  },
  {
    label: "Categoría",
    prop: "categoria",
    minWidth: 80,
  },
  {
    label: "Asunto",
    prop: "asunto",
    minWidth: 80,
  },
  {
    label: "Tema",
    prop: "tema",
    minWidth: 110,
  },
  {
    label: "Estado",
    prop: "estadoCaso",
    minWidth: 110,
  },
  {
    label: "Propietario del Caso",
    prop: "propietarioCaso",
    minWidth: 190,
  },
]


class SearchCases extends Component {
  constructor(props) {
    super(props)

    this.state = {
      page: 1,
      rowsPerPage: 10,
      selectedCase: null,
      selectContac: null,
      nameContac: '',
      body: {
        numCaso: '', numPoliza: '', numReclamo: '',
        placa: '', typeCase: '',
        dateFrom: null, dateUp: null
      },
      openModal: false
    }
  }

  componentDidMount() {
    const { toastManager } = this.props;
    PubSub.subscribe('WARNING_INPUT_SEARCH_CASE', (message) => {
      toastManager.add(message, {
        autoDismissTimeout: 8000,
        appearance: 'warning',
        autoDismiss: true,
        pauseOnHover: true,
      })
    })
  }

  changePageCase = (page) => {
    this.setState({
      page,
      selectedCase: null
    }, () => {
      this.getCasesList()
    })
  }

  changeRowsPerPageCase = (rowsPerPage) => {
    this.setState({
      rowsPerPage,
      selectedCase: null
    }, () => {
      this.getCasesList()
    })
  }

  validateInputSearchCase = () => {
    const {
      body,
      nameContac
    } = this.state
    let validate = false

    //Validamos las dependencias
    if ((body.dateFrom && body.dateFrom.toString().length > 0) || (body.dateUp && body.dateUp.toString().length > 0) || (body.typeCase && body.typeCase.length > 0)) {
      validate = ((body.dateFrom && body.dateFrom.toString().length > 0) && (body.dateUp && body.dateUp.toString().length > 0)) ? (body.typeCase && body.typeCase.length > 0 ? true : false) : false

      if (!(body.typeCase && body.typeCase.length > 0)) {
        PubSub.publish('WARNING_INPUT_SEARCH_CASE', 'Debe seleccionar un Tipo de Registro de Caso.');
      }

      if (!(body.dateFrom && body.dateFrom.toString().length > 0) || !(body.dateUp && body.dateUp.toString().length > 0)) {
        PubSub.publish('WARNING_INPUT_SEARCH_CASE', 'Debe ingresar un rango de Fechas.');
      }
    } else {
      //Validamos si hay un campo llenado
      if ((body.numCaso && body.numCaso.length > 0) || (body.numPoliza && body.numPoliza.length > 0) || (body.numReclamo && body.numReclamo.length > 0) || (body.placa && body.placa.length > 0) || (nameContac && nameContac.length > 0)) {
        validate = true
      } else {
        validate = false
        PubSub.publish('WARNING_INPUT_SEARCH_CASE', 'Llenar 1 campo minimo.');
      }
    }

    return validate
  }

  getCasesList = () => {
    const {
      salesforceOperations
    } = this.props

    const {
      page,
      rowsPerPage,
      body
    } = this.state

    if (this.validateInputSearchCase()) {
      const dateFrom = (body.dateFrom) ? body.dateFrom.toISOString().replace('.000Z', '-10:00') : null
      const dateUp = (body.dateUp) ? body.dateUp.toISOString().replace('.000Z', '-10:00') : null

      const payload = {
        filter: {
          ...body,
          dateFrom,
          dateUp
        },
        pagination: {
          "filasPorPag": rowsPerPage,
          "pagActual": page,
        },
      }

      salesforceOperations.casesList(payload)
      {/* Enviar endpoint auditoria*/ }

      // auditoria
      let requestLogging = {
        "path": window.location.href,
        "action": "CASE_LIST"
      }
      salesforceOperations.auditLogging(requestLogging)
    }
  }

  getDataList = () => {
    const {
      dataList
    } = this.props.salesforce.cases

    return dataList ? dataList.map(item => ({ ...item })) : []
  }

  //Seleccionar una fila de la tabla
  changeSelectedCase = (selectedCase) => {
    this.setState({
      selectedCase
    }, () => {
      setTimeout(() => {
        this.scrollToRef()
      }, 500)
    })
  }

  //Seleccionar una opcion del combo Select
  changeSelectCase = (typeCase) => {
    const value = (typeCase) ? typeCase.value : null

    const body = this.state.body

    this.setState({
      'body': {
        ...body,
        'typeCase': (value) ? value : ''
      }
    })
  }

  changeBody = (env) => {
    const {
      value,
      name
    } = env.target

    const body = this.state.body

    this.setState({
      'body': {
        ...body,
        [name]: value
      }
    })
  }

  changeDate = (name, value) => {
    const body = this.state.body

    this.setState({
      'body': {
        ...body,
        [name]: value
      }
    })
  }

  scrollToRef = () => {
    document.getElementById('searchCase_scrollable_div').scrollIntoView({ behavior: 'smooth' })
  }

  toggleModalOpen = () => {
    this.setState({
      openModal: !this.state.openModal
    })
  }

  selectContactModal = (codExterno, nameContac) => {

    this.setState({
      nameContac,
      body: {
        ...this.state.body,
        "codExterno": codExterno.toString(),
      },
      openModal: false
    })
  }

  clearSelectContactModal = () => {
    this.setState({
      nameContac: '',
      body: {
        ...this.state.body,
        "tipoDocumento": '',
        "numeroDocumento": ''
      }
    })
  }

  render() {
    const {
      page,
      rowsPerPage,
      selectedCase,
      body,
      openModal
    } = this.state

    const casesDataList = this.getDataList()
    const { pagination } = this.props.salesforce.cases
    const { loaderVisible } = this.props

    return (
      <div>
        <Animated animationIn="bounceInLeft" animationOut="bounceOutLeft" animationInDuration={2000} animationOutDuration={2000} isVisible={true}
          style={{
            "position": "relative",
            "zIndex": "0"
          }}
        >
          <div className="row">
            <div className="col-lg">
              <MDBCard>
                <MDBCardHeader color="lighten-1"
                  style={{
                    "backgroundColor": "#E52E2D"
                  }}
                >Busqueda de Casos</MDBCardHeader>
                <MDBCardBody>
                  <form>
                    <div className="row">
                      <div className="col-lg-6">
                        <div className="form-group row">
                          <label className="col-sm-5 col-form-label">Tipo de Registro de Caso:</label>
                          <div className="col-sm-7" >
                            <Select placeholder="Seleccionar"
                              name="tipoDocumento"
                              options={TYPE_REGISTER_CASE}
                              onChange={this.changeSelectCase}
                              isClearable
                            />
                          </div>
                        </div>
                      </div>
                      <div className="col-lg-6">
                        <div className="form-group row">
                          <label className="col-sm-5 col-form-label">Nombre Cuenta:</label>
                          <div className="col-sm-7" >


                            <div className="input-group mb-3">
                              <input
                                className="form-control"
                                type="text"
                                name="nameContac"
                                value={this.state.nameContac}
                                disabled
                              />

                              <div className="input-group-append">
                                <button
                                  className="btn btn-danger"
                                  type="button"
                                  id="button-addon2"
                                  onClick={() => {
                                    this.setState({
                                      openModal: !this.state.openModal
                                    })
                                  }}
                                  style={{
                                    'backgroundColor': '#ff3547 !important',
                                    color: 'white',
                                    paddingTop: '0.4rem',
                                    paddingBottom: '0.4rem',
                                    margin: '0rem',
                                    paddingLeft: '15px',
                                    paddingRight: '15px'
                                  }}
                                >
                                  <MDBIcon icon="search" />
                                </button>
                                <button
                                  className="btn btn-danger"
                                  type="button"
                                  id="button-addon2"
                                  onClick={() => {
                                    this.clearSelectContactModal()
                                  }}
                                  style={{
                                    'backgroundColor': '#ff3547 !important',
                                    color: 'white',
                                    paddingTop: '0.4rem',
                                    paddingBottom: '0.4rem',
                                    margin: '0rem',
                                    paddingLeft: '15px',
                                    paddingRight: '15px'
                                  }}
                                >
                                  <MDBIcon icon="times" />
                                </button>
                              </div>
                            </div>

                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-lg-6">
                        <div className="form-group row">
                          <label className="col-sm-5 col-form-label">Número Caso:</label>
                          <div className="col-sm-7" >
                            <input
                              className="form-control"
                              type="text"
                              name="numCaso"
                              value={body.numCaso}
                              onChange={this.changeBody}
                            />
                          </div>
                        </div>
                      </div>
                      <div className="col-lg-6">
                        <div className="form-group row">
                          <label className="col-sm-5 col-form-label">Número Póliza:</label>
                          <div className="col-sm-7" >
                            <input
                              className="form-control"
                              type="text"
                              name="numPoliza"
                              value={body.numPoliza}
                              onChange={this.changeBody}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-lg-6">
                        <div className="form-group row">
                          <label className="col-sm-5 col-form-label">Placa:</label>
                          <div className="col-sm-7" >
                            <input
                              className="form-control"
                              type="text"
                              name="placa"
                              value={body.placa}
                              onChange={this.changeBody}
                            />
                          </div>
                        </div>
                      </div>

                      <div className="col-lg-6">
                        <div className="form-group row">
                          <label className="col-sm-5 col-form-label">Número Reclamo:</label>
                          <div className="col-sm-7" >
                            <input
                              className="form-control"
                              type="text"
                              name="numReclamo"
                              value={body.numReclamo}
                              onChange={this.changeBody}
                            />
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-lg">
                        <div className="form-group row">
                          <label className="col-sm-5 col-form-label">Fecha de Apertura Desde:</label>
                          <DatePicker
                            className="col-sm-7"
                            format='dd/MM/yyyy'
                            value={body.dateFrom}
                            onChange={(date) => {
                              this.changeDate('dateFrom', date)
                            }}
                            placeholder='Fecha desde'
                            align='right'
                          />
                          {/* <DivPickerInput
                            className="col-sm-7"
                          >
                            <DatePicker
                              locale='es'
                              selected={body.dateFrom}
                              onChange={(date) => {
                                this.changeDate('dateFrom', date)
                              }}
                              dateFormat="dd/MM/yyyy"
                            />
                          </DivPickerInput> */}
                        </div>
                      </div>
                      <div className="col-lg">
                        <div className="form-group row">
                          <label className="col-sm-5 col-form-label">Fecha de Apertura Hasta:</label>
                          <DatePicker
                            className="col-sm-7"
                            format='dd/MM/yyyy'
                            value={body.dateUp}
                            onChange={(date) => {
                              this.changeDate('dateUp', date)
                            }}
                            placeholder='Fecha desde'
                            align='right'
                          />
                        </div>
                      </div>
                    </div>
                  </form>
                </MDBCardBody>
              </MDBCard>
            </div>
          </div>
        </Animated>
        <br />
        <Animated animationIn="pulse" animationOut="tada" animationInDuration={1000} animationOutDuration={1000} isVisible={true}>
          <div className="row justify-content-md-center">
            <div className="col-2">
              <Button
                type="danger"
                onClick={() => {
                  this.getCasesList()
                  setTimeout(() => {
                    this.scrollToRef()
                  }, 500)
                }}
              >
                <span className="icon text-white-50">
                  <i className="fas fa-search"></i>
                </span>
                <span className="text">{"  Buscar"}</span>
              </Button>
            </div>
            <div className="col-2">
              <Button
                type="danger"
              >
                <span className="icon text-white-50">
                  <i className="fas fa-eraser"></i>
                </span>
                <span className="text">{"  Limpiar"}</span>
              </Button>
            </div>
          </div>
        </Animated>
        
        <ModalUserSearch
          openModal={openModal}
          toggleModalOpen={this.toggleModalOpen}
          onSelectContact={this.selectContactModal}
        />
        <br />
        <MDBRow center>
          <div className="col-lg-12" style={{ cursor: 'pointer' }}>
            <ElementTable
              data={casesDataList}
              loader={loaderVisible}
              columns={COLUMNS}
              onChangeSelect={this.changeSelectedCase}
              onChangePage={this.changePageCase}
              onChangeRowPage={this.changeRowsPerPageCase}
              pagination={pagination}
              rowPage={rowsPerPage}
              page={page}
            />
          </div>
        </MDBRow>
        <div
          style={{ width: '10px', height: '10px' }}
          id="searchCase_scrollable_div"
        />
        {
          selectedCase &&
          <DetailCases
            selectedCase={selectedCase}
          />
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  salesforce: state.salesforce,
  loaderVisible: state.shared.loader.includes(loaderKeys.CASES)
});

const mapDispatchToProps = (dispatch) => ({
  salesforceOperations: bindActionCreators(salesforceOperations, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(withToastManager(SearchCases));
