import DataGeneral from './dataGeneral'
import InformationContact from './informationContact'
import Record from './record'

export {
    DataGeneral,
    InformationContact,
    Record
}