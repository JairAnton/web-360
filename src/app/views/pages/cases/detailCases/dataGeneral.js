import React, { Component } from 'react';
import {
  MDBCard, MDBCardBody, MDBCardTitle, MDBCardText, MDBCardHeader, MDBBtn, MDBContainer,
  MDBInput,
  MDBCollapse,
  MDBBreadcrumb,
  MDBBreadcrumbItem,
  MDBIcon
} from "mdbreact";
import styled from 'styled-components'

const DivInput = styled.div`
    .breadcrumb {
        cursor: pointer;
    }
    .md-form {
        margin-top: 0.5rem;
        margin-bottom: 0.5rem;
    }
`;

class DataGeneral extends Component {
  constructor(props) {
    super(props);

    this.state = {
      collapseDetail: false,
      collapsePoliza: false,
      collapseInfoContac: false,
      collapseTipifaCaso: false,
      collapseDatoVehiculo: false,
      collapseDireccion: false,
      collapseDatoConductor: false,
      collapseDetalleCaso: false,
      collapseDatoGps: false,
      collapseInfoAdicional: false,
      collapseObs: false,
      collapseCampoFotoPoliza: false,
      collapseWachList: false
    }
  }

  render() {
    const {
      collapseDetail,
      collapsePoliza,
      collapseInfoContac,
      collapseTipifaCaso,
      collapseDatoVehiculo,
      collapseDireccion,
      collapseDatoConductor,
      collapseDetalleCaso,
      collapseDatoGps,
      collapseInfoAdicional,
      collapseObs,
      collapseCampoFotoPoliza,
      collapseWachList
    } = this.state;

    return (
      <DivInput>
        <div>
          <div className="row">
            <div className="col-lg-12">
              <MDBBreadcrumb
                onClick={() => {
                  this.setState({
                    collapseDetail: !this.state.collapseDetail
                  })
                }}
              >
                <MDBBreadcrumbItem>
                  {
                    (collapseDetail) ? <MDBIcon icon="angle-down" /> : <MDBIcon icon="angle-right" />
                  }
                  {"  Detalle"}
                </MDBBreadcrumbItem>
              </MDBBreadcrumb>
              <br />
            </div>
          </div>
          <MDBCollapse id="collapseDetail" isOpen={this.state.collapseDetail}>
            <div className="row">
              <div className="col-lg">
                <MDBInput value=" " disabled label="Tipo de registro del caso" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Número del caso" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Propietario del caso" size="sm" />
              </div>
            </div>
            <div className="row">
              <div className="col-lg">
                <MDBInput value=" " disabled label="Grupo responsable" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Estado" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Responsable" size="sm" />
              </div>
            </div>
            <div className="row">
              <div className="col-lg">
                <MDBInput value=" " disabled label="Origen del caso" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Prioridad" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Fecha/Hora Recepción de correo" size="sm" />
              </div>
            </div>
            <div className="row">
              <div className="col-lg">
                <MDBInput value=" " disabled label="Fecha y hora recepción correo físico" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Luna Polarizadas" size="sm" />
              </div>
              <div className="col-lg">
              </div>
            </div>
          </MDBCollapse>
        </div>
        <div>
          <div className="row">
            <div className="col-lg-12">
              <MDBBreadcrumb
                onClick={() => {
                  this.setState({
                    collapsePoliza: !this.state.collapsePoliza
                  })
                }}
              >
                <MDBBreadcrumbItem>
                  {
                    (collapsePoliza) ? <MDBIcon icon="angle-down" /> : <MDBIcon icon="angle-right" />
                  }
                  {"  Póliza"}
                </MDBBreadcrumbItem>
              </MDBBreadcrumb>
              <br />
            </div>
          </div>
          <MDBCollapse id="collapsePoliza" isOpen={this.state.collapsePoliza}>
            <div className="row">
              <div className="col-lg">
                <MDBInput value=" " disabled label="Póliza" size="sm" />
              </div>
              <div className="col-lg">
              </div>
              <div className="col-lg">
              </div>
            </div>
          </MDBCollapse>
        </div>
        <div>
          <div className="row">
            <div className="col-lg-12">
              <MDBBreadcrumb
                onClick={() => {
                  this.setState({
                    collapseInfoContac: !this.state.collapseInfoContac
                  })
                }}
              >
                <MDBBreadcrumbItem>
                  {
                    (collapseInfoContac) ? <MDBIcon icon="angle-down" /> : <MDBIcon icon="angle-right" />
                  }
                  {"  Información de Contacto"}
                </MDBBreadcrumbItem>
              </MDBBreadcrumb>
              <br />
            </div>
          </div>
          <MDBCollapse id="collapseInfoContac" isOpen={this.state.collapseInfoContac}>
            <div className="row">
              <div className="col-lg">
                <MDBInput value=" " disabled label="Nombre del contacto" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Nombre de la cuenta" size="sm" />
              </div>
              <div className="col-lg">
              </div>
            </div>
          </MDBCollapse>
        </div>
        <div>
          <div className="row">
            <div className="col-lg-12">
              <MDBBreadcrumb
                onClick={() => {
                  this.setState({
                    collapseTipifaCaso: !this.state.collapseTipifaCaso
                  })
                }}
              >
                <MDBBreadcrumbItem>
                  {
                    (collapseTipifaCaso) ? <MDBIcon icon="angle-down" /> : <MDBIcon icon="angle-right" />
                  }
                  {"  Tipificación del Caso"}
                </MDBBreadcrumbItem>
              </MDBBreadcrumb>
              <br />
            </div>
          </div>
          <MDBCollapse id="collapseTipifaCaso" isOpen={this.state.collapseTipifaCaso}>
            <div className="row">
              <div className="col-lg">
                <MDBInput value=" " disabled label="Grupo" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Tipo" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Categoría" size="sm" />
              </div>
            </div>
            <div className="row">
              <div className="col-lg">
                <MDBInput value=" " disabled label="Asunto" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Tema" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Tipo de atención" size="sm" />
              </div>
            </div>
            <div className="row">
              <div className="col-lg">
                <MDBInput value=" " disabled label="Fecha de ocurrencia" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Hora de ocurrencia" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Cantidad día(s)/hora(s) desde ocurrencia" size="sm" />
              </div>
            </div>
            <div className="row">
              <div className="col-lg">
                <MDBInput value=" " disabled label="Semáforo de ocurrencia" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Fecha vencimiento ANS" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Sub tema" size="sm" />
              </div>
            </div>
            <div className="row">
              <div className="col-lg">
                <MDBInput value=" " disabled label="Estado ANS" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="ANS (minutos)" size="sm" />
              </div>
              <div className="col-lg">
              </div>
            </div>
          </MDBCollapse>
        </div>
        <div>
          <div className="row">
            <div className="col-lg-12">
              <MDBBreadcrumb
                onClick={() => {
                  this.setState({
                    collapseDatoVehiculo: !this.state.collapseDatoVehiculo
                  })
                }}
              >
                <MDBBreadcrumbItem>
                  {
                    (collapseDatoVehiculo) ? <MDBIcon icon="angle-down" /> : <MDBIcon icon="angle-right" />
                  }
                  {"  Datos del Vehículo"}
                </MDBBreadcrumbItem>
              </MDBBreadcrumb>
              <br />
            </div>
          </div>
          <MDBCollapse id="collapseDatoVehiculo" isOpen={this.state.collapseDatoVehiculo}>
            <div className="row">
              <div className="col-lg">
                <MDBInput value=" " disabled label="Placa" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Motor" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Clase de vehículo" size="sm" />
              </div>
            </div>
            <div className="row">
              <div className="col-lg">
                <MDBInput value=" " disabled label="Marca" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Modelo" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Color" size="sm" />
              </div>
            </div>
            <div className="row">
              <div className="col-lg">
                <MDBInput value=" " disabled label="Año" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="CPC " size="sm" />
              </div>
              <div className="col-lg">
              </div>
            </div>
          </MDBCollapse>
        </div>
        <div>
          <div className="row">
            <div className="col-lg-12">
              <MDBBreadcrumb
                onClick={() => {
                  this.setState({
                    collapseDireccion: !this.state.collapseDireccion
                  })
                }}
              >
                <MDBBreadcrumbItem>
                  {
                    (collapseDireccion) ? <MDBIcon icon="angle-down" /> : <MDBIcon icon="angle-right" />
                  }
                  {
                    ' Dirección'
                  }
                </MDBBreadcrumbItem>
              </MDBBreadcrumb>
              <br />
            </div>
          </div>
          <MDBCollapse id="collapseDireccion" isOpen={this.state.collapseDireccion}>
            <div className="row">
              <div className="col-lg">
                <MDBInput value=" " disabled label="País" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Departamento" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Provincia" size="sm" />
              </div>
            </div>
            <div className="row">
              <div className="col-lg">
                <MDBInput value=" " disabled label="Distrito" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Dirección del siniestro" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Referencias" size="sm" />
              </div>
            </div>
            <div className="row">
              <div className="col-lg">
                <MDBInput value=" " disabled label="Lugar de ocurrencia" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Anillo" size="sm" />
              </div>
              <div className="col-lg">
              </div>
            </div>
          </MDBCollapse>
        </div>
        <div>
          <div className="row">
            <div className="col-lg-12">
              <MDBBreadcrumb
                onClick={() => {
                  this.setState({
                    collapseDatoConductor: !this.state.collapseDatoConductor
                  })
                }}
              >
                <MDBBreadcrumbItem>
                  {
                    (collapseDatoConductor) ? <MDBIcon icon="angle-down" /> : <MDBIcon icon="angle-right" />
                  }
                  {
                    ' Datos del Conductor'
                  }
                </MDBBreadcrumbItem>
              </MDBBreadcrumb>
              <br />
            </div>
          </div>
          <MDBCollapse id="collapseDatoConductor" isOpen={this.state.collapseDatoConductor}>
            <div className="row">
              <div className="col-lg">
                <MDBInput value=" " disabled label="Nombre del conductor" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Apellido paterno del conductor" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Apellido materno del conductor" size="sm" />
              </div>
            </div>
            <div className="row">
              <div className="col-lg">
                <MDBInput value=" " disabled label="Tipo documento conductor" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Número documento conductor" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Licencia del Conductor" size="sm" />
              </div>
            </div>
            <div className="row">
              <div className="col-lg">
                <MDBInput value=" " disabled label="Teléfono del conductor" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Correo electrónico conductor" size="sm" />
              </div>
              <div className="col-lg">
              </div>
            </div>
          </MDBCollapse>
        </div>
        <div>
          <div className="row">
            <div className="col-lg-12">
              <MDBBreadcrumb
                onClick={() => {
                  this.setState({
                    collapseDetalleCaso: !this.state.collapseDetalleCaso
                  })
                }}
              >
                <MDBBreadcrumbItem>
                  {
                    (collapseDetalleCaso) ? <MDBIcon icon="angle-down" /> : <MDBIcon icon="angle-right" />
                  }
                  {
                    ' Detalle del Caso'
                  }
                </MDBBreadcrumbItem>
              </MDBBreadcrumb>
              <br />
            </div>
          </div>
          <MDBCollapse id="collapseDetalleCaso" isOpen={this.state.collapseDetalleCaso}>
            <div className="row">
              <div className="col-lg">
                <MDBInput value=" " disabled label="Descripción" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Fecha/Hora de apertura" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Proveedor" size="sm" />
              </div>
            </div>
            <div className="row">
              <div className="col-lg">
                <MDBInput value=" " disabled label="Fecha y hora de asignación al proveedor" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Fecha de programación" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Hora de programación" size="sm" />
              </div>
            </div>
            <div className="row">
              <div className="col-lg">
                <MDBInput value=" " disabled label="Fecha y hora de llegada al lugar" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Fecha inicio atención del proveedor" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Hora inicio atención proveedor" size="sm" />
              </div>
            </div>
            <div className="row">
              <div className="col-lg">
                <MDBInput value=" " disabled label="Fecha culminación proveedor" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Hora culminación proveedor" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Comisaría" size="sm" />
              </div>
            </div>
            <div className="row">
              <div className="col-lg">
                <MDBInput value=" " disabled label="Hora de transmisión del siniestro" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Fecha de transmisión del siniestro" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Tipo de servicio" size="sm" />
              </div>
            </div>
            <div className="row">
              <div className="col-lg">
                <MDBInput value=" " disabled label="Fecha/Hora de cierre" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Nro siniestro" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Deducibles simplificados" size="sm" />
              </div>
            </div>
            <div className="row">
              <div className="col-lg">
                <MDBInput value=" " disabled label="PDF reporte proveedor" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Número BPM" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Estado de reporte" size="sm" />
              </div>
            </div>
            <div className="row">
              <div className="col-lg">
                <MDBInput value=" " disabled label="Comentarios Bizagi" size="sm" />
              </div>
              <div className="col-lg">
              </div>
              <div className="col-lg">
              </div>
            </div>
          </MDBCollapse>
        </div>
        <div>
          <div className="row">
            <div className="col-lg-12">
              <MDBBreadcrumb
                onClick={() => {
                  this.setState({
                    collapseDatoGps: !this.state.collapseDatoGps
                  })
                }}
              >
                <MDBBreadcrumbItem>
                  {
                    (collapseDatoGps) ? <MDBIcon icon="angle-down" /> : <MDBIcon icon="angle-right" />
                  }
                  {
                    ' Datos GPS'
                  }
                </MDBBreadcrumbItem>
              </MDBBreadcrumb>
              <br />
            </div>
          </div>
          <MDBCollapse id="collapseDatoGps" isOpen={this.state.collapseDatoGps}>
            <div className="row">
              <div className="col-lg">
                <MDBInput value=" " disabled label="GPS obligatorio" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="GPS instalado" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Proveedor GPS" size="sm" />
              </div>
            </div>
          </MDBCollapse>
        </div>
        <div>
          <div className="row">
            <div className="col-lg-12">
              <MDBBreadcrumb
                onClick={() => {
                  this.setState({
                    collapseInfoAdicional: !this.state.collapseInfoAdicional
                  })
                }}
              >
                <MDBBreadcrumbItem>
                  {
                    (collapseInfoAdicional) ? <MDBIcon icon="angle-down" /> : <MDBIcon icon="angle-right" />
                  }
                  {
                    ' Información Adicional(Siniestro)'
                  }
                </MDBBreadcrumbItem>
              </MDBBreadcrumb>
              <br />
            </div>
          </div>
          <MDBCollapse id="collapseInfoAdicional" isOpen={this.state.collapseInfoAdicional}>
            <div className="row">
              <div className="col-lg">
                <MDBInput value=" " disabled label="Muertos/heridos" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Cantidad de heridos" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Hay heridos?" size="sm" />
              </div>
            </div>
            <div className="row">
              <div className="col-lg">
                <MDBInput value=" " disabled label="Cantidad fallecidos" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Hay fallecidos?" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Vehículos tipo perdida" size="sm" />
              </div>
            </div>
          </MDBCollapse>
        </div>
        <div>
          <div className="row">
            <div className="col-lg-12">
              <MDBBreadcrumb
                onClick={() => {
                  this.setState({
                    collapseObs: !this.state.collapseObs
                  })
                }}
              >
                <MDBBreadcrumbItem>
                  {
                    (collapseObs) ? <MDBIcon icon="angle-down" /> : <MDBIcon icon="angle-right" />
                  }
                  {
                    ' Observaciones'
                  }
                </MDBBreadcrumbItem>
              </MDBBreadcrumb>
              <br />
            </div>
          </div>
          <MDBCollapse id="collapseObs" isOpen={this.state.collapseObs}>
            <div className="row">
              <div className="col-lg">
                <MDBInput value=" " disabled label="Anotaciones" size="sm" />
              </div>
              <div className="col-lg">
              </div>
              <div className="col-lg">
              </div>
            </div>
          </MDBCollapse>
        </div>
        <div>
          <div className="row">
            <div className="col-lg-12">
              <MDBBreadcrumb
                onClick={() => {
                  this.setState({
                    collapseWachList: !this.state.collapseWachList
                  })
                }}
              >
                <MDBBreadcrumbItem>
                  {
                    (collapseWachList) ? <MDBIcon icon="angle-down" /> : <MDBIcon icon="angle-right" />
                  }
                  {
                    ' Wach List'
                  }
                </MDBBreadcrumbItem>
              </MDBBreadcrumb>
              <br />
            </div>
          </div>
          <MDBCollapse id="collapseWachList" isOpen={this.state.collapseWachList}>
            <div className="row">
              <div className="col-lg">
                <MDBInput value=" " disabled label="Conducto" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Tipo de Lista" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Motivo" size="sm" />
              </div>
            </div>
            <div className="row">
              <div className="col-lg">
                <MDBInput value=" " disabled label="Ramo" size="sm" />
              </div>
              <div className="col-lg">
              </div>
              <div className="col-lg">
              </div>
            </div>
          </MDBCollapse>
        </div>
        <div>
          <div className="row">
            <div className="col-lg-12">
              <MDBBreadcrumb
                onClick={() => {
                  this.setState({
                    collapseCampoFotoPoliza: !this.state.collapseCampoFotoPoliza
                  })
                }}
              >
                <MDBBreadcrumbItem>
                  {
                    (collapseCampoFotoPoliza) ? <MDBIcon icon="angle-down" /> : <MDBIcon icon="angle-right" />
                  }
                  {
                    ' Campos foto póliza'
                  }
                </MDBBreadcrumbItem>
              </MDBBreadcrumb>
              <br />
            </div>
          </div>
          <MDBCollapse id="collapseCampoFotoPoliza" isOpen={this.state.collapseCampoFotoPoliza}>
            <div className="row">
              <div className="col-lg">
                <MDBInput value=" " disabled label="Estado de la póliza foto" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Indicador de morosidad foto" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Titular foto" size="sm" />
              </div>
            </div>
            <div className="row">
              <div className="col-lg">
                <MDBInput value=" " disabled label="Rol del contacto foto" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Tipo de documento contratante foto" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Código de bróker foto" size="sm" />
              </div>
            </div>
            <div className="row">
              <div className="col-lg">
                <MDBInput value=" " disabled label="Número de documento contratante foto" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Bróker foto" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Contratante foto" size="sm" />
              </div>
            </div>
            <div className="row">
              <div className="col-lg">
                <MDBInput value=" " disabled label="Suma asegurada foto" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Código de producto foto" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Prima total foto" size="sm" />
              </div>
            </div>
            <div className="row">
              <div className="col-lg">
                <MDBInput value=" " disabled label="Fecha de inicio (vigencia actual) foto" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Moneda de la póliza foto" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Fecha fin de vigencia foto" size="sm" />
              </div>
            </div>
            <div className="row">
              <div className="col-lg">
                <MDBInput value=" " disabled label="Responsable de pago foto" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Código afiliado foto" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Placa foto" size="sm" />
              </div>
            </div>
            <div className="row">
              <div className="col-lg">
                <MDBInput value=" " disabled label="Fecha de cobertura foto" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Color foto" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Número de certificado foto" size="sm" />
              </div>
            </div>
            <div className="row">
              <div className="col-lg">
                <MDBInput value=" " disabled label="Nro máximo atenciones (Aux Mecá) foto" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Estado del certificado foto" size="sm" />
              </div>
              <div className="col-lg">
                <MDBInput value=" " disabled label="Nro máximo atenciones (CDR) foto" size="sm" />
              </div>
            </div>
          </MDBCollapse>
        </div>
      </DivInput>
    );
  }
}

export default DataGeneral;
