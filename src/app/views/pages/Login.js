// @ts-check
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Animated } from "react-animated-css";
import Loader from 'react-loader-spinner';
import { MDBInput } from 'mdbreact'
import { Auth, Hub } from 'aws-amplify'

import { userOperations } from 'state/ducks/user';
import envAws from 'state/utils/envAws'
import PubSub from 'state/utils/PubSub'
import { withToastManager } from 'react-toast-notifications'
import { userLoaderKeys } from 'state/ducks/user'

class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: process.env.NODE_ENV === 'development' ? envAws.credentials.USER_NAME : '',
      password: process.env.NODE_ENV === 'development' ? envAws.credentials.PASSWORD : '',
      submitted: false,
      jwtToken: null,
      loader: false,
    }
  }

  componentDidMount() {
    const { toastManager } = this.props

    PubSub.subscribe('ERR_LOGIN', (message) => {
      toastManager.add(message, {
        autoDismissTimeout: 8000,
        appearance: 'error',
        autoDismiss: true,
        pauseOnHover: true,
      })
    })
    /*
    window.addEventListener('beforeunload',(event) => {
      const { userOperations, user } = this.props
      let requestBody = {
        "clientId": user.sessionId
      }

      console.log("SESSION_REMOVE_EVENT")
      userOperations.sessionRemove(requestBody)

      event.returnValue = 'Estás seguro de salir?';
    })*/
  }

  handleChange = evt => {
    const { name, value } = evt.target
    this.setState({ [name]: value })
  }

  handleSubmit = evt => {
    evt.preventDefault()
    this.callLogin()
  }

  callLogin = () => {
    const { userOperations } = this.props
    userOperations.login()
    // Auth.federatedSignIn()
  }

  render() {
    return (
      <div>
        <Animated animationIn="fadeIn" animationOut="fadeOutDownBig" animationInDuration={2000} animationOutDuration={2000} isVisible={true}>
          <div className="container" style={{ position: 'absolute', top: 0, bottom: 0, left: 0, right: 0, width: '60%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            <div className="row justify-content-center" style={{ width: '100%' }}>
              <div className="col-xl-10 col-lg-12 col-md-9">
                <div className="card o-hidden border-0 shadow-lg my-5">
                  <div className="card-body">
                    <div className="row" style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                      <Animated animationIn="bounceInUp" animationOut="bounceOutDown" animationInDuration={2000} animationOutDuration={2000} isVisible={true}>
                        <div className="col-lg-12" style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                          <img src="/img/logo-rimac1.png" />
                          <hr />
                        </div>
                      </Animated>
                    </div>
                    <div className="row">
                      <div className="col-lg-12" style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                        <form className="user" onSubmit={this.handleSubmit}>
                          <input
                            className="btn btn-danger btn-user btn-block"
                            type="submit"
                            value="Ingresar"
                          />
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Animated>
      </div>
    )
  }
}

const mapDispatchToProps = (dispatch) => ({
  userOperations: bindActionCreators(userOperations, dispatch)
});

const mapStateToProps = (state) => ({
  user: state.user,
  shared: state.shared
});

export default connect(mapStateToProps, mapDispatchToProps)(withToastManager(Login));
