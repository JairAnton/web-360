// @ts-check
import React, { Component } from 'react'
import { HashLink as Link } from 'react-router-hash-link'
import { withToastManager } from 'react-toast-notifications'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { MDBCard, MDBCardBody, MDBCardHeader, MDBRow, } from "mdbreact"
import { Layout } from 'element-react'
import { personOperations, personLoaderKeys } from 'state/ducks/person'
import { generalOperations } from 'state/ducks/general'
import { policyOperations } from 'state/ducks/policy'
import { salesforceOperations } from 'state/ducks/salesforce'
import { sinisterOperations } from 'state/ducks/sinister'
import { operationOperations } from 'state/ducks/operation'
import ElementTable from 'components/table/elementTable'
import { Animated } from "react-animated-css"
import PubSub from 'state/utils/PubSub'

import { Alert } from 'element-react'

import { changeFormatDate, } from 'state/utils/functionUtils'


//import UserFrequent from 'components/layout/userFrequent'

const dataSelectPN = [
  {
    "usuCreacion": "MIGRACION",
    "fecCreacion": "2012-04-19 03:12:37",
    "usuModif": "DS_SASPRI_WEB",
    "fecModif": "2018-12-28 14:28:43",
    "idePar": "7348",
    "codExterno": "2",
    "ideTipPar": "TER_TIPODOCUMENTO",
    "ideTipParPadre": null,
    "codigo": "2",
    "abreviatura": "DNI            ",
    "descripcion": "DOC. NACIONAL IDENTIDAD",
    "descripcion2": null,
    "indActivo": "S",
    "masterDetail": null,
    "refmigracionrs": "2",
    "codigoN": 2,
    "codigoC": "2"
  },
  {
    "usuCreacion": "DS_CNTWEB",
    "fecCreacion": "2012-05-28 15:15:06",
    "usuModif": "DS_CNTWEB",
    "fecModif": "2017-07-11 15:39:23",
    "idePar": "7435",
    "codExterno": "4",
    "ideTipPar": "TER_TIPODOCUMENTO",
    "ideTipParPadre": null,
    "codigo": "4",
    "abreviatura": "C.E.",
    "descripcion": "CARNET DE EXTRANJERIA",
    "descripcion2": "C.E.",
    "indActivo": "S",
    "masterDetail": null,
    "refmigracionrs": "4",
    "codigoN": 4,
    "codigoC": "4"
  },
  {
    "usuCreacion": "DS_CNTWEB",
    "fecCreacion": "2012-05-28 15:14:15",
    "usuModif": "DS_CNTWEB",
    "fecModif": "2017-07-11 15:39:23",
    "idePar": "7434",
    "codExterno": "6",
    "ideTipPar": "TER_TIPODOCUMENTO",
    "ideTipParPadre": null,
    "codigo": "3",
    "abreviatura": "PASAPORTE",
    "descripcion": "PASAPORTE",
    "descripcion2": "PASAPORTE",
    "indActivo": "S",
    "masterDetail": null,
    "refmigracionrs": "6",
    "codigoN": 3,
    "codigoC": "3"
  },
  {
    "usuCreacion": "MIGRACION",
    "fecCreacion": "2012-04-19 03:12:37",
    "usuModif": "DS_SASPRI_WEB",
    "fecModif": "2018-12-14 11:31:17",
    "idePar": "7349",
    "codExterno": "1",
    "ideTipPar": "TER_TIPODOCUMENTO",
    "ideTipParPadre": null,
    "codigo": "1",
    "abreviatura": "RUC            ",
    "descripcion": "REG. UNICO CONTRIBUY.",
    "descripcion2": null,
    "indActivo": "S",
    "masterDetail": null,
    "refmigracionrs": "1",
    "codigoN": 1,
    "codigoC": "1"
  }
]

const dataSelectPJ = [
  {
    "usuCreacion": "MIGRACION",
    "fecCreacion": "2012-04-19 03:12:37",
    "usuModif": "DS_SASPRI_WEB",
    "fecModif": "2018-12-14 11:31:17",
    "idePar": "7349",
    "codExterno": "1",
    "ideTipPar": "TER_TIPODOCUMENTO",
    "ideTipParPadre": null,
    "codigo": "1",
    "abreviatura": "RUC            ",
    "descripcion": "REG. UNICO CONTRIBUY.",
    "descripcion2": null,
    "indActivo": "S",
    "masterDetail": null,
    "refmigracionrs": "1",
    "codigoN": 1,
    "codigoC": "1"
  }
]

class SearchThird extends Component {

  state = {
    checkPN: true,
    checkPJ: false,
    selectTD: '2',
    dataSelectTD: dataSelectPN,
    body: {
      'tipoTercero': 'N',// J juridico
      'tipoDocumento': '2',
      'numDocumento': '',
      // 'numDocumento': '41304548', // DNI  de Danny
      // 'numDocumento': '06935312', DNI con Direcciones de Riesgos Generales
      'nombre': '', // solo N
      'apePaterno': '',
      'apeMaterno': '',
      'nombreComercial': '',
      'codExterno': '',
      'razonSocial': '',
    },
    page: 1,
    rowPage: 10,
    requestBodyData: {}

  }

  length = 15

  componentDidMount() {
    const { toastManager, policyOperations, salesforceOperations, sinisterOperations, operationOperations } = this.props
    PubSub.subscribe('ERR_PERSON_SEARCH', (message) => {
      toastManager.add(message, {
        autoDismissTimeout: 8000,
        appearance: 'error',
        autoDismiss: true,
        pauseOnHover: true,
      })
    })

    PubSub.subscribe('WARNING_LOGIN', (message) => {
      toastManager.add(message, {
        autoDismissTimeout: 8000,
        appearance: 'warning',
        autoDismiss: true,
        pauseOnHover: true,
      })
    })

    PubSub.subscribe('WARNING_SEARCH_THIRD', (message) => {
      toastManager.add(message, {
        autoDismissTimeout: 8000,
        appearance: 'warning',
        autoDismiss: true,
        pauseOnHover: true,
      })
    })
    // auditoria
    let requestLogging = {
      "path": window.location.href,
      "action": "PAGE_CLIENTE"
    }
    personOperations.auditLogging(requestLogging)
    
    policyOperations.resetDataPolicy()
    salesforceOperations.resetDataSalesforce()
    sinisterOperations.resetDataSinister()
    operationOperations.resetDataOperation()
  }

  changeCheck = (pn, pj) => {
    let body = this.state.body
    let dataSelectTD = []

    if (pn) {
      dataSelectTD = dataSelectPN

      body.numDocumento = ''
      body.nombre = ''
      body.apeMaterno = ''
      body.apePaterno = ''
    }

    if (pj) {
      dataSelectTD = dataSelectPJ

      body.numDocumento = ''
      body.nombreComercial = ''
      body.razonSocial = ''
    }

    this.setState({
      checkPJ: pj,
      checkPN: pn,
      selectTD: pj ? '1' : '2',
      dataSelectTD,
      body: {
        ...body,
        tipoTercero: pj ? 'J' : 'N',
        tipoDocumento: pj ? '1' : '2'
      }
    })
  }

  /**
   * TODO:
   * RUC: 1,
   * DNI: 2,
   * PASAPORTE: 3
   * CE: 4,
   *  */
  handleChange = evt => {
    let { value, name } = evt.target
    const { selectTD, body } = this.state
    let cutNumDoc = false

    if (name === "numDocumento") {
      if (`${selectTD}` === '1') {
        this.length = 11
        cutNumDoc = value.length > this.length
      } else if (`${selectTD}` === '2') {
        this.length = 8
        cutNumDoc = value.length > this.length
      }

      if (cutNumDoc) {
        value = value.slice(0, this.length)
      }
    }

    if (name === "tipoDocumento") body.numDocumento = ''

    this.setState({
      selectTD: name === "tipoDocumento" ? value : selectTD,
      body: {
        ...body,
        [name]: value,
      },
    })
  }

  validateForm = () => {
    const body = this.state.body
    if (body.tipoTercero === 'N') {
      if (body.numDocumento.replace(/ /gi, '').length === 0) {
        const v1 = body.nombre.replace(/ /gi, '').length > 0 ? 1 : 0
        const v2 = body.apeMaterno.replace(/ /gi, '').length > 0 ? 1 : 0
        const v3 = body.apePaterno.replace(/ /gi, '').length > 0 ? 1 : 0

        if ((v1 + v2 + v3) >= 2)
          return true

        if ((v1 + v2 + v3) === 0) {
          PubSub.publish('WARNING_SEARCH_THIRD', 'Debe llenar los campos vacios.');
          return false
        }

        PubSub.publish('WARNING_SEARCH_THIRD', 'Debe llenar 2 datos entre Nombres y Apellidos.');
        return false
      }

      if (body.numDocumento.replace(/ /gi, '').length > 0 && body.numDocumento.replace(/ /gi, '').length < 6) {
        PubSub.publish('WARNING_SEARCH_THIRD', 'Número de Documento: la cantidad de caracteres debe ser al menos 6.');
        return false
      }

      if (body.numDocumento.replace(/ /gi, '').length > 0 || body.nombre.replace(/ /gi, '').length > 0 || body.apeMaterno.replace(/ /gi, '').length > 0 || body.apePaterno.replace(/ /gi, '').length > 0)
        return true

      PubSub.publish('WARNING_SEARCH_THIRD', 'Debe llenar los campos vacios.');
    } else {
      if (body.numDocumento.replace(/ /gi, '').length > 0 || body.nombreComercial.replace(/ /gi, '').length > 0 || body.razonSocial.replace(/ /gi, '').length > 0)
        return true

      PubSub.publish('WARNING_SEARCH_THIRD', 'Debe llenar la Razón Social.');
    }

    return false
  }

  keyPress = evt => {
    if (evt.keyCode === 13)
      this.buscarTercero()
    // return evt.keyCode !== 69
  }

  buscarTercero = async () => {
    const { personOperations } = this.props
    let { body, page, rowPage } = this.state
    if (this.validateForm()) {
      let { apeMaterno, apePaterno, nombre } = body
      body = {
        ...body,
        apeMaterno: apeMaterno.trim().toUpperCase(),
        apePaterno: apePaterno.trim().toUpperCase(),
        nombre: nombre.trim().toUpperCase(),
      }

      let requestBody = {
        filter: body,
        "pagination": {
          "filasPorPag": rowPage,
          "pagActual": page
        }
      }
      this.setState({
        requestBodyData: requestBody
      })
      
      personOperations.personSearch(requestBody)
      // auditoria
      let requestLogging = {
        "path": window.location.href,
        "action": "PERSON_SEARCH"
      }
      personOperations.auditLogging(requestLogging)
      
      setTimeout(() => {
        this.scrollToRef()
      }, 1500)
    }
  }

  changePageTercero = (currentPage) => {
    this.setState({
      page: currentPage
    }, () => {
      this.buscarTercero()
    });
  }

  changeRowsPerPageTercero = (numberOfRows) => {
    this.setState({
      rowPage: numberOfRows,
      page: 1
    }, () => {
      this.buscarTercero()
    });
  }

  selectThird = (numDocument, row) => {
    //this.addUserFrequent(row);
    //save document in sessionstorage
    var dataJson = {
      'data': row,
      'request': this.state.requestBodyData
    }
    var cantidadSession = sessionStorage.length;
    var maximoSession = 10

    if (cantidadSession === maximoSession) {
      var numExiste = sessionStorage.getItem(numDocument) !== null ? true : false
      if (numExiste === false) {
        sessionStorage.removeItem(sessionStorage.key(1))
        sessionStorage.setItem(numDocument, JSON.stringify(dataJson));
      }
    }
    else if (cantidadSession < maximoSession) {
      sessionStorage.setItem(numDocument, JSON.stringify(dataJson));
    }

    const { personOperations } = this.props
    personOperations.personSelect(numDocument)
  }

  selectThird2 = (numDocument) => {
    const { personOperations } = this.props
    personOperations.personSelect(numDocument)
  }
  limpiarFormTercero = () => {
    this.setState({
      body: {
        "numDocumento": "",
        "nombre": "",
        "apePaterno": "",
        "apeMaterno": "",
        "nombreComercial": "",
        "codExterno": ""
      }
    })
  }

  getColumnsTableSearchThird = () => {
    const personType = this.state.checkPJ ? 'PJ' : 'PN'
    return [
      {
        label: `Tipo \n Documento`,
        prop: "tipoDocumento",
        width: 80,
        personTypes: ['PN', 'PJ'],
      },
      {
        label: "N° Documento",
        prop: "numDocumento",
        width: 100,
        render: (row, column, _index) => (
          <strong style={{ paddingTop: 15, paddingBottom: 15, color: 'dodgerblue' }}>
            <strong>
              <Link to="#" onClick={() => this.selectThird(row[column.prop], row)}>
                <u>{row[column.prop]}</u>
              </Link>
            </strong>
          </strong>
        ),
        personTypes: ['PN', 'PJ'],
      },
      {
        label: "Razón social",
        prop: "razonSocial",
        minWidth: 130,
        render: (row, column, _index) => (
          <div style={{ paddingTop: 15, paddingBottom: 15 }}>
            <span>{row[column.prop]}</span>
          </div>
        ),
        personTypes: ['PJ'],
      },
      {
        label: "Nombres",
        prop: "nombre",
        minWidth: 130,
        personTypes: ['PN'],
      },
      {
        label: "Nombre Comercial",
        prop: "nombre",
        minWidth: 170,
        personTypes: ['PJ'],
      },
      {
        label: "Apellido Paterno",
        prop: "apePaterno",
        minWidth: 155,
        personTypes: ['PN'],
      },
      {
        label: "Apellido Materno",
        prop: "apeMaterno",
        minWidth: 165,
        personTypes: ['PN'],
      },
      {
        label: `Fecha \n Nacimiento`,
        prop: "fecNacimiento",
        minWidth: 85,
        personTypes: ['PN'],
      },
      {
        label: "Estado Civil",
        prop: "estadoCivil",
        minWidth: 90,
        personTypes: ['PN'],
      },
      {
        label: "Tratamiento",
        prop: "tratamiento",
        width: 90,
        personTypes: ['PN', 'PJ'],
      },
      {
        label: "Ley de Datos",
        prop: "leyProteccionDatos",
        width: 85,
        personTypes: ['PN', 'PJ'],
      },
      {
        label: "Código AcselX",
        prop: "codigoExterno",
        width: 90,
        personTypes: ['PN', 'PJ'],
      }
    ].filter(item => item.personTypes.includes(personType))
  }

  getDataRowsTableSearchThird = () => {
    const { page, rowPage } = this.state
    const { search: { dataList } } = this.props.person

    if (dataList)
      return dataList.map((item, index) => ({
        ...item,
        "id": ((page - 1) * rowPage + index + 1),
        "tipoDocumento": item.desTipoDocumento,
        "codigoExterno": (item.codExterno) ? item.codExterno : "",
        "fecNacimiento": changeFormatDate(item.fecNacimiento, "-", "/"),//Cambio del formato fecha
      }))
    return []
  }

  scrollToRef = () => {
    // document.getElementById('my_scrollable_search').scrollIntoView({ behavior: 'smooth' })
  }



  render() {
    const {
      person: {
        firtsRequestSearch,
        search: { pagination }
      },
      loader,
    } = this.props
    // window._log(`render, firtsRequestSearch: ${firtsRequestSearch}`)
    const loaderVisible = loader.includes(personLoaderKeys.PERSON_SEARCH)

    const {
      dataSelectTD,
      page,
      rowPage
    } = this.state

    return (
      <div>

        <Animated
          animationIn="bounceInLeft"
          animationOut="bounceOutLeft"
          animationInDuration={2000}
          animationOutDuration={2000}
          isVisible={true}
        >
          <div className="row">
            <div className="col-lg">
              <MDBCard>
                <MDBCardHeader color="lighten-1" style={{ "backgroundColor": "#E52E2D" }}>
                  Búsqueda de Cliente
                </MDBCardHeader>
                <MDBCardBody>
                  <form>
                    <div className="row">
                      <div className="col-sm">
                        Criterios de Búsqueda:
                      </div>
                      <div className="form-check col-sm">
                        <input
                          className="form-check-input"
                          type="radio"
                          name="tipoPersona"
                          id="exampleRadios1"
                          value="PN"
                          defaultChecked
                          onClick={() => this.changeCheck(true, false)}
                        />
                        <label className="form-check-label" htmlFor="exampleRadios1">
                          Persona natural
                        </label>
                      </div>
                      <div className="form-check col-sm">
                        <input
                          className="form-check-input"
                          type="radio"
                          name="tipoPersona"
                          id="exampleRadios2"
                          value="PJ"
                          onClick={() => this.changeCheck(false, true)}
                        />
                        <label className="form-check-label" htmlFor="exampleRadios2">
                          Persona jurídica
                        </label>
                      </div>
                    </div>
                    <hr />
                    <div>
                      <Layout.Row gutter="10" style={{ marginBottom: '15px' }}>
                        <Layout.Col span="4">
                          <label className="col-form-label">Tipo documento:</label>
                        </Layout.Col>
                        <Layout.Col span="7">
                          <select className="custom-select"
                            name="tipoDocumento"
                            value={this.state.selectTD}
                            onChange={this.handleChange}
                          >
                            {
                              dataSelectTD &&
                              dataSelectTD.length > 0 &&
                              dataSelectTD.map((val, key) => {
                                return (<option value={val.codigoN} key={key}>{val.abreviatura}</option>)
                              })
                            }
                          </select>
                        </Layout.Col>
                        <Layout.Col span="4" offset="1">
                          <label className="col-form-label">Número documento:</label>
                        </Layout.Col>
                        <Layout.Col span="8">
                          <input
                            className="form-control"
                            type="number"
                            name="numDocumento"
                            value={this.state.body.numDocumento}
                            onChange={this.handleChange}
                            onKeyDown={this.keyPress}
                          />
                        </Layout.Col>
                      </Layout.Row>
                    </div>
                    {
                      this.state.checkPN &&
                      <div>
                        <Layout.Row gutter="10" style={{ marginBottom: '15px' }}>
                          <Layout.Col span="4">
                            <label className="col-form-label">Apellido paterno:</label>
                          </Layout.Col>
                          <Layout.Col span="7">
                            <input
                              className="form-control"
                              type="text"
                              name="apePaterno"
                              value={this.state.body.apePaterno}
                              onChange={this.handleChange}
                              onKeyDown={this.keyPress}
                            />
                          </Layout.Col>
                          <Layout.Col span="4" offset="1">
                            <label className="col-form-label">Apellido materno:</label>
                          </Layout.Col>
                          <Layout.Col span="8">
                            <input
                              className="form-control"
                              type="text"
                              name="apeMaterno"
                              value={this.state.body.apeMaterno}
                              onChange={this.handleChange}
                              onKeyDown={this.keyPress}
                            />
                          </Layout.Col>
                        </Layout.Row>
                        <Layout.Row gutter="10">
                          <Layout.Col span="4">
                            <label className="col-form-label">Nombres:</label>
                          </Layout.Col>
                          <Layout.Col span="20">
                            <input
                              className="form-control"
                              type="text"
                              name="nombre"
                              value={this.state.body.nombre}
                              onChange={this.handleChange}
                              onKeyDown={this.keyPress}
                            />
                          </Layout.Col>
                        </Layout.Row>
                      </div>
                    }
                    {
                      this.state.checkPJ &&
                      <div>
                        <Layout.Row gutter="10" style={{ marginBottom: '15px' }}>
                          <Layout.Col span="4">
                            <label className="col-form-label">Razón social:</label>
                          </Layout.Col>
                          <Layout.Col span="20">
                            <input
                              className="form-control"
                              type="text"
                              name="razonSocial"
                              value={this.state.body.razonSocial}
                              onChange={this.handleChange}
                              onKeyDown={this.keyPress}
                            />
                          </Layout.Col>
                        </Layout.Row>
                        <Layout.Row gutter="10">
                          <Layout.Col span="4">
                            <label className="col-form-label">Nombre comercial:</label>
                          </Layout.Col>
                          <Layout.Col span="20">
                            <input
                              className="form-control"
                              type="text"
                              name="nombreComercial"
                              value={this.state.body.nombreComercial}
                              onChange={this.handleChange}
                              onKeyDown={this.keyPress}
                            />
                          </Layout.Col>
                        </Layout.Row>
                      </div>
                    }
                  </form>
                </MDBCardBody>
              </MDBCard>
            </div>
          </div>
          <p style={{ fontSize: '11px', marginTop: '8px' }}>*Para realizar una búsqueda solo es necesario el número documento o dos campos del nombre completo.</p>
        </Animated>
        <br />
        <Animated animationIn="pulse" animationOut="tada" animationInDuration={1000} animationOutDuration={1000} isVisible={true}>
          <div className="row justify-content-md-center">
            <div className="col-2">
              <button
                className="btn btn-danger btn-icon-split"
                onClick={this.buscarTercero}
              >
                <span className="icon text-white-50">
                  <i className="fas fa-search" style={{ color: 'white' }} />
                </span>
                <span className="text">
                  {'    Buscar'}
                </span>
              </button>
            </div>
            <div className="col-2">
              <button
                className="btn btn-danger btn-icon-split"
                onClick={this.limpiarFormTercero}
              >
                <span className="icon text-white-50">
                  <i className="fas fa-eraser" style={{ color: 'white' }} />
                </span>
                <span className="text">
                  {'    Limpiar'}
                </span>
              </button>
            </div>
          </div>
        </Animated>
        <br />

        <MDBRow center>
          <div
            style={{ width: '10px', height: '10px' }}
            id="my_scrollable_search"
          />
          {
            loaderVisible ?
              <div className="spinner-grow text-primary" role="status">
                <span className="sr-only">Loading...</span>
              </div>
              : (
                !firtsRequestSearch ?
                  <div className="col-lg-12">
                    <ElementTable
                      data={this.getDataRowsTableSearchThird()}
                      columns={this.getColumnsTableSearchThird()}
                      onChangePage={this.changePageTercero}
                      onChangeRowPage={this.changeRowsPerPageTercero}
                      pagination={pagination}
                      loader={false}
                      page={page}
                      rowPage={rowPage}
                    />
                  </div>
                  : <div></div>
              )
          }
        </MDBRow>

      </div>
    );
  }

}

const mapStateToProps = state => ({
  user: state.user,
  person: state.person,
  parameters: state.general.parameters,
  loader: state.shared.loader,
  //userFrequents:state.general.userFrequents
});

const mapDispatchToProps = (dispatch) => ({
  policyOperations: bindActionCreators(policyOperations, dispatch),
  personOperations: bindActionCreators(personOperations, dispatch),
  salesforceOperations: bindActionCreators(salesforceOperations, dispatch),
  sinisterOperations: bindActionCreators(sinisterOperations, dispatch),
  generalOperations: bindActionCreators(generalOperations, dispatch),
  operationOperations: bindActionCreators(operationOperations, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(withToastManager(SearchThird));