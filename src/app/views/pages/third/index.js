import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Route, Switch } from 'react-router-dom'
import { routes } from 'state/utils/routes'
import { MDBIcon } from 'mdbreact'

import SearchThird from './searchThird'
import DetailThird from './detailThird'
import Footer from 'components/layout/footerMB'
import SideMenu, { SIDEBAR_WIDTH } from 'components/layout/sideMenu'
import { bindActionCreators } from 'redux'

import { Auth } from 'aws-amplify'
import { Loading, Alert } from 'element-react'
import { generalOperations } from 'state/ducks/general'
import UserFrequent from 'components/layout/userFrequent'

class Third extends Component {

  constructor(props) {
    super(props)
    const menuOpened = localStorage.getItem('menuOpened') === 'true'
    this.state = {
      menuOpened: menuOpened,
    }
  }

  componentDidMount() {
  //   process.env.NODE_ENV === 'development' &&
  //     !this.props.user.jwtToken &&
  //     this.props.history.push('/cliente')
  }


  handleClick = (fromSidemenu) => {
    if (!fromSidemenu) {
      this.props.generalOperations.setSidemenuStatus()
      localStorage.setItem('menuOpened', `${!this.props.menuOpened}`)
    } else {
      this.props.menuOpened && this.props.generalOperations.setSidemenuStatus()
      this.props.menuOpened && localStorage.setItem('menuOpened', `${!this.props.menuOpened}`)
    }
  }

  render() {
    const {
      auth: {
        loader: loaderAuth,
        alert: alertAuth
      }
    } = this.props.user

    return (
      <div style={{
        width: '100%',
        minHeight: '110%',
        overflow: 'hidden',
        position: 'fixed',
      }}>

      

        <SideMenu
          // marginLeft={this.state.menuOpened ? '0px' : `-${SIDEBAR_WIDTH}`}
          marginLeft={this.props.menuOpened ? '0px' : '-180px'}
          toggle={(toggleStatus) => this.handleClick(toggleStatus)}
          isOpenMenu={this.props.menuOpened}
        />      
     
        {
          <UserFrequent/>
        }
        
   
        <div
          style={{
            marginLeft: this.props.menuOpened ? SIDEBAR_WIDTH : '50px',
            //marginLeft: this.state.menuOpened ? SIDEBAR_WIDTH : '0px',
            transition: 'margin .5s',
            height: '100vh',
            overflow: 'auto',
          }}
        >
          <div
            className="container-fluid"
            style={{
              backgroundColor: 'white',
              paddingBottom: '30px',
              minHeight: 'calc(100vh - 77px)',
            }}
          >

          {
            
            <br/>
            /*
            <p style={{ fontSize: '30px', color: 'rgb(229, 46, 45)' }}>
              <MDBIcon
                icon={this.props.menuOpened
                  ? 'angle-double-left'
                  : 'angle-double-right'}
                onClick={() => this.handleClick()}
              />
            </p>*/
          }
         
          
            {
              loaderAuth ?
                <div>
                  <Loading text="Cargando..." />
                </div>
                : (
                  alertAuth ?
                    <Alert
                      title="Usuario no autorizado"
                      type="warning"
                      description="Usted no tiene permisos para acceder a la WEB360, pida los permisos a sus administrador."
                      showIcon={true}
                      closable={false}
                    />
                    : (
                      <Switch>
                        <Route exact path={routes.THIRD_DETAIL} component={DetailThird} />
                        <Route exact path={routes.THIRD} component={SearchThird} />
                      </Switch>
                    )
                )
            }

          </div>
          <Footer />
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
  menuOpened: state.general.sideMenuOpened
})

const mapDispatchToProps = (dispatch) => ({
  generalOperations: bindActionCreators(generalOperations, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Third)