// @ts-check
import React, { useState, useMemo, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { bindActionCreators } from 'redux'
import ElementTable from 'components/table/elementTable'
import { warrantyLetterLoaderKeys, warrantyLetterOperations } from 'state/ducks/warrantyLetter'
import DetailWarrantyLetter from './detailWarrantyLetterNav/detailWarrantyLetter'
import { changeFormatDate } from 'state/utils/functionUtils'
/**
* con Rocio se quedó en mostrar solo estos campos en la grilla,
* los demás en el detalle.
*/
const COLUMNS = [
  {
    label: 'N° Solicitud',
    prop: 'numSolicitud',
    minWidth: 130,
  },
  {
    label: 'N° de Carta',
    prop: 'numCartaGarantia',
    width: 150,
  },
  {
    label: 'Proveedor',
    prop: 'proveedor',
    minWidth: 150,
  },
  {
    label: 'Fecha/Hora solicitud',
    minWidth: 150,
    render: (row, column, index) => (
      <span>
        {changeFormatDate(`${row.fechaSolicitud}*${row.horaSolicitud}`, '-', '/').replace('*', ' - ')}
      </span>
    ),
  },
  {
    label: 'Estado',
    prop: 'estado',
    minWidth: 160,
  },
];

const WarrantyLetter = (props) => {
  const dispatch = useDispatch()
  const wlOperationsActions = useMemo(() => {
    return bindActionCreators(warrantyLetterOperations, dispatch)
  }, [dispatch])

  const [selectedWarrantyLetter, setSelectedWarrantyLetter] = useState(null)
  const loaderVisible = useSelector(state => state.shared.loader.includes(warrantyLetterLoaderKeys.WARRANTY_LOADER))
  const { select: selectedPerson } = useSelector(store => store.person)

  useEffect(() => {
    const { codAfiliado } = selectedPerson || {}
    const payload = {
      "codAfiliado": codAfiliado ? codAfiliado.toString() : "",
      "filtrar": true,
    }
    wlOperationsActions.warrantyLetterList(payload);

    // auditoria
    let requestLogging = {
      "path": window.location.href,
      "action": "WARRANTY_LETTER_LIST"
    }
    wlOperationsActions.auditLogging(requestLogging)
  }, [])

  useEffect(() => {
    selectedWarrantyLetter && document.getElementById('my_scrollable_div_wl').scrollIntoView({ behavior: 'smooth' })
  }, [selectedWarrantyLetter])

  const getData = useMemo(() => {
    const { warrantyLetter: { cartasGarantia = [] } } = props
    return cartasGarantia.map(item => ({ ...item }))
  }, [props.warrantyLetter.cartasGarantia])

  //Cuando haces Click a la fila - Row
  const clickRowTable = selectedItem => {
    setSelectedWarrantyLetter(selectedItem)
  }

  return (
    <div className="row">
      <div className="col-lg-12" style={{ cursor: 'pointer' }}>
        <ElementTable
          loader={loaderVisible}
          data={getData}
          columns={COLUMNS}
          onChangeSelect={clickRowTable}
          allRow
        />
      </div>

      <div className="col-lg-12">
        <div
          style={{ width: '10px', height: '10px' }}
          id="my_scrollable_div_wl"
        />
        {selectedWarrantyLetter &&
          <DetailWarrantyLetter
            warrantyLetter={selectedWarrantyLetter}
          />
        }
      </div>
    </div>
  )
}

export default WarrantyLetter