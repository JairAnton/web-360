//@ts-check
import React, { useState, useMemo, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { bindActionCreators } from 'redux'
// import _ from 'lodash'
import ElementTable from 'components/table/elementTable'
import { personLoaderKeys, personOperations } from 'state/ducks/person'
import DetailCommunication from './detailCommunicationNav/detailCommunication'

const COLUMNS = [
  {
    label: "Tipo Comunicación",
    prop: "tipoComunicacion",
    minWidth: 150,
  },
  {
    label: "Fecha Envío",
    prop: "fechaEnvio",
    minWidth: 120,
  },
  {
    label: "Canal Envío",
    prop: "usuarioEnvio",
    minWidth: 230,
  },
  {
    label: "Destinatario",
    prop: "destinatario",
    minWidth: 250,
  },
  {
    label: "Estado",
    prop: "estado",
    minWidth: 80,
  },
  {
    label: "Enviado",
    prop: "indEnviado",
    minWidth: 70,
  },
  {
    label: "Recibido",
    prop: "indRecibo",
    minWidth: 70,
  },
  {
    label: "Abierto",
    prop: "indAbierto",
    minWidth: 70,
  },
]

const CommunicationNav = props => {
  const dispatch = useDispatch()
  const personOperationsActions = useMemo(() => {
    return bindActionCreators(personOperations, dispatch)
  }, [dispatch])

  const [selectedCommunication, setSelectedCommunication] = useState(null)
  const { select: selectedPerson } = useSelector(store => store.person)
  const loaderVisible = useSelector(state => {
    return state.shared.loader.includes(personLoaderKeys.COMMUNICATION)
  })

  const selectRowTable = (selectedItem) => {
    setSelectedCommunication(selectedItem)
  }

  const dataList = useMemo(() => {
    return (props.communication.dataList) ? props.communication.dataList : []
  }, [props.communication.dataList]);

  useEffect(() => {
    const { ideTercero, numDocumento, codTipoDocumento, codExterno } = selectedPerson || {}
    const requestBody = {
      "idTercero": ideTercero ? `${ideTercero}` : "",
      "numeroDocumento": numDocumento ? `${numDocumento}` : "",
      "tipoDocumento": codTipoDocumento ? `${codTipoDocumento}` : "",
      "codExterno": codExterno ? `${codExterno}` : "",
    }
    personOperationsActions.personCommunicationList(requestBody);
  }, [])

  useEffect(() => {
    document.getElementById('my_scrollable_div_communication').scrollIntoView({ behavior: 'smooth' })
  }, [selectedCommunication])


  return (
    <div className="row">
      <div className="col-lg-12" style={{ cursor: 'pointer' }}>
        <ElementTable
          loader={loaderVisible}
          data={dataList}
          columns={COLUMNS}
          onChangeSelect={selectRowTable}
          allRow
        />
      </div>

      <div className="col-lg-12">
        <div
          style={{ width: '10px', height: '10px' }}
          id="my_scrollable_div_communication"
        />
        {selectedCommunication &&
          <DetailCommunication
            communication={selectedCommunication}
          />}
      </div>
    </div>
  )
}

export default CommunicationNav