export { default as CasesNav } from "./casesNav";
export { default as ClinicWorkshotsNav } from "./clinicWorkshotsNav";
export { default as CommunicationNav } from "./communicationNav";
export { default as ProcedureNav } from './procedureNav'
export { default as ProductsNav } from './productsNav'
export { default as ResumenNav } from './resumeNav'
export { default as RetentionNav } from './retentionNav'
export { default as SinesterNav } from './sinesterNav'
export { default as WarrantyLetter } from './warrantyLetter'
export { default as RefundNav } from './refundNav'
export { default as ClaimsNav } from './claimsNav'
export { default as PreexistenceNav} from './preexistenceNav'
export {default as ProductsSuggestNav} from './productsSuggestNav'




