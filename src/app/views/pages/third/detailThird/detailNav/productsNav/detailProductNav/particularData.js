// @ts-check
import React,{useEffect} from 'react'
import { useSelector } from 'react-redux'
import TableUI from '../../../../../../../components/table'
import { policyLoaderKeys } from '../../../../../../../state/ducks/policy'
import ElementTable from 'components/table/elementTable';

const COLUMNS = [
  {
    label: "Dato Particular",
    prop: "datoParticular",
    width: 300
  },
  {
    label: "Valor",
    prop: "valor",
    minWidth: 150
  },
  {
    label: "Riesgo",
    prop: "riesgo",
    width: 300
  },
];

const getData = particularData => {
  const data = (particularData.dataList)
    ? particularData.dataList
    : [];
  return data.length
    ? data.map(item => ({
      'datoParticular': item.etiqueta,
      'valor': item.valor ? item.valor : ' - ',
      'riesgo': item.riesgo,
    }))
    : []
}

const ParticularData = ({ datosParticulares, getDatosParticulares }) => {

  const loaderVisible = useSelector(state => state.shared.loader)
    .includes(policyLoaderKeys.PARTICULAR_DATA)
  
  useEffect(() => {
    getDatosParticulares();
  },[])

  return (
    <ElementTable
      loader={loaderVisible}
      data={getData(datosParticulares)}
      columns={COLUMNS}
      allRow
    />
  );
}

export default ParticularData