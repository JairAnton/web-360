import React, { Component, useMemo, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { bindActionCreators } from 'redux'

import ElementTable from 'components/table/elementTable';
import { policyOperations, policyLoaderKeys } from 'state/ducks/policy'

const COLUMNS = [
  {
    label: "Nivel",
    prop: "nivel",
    width: 200
  },
  {
    label: "Descripción",
    prop: "descripcion",
    minWidth: 150
  },
]

const Deductible = props => {
  const dispatch = useDispatch()
  const policyOperationsActions = useMemo(() => {
    return bindActionCreators(policyOperations, dispatch)
  }, [dispatch])

  const { dataList } = useSelector(store => store.policy.deducible)
  const select = useSelector(store => store.policy.select)
  const loaderVisible = useSelector(store => store.shared.loader.includes(policyLoaderKeys.POLICY_COVERAGE_DEDUCTIBLE_LIST))

  useEffect(() => {
    let requestBody = {
      "core": select.core,
      "codProdAcselx": select.codProdAcselx ? select.codProdAcselx.toString() : '',
      "codProducto": select.codProducto ? select.codProducto.toString() : '',
      "numPoliza": select.numPoliza ? select.numPoliza.toString() : '',
      "numCertificado": select.numCertificado ? select.numCertificado.toString() : '',
      "codCobertura": '',
      "idePoliza": select.idePoliza ? select.idePoliza.toString() : '',
    }

    policyOperationsActions.policyCoverageDeductibleList(requestBody)

    // auditoria
    let requestLogging = {
      "path": window.location.href,
      "action": "POLICY_COVERAGE_DEDUCTIBLE_LIST"
    }
    policyOperationsActions.auditLogging(requestLogging)
  }, [])

  return (
    <div>
      <ElementTable
        data={dataList}
        columns={COLUMNS}
        loader={loaderVisible}
        allRow
      />
    </div>
  )
}

export default Deductible