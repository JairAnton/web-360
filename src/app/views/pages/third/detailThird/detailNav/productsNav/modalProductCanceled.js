import React, { Component } from 'react';

class ModalProductCanceled extends Component {
  constructor(props) {
    super(props);
  }

  getColumnsProductCanceled = () => {
    const columns = [
      {
        Header: "Ramo",
        accessor: "ramo",
        style: {
          textAlign: "center",
        }
      },
      {
        Header: "Producto",
        accessor: "producto",
        style: {
          textAlign: "center",
        }
      },
      {
        Header: "N° Póliza",
        accessor: "numPoliza",
        style: {
          textAlign: "center",
        }
      },
      {
        Header: "N° Tramite Anulación",
        accessor: "numTramiteAnulacion",
        style: {
          textAlign: "center",
        }
      },
      {
        Header: "Fecha Inicio",
        accessor: "fechaInicio",
        style: {
          textAlign: "center",
        }
      },
      {
        Header: "Fecha Anulación",
        accessor: "fechaAnulacion",
        style: {
          textAlign: "center",
        }
      },
      {
        Header: "Motivo",
        accessor: "motivo",
        style: {
          textAlign: "center",
        }
      }
    ]

    return columns;
  }

  getDataProductCanceled = () => {
    return [];
  }


  render() {
    return (
      <div className="modal fade bd-modal-product-canceled-xl" tabIndex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
        <div className="modal-dialog modal-xl">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Productos Anulados</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Cerrar">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              {/* <DetailTable
                  columns={this.getColumnsProductCanceled()}
                  data={this.getDataProductCanceled()}
              /> */}
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ModalProductCanceled;