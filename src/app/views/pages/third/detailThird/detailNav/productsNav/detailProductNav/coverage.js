import React, { useState, useEffect, useRef } from 'react'
import { useSelector } from 'react-redux'
import Select from 'react-select'
// import styled from 'styled-components'
import { Loading, Button, Layout } from 'element-react'

import ElementTable from 'components/table/elementTable';
import policyLoaderKeys from 'state/ducks/policy/loaderKeys'
import constantsRamo from 'state/utils/constantsRamo'
import { withToastManager } from 'react-toast-notifications'
import PubSub from 'state/utils/PubSub'

// const DivInput = styled.div`
//     .breadcrumb {
//         cursor: pointer;
//     }
//     .md-form {
//         margin-top: 0.5rem;
//         margin-bottom: 0.5rem;
//     }
// `;

//Vehiculos, RRGG, Vida
const COLUMNS_VRRGGV = [
  {
    label: "Código Cobertura",
    prop: "codCobertura",
    minWidth: 160
  },
  {
    label: "Cobertura",
    prop: "desCobertura",
    minWidth: 160
  },
  {
    label: "Moneda",
    prop: "moneda",
    minWidth: 160
  },
  {
    label: "Suma Asegurada",
    prop: "sumaAsegurada",
    minWidth: 160
  },
]

const COLUMNS_SALUD = [
  {
    label: "Código \n Cobertura",
    prop: "codCobertura",
    minWidth: 110
  },
  {
    label: "Cobertura",
    prop: "desCobertura",
    minWidth: 120
  },
  {
    label: "Modalidad",
    prop: "modalidadAtencion",
    minWidth: 115
  },
  {
    label: "Tipo de Proveedor",
    prop: "tipoProveedor",
    minWidth: 170
  },
  {
    label: "Nombre Proveedor",
    prop: "proveedor",
    minWidth: 180
  },
  // {
  //   label: "RUC Proveedor",
  //   prop: "rucProveedor",
  //   minWidth: 160
  // },
  {
    label: "Sede Proveedor",
    prop: "sedeProveedor",
    minWidth: 160
  },
  {
    label: "Ubicación",
    prop: "ubicacion",
    minWidth: 130
  },
  {
    label: "Dirección Proveedor",
    prop: "direccionProveedor",
    minWidth: 190
  },
  {
    label: "Dpto/Prov/Distrito",
    prop: "dptoProvDist",
    minWidth: 170
  },
  {
    label: "% Coaseguro",
    prop: "porcenCoaseguro",
    minWidth: 135
  },
  {
    label: "Moneda",
    prop: "moneda",
    minWidth: 100
  },
  {
    label: "Monto \n Deducible",
    prop: "montoDeducible",
    minWidth: 110
  },
  {
    label: "Frecuencia de Cobro",
    prop: "frecuenciaCobro",
    minWidth: 200
  },
  {
    label: "Suma Asegurada",
    prop: "sumaAsegurada",
    minWidth: 160
  },
  {
    label: "Ind. Recupero",
    prop: "indRecupero",
    minWidth: 140
  },
  {
    label: "Carencia",
    prop: "carencia",
    minWidth: 100
  },
  {
    label: "Excepción \n Carencia",
    prop: "excepcionCarencia",
    minWidth: 110
  },
  {
    label: "Deducible \n Mayor",
    prop: "deducibleMayor",
    minWidth: 110
  },
  {
    label: "Edad Limite \n Cobertura",
    prop: "edadLimiteDeCobertura",
    minWidth: 130
  },
  {
    label: "Limite x \n Proveedor",
    prop: "limiteProveedor",
    minWidth: 130
  },
]

const optionsAX = [
  { value: "G", label: "GENERAL" },
  { value: "B", label: "BIENES" },
  { value: "P", label: "PERSONAS" },
];

const optModalidad = [
  {
    value: 'C',
    label: 'Crédito'
  },
  {
    value: 'R',
    label: 'Reembolso'
  }
]

const optUbicacion = [
  { value: "A", label: "PROVINCIAS ALEJADAS" },
  { value: "B", label: "BOTICA" },
  { value: "E", label: "EXTRANJERO" },
  { value: "L", label: "LIMA" },
  { value: "P", label: "PROVINCIA" },
]

const optDistrito = [
  { value: "1", label: "LIMA" },
  { value: "2", label: "ANCON" },
  { value: "3", label: "ATE" },
  { value: "4", label: "BARRANCO" },
  { value: "5", label: "BREÑA" },
  { value: "6", label: "CARABAYLLO" },
  { value: "7", label: "CHACLACAYO" },
  { value: "8", label: "CHORRILLOS" },
  { value: "9", label: "CIENEGUILLA" },
  { value: "10", label: "COMAS" },
  { value: "11", label: "EL AGUSTINO" },
  { value: "12", label: "INDEPENDENCIA" },
  { value: "13", label: "JESUS MARIA" },
  { value: "14", label: "LA MOLINA" },
  { value: "15", label: "LA VICTORIA" },
  { value: "16", label: "LINCE" },
  { value: "17", label: "LOS OLIVOS" },
  { value: "18", label: "LURIGANCHO (CHOSICA)" },
  { value: "19", label: "LURIN" },
  { value: "20", label: "MAGDALENA DEL MAR" },
  { value: "21", label: "PUEBLO LIBRE" },
  { value: "22", label: "MIRAFLORES" },
  { value: "23", label: "PACHACAMAC" },
  { value: "24", label: "PUCUSANA" },
  { value: "25", label: "PUENTE PIEDRA" },
  { value: "26", label: "PUNTA HERMOSA" },
  { value: "27", label: "PUNTA NEGRA" },
  { value: "28", label: "RIMAC" },
  { value: "29", label: "SAN BARTOLO" },
  { value: "30", label: "SAN BORJA" },
  { value: "31", label: "SAN ISIDRO" },
  { value: "32", label: "SAN JUAN DE LURIGANCHO" },
  { value: "33", label: "SAN JUAN DE MIRAFLORES" },
  { value: "34", label: "SAN LUIS" },
  { value: "35", label: "SAN MARTIN DE PORRES" },
  { value: "36", label: "SAN MIGUEL" },
  { value: "37", label: "SANTA ANITA" },
  { value: "38", label: "SANTA MARIA DEL MAR" },
  { value: "39", label: "SANTA ROSA" },
  { value: "40", label: "SANTIAGO DE SURCO" },
  { value: "41", label: "SURQUILLO" },
  { value: "42", label: "VILLA EL SALVADOR" },
  { value: "43", label: "VILLA MARIA DEL TRIUNFO" }
]

const optDepartamento = [
  { value: "1", label: "AMAZONAS" },
  { value: "2", label: "ANCASH" },
  { value: "3", label: "APURIMAC" },
  { value: "4", label: "AREQUIPA" },
  { value: "5", label: "AYACUCHO" },
  { value: "6", label: "CAJAMARCA" },
  { value: "7", label: "CALLAO" },
  { value: "8", label: "CUSCO" },
  { value: "9", label: "HUANCAVELICA" },
  { value: "10", label: "HUANUCO" },
  { value: "11", label: "ICA" },
  { value: "12", label: "JUNIN" },
  { value: "13", label: "LA LIBERTAD" },
  { value: "14", label: "LAMBAYEQUE" },
  { value: "15", label: "LIMA" },
  { value: "16", label: "LORETO" },
  { value: "17", label: "MADRE DE DIOS" },
  { value: "18", label: "MOQUEGUA" },
  { value: "19", label: "PASCO" },
  { value: "20", label: "PIURA" },
  { value: "21", label: "PUNO" },
  { value: "22", label: "SAN MARTIN" },
  { value: "23", label: "TACNA" },
  { value: "24", label: "TUMBES" },
  { value: "25", label: "UCAYALI" }
]

const RAMO_1 = [constantsRamo.VEHICULO, constantsRamo.RRGG, constantsRamo.VIDA, constantsRamo.SAPP];
const RAMO_2 = [constantsRamo.SALUD];

const Coverages = (props) => {
  const firstUpdate = useRef(true);
  const [useRender, setUserRender] = useState(1)
  const [page, setPage] = useState(1);
  const [rowPage, setRowPage] = useState(10);
  const [filterCoverage, setFilterCoverage] = useState({
    tipoCobertura: null,
    modalidadAtencion: null,
    ubicacion: null,
    distrito: null,
    departamento: null,
    proveedor: null,
    tipoProveedor: null,
  });

  const loaderVisible = useSelector(state => state.shared.loader.includes(policyLoaderKeys.COVERAGE))
  const loaderVisibleTypeCoverage = useSelector(state => state.shared.loader.includes(policyLoaderKeys.POLICY_COVERAGE_TYPE_LIST))
  const { coberturas, select } = useSelector(state => state.policy);

  useEffect(() => {
    const { toastManager } = props

    PubSub.subscribe('WARNING_INCOMPLETE_INPUT_COVERAGES', (message) => {
      toastManager.add(message, {
        autoDismissTimeout: 8000,
        appearance: 'warning',
        autoDismiss: true,
        pauseOnHover: true,
      })
    })
  }, [])

  useEffect(() => {
    if (firstUpdate.current) {
      if (RAMO_2.includes(select.ramo)) {
        let request = {
          "codProdAcselx": (select.codProdAcselx) ? select.codProdAcselx.toString() : '',
          "numPoliza": (select.numPoliza) ? select.numPoliza.toString() : '',//Opcional
          "codAfiliado": (select.codAfiliado) ? select.codAfiliado.toString() : '',
          "codCliente": (select.codCliente) ? select.codCliente.toString() : ''
        }
        props.getCoverageTypeList(request)
      }

      if (RAMO_1.includes(select.ramo)) {
        getCoveragesService()
      }

      firstUpdate.current = false
      return
    }

    getCoveragesService()
  }, [useRender]);

  const getCoveragesService = () => {
    let tipoCobertura;
    let modalidadAtencion;
    let ubicacion;
    let requestBody = {
      "filter": {},
      "pagination": {
        "filasPorPag": rowPage,
        "pagActual": page
      }
    }

    if (RAMO_2.includes(select.ramo)) {
      tipoCobertura = (filterCoverage.tipoCobertura) ? filterCoverage.tipoCobertura.value : ''
      modalidadAtencion = (filterCoverage.modalidadAtencion) ? filterCoverage.modalidadAtencion.value : ''
      ubicacion = (filterCoverage.ubicacion) ? filterCoverage.ubicacion.value : ''

      if (!(tipoCobertura && modalidadAtencion && ubicacion)) {
        PubSub.publish('WARNING_INCOMPLETE_INPUT_COVERAGES', 'Debe llenar los campo Obligatorios...')
        return
      }

      requestBody.filter = {
        "core": (select.core) ? select.core.toString() : '',
        "codProducto": "",
        "codProdAcselx": (select.codProdAcselx) ? select.codProdAcselx.toString() : '',
        "numPoliza": (select.numPoliza) ? select.numPoliza.toString() : '',//Opcional
        "numCertificado": "",
        "tipoCobertura": tipoCobertura,//
        "modalidadAtencion": modalidadAtencion,//
        "ubicacion": ubicacion,//
        "proveedor": (filterCoverage.proveedor) ? filterCoverage.proveedor : '',//
        "tipoProveedor": (filterCoverage.tipoProveedor) ? filterCoverage.tipoProveedor.value.toString() : '',//
        "codDepartamento": (filterCoverage.ubicacion && filterCoverage.ubicacion.value === 'P') ? (filterCoverage.departamento ? filterCoverage.departamento.value.toString() : '') : '',//,
        "codDistrito": (filterCoverage.ubicacion && filterCoverage.ubicacion.value === 'L') ? (filterCoverage.distrito ? filterCoverage.distrito.value.toString() : '') : '',//,
        "idePoliza": (select.idePoliza) ? `${select.idePoliza}` : '',
        "codAfiliado": (select.codAfiliado) ? select.codAfiliado.toString() : '',
        "codCliente": (select.codCliente) ? select.codCliente.toString() : ''
      }
    }

    if (RAMO_1.includes(select.ramo)) {
      tipoCobertura = (filterCoverage.tipoCobertura) ? filterCoverage.tipoCobertura.value : ''

      requestBody.filter = {
        "core": select.core,
        "codProducto": (select.core === 'AX' || select.core === 'SAPP' || select.core === 'VUL')
          ? select.codProducto.toString()
          : '',
        "codProdAcselx": '',
        "numPoliza": (select.numPoliza) ? select.numPoliza.toString() : '',
        "numCertificado": (select.core === 'AX' || select.core === 'SAPP' || select.core === 'VUL')
          ? select.numCertificado.toString()
          : '',
        "tipoCobertura": tipoCobertura,//
        "modalidadAtencion": "",//
        "ubicacion": "",//
        "proveedor": "",
        "tipoProveedor": "",
        "codDepartamento": "",
        "codDistrito": "",
        "idePoliza": (select.idePoliza) ? `${select.idePoliza}` : '',
        "codAfiliado": (select.codAfiliado) ? select.codAfiliado.toString().toString() : '',
        "codCliente": (select.codCliente) ? select.codCliente.toString() : ''
      }
    }
    props.getCoverages(requestBody);
  }

  const changePage = (numPage) => {
    setPage(numPage)
    setUserRender(useRender + 1)
  }

  const changeRowPage = (sizePage) => {
    setRowPage(sizePage)
    setPage(1)
    setUserRender(useRender + 1)
  }

  const getData = () => {
    const {
      dataList
    } = coberturas;

    return (dataList) ? dataList : []
  }

  const getCoverageType = () => {
    const {
      coverageTypes,
    } = coberturas

    return coverageTypes
      ? coverageTypes.map((item) => ({ 'value': item.codBeneficio, 'label': item.descripcion })).sort((a, b) => {
        if (a.label > b.label) {
          return 1
        }
        if (b.label > a.label) {
          return -1
        }
        return 0
      })
      : []
  }

  const getProviderType = () => {
    const {
      providerTypes
    } = coberturas
    return providerTypes
      ? providerTypes.map((item) => ({ 'value': item.tipoProveedor, 'label': item.descripcion })).sort((a, b) => {
        if (a.label > b.label) {
          return 1
        }
        if (b.label > a.label) {
          return -1
        }
        return 0
      })
      : []
  }

  const handleChangeInput = (evnt) => {
    const {
      name,
      value
    } = evnt.target
    setFilterCoverage({ ...filterCoverage, [name]: value })
  }

  const changeSelect = (selected, name) => {
    setFilterCoverage({ ...filterCoverage, [name]: (selected) ? selected : '' })
  }

  const changeSelectTypeRamo1 = (selected, name) => {
    setFilterCoverage({ ...filterCoverage, [name]: (selected) ? selected : '' })
    setPage(1)
    setUserRender(useRender + 1)
  }

  const getColumnsCoverage = () => {
    if (RAMO_1.includes(select.ramo)) {
      return COLUMNS_VRRGGV;
    }

    if (RAMO_2.includes(select.ramo)) {
      return COLUMNS_SALUD;
    }

    return COLUMNS_VRRGGV;
  }

  return (
    <div>
      {
        (RAMO_1.includes(select.ramo)) ?
          <Layout.Row type='flex' justify='center'>
            <Layout.Col span='2'
              style={{
                alignItems: "center",
                justifyContent: "center",
                display: "flex"
              }}
            >
              <label htmlFor="select1">Tipo de cobertura:</label>
            </Layout.Col>
            <Layout.Col span='6'>
              <Select
                placeholder="Seleccionar"
                name="form-field-name"
                options={optionsAX}
                value={filterCoverage.tipoCobertura || ''}
                onChange={(val) => { changeSelectTypeRamo1(val, 'tipoCobertura') }}
                isClearable
              />
            </Layout.Col>
          </Layout.Row>
          : <div></div>
      }
      {
        (RAMO_2.includes(select.ramo)) ?
          <div>
            <div className="row" style={{ zIndex: 1010 }}>
              <div className="col-lg">
                <div className="col-lg-12">
                  <label htmlFor="select1">Tipo de cobertura (*)</label>
                </div>
                <div className="col-lg-12">
                  {
                    loaderVisibleTypeCoverage ?
                      <Loading>
                        <Select placeholder="Seleccionar"
                          name="form-field-name"
                          value={filterCoverage.tipoCobertura || ''}
                          options={getCoverageType()}
                          onChange={(val) => { changeSelect(val, 'tipoCobertura') }}
                          isClearable
                        />
                      </Loading>
                      : <Select placeholder="Seleccionar"
                        name="form-field-name"
                        value={filterCoverage.tipoCobertura || ''}
                        options={getCoverageType()}
                        onChange={(val) => { changeSelect(val, 'tipoCobertura') }}
                        isClearable
                      />
                  }
                </div>
              </div>
              <div className="col-lg">
                <div className="col-lg-12">
                  <label htmlFor="select4">Tipo de proveedor</label>
                </div>
                <div className="col-lg-12">
                  {
                    loaderVisibleTypeCoverage ?
                      <Loading>
                        <Select placeholder="Seleccionar"
                          name="form-field-name"
                          options={getProviderType()}
                          value={filterCoverage.tipoProveedor || ''}
                          onChange={(val) => { changeSelect(val, 'tipoProveedor') }}
                          isClearable
                        />
                      </Loading>
                      : <Select placeholder="Seleccionar"
                        name="form-field-name"
                        options={getProviderType()}
                        value={filterCoverage.tipoProveedor || ''}
                        onChange={(val) => { changeSelect(val, 'tipoProveedor') }}
                        isClearable
                      />
                  }
                </div>
              </div>

              <div className="col-lg">
                <div className="col-lg-12">
                  <label htmlFor="select5">Proveedor</label>
                </div>
                <div className="col-lg-12">
                  <input
                    type="text"
                    className="form-control"
                    id="formGroupExampleInput"
                    name="proveedor"
                    value={filterCoverage.proveedor || ''}
                    onChange={handleChangeInput}
                  />
                </div>
              </div>
              <div className="col-lg">
                <div className="col-lg-12">
                  <label htmlFor="select5" style={{ color: 'white' }}>Consultar</label>
                </div>
                <div className="col-lg-12">
                  <Button type='danger'
                    className="col-lg-12"
                    style={{
                      height: "calc(1.5em + .75rem + 4px)"
                    }}
                    onClick={() => {
                      getCoveragesService()
                    }}
                  >Consultar</Button>
                </div>
                <div className="col-lg-12">
                  <span>(*): Obligatorio</span>
                </div>
              </div>
            </div>
            <div className="row" style={{ zIndex: 1000, paddingTop: "10px" }}>
              <div className="col-lg">
                <div className="col-lg-12">
                  <label htmlFor="select2">Modalidad (*)</label>
                </div>
                <div className="col-lg-12">
                  <Select placeholder="Seleccionar"
                    name="form-field-name"
                    options={optModalidad}
                    value={filterCoverage.modalidadAtencion || ''}
                    onChange={(val) => { changeSelect(val, 'modalidadAtencion') }}
                    isClearable
                  />
                </div>
              </div>
              <div className="col-lg">
                <div className="col-lg-12">
                  <label htmlFor="select3">Ubicación (*)</label>
                </div>
                <div className="col-lg-12">
                  <Select placeholder="Seleccionar"
                    name="form-field-name"
                    options={optUbicacion}
                    value={filterCoverage.ubicacion || ''}
                    onChange={(val) => { changeSelect(val, 'ubicacion') }}
                    isClearable
                  />
                </div>
              </div>
              {
                filterCoverage.ubicacion &&
                (filterCoverage.ubicacion.value === 'L') &&
                <div className="col-lg">
                  <div className="col-lg-12">
                    <label htmlFor="select4">Distrito</label>
                  </div>
                  <div className="col-lg-12">
                    <Select placeholder="Seleccionar"
                      name="form-field-name"
                      options={optDistrito}
                      value={filterCoverage.distrito || ''}
                      onChange={(val) => { changeSelect(val, 'distrito') }}
                      isClearable
                    />
                  </div>
                </div>
              }
              {
                filterCoverage.ubicacion &&
                (filterCoverage.ubicacion.value === 'P') &&
                <div className="col-lg">
                  <div className="col-lg-12">
                    <label htmlFor="select4">Departamento</label>
                  </div>
                  <div className="col-lg-12">
                    <Select
                      name="form-field-name"
                      options={optDepartamento}
                      value={filterCoverage.departamento || ''}
                      onChange={(val) => { changeSelect(val, 'departamento') }}
                      isClearable
                    />
                  </div>
                </div>
              }
            </div>
          </div>
          : <div></div>
      }

      <br />
      <ElementTable
        loader={loaderVisible}
        data={getData()}
        columns={getColumnsCoverage()}

        page={page}
        rowPage={rowPage}
        onChangePage={changePage}
        onChangeRowPage={changeRowPage}
        pagination={coberturas.pagination}
      />
      <br />
      {
        // (dataSelected) &&
        // (
        //   <DivInput>
        //     <div className="row">
        //       <div className="col-lg-12">
        //         <MDBBreadcrumb
        //           onClick={() => {
        //             setCollapseDetalle(!collapseDetalle)
        //           }}
        //         >
        //           <MDBBreadcrumbItem>
        //             {
        //               (collapseDetalle) ? <MDBIcon icon="angle-down" /> : <MDBIcon icon="angle-right" />
        //             }
        //             {"  Detalle de Cobertura"}
        //           </MDBBreadcrumbItem>
        //         </MDBBreadcrumb>
        //         <br />
        //       </div>
        //     </div>
        //     <div className="container-fluid">
        //       <MDBCollapse id="collapseDetalle" isOpen={collapseDetalle}>
        //         <div className="row">
        //           <div className="col-lg">
        //             <MDBInput value={dataSelected.modalidadAtencion || ''} disabled label='Modalidad de Atención' size="sm" />
        //           </div>
        //           <div className="col-lg">
        //             <MDBInput value={dataSelected.codCobertura || ''} disabled label='Cod. de Cobertura' size="sm" />
        //           </div>
        //           <div className="col-lg">
        //             <MDBInput value={dataSelected.desCobertura || ''} disabled label='Descrip. de Cobertura' size="sm" />
        //           </div>
        //         </div>
        //       </MDBCollapse>
        //     </div>
        //   </DivInput>
        // )
      }
    </div>
  );

}

export default withToastManager(Coverages);