// @ts-check
import React, {useEffect} from 'react'
import { useSelector} from 'react-redux'
import {
  MDBRow
} from 'mdbreact'
import ElementTable from 'components/table/elementTable'
import { policyLoaderKeys } from 'state/ducks/policy';
import {changeFormatDate} from 'state/utils/functionUtils'

const COLUMNS = [
  {
    label: "Parentesco",
    prop: "parentesto",
    minWidth: 160,
  },
  {
    label: "Nombre Completo",
    prop: "nomCompletoAfiliado",
    minWidth: 180,
  },
  {
    label: "Edad",
    prop: "edad",
    minWidth: 160,
  },
  {
    label: "Sexo",
    prop: "sexo",
    minWidth: 160,
  },
  {
    label: "Beneficiario",
    prop: "beneficiario",
    minWidth: 160,
  },
  {
    label: "Beneficio",
    prop: "beneficio",
    minWidth: 160,
  },
  {
    label: "Fecha Ingreso",
    prop: "fechaIngreso",
    minWidth: 160,
    render: (row, column, _index) => (
      <span>
        {changeFormatDate(row[column.prop], '-', '/')}
      </span>
    ),
  },
  {
    label: "Fecha Nacimiento",
    prop: "fechaNacimiento",
    minWidth: 170,
    render: (row, column, _index) => (
      <span>
        {changeFormatDate(row[column.prop], '-', '/')}
      </span>
    ),
  },
];


const Beneficiaries = (props) => {
    const loaderVisible = useSelector(state => state.shared.loader.includes(policyLoaderKeys.BENEFICIARIES))
    let beneficiarios = (props.data.dataList && props.data.dataList.length > 0) ? props.data.dataList : []

    useEffect(() => {
      props.getListPolicyBenefiaries();
    },[]);

    return (
      <div>
        <ElementTable
          data={beneficiarios}
          columns={COLUMNS}
          loader={loaderVisible}
          allRow
        />
      </div>
    );
}

export default Beneficiaries;