// @ts-check
import React, { useState, useEffect,useMemo } from 'react'
import { useDispatch } from 'react-redux'
import { bindActionCreators } from 'redux'
import { MDBRow, MDBIcon } from 'mdbreact'
import { connect, useSelector } from 'react-redux'
import ElementTable from 'components/table/elementTable'
import Loader from 'components/loader'
import { policyLoaderKeys } from 'state/ducks/policy';
import { changeFormatDate, removeSeconds } from 'state/utils/functionUtils'
import { Layout } from 'element-react'
import { Button } from 'element-react';
import Select from 'react-select'
import { policyOperations } from 'state/ducks/policy'
import PubSub from 'state/utils/PubSub'


const getColumns = [
  {
    label: "Número Certificado",
    prop: "numCertificado",
    minWidth: 150,
  },
  {
    label: "Plan",
    prop: "plan",
    minWidth: 150,
  },
  // {
  //   label: "Tipo Plan",
  //   prop: "nombreCompletoAfiliado",
  //   minWidth: 160,
  // },
  {
    label: "Fecha Inicio",
    prop: "fechaInicio",
    minWidth: 140,
    render: (row, column, idx) => (
      <span>
        {
          row[column.prop] &&
          removeSeconds(changeFormatDate(row[column.prop], '-', '/').replace('T', ' ').split('.')[0])
        }
      </span>
    ),
  },
  {
    label: "Fecha Fin",
    prop: "fechaFin",
    minWidth: 140,
    render: (row, column, idx) => (
      <span>
        {
          row[column.prop] &&
          removeSeconds(changeFormatDate(row[column.prop], '-', '/').replace('T', ' ').split('.')[0])
        }
      </span>
    ),
  },
  {
    label: "Estado",
    prop: "estado",
    minWidth: 160,
  },
  {
    label: "Placa",
    prop: "placa",
    minWidth: 150,
  },
  {
    label: "Motor",
    prop: "motor",
    minWidth: 150,
  },
  {
    label: "Asegurado",
    prop: "asegurado",
    minWidth: 140,
  },
];



const OPTIONS = [
  { value: '1', label: 'RUC' },
  { value: '2', label: 'DNI' }
]
  


const CertificatePJ = props => {
  const dispatch = useDispatch()


  useEffect(() => {
    return () => {
    }
  }, [])
  
  const policyOperationAction = useMemo(() => {
    return bindActionCreators(policyOperations, dispatch)
  }, [dispatch])

  const [page, setPage] = useState(1)
  const [rowPage, setRowPage] = useState(10)
  const [callService, setCallService] = useState(true)

  const [body, setBody] = useState({
      "placa": '',
      "motor": '',
      "tipoDocumento":'',
      "numDocumento":'',
      "apePaterno":'',
      "apeMaterno":'',
      "nombre":'',
      "numCertificado":'',
      "selected":false
    })
  
  const { 
      dataList, 
      pagination 
    } = useSelector(state => state.policy.certificado)
  const dataCertificate = (dataList) ? dataList : []
  const select = useSelector(state => state.policy.select)
  const loaderVisible = useSelector(state => state.shared.loader.includes(policyLoaderKeys.POLICY_CERTIFICATE_LIST_DETALLE))
  
  const validateForm = () =>{
    if(body.tipoDocumento !== ''  && body.numDocumento.length === 0){
      PubSub.publish('WARNING_SEARCH_THIRD', 'Debe llenar el nùmero de documento.');
          return false;
    }
    if(body.tipoDocumento === '1' && body.numDocumento.length !== 11)
    {
      PubSub.publish('WARNING_SEARCH_THIRD', 'Debe llenar el nùmero de documento válido');
      return false;
    }
    
    if(body.tipoDocumento === '2' && body.numDocumento.length !== 8)
    {
      PubSub.publish('WARNING_SEARCH_THIRD', 'Debe llenar el nùmero de documento válido');
      return false;
    }

    return true;
  }

  
  useEffect(() => {
    if(validateForm()){
      let requestBody = {
        "filter":{
          "core": (select) ? select.core.toString() : '',
          "codProducto": (select) ? select.codProducto.toString() : '',
          "codProdAcselx": (select) ? select.codProdAcselx.toString() : '',
          "numPoliza": (select) ? select.numPoliza.toString() : '',
          "idePoliza": (select) ? select.idePoliza.toString() : '',
          "placa": body.placa ? body.placa.toUpperCase() : '',
          "motor": body.motor ? body.motor.toUpperCase() : '',
          "tipoDocumento": (body.tipoDocumento && body.tipoDocumento.value) ? body.tipoDocumento.value : '',
          "numDocumento": body.numDocumento || '',
          "apePaterno": body.apeMaterno ? body.apeMaterno.toUpperCase() : '',
          "apeMaterno": body.apePaterno ? body.apePaterno.toUpperCase() : '',
          "nombre": body.nombre ? body.nombre.toUpperCase() : '',
          "numCertificado": body.numCertificado ? body.numCertificado : ''
        },
        "pagination": {
          "filasPorPag": rowPage,
          "pagActual": page
        }
      }
      
      policyOperationAction.policyCertificateListDetalle(requestBody)
    }
    
  }, [callService])
  
  const changeRowPage = _rowPage => {
    setRowPage(_rowPage)
    setPage(1)
    setCallService(!callService)
  }
  const changePage = _page => {
    setPage(_page)
    setCallService(!callService)
  }


  
  const handleChange = evnt => {
    
    let cutNumDoc = false
    let long = 15;
    let { value, name } = evnt.target

    if (name === "numDocumento") {
      if (body.tipoDocumento === '1') {
        long = 11
        cutNumDoc = value.length > long
        
      } 
      else if (body.tipoDocumento === '2') {
        long = 8
        cutNumDoc = value.length > long
      }
      else if (body.tipoDocumento === '') {
        long = 0
        cutNumDoc = value.length > long
      }
      
      if (cutNumDoc) {
        value = value.slice(0, long)
      }
    }
    
    if (name === "tipoDocumento") body.numDocumento = ''
    
    setBody({
      ...body,
      [name]: value
    })
  }
  const LimpiarFormulario = () =>{
    setBody({
      ...body,
      "placa": '',
      "motor": '',
      "tipoDocumento":'',
      "numDocumento":'',
      "apePaterno":'',
      "apeMaterno":'',
      "nombre":'',
      "numCertificado":''
    })
  }

  const selectRowTable = item => {
    
    props.selectCertificate(item)
  }

  return (

    <div>

    <Layout.Row gutter="10" style={{ marginBottom: '15px' }}>
        <Layout.Col span="4" offset="1">
          <label className="col-form-label">Placa:</label>
        </Layout.Col>
        <Layout.Col span="7">
          <input
            className="form-control"
            type="text"
            name="placa"
            onChange={handleChange}
            value={body.placa}
          />
        </Layout.Col>
        <Layout.Col span="4" offset="1">
          <label className="col-form-label">Motor:</label>
        </Layout.Col>
        <Layout.Col span="7">
          <input
            className="form-control"
            type="text"
            name="motor"
            onChange={handleChange}
            value={body.motor}
          />
        </Layout.Col>
      </Layout.Row>

    <Layout.Row gutter="10" style={{ marginBottom: '15px' }}>
        <Layout.Col span="4" offset="1">
          <label className="col-form-label">Tipo documento:</label>
        </Layout.Col>
        <Layout.Col span="7">
          <Select 
            name="form-field-name"
            placeholder="Seleccionar"
            options={OPTIONS}
            onChange={(val) =>{
              handleChange({
                target: {
                  name: 'tipoDocumento',
                  value: val ? val.value:''

                }
              })
            }}
            isClearable
            />
        </Layout.Col>
        <Layout.Col span="4" offset="1">
          <label className="col-form-label">Número documento:</label>
        </Layout.Col>
        <Layout.Col span="7">
          <input
            className="form-control"
            type="number"
            name="numDocumento"
            onChange={handleChange}
            value={body.numDocumento}
          />
        </Layout.Col>
      </Layout.Row>
      
      <Layout.Row gutter="10" style={{ marginBottom: '15px' }}>
        <Layout.Col span="4" offset="1">
          <label className="col-form-label">Apellido Paterno:</label>
        </Layout.Col>
        <Layout.Col span="7">
          <input
            className="form-control"
            type="text"
            name="apePaterno"
            onChange={handleChange}
            value={body.apePaterno}
          />
        </Layout.Col>
        <Layout.Col span="4" offset="1">
          <label className="col-form-label">Apellido Materno:</label>
        </Layout.Col>
        <Layout.Col span="7">
          <input
            className="form-control"
            type="text"
            name="apeMaterno"
            onChange={handleChange}
            value={body.apeMaterno}
          />
        </Layout.Col>
      </Layout.Row>

      <Layout.Row gutter="10" style={{ marginBottom: '15px' }}>
        <Layout.Col span="4" offset="1">
          <label className="col-form-label">Nombres:</label>
        </Layout.Col>
        <Layout.Col span="7">
          <input
            className="form-control"
            type="text"
            name="nombre"
            onChange={handleChange}
            value={body.nombre}
          />
        </Layout.Col>
        <Layout.Col span="4" offset="1">
          <label className="col-form-label">Nro Certificado:</label>
        </Layout.Col>
        <Layout.Col span="7">
          <input
            className="form-control"
            type="number"
            name="numCertificado"
            onChange={handleChange}
            value={body.numCertificado}
          />
        </Layout.Col>
      </Layout.Row>


      <div className="row justify-content-md-center">
        <div className="col-2">
          <Button
            type="danger"
            onClick={() => {
              setCallService(!callService)
            }}
          >
            <MDBIcon icon="search" />
            {'  Buscar'}
          </Button>
        </div>
        <div className="col-2">
          <Button type="danger" 
            onClick={() => {
              LimpiarFormulario()
            }}>
            <MDBIcon icon="eraser" />
            {'  Limpiar'}
          </Button>
        </div>
      </div>
      <br />
    <div style={{"cursor":"pointer"}}>
      <ElementTable
        data={dataCertificate}
        columns={getColumns}
        page={page}
        rowPage={rowPage}
        pagination={pagination}
        loader={loaderVisible}
        onChangeRowPage={changeRowPage}
        onChangePage={changePage}
        onChangeSelect={selected => {
          selectRowTable(selected)
        }}
      />
    </div>
    </div>
  )
}

export default CertificatePJ