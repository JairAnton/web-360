// @ts-check
import React from 'react'
import { connect } from 'react-redux'
import { MDBRow, MDBInput } from 'mdbreact'
import _ from 'lodash'
import styled from 'styled-components'
import Loader from 'components/loader'
import { policyLoaderKeys } from 'state/ducks/policy'
import ElementTable from 'components/table/elementTable'
import { changeFormatDate } from 'state/utils/functionUtils'

import ModalAccountStatus from './accountStatusGeneratePDF/modalAccountStatusPDF'

const DivInput = styled.div`
    .breadcrumb {
        cursor: pointer;
    }
    .md-form {
        margin-top: 0.5rem;
        margin-bottom: 0.5rem;
    }
`;

const COLUMNS = [
  {
    label: "Vencimiento",
    prop: "fechaVenciCuota",
    minWidth: 135,
    render: (row, column, _index) => (
      <span>
        {changeFormatDate(row[column.prop], '-', '/')}
      </span>
    ),
  },
  {
    label: "N° Documento",
    prop: "numDocumento",
    minWidth: 145,
  },
  {
    label: "N° Operación",
    prop: "numOperacion",
    minWidth: 140,
  },
  {
    label: "N° Cuota",
    prop: "numCuota",
    minWidth: 105,
  },
  {
    label: "Moneda",
    prop: "monedaCuota",
    minWidth: 100,
  },
  {
    label: "Cuota",
    prop: "valorCuota",
    minWidth: 90,
  },
  {
    label: "Estado",
    prop: "estadoCuota",
    minWidth: 100,
  },
  {
    label: "Fecha Pago",
    prop: "fechaPago",
    minWidth: 130,
    render: (row, column, _index) => (
      <span>
        {changeFormatDate(row[column.prop], '-', '/')}
      </span>
    ),
  },
  {
    label: "Tipo Doc. Pago",
    prop: "tipoDocPago",
    minWidth: 150,
  },
  {
    label: "Tipo Doc.",
    prop: "tipoDoc",
    minWidth: 130,
  },
  {
    label: "Forma Pago",
    prop: "formaPago",
    minWidth: 150,
  }
]

const paramsAccountStatus = [
  [
    { field: "responsablePago", label: "Responsable de Pago" },
    { field: "formaPago", label: "Forma Pago" },
    { field: "tipoPagoFacil", label: "Tipo Pago Fácil" },
  ],
  [
    { field: "bancoOperador", label: "Banco/Operador" },
    { field: "viaCobro", label: "Vía Cobro" },
    { field: "estadoViaCobro", label: "Estado Via Cobro" },
  ],
  [
    { field: "numPagoFacil", label: "N° Pago Fácil" },
    //{ field: "numTarjetaCuenta", label: "N° Tarjeta / Cuenta" },
    { field: "titularCuenta", label: "Titular Cuenta" },
    { field: "monedaCuenta", label: "Moneda Cuenta" },
  ],
  [
    { field: "frecuenciaPago", label: "Frecuencia Pago" },
    { field: "numCuotasPagadas", label: "N° Cuotas Pagadas" },
    { field: "numCuotasPendientes", label: "N° Cuotas Pendientes" },
  ],
  [
    { field: "deudaTotal", label: "Deuda Total" },
    { field: "fechaVenciPago", label: "Fecha Vencimiento más antigua" },
    { field: "numDiasDeuda", label: "Número días deuda" },
  ],

]

const _notNull = value => (_.isNil(value) || !`${value}`.trim()) ? '-' : value

class AccountStatus extends React.Component {
  constructor(props) {
    super(props)

    this.pdfAccount = React.createRef()
    this.state = {
      modalAccountStatus: false
    }
  }

  componentDidMount() {
    // const { getStateAccount } = this.props
    // getStateAccount()
    this.props.getStateAccount()
  }

  // componentDidUpdate() {
  //   const { stateAccount } = this.props
  // }

  changeModalAccoutState = () => {
    this.setState({
      modalAccountStatus: !this.state.modalAccountStatus
    })
  }

  getData = () => {
    let accountStatus = (this.props.stateAccount) ? this.props.stateAccount : {};
    let data = [];
    if (accountStatus.data && accountStatus.data.documentoPagos) {
      let documentoPagos = (accountStatus.data.documentoPagos) ? accountStatus.data.documentoPagos : [];
      data = documentoPagos.map(prop => ({ ...prop, retornoMotivo: "No Mapeado" }))
    }
    return {
      data,
      accountStatus: accountStatus.data,
    }
  }


  render() {
    const { accountStatus, data } = this.getData()
    const loaderVisible = this.props.loader.includes(policyLoaderKeys.ACCOUNT_STATUS)

    return (
      <div>
        <div id="myMm" style={{ height: "1mm" }} />
        {(() => {
          if (loaderVisible) {
            return (
              <MDBRow center>
                <Loader />
              </MDBRow>
            );
          } else if (accountStatus) {
            return (
              <div>
                <div className="container-fluid">
                  <DivInput>
                    {
                      paramsAccountStatus.map((prop, index1) => {
                        return (
                          <div className="div-group row" key={index1}>
                            {
                              prop.map((row, index2) => {
                                let valueParams = accountStatus[row.field]

                                if (row.field === 'fechaVenciPago') {
                                  valueParams = changeFormatDate(valueParams, '-', '/')
                                }
                                return (<div className="col-lg-4" key={index2}>
                                  <MDBInput value={_notNull(valueParams)} disabled label={row.label} size="sm" />
                                </div>)
                              })
                            }
                          </div>
                        )
                      })
                    }

                    <div className="div-group row">
                      <div className="offset-lg-8 col-lg-4">
                        <button
                          className="btn btn-danger btn-icon-split"
                          onClick={this.changeModalAccoutState}
                        >
                          <span className="text">
                            {'Descargar documento'}
                          </span>
                        </button>
                      </div>
                    </div>
                  </DivInput>

                  <p>Información de Pagos</p>
                  <ElementTable
                    loader={false}
                    data={data}
                    columns={COLUMNS}
                    allRow
                  />

                </div>
                <div>
                  <ModalAccountStatus
                    openModal={this.state.modalAccountStatus}
                    changeModalAccoutState={this.changeModalAccoutState}
                    data={data}
                    accountStatus={accountStatus}
                  />
                </div>
              </div>
            );

          } else {
            return (
              <MDBRow center>
                <p><b>No se encontró información de Estado de Cuenta asociada a ésta póliza</b></p>
              </MDBRow>
            )
          }
        })()}
      </div>
    )
  }
}

const mapStatetoProps = (state) => {
  return {
    loader: state.shared.loader,
    person: state.person,
    policy: state.policy
  }
}

export default connect(mapStatetoProps, null)(AccountStatus)