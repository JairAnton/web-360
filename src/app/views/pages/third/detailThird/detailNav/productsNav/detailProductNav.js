import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import styled from 'styled-components'
import { withToastManager } from 'react-toast-notifications'
import _ from 'lodash'

import CertificatePJ from './detailProductNav/certificatePJ'
import InsuredPJ from './detailProductNav/insuredPJ'

import AccountStatus from './detailProductNav/accountStatus'
import Coverage from './detailProductNav/coverage'
import GeneralData from './detailProductNav/generalData'
import Beneficiaries from './detailProductNav/beneficiaries'
import Insured from './detailProductNav/insured'
import ParticularData from './detailProductNav/particularData'
import Sinister from './detailProductNav/sinister'
import Operations from './detailProductNav/operations'
import Clause from './detailProductNav/clause'
import Deductible from './detailProductNav/deductible';
import RiskAddresses from './detailProductNav/riskAddresses'
import { constants as CONSTANTS } from 'state/utils/constants'

import PubSub from 'state/utils/PubSub'
import { policyOperations } from 'state/ducks/policy'


const DivHeaderDetailProduct = styled.div`
    .nav-pills .navHeader .white-text{
        color: red
    }

    .nav-pills .nav-item .nav-link.active{
        background-color: red
        color: white
    }
     
    .nav-pills .nav-link{
        color: black
        padding: 0.5rem 0.5rem
    }
`
const PN_TABS = [
  {
    label: 'Datos Particulares',
    key: 'PN_DatosParticulares',
    access: ['SALUD', 'SCTR', 'VEHICULOS Y SOAT', 'VIDA', 'RENTAS VITALICIAS', 'RIESGOS GENERALES'],
  },
  {
    label: 'Coberturas',
    key: 'PN_Coberturas',
    access: ['SALUD', 'SCTR', 'VEHICULOS Y SOAT', 'VIDA', 'RENTAS VITALICIAS', 'RIESGOS GENERALES'],
  },
  {
    label: 'Asegurados',
    key: 'PN_Asegurados',
    access: ['SALUD', 'SCTR', 'VIDA', 'RENTAS VITALICIAS'],
  },
  {
    label: 'Beneficiarios',
    key: 'PN_Beneficiarios',
    access: ['VIDA', 'RENTAS VITALICIAS'],
  },
  {
    label: 'Siniestro',
    key: 'PN_Siniestro',
    access: ['SALUD', 'SCTR', 'VEHICULOS Y SOAT', 'VIDA', 'RENTAS VITALICIAS', 'RIESGOS GENERALES'],
  },
  {
    label: 'Estado de Cuenta',
    key: 'PN_EstadoCuenta',
    access: ['SALUD', 'SCTR', 'VEHICULOS Y SOAT', 'VIDA', 'RENTAS VITALICIAS', 'RIESGOS GENERALES'],
  },
  {
    label: 'Clausulas',
    key: 'PN_Clausulas',
    access: ['VEHICULOS Y SOAT', 'RIESGOS GENERALES'],
  },
  {
    label: 'Deducibles',
    key: 'PN_Deducibles',
    access: ['VEHICULOS Y SOAT', 'RIESGOS GENERALES'],
  },
  {
    label: 'Operaciones',
    key: 'PN_Operaciones',
    access: ['SALUD', 'SCTR', 'VEHICULOS Y SOAT', 'VIDA', 'RENTAS VITALICIAS', 'RIESGOS GENERALES']
  },
  {
    label: 'Direcciones de Riesgo',
    key: 'PN_DireccionesDeRiesgo',
    access: ['RIESGOS GENERALES'],
  },

]

const PJ_TABS = [
  {
    label: 'Certificados',
    key: 'PJ_Certificado',
    access: ['VEHICULOS Y SOAT', 'RIESGOS GENERALES']
  },
  {
    label: 'Asegurados',
    key: 'PJ_Asegurados',
    access: ['SALUD', 'VIDA', 'SCTR']
  },
]

const PJ_CERTIFICATE_SELECTED_TABS = [
  {
    label: 'Datos Particulares',
    key: 'PJ_DatosParticulares',
    access: ['SALUD', 'SCTR', 'VEHICULOS Y SOAT', 'VIDA', 'RENTAS VITALICIAS', 'RIESGOS GENERALES'],
  },
  {
    label: 'Coberturas',
    key: 'PJ_Coberturas',
    access: ['SALUD', 'SCTR', 'VEHICULOS Y SOAT', 'VIDA', 'RENTAS VITALICIAS', 'RIESGOS GENERALES'],
  },
  {
    label: 'Beneficiarios',
    key: 'PJ_Beneficiarios',
    access: ['VIDA', 'RENTAS VITALICIAS'],
  },
  {
    label: 'Siniestro',
    key: 'PJ_Siniestro',
    access: ['SALUD', 'SCTR', 'VEHICULOS Y SOAT', 'VIDA', 'RENTAS VITALICIAS', 'RIESGOS GENERALES'],
  },
  {
    label: 'Estado de Cuenta',
    key: 'PJ_EstadoCuenta',
    access: ['SALUD', 'SCTR', 'VEHICULOS Y SOAT', 'VIDA', 'RENTAS VITALICIAS', 'RIESGOS GENERALES'],
  },
  {
    label: 'Clausulas',
    key: 'PJ_Clausulas',
    access: ['VEHICULOS Y SOAT', 'RIESGOS GENERALES'],
  },
  {
    label: 'Deducibles',
    key: 'PJ_Deducibles',
    access: ['VEHICULOS Y SOAT', 'RIESGOS GENERALES'],
  },
  {
    label: 'Operaciones',
    key: 'PJ_Operaciones',
    access: ['SALUD', 'SCTR', 'VEHICULOS Y SOAT', 'VIDA', 'RENTAS VITALICIAS', 'RIESGOS GENERALES']
  },
  {
    label: 'Direcciones de Riesgo',
    key: 'PJ_DireccionesDeRiesgo',
    access: ['RIESGOS GENERALES'],
  },
]


class DetailProductNav extends Component {

  state = {
    nameTabKeyPJ: null,
  };

  componentDidMount() {
    
    const { toastManager } = this.props
    PubSub.subscribe('ERR_PARTICULAR_DATA_LIST', (message) => {
      console.log('ERR_PARTICULAR_DATA_LIST')
      toastManager.add(message, {
        autoDismissTimeout: 6000,
        appearance: 'error',
        autoDismiss: true,
        pauseOnHover: true,
      })
    })

    PubSub.subscribe('ERR_POLICY_CERTIFICATE_INSURED_LIST', (message) => {
      console.log('ERR_COVERAGES_LIST')
      toastManager.add(message, {
        autoDismissTimeout: 6000,
        appearance: 'error',
        autoDismiss: true,
        pauseOnHover: true,
      })
    })

    PubSub.subscribe('ERR_POLICY_SINISTER_LIST', (message) => {
      console.log('ERR_POLICY_SINISTER_LIST')
      toastManager.add(message, {
        autoDismissTimeout: 6000,
        appearance: 'error',
        autoDismiss: true,
        pauseOnHover: true,
      })
    })

    PubSub.subscribe('ERR_RISK_ADDRESSES', (message) => {
      console.log('ERR_RISK_ADDRESSES')
      toastManager.add(message, {
        autoDismissTimeout: 6000,
        appearance: 'error',
        autoDismiss: true,
        pauseOnHover: true,
      })
    })
  }

  getDatosParticulares = () => {
    const { policyOperations, policy, person } = this.props

    const core = (policy.select && !_.isNil(policy.select.core))
      ? policy.select.core.toString()
      : ''
    const numPoliza = (policy.select && !_.isNil(policy.select.numPoliza))
      ? policy.select.numPoliza.toString()
      : ''
    const codProducto = (policy.select && !_.isNil(policy.select.codProducto))
      ? policy.select.codProducto.toString()
      : ''
    const codCliente = (policy.select && !_.isNil(policy.select.codCliente))
      ? policy.select.codCliente.toString()
      : ''
    const codProdAcselx = (policy.select && !_.isNil(policy.select.codProdAcselx))
      ? policy.select.codProdAcselx.toString()
      : ''
      /*
    const codAfiliado = (person.detail && person.detail.codAfiliado)
      ? person.detail.codAfiliado.toString()
      : ''*/
    const codAfiliado = (policy.select && policy.select.codAfiliado)
      ? policy.select.codAfiliado.toString()
      : ''  
    const codExterno = (person.detail && person.detail.codExterno)
      ? person.detail.codExterno.toString()
      : ''
    const numCertificado = (policy.select && !_.isNil(policy.select.numCertificado))
      ? policy.select.numCertificado.toString()
      : ''
    const idePoliza = (policy.select && !_.isNil(policy.select.idePoliza))
      ? policy.select.idePoliza.toString()
      : ''

    const requestBody = {
      core,
      codProducto,
      numPoliza,
      numCertificado,
      codAfiliado,
      codCliente,
      codProdAcselx,
      codExterno,
      idePoliza,
    }
    //cod ClientRect, asxcel x 
    policyOperations.ParticularDataList(requestBody)
    
    // auditoria
    let requestLogging = {
      "path": window.location.href,
      "action": "PARTICULAR_DATA_LIST"
    }
    policyOperations.auditLogging(requestLogging)
  }

  getCoverageTypeList = (requestBody) => {
    const {
      policyOperations
    } = this.props
    policyOperations.policyCoverageTypeList(requestBody)
    
    // auditoria
    let requestLogging = {
      "path": window.location.href,
      "action": "POLICY_COVERAGE_TYPE_LIST"
    }
    policyOperations.auditLogging(requestLogging)
  }

  getCoverages = (requestBody) => {
    const { policyOperations } = this.props
    policyOperations.coveragesList(requestBody)

    // auditoria
    let requestLogging = {
      "path": window.location.href,
      "action": "POLICY_COVERAGES_LIST"
    }
    policyOperations.auditLogging(requestLogging)
  }

  getPolicySinisterList = (page = 1, rowPage = 10) => {
    const { policyOperations, policy, person } = this.props

    const codCliente = (policy.select && policy.select.codCliente) ? policy.select.codCliente.toString() : ''
    const codProdAcselx = (policy.select && policy.select.codProdAcselx) ? policy.select.codProdAcselx.toString() : ''
    const codAfiliado = (policy.select && policy.select.codAfiliado) ? policy.select.codAfiliado.toString() : ''
    const numPoliza = (policy.select && policy.select.numPoliza) ? policy.select.numPoliza.toString() : ''
    const idePoliza = (policy.select && policy.select.idePoliza) ? policy.select.idePoliza.toString() : ''

    const requestBody = {
      "filter": {
        "core": `${policy.select.core}`,
        "casoBizagi": "",
        "codExterno": `${person.select.codExterno ? person.select.codExterno : ''}`,
        "codProdAcselx": `${codProdAcselx}`,
        "numPoliza": `${numPoliza}`,
        "numCertificado": policy.select.core === 'AE' ? `${policy.select.numCertificado}` : '',
        "codAfiliado": policy.select.core === 'AE' ? '' : codAfiliado,
        "codCliente": policy.select.core === 'RS' ? '' : codCliente,
        idePoliza,
      },
      "pagination": {
        "filasPorPag": rowPage,
        "pagActual": page
      }
    }

    policyOperations.policySinisterList(requestBody)

    // auditoria
    let requestLogging = {
      "path": window.location.href,
      "action": "POLICY_SINISTER_LIST"
    }
    policyOperations.auditLogging(requestLogging)
  }

  getRiskAddresses = () => {
    const {
      core, codProdAcselx, codProducto, numPoliza, numCertificado, idePoliza
    } = this.props.policy.select;

    const requestBody = {
      "core": `${core}`,
      "codProdAcselx": `${codProdAcselx}`,
      "codProducto": `${codProducto}`,
      "numPoliza": `${numPoliza}`,
      "numCertificado": `${numCertificado}`,
      "idePoliza": `${idePoliza}`,
    }
    this.props.policyOperations.riskAddresses(requestBody)
    
    // auditoria
    let requestLogging = {
      "path": window.location.href,
      "action": "POLICY_RISK_ADDRESSES"
    }
    this.props.policyOperations.auditLogging(requestLogging)
  }

  getPolicyOperationsList = () => {
    const { policyOperations, policy } = this.props
    const codProducto = (policy.select && policy.select.codProducto)
      ? policy.select.codProducto.toString()
      : ''
    const numPoliza = (policy.select && policy.select.numPoliza) ? policy.select.numPoliza.toString() : ''
    const idePoliza = (policy.select && policy.select.idePoliza) ? policy.select.idePoliza.toString() : ''
    const requestBody = {
      "core": `${policy.select.core}`,
      "codProducto": codProducto,
      "numPoliza": `${numPoliza}`,
      "numCertificado": `${policy.select.numCertificado}`,
      idePoliza,
    }
    policyOperations.policyOperationList(requestBody)
    
    // auditoria
    let requestLogging = {
      "path": window.location.href,
      "action": "POLICY_OPERATION_LIST"
    }
    policyOperations.auditLogging(requestLogging)
  }

  getStateAccount = () => {
    const { policyOperations, policy, person } = this.props
    const requestBody = {
      core: (policy.select && !_.isNil(policy.select.core)) ? policy.select.core.toString() : "",
      codProdAcselx: (policy.select && !_.isNil(policy.select.codProdAcselx)) ? policy.select.codProdAcselx.toString() : "",
      numPoliza: (policy.select && !_.isNil(policy.select.numPoliza)) ? policy.select.numPoliza.toString() : "",
      numCertificado: (policy.select && !_.isNil(policy.select.numCertificado)) ? policy.select.numCertificado.toString() : "",
      idePoliza: (policy.select && !_.isNil(policy.select.idePoliza)) ? policy.select.idePoliza.toString() : "",
      codTerceroTitular: `${
        (policy.select && !_.isNil(policy.select.codTerceroTitular))
          ? policy.select.codTerceroTitular.toString()
          : (
            (person.select && !_.isNil(person.select.codExterno))
              ? person.select.codExterno
              : (
                (person.select && !_.isNil(person.select.codAfiliado))
                  ? person.select.codAfiliado
                  : ''
              )
          )
        }`,
      codProducto: (policy.select && !_.isNil(policy.select.codProducto)) ? policy.select.codProducto.toString() : "",
    }
    policyOperations.policyStateAccount(requestBody)
    
    // auditoria
    let requestLogging = {
      "path": window.location.href,
      "action": "POLICY_STATE_ACCOUNT"
    }
    policyOperations.auditLogging(requestLogging)
  }

  selectCertificate = (selectedCertificate) => {
    this.props.policyOperations.selectCertificate(selectedCertificate)
    
    // auditoria
    let requestLogging = {
      "path": window.location.href,
      "action": "POLICY_SELECT_CERTIFICATE"
    }
    this.props.policyOperations.auditLogging(requestLogging)
  }

  selectInsured = (selectedInsured) => {
    this.props.policyOperations.selectInsured(selectedInsured)
    
    // auditoria
    let requestLogging = {
      "path": window.location.href,
      "action": "POLICY_SELECT_INSURED"
    }
    this.props.policyOperations.auditLogging(requestLogging)
  }

  render() {
    const { nameTabKeyPJ } = this.state
    const {
      select,
      detalle,
      certificadoAsegurado,
      certificadoBeneficiario,
      stateAccount,
      coberturas,
      sinister,
      operacion,
      riskAddresses,
      certificado,
      asegurado,
    } = this.props.policy

    const { nameTabKey, personType } = this.props
    return (
  
      <div>
        <div>
          <hr />
          {
            // cabecera de seleccionar
          }
          <DivHeaderDetailProduct>
            <ul className="nav nav-pills mb-1" id="pills-tab" role="tablist">
              <li className="nav-item">
                <a href="#detailProductNav" className={"nav-link navHeader " + ('GeneralData' === nameTabKey ? 'active' : "")}
                  onClick={() => this.props.changeTabKey('GeneralData')}
                >Datos de Póliza</a>
              </li>
              {
                (personType === CONSTANTS.PN) ?
                  PN_TABS.map((tab, index) => {
                    //tab.access.includes(select.core)
                    if (select && select.ramo && tab.access.includes(select.ramo)) {
                      return (
                        <li className="nav-item" key={`*${index}`}>
                          <a href="#detailProductNav" className={"nav-link navHeader " + (tab.key === nameTabKey ? 'active' : "")}
                            onClick={() => this.props.changeTabKey(tab.key)}
                          >{tab.label}</a>
                        </li>
                      )
                    }

                    return <li key={index}></li>
                  })
                  :
                  PJ_TABS.map((tab, index) => {
                    if (select && select.ramo && tab.access.includes(select.ramo)) {
                      return (
                        <li className="nav-item" key={`_${index}`}>
                          <a href="#detailProductNav" className={"nav-link navHeader " + (tab.key === nameTabKey ? 'active' : "")}
                            onClick={() => this.props.changeTabKey(tab.key)}
                          >{tab.label}</a>
                        </li>
                      )
                    }
                    return <li key={index}></li>
                  })
              }
            </ul>
          </DivHeaderDetailProduct>
          <hr />
        </div>
        {
            <div>
              {(() => {
                switch (nameTabKey) {
                  case 'GeneralData':
                    return (
                      <GeneralData
                        detalle={detalle}
                        select={select}
                        personType={personType}
                      />
                    )
                  case 'PN_DatosParticulares':
                    return (
                      <ParticularData
                        datosParticulares={this.props.policy.datosParticulares}
                        getDatosParticulares={this.getDatosParticulares}
                      />
                    )
                  case 'PN_EstadoCuenta':
                    return (
                      <AccountStatus
                        stateAccount={stateAccount}
                        getStateAccount={this.getStateAccount}
                      />
                    )
                  case 'PN_Beneficiarios':
                    return (
                      <Beneficiaries
                        data={certificadoBeneficiario}
                        getListPolicyBenefiaries={this.props.getListPolicyBenefiaries}
                      />
                    )
                  case 'PN_Asegurados':
                    return (
                      <Insured
                        data={certificadoAsegurado}
                        getListPolicyInsured={this.props.getListPolicyInsured}
                      />
                    )
                  case 'PN_Coberturas':
                    return (
                      <Coverage
                        coberturas={coberturas}
                        getCoverages={this.getCoverages}
                        getCoverageTypeList={this.getCoverageTypeList}
                      />
                    )
                  case 'PN_Siniestro':
                    return (
                      <Sinister
                        sinister={sinister}
                        getPolicySinisterList={this.getPolicySinisterList}
                      />
                    )
                  case 'PN_Operaciones':
                    return (
                      <Operations
                        operacion={operacion}
                        getPolicyOperationsList={this.getPolicyOperationsList}
                      />
                    )
                  case 'PJ_Asegurados':
                    return (
                      <InsuredPJ
                        selectInsured={this.selectInsured}
                      />
                    )
                  case 'PN_Clausulas':
                    return (
                      <Clause />
                    )
                  case 'PJ_Certificado':
                    return (
                      <CertificatePJ
                        getPolicyCertificateList={this.props.getPolicyCertificateList}
                        selectCertificate={this.selectCertificate}
                      />
                    )
                  case 'PN_Deducibles':
                    return (
                      <Deductible />
                    )
                  case 'PN_DireccionesDeRiesgo':
                    return (
                      <RiskAddresses
                        getRiskAddresses={this.getRiskAddresses}
                        riskAddresses={riskAddresses}
                      />
                    )
                  default:
                    return null
                }
              })()}
            </div>
        }

        {/* Persona juridica && CORE == AX && certificado seleccionado */}
        {
            ( 
              personType === CONSTANTS.PJ &&
              !_.isNil(select) &&
              (!_.isNil(certificado.select) || !_.isNil(asegurado.select))
            )
              ? <div>
                <div>
                  <hr />
                  {/*Cuando seleccionas item de la tabla*/}
                  <DivHeaderDetailProduct>
                    <ul className="nav nav-pills mb-1" id="pills-tab" role="tablist">
                      {PJ_CERTIFICATE_SELECTED_TABS.map((tab, index) => {
                        //tab.access.includes(select.core)
                        if (select && select.ramo && tab.access.includes(select.ramo)) {
                          return (
                            <li className="nav-item" key={`*${index}`}>
                              <a
                                // href="##"
                                className={"nav-link navHeader " + (tab.key === nameTabKeyPJ ? 'active' : "")}
                                onClick={() => this.setState({ nameTabKeyPJ: tab.key })}
                              >
                                {tab.label}
                              </a>
                            </li>
                          )
                        }
                        return <li key={index}></li>
                      })}
                    </ul>
                  </DivHeaderDetailProduct>
                  <hr />
                </div>
                <div>
                  {/*Dibuja los tabs*/}
                  {(() => {
                    switch (nameTabKeyPJ) {
                      // case 'GeneralData':
                      //   return (
                      //     <GeneralData
                      //       detalle={detalle}
                      //       select={select}
                      //       personType={personType}
                      //     />
                      //   )
                      case 'PJ_DatosParticulares':
                        return (
                          <ParticularData
                            datosParticulares={this.props.policy.datosParticulares}
                            getDatosParticulares={this.getDatosParticulares}
                          />
                        )
                      case 'PJ_EstadoCuenta':
                        return (
                          <AccountStatus
                            stateAccount={stateAccount}
                            getStateAccount={this.getStateAccount}
                          />
                        )
                      case 'PJ_Beneficiarios':
                        return (
                          <Beneficiaries
                            data={certificadoBeneficiario}
                            getListPolicyBenefiaries={this.props.getListPolicyBenefiaries}
                          />
                        )
                      // case 'PJ_Asegurados':
                      //   return (
                      //     <Insured
                      //       data={certificadoAsegurado}
                      //       getListPolicyInsured={this.props.getListPolicyInsured}
                      //     />
                      //   )
                      case 'PJ_Coberturas':
                        return (
                          <Coverage
                            coberturas={coberturas}
                            getCoverages={this.getCoverages}
                            getCoverageTypeList={this.getCoverageTypeList}
                          />
                        )
                      case 'PJ_Siniestro':
                        return (
                          <Sinister
                            sinister={sinister}
                            getPolicySinisterList={this.getPolicySinisterList}
                          />
                        )
                      case 'PJ_Operaciones':
                        return (
                          <Operations
                            operacion={operacion}
                            getPolicyOperationsList={this.getPolicyOperationsList}
                          />
                        )
                      // case 'PJ_Asegurados':
                      //   return (
                      //     <InsuredPJ />
                      //   )
                      case 'PJ_Clausulas':
                        return (
                          <Clause />
                        )
                      // case 'PJ_Certificado':
                      //   return (
                      //     <CertificatePJ
                      //       getPolicyCertificateList={this.props.getPolicyCertificateList}
                      //     />
                      //   )
                      case 'PJ_Deducibles':
                        return (
                          <Deductible />
                        )
                      case 'PJ_DireccionesDeRiesgo':
                        return (
                          <RiskAddresses
                            getRiskAddresses={this.getRiskAddresses}
                            riskAddresses={riskAddresses}
                          />
                        )
                      default:
                        this.setState({ nameTabKeyPJ: 'PJ_DatosParticulares' }) 
                    }
                  })()}
                </div>
  
              </div> : null
              
        }

      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  policy: state.policy,
  person: state.person
})

const mapDispatchToProps = (dispatch) => ({
  policyOperations: bindActionCreators(policyOperations, dispatch),
})

//export default DetailProductNav
export default connect(mapStateToProps, mapDispatchToProps)(withToastManager(DetailProductNav))