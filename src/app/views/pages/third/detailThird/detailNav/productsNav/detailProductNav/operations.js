import React, { useEffect } from 'react'
import { useSelector } from 'react-redux'
import ElementTable from 'components/table/elementTable'
import { policyLoaderKeys } from 'state/ducks/policy';

const columns = [
  {
    label: "Tipo Operación",
    prop: "tipoOperacion",
    minWidth: 120,
  },
  {
    label: "Codigo Plan",
    prop: "codPlan",
    minWidth: 90,
  },
  {
    label: "Nro Certif.",
    prop: "nroCert",
    minWidth: 70,
  },
  {
    label: "Nro Operación",
    prop: "nroOper",
    minWidth: 130,
  },
  {
    label: "Fecha\nMovimiento",
    prop: "fechaMovimiento",
    minWidth: 105,
  },
  {
    label: "Motivo",
    prop: "motivo",
    minWidth: 140,
  },
  {
    label: "Monto\nAnulación",
    prop: "montoAnulacion",
    minWidth: 140,
  },
  {
    label: "Monto\nOperación",
    prop: "montoOper",
    minWidth: 130,
  },
  {
    label: "Nro Trámite",
    prop: "nroTramite",
    minWidth: 130,
  },
  {
    label: "Estado",
    prop: "estado",
    minWidth: 130,
  },
  {
    label: "Usuario",
    prop: "usuario",
    minWidth: 140,
  },
  {
    label: "Anulado Por",
    prop: "anuladoPor",
    minWidth: 140,
  },
  {
    label: "Motivo Anulación",
    prop: "motivoAnulacion",
    minWidth: 160,
  },
  {
    label: "Fecha\nAnulación",
    prop: "fechaAnulacion",
    minWidth: 105,
  },
  {
    label: "Nro\nAnulación",
    prop: "nroOperAnulacion",
    minWidth: 150,
  },
  {
    label: "Nro Póliza",
    prop: "nroPoliza",
    minWidth: 130,
  },
]

const Operations = props => {

  useEffect(() => {
    props.getPolicyOperationsList()
  }, [])

  const loaderVisible = useSelector(state => state.shared.loader).includes(policyLoaderKeys.POLICY_OPERATION_LIST)
  const { dataList } = props.operacion

  return (
    <div>
      <ElementTable
        data={dataList}
        columns={columns}
        loader={loaderVisible}
      />
    </div>
  )
}

export default Operations