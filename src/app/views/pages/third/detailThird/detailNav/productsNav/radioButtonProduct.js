import React from 'react'
import { Radio, Layout } from 'element-react'


const RADIO_BUTTON = [
  {
    name: 'rbProductosVigentes',
    label: 'Vigentes',
    value: 'PV'
  },
  {
    name: 'rbProductosNoVigentes',
    label: 'No-vigentes',
    value: 'PN'
  },
  {
    name: 'rbProductosPorRenovar',
    label: 'Futuros',
    value: 'PR'
  },
  {
    name: 'rbProductosAnulada',
    label: 'Anulados',
    value: 'PA'
  },
  {
    name: 'rbProductosAll',
    label: 'Todos',
    value: ''
  },
]

const RadioButtonProduct = (props) => {

  return (
    <Layout.Row gutter="10" type="flex" justify="center">
      {RADIO_BUTTON.map((radioButton, index) => (
        <Layout.Col span="4" key={index}>
          <Radio
            value={radioButton.value}
            checked={props.rbProductos === radioButton.name}
            onChange={(val) => props.changeCheckProductNav(radioButton.name, val)}
          >
            {radioButton.label}
          </Radio>
        </Layout.Col>
      ))}
    </Layout.Row>
  )
}

export default RadioButtonProduct;