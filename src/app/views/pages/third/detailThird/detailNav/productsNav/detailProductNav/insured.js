// @ts-check
import React from 'react'
// import { MDBRow } from 'mdbreact'
import { connect } from 'react-redux'
import ElementTable from 'components/table/elementTable'
// import Loader from 'components/loader'
import { policyLoaderKeys } from 'state/ducks/policy'
import { changeFormatDate } from 'state/utils/functionUtils'


const COLUMNS = [
  {
    label: "Código Afiliado",
    prop: "codAfiliado",
    minWidth: 160,
  },
  {
    label: "Parentesco",
    prop: "desParentesco",
    minWidth: 150,
  },
  {
    label: "Nombre Completo",
    prop: "nombreCompletoAfiliado",
    minWidth: 180,
  },
  {
    label: "Edad",
    prop: "edad",
    minWidth: 110,
  },
  {
    label: "Fecha Nacimiento",
    prop: "fechaNacimiento",
    minWidth: 180,
    render: (row, column) => (
      <span>
        {changeFormatDate(row[column.prop], '-', '/')}
      </span>
    ),
  },
  {
    label: "Fecha Inclusión",
    prop: "fechaInclusion",
    minWidth: 160,
    render: (row, column) => (
      <span>
        {changeFormatDate(row[column.prop], '-', '/')}
      </span>
    ),
  },
  {
    label: "Fecha Exclusión",
    prop: "fechaExclusion",
    minWidth: 160,
    render: (row, column) => (
      <span>
        {changeFormatDate(row[column.prop], '-', '/')}
      </span>
    ),
  },

  {
    label: "Sexo",
    prop: "sexo",
    minWidth: 110,
  },

];


class Insured extends React.Component {

  componentDidMount() {
    const { getListPolicyInsured } = this.props
    getListPolicyInsured()
  }

  // componentDidUpdate() {
  //   const { data } = this.props
  // }

  render() {
    const asegurados = this.props.data.dataList ? this.props.data.dataList : [];
    const loaderVisible = this.props.loader.includes(policyLoaderKeys.INSURED_LIST)

    const data = (asegurados && asegurados.length > 0)
      ? asegurados.map(asegurado => ({ ...asegurado }))
      : []

    // if (asegurados && asegurados.length > 0) {
    //   asegurados.forEach((asegurado, index) => {
    //     getData.push({
    //       "codProducto": asegurado.codProducto,
    //       "numPoliza": asegurado.numPoliza,
    //       "numCertificado": asegurado.numCertificado,
    //       "desParentesco": asegurado.desParentesco,
    //       "codAfiliado": asegurado.codAfiliado,
    //       "nombreCompletoAfiliado": asegurado.nombreCompletoAfiliado,
    //       "apellidoPaterno": asegurado.apellidoPaterno,
    //       "apellidoMaterno": asegurado.apellidoMaterno,
    //       "nombres": asegurado.nombres,
    //       "sexo": asegurado.sexo,
    //       "edad": asegurado.edad,
    //       "fechaInclusion": asegurado.fechaInclusion,
    //       "fechaExclusion": asegurado.fechaExclusion,
    //       "fechaNacimiento": asegurado.fechaNacimiento,
    //       "estado": asegurado.estado,
    //       "codAfiliadoVin": asegurado.codAfiliadoVin,
    //       "desTitular": asegurado.desTitular,
    //     })
    //   })
    // }

    return (
      <div>
        <ElementTable
          data={data}
          columns={COLUMNS}
          allRow
          loader={loaderVisible}
        />
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    loader: state.shared.loader,
  }
}

export default connect(mapStateToProps, null)(Insured);