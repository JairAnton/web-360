import { savePDF } from '@progress/kendo-react-pdf';

class DocService {
  createPdf = (html) => {
    savePDF(html, {
      paperSize: 'A4',
      fileName: 'AccountStatus.pdf',
      scale: 0.75
    })
  }
}

export const Doc = new DocService()