import React from 'react'
import { connect } from 'react-redux'
import { MDBContainer, MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter, MDBIcon } from 'mdbreact';
import { Layout, Button } from 'element-react';
import logoRimac from './logoRimac.png'
import ElementTable from 'components/table/elementTable';
import styled from 'styled-components'
import _ from 'lodash'
import { changeFormatDate } from 'state/utils/functionUtils'
import { Doc } from './accountStatusPdfDocument'

const DivTablePDF = styled.div`
    .cell{
      padding-left: 5px !important;
      padding-right: 5px !important;
    }
`

const DivModal = styled.div`
    .widthModalAccountStatus{
      max-width: 821px !important;
    }

    .el-button--info.is-plain:focus, .el-button--info.is-plain:hover {
      background: #fff;
      border-color: rgb(210,83,101);
      color: rgb(210,83,101);
    }

    .el-button--info.is-plain:active {
      background: #fff !important;
      border-color: rgb(210,83,101) !important;
      color: rgb(210,83,101) !important;
      outline: 0 !important;
    }
`

const COLUMNS_PDF = [
  {
    label: "Fecha \n Vencimiento",
    prop: "fechaVenciCuota",
    width: 90,
    render: (row, column, _index) => (
      <span>
        {changeFormatDate(row[column.prop], '-', '/')}
      </span>
    ),
  },
  {
    label: "Tipo Doc.",
    prop: "tipoDoc",
    width: 98,
  },
  {
    label: "N° Documento",
    prop: "numDocumento",
    width: 105
  },
  {
    label: "N° Cuota",
    prop: "numCuota",
    width: 70,
  },
  {
    label: "Cuota",
    prop: "valorCuota",
    width: 55,
  },
  {
    label: "Moneda",
    prop: "monedaCuota",
    width: 65,
  },
  {
    label: "Estado",
    prop: "estadoCuota",
    width: 60,
  },
  {
    label: "Fecha Pago",
    prop: "fechaPago",
    width: 85,
    render: (row, column, _index) => (
      <span>
        {changeFormatDate(row[column.prop], '-', '/')}
      </span>
    ),
  },
  {
    label: "Forma Pago",
    prop: "formaPago",
    width: 125,
  }
]

const paramsPDF_EstadoDeCuenta = [
  [
    { field: "formaPago", label: "Forma Pago" },
    { field: "tipoPagoFacil", label: "Tipo Pago Fácil" },
  ],
  [
    { field: "bancoOperador", label: "Banco/Operador" },
    { field: "viaCobro", label: "Vía Cobro" },
  ],
  [
    { field: "estadoViaCobro", label: "Estado Via Cobro" },
    { field: "numPagoFacil", label: "N° Pago Fácil" },
  ],
  [
    { field: "numTarjetaCuenta", label: "N° Tarjeta / Cuenta" },
    { field: "titularCuenta", label: "Titular Cuenta" },
  ],
  [
    { field: "monedaCuenta", label: "Moneda Cuenta" },
    { field: "montoCuota", label: "Monto Cuenta" },
  ],
  [
    { field: "frecuenciaPago", label: "Frecuencia Pago" },
    { field: "numCuotasPagadas", label: "N° Cuotas Pagadas" },
  ],
  [
    { field: "numCuotasPendientes", label: "N° Cuotas Pendientes" },
    { field: "deudaTotal", label: "N° Deuda Total" },
  ],
  [
    { field: "estadoPago", label: "Estado Pago" },
    { field: "fechaVenciPago", label: "F.V más antigua" },
  ]
]

const _notNull = value => (_.isNil(value) || !`${value}`.trim()) ? '-' : value

class ModalAccountStatusPDF extends React.Component {
  constructor(props) {
    super(props)

    this.pdfAccount = React.createRef()
  }

  createPdf = () => {
    Doc.createPdf(this.pdfAccount.current)
  }

  render() {
    const {
      data,
      accountStatus,
      openModal,
      changeModalAccoutState,
      person: { select: selectPerson },
      policy: { select: selectPolicy }
    } = this.props

    return (
      <DivModal>

        <MDBModal isOpen={openModal} toggle={changeModalAccoutState} size="lg"
          style={{
            width: '821px'
          }}
          className='widthModalAccountStatus'
        >
          <MDBModalHeader toggle={changeModalAccoutState}>Plantilla PDF</MDBModalHeader>
          <MDBModalBody
            style={{
              height: '400px',
              overflow: 'scroll',
              padding: '0px',
            }}
          >
            <div
              id="pdfDocumentAccountStatus"
              style={{
                width: '210mm',
              }}
              ref={this.pdfAccount}
            >
              <Layout.Row type='flex' justify='center'
                style={{
                  paddingTop: '10px',
                  paddingBottom: '10px'
                }}
              >
                <div
                  style={{
                    width: '95%',
                    height: '70px'
                  }}
                >
                  <img src={logoRimac} alt='logoRimac' style={{ height: '100%' }} />
                </div>
              </Layout.Row>
              <Layout.Row type='flex' justify='center'
                style={{
                  paddingBottom: '10px'
                }}
              >
                <div
                  style={{
                    width: '95%',
                    height: '30px',
                    display: 'flex',
                    alignItems: 'center',
                    textAlign: 'center',
                    justifyContent: 'center'
                  }}
                >
                  <h2><strong> Estado de Cuenta</strong></h2>
                </div>
              </Layout.Row>
              <Layout.Row type='flex' justify='center'
                style={{
                  paddingTop: '10px',
                  paddingBottom: '10px'
                }}
              >
                <div
                  style={{
                    width: '95%',
                    height: '40px',
                    backgroundColor: 'red',
                    color: 'white',
                    display: 'flex',
                    alignItems: 'center'
                  }}
                >
                  <strong>
                    &nbsp;&nbsp;&nbsp;Datos del Cliente
                      </strong>
                </div>
              </Layout.Row>
              <Layout.Row gutter='20'>
                <Layout.Col span='5' offset='1'>
                  <strong>Apellido paterno:</strong>
                </Layout.Col>
                <Layout.Col span='6'>
                  {
                    selectPerson.apePaterno ? selectPerson.apePaterno : ''
                  }
                </Layout.Col>
              </Layout.Row>
              <Layout.Row gutter='20'>
                <Layout.Col span='5' offset='1'>
                  <strong>Apellido materno:</strong>
                </Layout.Col>
                <Layout.Col span='6'>
                  {
                    selectPerson.apeMaterno ? selectPerson.apeMaterno : ''
                  }
                </Layout.Col>
              </Layout.Row>
              <Layout.Row gutter='20'>
                <Layout.Col span='5' offset='1'>
                  <strong>Nombre:</strong>
                </Layout.Col>
                <Layout.Col span='6'>
                  {
                    selectPerson.nombre ? selectPerson.nombre : ''
                  }
                </Layout.Col>
              </Layout.Row>
              <Layout.Row gutter='20'>
                <Layout.Col span='5' offset='1'>
                  <strong>Tipo documento:</strong>
                </Layout.Col>
                <Layout.Col span='6'>
                  {
                    selectPerson.desTipoDocumento ? selectPerson.desTipoDocumento : ''
                  }
                </Layout.Col>
                <Layout.Col span='6'>
                  <strong>Número documento:</strong>
                </Layout.Col>
                <Layout.Col span='6'>
                  {
                    selectPerson.numDocumento ? selectPerson.numDocumento : ''
                  }
                </Layout.Col>
              </Layout.Row>
              {
                //Datos de Poliza
              }
              <Layout.Row type='flex' justify='center'
                style={{
                  paddingTop: '10px',
                  paddingBottom: '10px'
                }}
              >
                <div
                  style={{
                    width: '95%',
                    height: '40px',
                    backgroundColor: 'red',
                    color: 'white',
                    display: 'flex',
                    alignItems: 'center'
                  }}
                >
                  <strong>
                    &nbsp;&nbsp;&nbsp;Datos de Póliza
                      </strong>
                </div>
              </Layout.Row>
              <Layout.Row gutter='20'>
                <Layout.Col span='5' offset='1'>
                  <strong>Producto:</strong>
                </Layout.Col>
                <Layout.Col span='6'>
                  {
                    selectPolicy.desProducto ? selectPolicy.desProducto : ''
                  }
                </Layout.Col>
              </Layout.Row>
              <Layout.Row gutter='20'>
                <Layout.Col span='5' offset='1'>
                  <strong>N° Póliza:</strong>
                </Layout.Col>
                <Layout.Col span='6'>
                  {
                    selectPolicy.numPoliza ? selectPolicy.numPoliza : ''
                  }
                </Layout.Col>
              </Layout.Row>
              <Layout.Row gutter='20'>
                <Layout.Col span='5' offset='1'>
                  <strong>N° Certificado:</strong>
                </Layout.Col>
                <Layout.Col span='6'>
                  {
                    selectPolicy.numCertificado ? selectPolicy.numCertificado : ''
                  }
                </Layout.Col>
              </Layout.Row>
              {
                //Datos de Cuenta
              }
              <Layout.Row type='flex' justify='center'
                style={{
                  paddingTop: '10px',
                  paddingBottom: '10px'
                }}
              >
                <div
                  style={{
                    width: '95%',
                    height: '40px',
                    backgroundColor: 'red',
                    color: 'white',
                    display: 'flex',
                    alignItems: 'center'
                  }}
                >
                  <strong>
                    &nbsp;&nbsp;&nbsp;Datos de Cuenta
                      </strong>
                </div>
              </Layout.Row>
              {
                paramsPDF_EstadoDeCuenta.map((row, index) => {
                  return (
                    <Layout.Row key={index} gutter='20' style={{ fontSize: '13px' }}>
                      {
                        row.map((ec, index2) => {
                          if (index2 === 0) {
                            return (
                              <div key={index2}>
                                <Layout.Col span='5' offset='1'>
                                  <strong>{ec.label}</strong>
                                </Layout.Col>
                                <Layout.Col span='6'>
                                  {
                                    _notNull(accountStatus[ec.field])
                                  }
                                </Layout.Col>
                              </div>
                            )
                          }

                          return (
                            <div key={index2}>
                              <Layout.Col span='5'>
                                <strong>{ec.label}</strong>
                              </Layout.Col>
                              <Layout.Col span='6'>
                                {
                                  _notNull(accountStatus[ec.field])
                                }
                              </Layout.Col>
                            </div>
                          )
                        })
                      }
                    </Layout.Row>
                  )
                })
              }
              {
                //Detalle de Pagos
              }
              <Layout.Row type='flex' justify='center'
                style={{
                  paddingTop: '10px',
                  paddingBottom: '10px'
                }}
              >
                <div
                  style={{
                    width: '95%',
                    height: '40px',
                    backgroundColor: 'red',
                    color: 'white',
                    display: 'flex',
                    alignItems: 'center'
                  }}
                >
                  <strong>
                    &nbsp;&nbsp;&nbsp;Datalles de Pagos
                      </strong>
                </div>
              </Layout.Row>
              <Layout.Row type='flex' justify='center'
                style={{
                  paddingTop: '10px',
                  paddingBottom: '10px'
                }}
              >
                <div
                  style={{
                    width: '95%'
                  }}
                >
                  <DivTablePDF>
                    <ElementTable
                      loader={false}
                      data={data}
                      columns={COLUMNS_PDF}
                      allRow
                      fontSize='13px'
                      strong
                    />
                  </DivTablePDF>
                </div>
              </Layout.Row>
            </div>
          </MDBModalBody>
          <MDBModalFooter>
            <Button plain={true} type="info" onClick={this.createPdf}>
              <MDBIcon far icon="arrow-alt-circle-down" />
              {'  Descargar PDF'}
            </Button>
          </MDBModalFooter>
        </MDBModal>

      </DivModal>
    )
  }
}


const mapStatetoProps = (state) => {
  return {
    loader: state.shared.loader,
    person: state.person,
    policy: state.policy
  }
}

export default connect(mapStatetoProps, null)(ModalAccountStatusPDF)