// @ts-check
import React, { useState, useMemo } from 'react'
import { useSelector } from 'react-redux'
import { MDBInput, MDBBreadcrumb, MDBBreadcrumbItem, MDBIcon, MDBCollapse, MDBRow } from 'mdbreact'
import styled from 'styled-components'
import _ from 'lodash'
import policyLoaderKeys from 'state/ducks/policy/loaderKeys'
import Loader from 'components/loader'
import { changeFormatDate } from 'state/utils/functionUtils'

const DivInput = styled.div`
  .breadcrumb {
    cursor: pointer;
  }
  .md-form {
    margin-top: 0.5rem;
    margin-bottom: 0.5rem;
  }
`;

const paramsInfoPN = [
  [
    { field: 'numPoliza', label: 'N° Póliza', access: ['PN', 'PJ'] },
    { field: 'estadoPoliza', label: 'Estado Póliza', access: ['PN', 'PJ'] },
    { field: 'numCertificado', label: 'N° Certificado', access: ['PN'] },
  ],
  [
    { field: 'codPlan', label: 'Còdigo del Plan', access: ['PN'] },//TODO: Que salga de 2, juntar con codPlan
    { field: 'desPlan', label: 'Descripciòn del Plan', access: ['PN'] },//TODO: Que salga de 2, juntar con codPlan
    { field: 'fecIniVigencia', label: 'Fecha Inicio Vigencia Póliza', access: ['PN', 'PJ'] },
  ],
  [
    { field: 'fecFinVigencia', label: 'Fecha Fin Vigencia Póliza', access: ['PN', 'PJ'] },
    { field: "canalComercial", label: "Canal Comercial", access: ['PN', 'PJ'] },
    { field: "subCanalComercial", label: "Sub Canal Comercial", access: ['PN', 'PJ'] },
  ],
  [
    { field: 'fecIniPrimeraVigencia', label: 'Fecha Primera Vigencia Póliza', access: ['PN', 'PJ'] },
    { field: 'fecAnulaPoliza', label: 'Fecha Anulación Póliza', access: ['PN', 'PJ'] },
    { field: 'motivoAnulaPoliza', label: 'Motivo Anulación Póliza', access: ['PN', 'PJ'] },
  ],
  [
   // { field: 'rol', label: 'Rol', access: ['PJ'] },
    { field: 'nomBroker', label: 'Corredor', access: ['PN', 'PJ'] },
    { field: 'nomIntermediario', label: 'Intermediario', access: ['PN', 'PJ'] },
    { field: 'codProdAcselx', label: 'Producto', access: ['PN', 'PJ'] },
  ],
  [
    { field: 'desProducto', label: 'Descripción', access: ['PN', 'PJ'] },
    { field: 'ramo', label: 'Ramo', access: ['PN'] },
    { field: 'titular', label: 'Titular', access: ['PN'] },
  ],
  [
    { field: 'responsablePago', label: 'Responsable de Pago', access: ['PN'] },
    { field: 'contratante', label: 'Contratante', access: ['PN', 'PJ'] },
  ],
  // [
  //   { field: 'nroMaxAtencionesAux', label: 'N° Auxilio Mecánico', access: ['PN'] },
  //   { field: 'nroMaxAtencionesCdr', label: 'N° de Chofer de Reemplazo', access: ['PN'] },
  // ],
]

const paramsInfoPJ = [
  [
    { field: 'numPoliza', label: 'N° Póliza', access: ['PN', 'PJ'] },
    { field: 'estadoPoliza', label: 'Estado Póliza', access: ['PN', 'PJ'] },
    { field: 'fecIniVigencia', label: 'Fecha Inicio Vigencia Póliza', access: ['PN', 'PJ'] },
  ],
  [
    { field: 'fecFinVigencia', label: 'Fecha Fin Vigencia Póliza', access: ['PN', 'PJ'] },
    { field: "canalComercial", label: "Canal Comercial", access: ['PN', 'PJ'] },
    { field: "subCanalComercial", label: "Sub Canal Comercial", access: ['PN', 'PJ'] },
  ],
  [
    { field: 'fecIniPrimeraVigencia', label: 'Fecha Primera Vigencia Póliza', access: ['PN', 'PJ'] },
    { field: 'fecAnulaPoliza', label: 'Fecha Anulación Póliza', access: ['PN', 'PJ'] },
    { field: 'motivoAnulaPoliza', label: 'Motivo Anulación Póliza', access: ['PN', 'PJ'] },
  ],
  [
    { field: 'rol', label: 'Rol', access: ['PJ'] },
    { field: 'nomBroker', label: 'Corredor', access: ['PN', 'PJ'] },
    { field: 'nomIntermediario', label: 'Intermediario', access: ['PN', 'PJ'] },
  ],
  [
    { field: 'codProdAcselx', label: 'Producto', access: ['PN', 'PJ'] },
    { field: 'desProducto', label: 'Descripción', access: ['PN', 'PJ'] },
    { field: 'contratante', label: 'Contratante', access: ['PN', 'PJ'] },
  ],
]

const paramsInfoAndDetail = [
  [
    { field: 'estadoCertificado', label: 'Estado Certificado', access: ['PN'], obj: 'select' },
    { field: 'fechaIniVigenciaCert', label: 'Fecha Inicio Vigencia Certificado', access: ['PN'], obj: 'detail' },
    { field: 'fechaFinVigenciaCert', label: 'Fecha Fin Vigencia Certificado', access: ['PN'], obj: 'detail' },
  ],
  [
    // { field: 'numRenovaciones', label: 'N° Renovaciones', access: ['PN', 'PJ'], obj: 'select' },
    { field: 'fecRenovacion', label: 'Fecha Renovación', access: ['PN', 'PJ'], obj: 'detail' },
    // { field: 'indMorosidad', label: 'Indicador morosidad', access: ['PN', 'PJ'], obj: 'select' },
    { field: 'indPolizaDigital', label: 'Póliza digital', access: ['PN', 'PJ'], obj: 'select' },//TODO: Si o No
    { field: 'correoPolizaElect', label: 'Correo Póliza Electronica', access: ['PN', 'PJ'], obj: 'detail' },

  ],
  [
    { field: 'correoFactura', label: 'Correo Facturación', access: ['PN', 'PJ'], obj: 'detail' },
  ],
]

const notNull = value => (_.isNil(value) || `${value}`.trim() === '') ? '-' : value

const GeneralData = props => {
  const [collapseData, setCollapseData] = useState(true)

  const loaderVisible = useSelector(state => state.shared.loader)
    .includes(policyLoaderKeys.POLICY_DETAIL)

  const select = useMemo(() => {
    let s = (props.select) ? props.select : {};

    s.fecIniVigencia = (s.fecIniVigencia) ? changeFormatDate(s.fecIniVigencia, '-', '/') : '-';
    s.fecFinVigencia = (s.fecFinVigencia) ? changeFormatDate(s.fecFinVigencia, '-', '/') : '-';
    s.fecIniPrimeraVigencia = (s.fecIniPrimeraVigencia) ? changeFormatDate(s.fecIniPrimeraVigencia, '-', '/') : '-';
    s.fecAnulaPoliza = (s.fecAnulaPoliza) ? changeFormatDate(s.fecAnulaPoliza, '-', '/') : '-';

    return s;
  }, [props.select]);

  const detail = useMemo(() => {
    let d = (props.detalle.data) ? props.detalle.data : {};

    d.fechaIniVigenciaCert = (d.fechaIniVigenciaCert) ? changeFormatDate(d.fechaIniVigenciaCert, '-', '/') : '-';
    d.fechaFinVigenciaCert = (d.fechaFinVigenciaCert) ? changeFormatDate(d.fechaFinVigenciaCert, '-', '/') : '-';
    d.fecRenovacion = (d.fecRenovacion) ? changeFormatDate(d.fecRenovacion, '-', '/') : '-';

    return d;
  }, [props.detalle]);

  return (
    <DivInput>
      <div className="row">
        <div className="col-lg-12">
          <MDBBreadcrumb onClick={() => setCollapseData(!collapseData)}>
            <MDBBreadcrumbItem>
              {collapseData
                ? <MDBIcon icon="angle-down" />
                : <MDBIcon icon="angle-right" />}
              {"  Datos de la Póliza"}
            </MDBBreadcrumbItem>
          </MDBBreadcrumb>
          <br />
        </div>
      </div>

      <div className="container-fluid">
        <MDBCollapse id="collapseData" isOpen={collapseData}>
          { (props.personType === 'PN'?paramsInfoPN:paramsInfoPJ).map((item, key) => {
            const inputGroup = item.map((item2, index) => {
              if (item2.access.includes(props.personType)) {
                switch (item2.field) {
                  /*case 'plan':
                    return (
                      <div className="col-lg-8" key={`#${index}`}>
                        <MDBInput
                          value={`${notNull(select['codPlan'])} - ${notNull(select['desPlan'])}`}
                          disabled
                          label={item2.label} size="sm" />
                      </div>
                    )*/
                    /*
                  case 'producto':
                    return (
                      <div className="col-lg-8" key={`#${index}`}>
                        <MDBInput
                          value={`${notNull(select['codProdAcselx'])} - ${notNull(select['desProducto'])}`}
                          disabled
                          label={item2.label} size="sm" />
                      </div>
                    )*/
                  default:
                    return (
                      <div className="col-lg-4" key={`#${index}`}>
                        <MDBInput value={`${notNull(select[item2.field])}`} disabled label={item2.label} size="sm" />
                      </div>
                    )
                }
              }
              return <div key={`%${index}`}></div>
            })
            return <div className="row" key={key}>{inputGroup}</div>;
          })}
          {loaderVisible
            ?
            <MDBRow center>
              <Loader />
            </MDBRow>
            : <div>
              {paramsInfoAndDetail.map((item, index) => {
                const inputGroup = item.map((item2, keyInput) => {
                  if (item2.access.includes(props.personType)) {
                    if (item2.obj === 'select') {
                      return (
                        <div className="col-lg-4" key={`${keyInput}-${index}`}>
                          <MDBInput value={`${notNull(select[item2.field])}`} disabled label={item2.label} size="sm" />
                        </div>
                      )
                    } else {
                      return (
                        <div className="col-lg-4" key={`${keyInput}-${index}`}>
                          <MDBInput value={`${notNull(detail[item2.field])}`} disabled label={item2.label} size="sm" />
                        </div>
                      )
                    }

                  }
                  return <div key={`${keyInput}-${index}`}></div>
                })
                return <div className="row" key={`&${index}`}>{inputGroup}</div>;
              })}
            </div>
          }
        </MDBCollapse>
      </div>
    </DivInput>
  );
}

export default GeneralData