import React, { Component, useMemo, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { bindActionCreators } from 'redux'
import ElementTable from 'components/table/elementTable'

import { policyOperations, policyLoaderKeys } from 'state/ducks/policy'

const COLUMNS = [
  {
    label: "Código Clausula",
    prop: "codClausula",
    minWidth: 150
  },
  {
    label: "Descripción",
    prop: "desClausula",
    minWidth: 250
  },
  {
    label: "Número Versión",
    prop: "numVers",
    minWidth: 150
  },
]

const Clause = props => {
  const dispatch = useDispatch()
  const policyOperationsAction = useMemo(() => {
    return bindActionCreators(policyOperations, dispatch)
  }, [dispatch])

  const { select, clause: { dataList } } = useSelector(state => state.policy)
  const loaderVisible = useSelector(state => state.shared.loader.includes(policyLoaderKeys.POLICY_CLAUSE))

  useEffect(() => {
    let requestBody = {
      "core": (select.core) ? `${select.core}` : '',
      "codProdAcselx": (select.codProdAcselx) ? `${select.codProdAcselx}` : '',
      "codProducto": (select.codProducto) ? `${select.codProducto}` : '',
      "numPoliza": (select.numPoliza) ? `${select.numPoliza}` : '',
      "numCertificado": (select.numCertificado) ? `${select.numCertificado}` : '',
      "codPlan": (select.codPlan) ? `${select.codPlan}` : '',
      "codCobertura": '',
      "idePoliza": (select.idePoliza) ? `${select.idePoliza}` : '',
    }
    
    policyOperationsAction.policyClauseList(requestBody)
    // auditoria
    let requestLogging = {
      "path": window.location.href,
      "action": "POLICY_CLAUSE_LIST"
    }
    policyOperationsAction.auditLogging(requestLogging)
  }, [])

  return (
    <div>
      <ElementTable
        columns={COLUMNS}
        data={dataList}
        loader={loaderVisible}
        allRow
      />
    </div>
  )
}

export default Clause