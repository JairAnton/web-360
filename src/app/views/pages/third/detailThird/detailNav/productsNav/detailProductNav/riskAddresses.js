import React, { useEffect, useMemo } from 'react'
import { useSelector } from 'react-redux'
import ElementTable from 'components/table/elementTable'
import { policyLoaderKeys } from 'state/ducks/policy';

const COLUMNS = [
  {
    label: "Dirección",
    prop: "direccion",
    minWidth: 300,
  },
  {
    label: "Departamento",
    prop: "departamento",
    minWidth: 150,
  },
  {
    label: "Provincia",
    prop: "provincia",
    minWidth: 150,
  },
  {
    label: "Distrito",
    prop: "distrito",
    minWidth: 150,
  },
];

function RiskAddresses(props) {

  const loaderVisible = useSelector(state => state.shared.loader)
    .includes(policyLoaderKeys.RISK_ADDRESSES)

  const riskAddresses = useMemo(() => {
    return props.riskAddresses.dataList
  }, [props.riskAddresses])


  useEffect(() => {
    props.getRiskAddresses()
  }, [])

  return (
    <div>
      <ElementTable
        data={riskAddresses}
        columns={COLUMNS}
        loader={loaderVisible}
        allRow
      />
    </div>
  )
}

export default RiskAddresses