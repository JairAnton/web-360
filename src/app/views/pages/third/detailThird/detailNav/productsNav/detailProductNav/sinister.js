import React, { useState, useEffect, useMemo } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { bindActionCreators } from 'redux'
import _ from 'lodash'
import styled from 'styled-components'
import { MDBBreadcrumb, MDBCollapse, MDBInput, MDBBreadcrumbItem, MDBIcon } from 'mdbreact'
import policyLoaderKeys from 'state/ducks/policy/loaderKeys'
import ElementTable from 'components/table/elementTable';
import { changeFormatDate } from 'state/utils/functionUtils'

const DivInput = styled.div`
    .breadcrumb {
        cursor: pointer;
    }
    .md-form {
        margin-top: 0.5rem;
        margin-bottom: 0.5rem;
    }
`;

const columns = [
  {
    label: "N° Siniestro",
    prop: "numSiniestroBpmAx",
    width: 130,
  },
  // {
  //   label: "N° Caso CRM",
  //   prop: "numCasoCrm",
  //   minWidth: 150,
  // },
  {
    label: "Producto",
    prop: "producto",
    minWidth: 150,
  },
  {
    label: "N° poliza",
    prop: "numPoliza",
    minWidth: 130,
  },
  {
    label: "N° Certificado",
    prop: "nroCertificado",
    minWidth: 150,
  },
  {
    label: "Fecha Ocurrencia",
    prop: "fechaOcurrencia",
    minWidth: 150,
  },
  {
    label: "Tipo siniestro",
    prop: "tipoSiniestro",
    minWidth: 150,
  },
  {
    label: "Tipo servicio",
    prop: "tipoServicio",
    minWidth: 150,
  },
  {
    label: "Estado siniestro",
    prop: "estadoSiniestro",
    minWidth: 160,
  },
]

const params = [
  [
    { field: "numSiniestroBpmAx", label: "Número Siniestro" },
    { field: "numCasoBpm", label: "Número Caso BPM" },
    { field: "numCasoCrm", label: "Número Caso CRM" },
  ],
  [
    { field: "causa", label: "Causa" },
    { field: "consecuencia", label: "Consecuencia" },
    { field: "producto", label: "Producto" },
  ],
  [
    { field: "lugarOcurrencia", label: "Lugar de ocurrencia" },
    { field: "observacion", label: "Observacion" },
    { field: "tipoSiniestro", label: "Tipo de siniestro" },
  ],
  [
    { field: "desSiniestro", label: "Descripción Siniestro" },
    { field: "asegurado", label: "Asegurado" },
    { field: "agraviado", label: "Agraviado" },
  ],
  [
    { field: "tipoServicio", label: "Tipo Servicio" },
    { field: "ejecutivoAsignado", label: "Ejecutivo asignado" },
    { field: "comisaria", label: "Comisaria" },
  ],
  [
    { field: "clinica", label: "Clinica" },
    { field: "numPlaca", label: "Placa" },
    { field: "numMotor", label: "Motor" },
  ],
  [
    { field: "direccionTaller", label: "Dirección del taller" },
    { field: "nombreProcurador", label: "Nombre Procurador" },
    { field: "estadoAnalisis", label: "Estado Analisis" },
  ],
  [
    { field: "estado", label: "Estado Siniestro" },
    { field: "motivoRechazo", label: "Motivo Rechazo" },
    { field: "beneficio", label: "Beneficio" },
  ],
]

const paramsDate = [
  [
    { field: "fechaAtencion", label: "Fecha de Atención" },
    { field: "fechaRecepcion", label: "Fecha de Recepción" },
    { field: "fechaHoraReporte", label: "Fecha Hora Reporte" },
  ],
  [
    { field: "fechaLiquidacion", label: "Fecha Liquidación" },
    { field: "fechaOcurrencia", label: "Fecha de Ocurrencia" },
  ]
]

const paramsAmount = [
  [
    { field: "moneda", label: "Moneda" },
    { field: "coaseguro", label: "Coasegurado" },
    { field: "deducible", label: "Deducible" },
  ],
  [
    { field: "totalGastosExcluidos", label: "Total de Gastos Excluidos" },
    { field: "totalGastosPresentados", label: "Total de Gastos Presentados" },
    { field: "tipoPago", label: "Tipo de Pago" },
  ]
]

const notNull = value => (_.isNil(value) || `${value}`.trim() === '') ? '-' : value

const Sinister = props => {
  const [collapseSinister, setCollapseSinister] = useState({ detail: true, date: true, amount: true })
  const [data, setData] = useState(null);
  const [page, setPage] = useState(1);
  const [rowPage, setRowPage] = useState(10);

  const loaderVisible = useSelector(state => state.shared.loader).includes(policyLoaderKeys.POLICY_SINISTER_LIST)

  const pagination = (props.sinister && props.sinister.pagination) ? props.sinister.pagination : {};

  const dataTable = useMemo(() => {
    const dataList = (props.sinister && props.sinister.dataList) ? props.sinister.dataList : [];
    let newData = [];

    dataList.forEach(item => {
      if (item) {
        item.fechaAtencion = (item.fechaAtencion) ? changeFormatDate(item.fechaAtencion, '-', '/') : '-';
        item.fechaHoraReporte = (item.fechaHoraReporte) ? changeFormatDate(item.fechaHoraReporte, '-', '/') : '-';
        item.fechaLiquidacion = (item.fechaLiquidacion) ? changeFormatDate(item.fechaLiquidacion, '-', '/') : '-';
        item.fechaOcurrencia = (item.fechaOcurrencia) ? changeFormatDate(item.fechaOcurrencia, '-', '/') : '-';
        item.fechaRecepcion = (item.fechaRecepcion) ? changeFormatDate(item.fechaRecepcion, '-', '/') : '-';
        item.numSiniestroBpmAx = (item.numSiniestroBpm) ? item.numSiniestroBpm : item.numSiniestroAX;

        newData.push(item)
      }
    });


    return newData;
  }, [props.sinister.dataList]);

  //#region Funciones de la tabla Siniestros

  //Cuando se cambia el número de pagina, siguiente o anterior
  const changePageSinister = (currentPage) => {
    setPage(currentPage)
  }

  //cuando se cambia el número de registros a visualizar
  const changeRowsPageSinister = (numberOfRows) => {
    setRowPage(numberOfRows);
  }

  //cuando se selecciona un row
  const changeSelectRow = (row) => {
    setData(row)
    setTimeout(() => {
      scrollToRefSinister()
    }, 500)
  }

  //#endregion

  const scrollToRefSinister = () => {
    document.getElementById('my_scrollable_policy_sinister').scrollIntoView({ behavior: 'smooth' })
  }

  useEffect(() => {
    setData(null);
    props.getPolicySinisterList(page, rowPage);
  }, [page, rowPage])

  return (
    <div>
      <div style={{"cursor":"pointer"}}>
        <ElementTable
          data={dataTable}
          columns={columns}
          onChangeSelect={changeSelectRow}
          page={page}
          rowPage={rowPage}
          onChangePage={changePageSinister}
          onChangeRowPage={changeRowsPageSinister}
          pagination={pagination}
          loader={loaderVisible}
        />
      </div>
      <br />
      <div
        style={{ width: '10px', height: '10px' }}
        id="my_scrollable_policy_sinister"
      />
      {
        data &&
        <DivInput>
          <div className="row">
            <div className="col-lg-12">
              <MDBBreadcrumb
                onClick={() => {
                  setCollapseSinister({ ...collapseSinister, detail: !collapseSinister.detail })
                }}
              >
                <MDBBreadcrumbItem>
                  {
                    (collapseSinister.detail) ? <MDBIcon icon="angle-down" /> : <MDBIcon icon="angle-right" />
                  }
                  {"  Detalle de Siniestro"}
                </MDBBreadcrumbItem>
              </MDBBreadcrumb>
              <br />
            </div>
          </div>

          <div className="container-fluid">
            <MDBCollapse id="collapseData" isOpen={collapseSinister.detail}>
              {
                params.map((prop, key) => {
                  let inputGroup = prop.map((param, keyInput) => {
                    return (
                      <div className="col-lg" key={keyInput}>
                        <MDBInput value={`${notNull(data[param.field])}`} disabled label={param.label} size="sm" />
                      </div>
                    )
                  });

                  return <div className="row" key={key}>{inputGroup}</div>;
                })
              }
            </MDBCollapse>
          </div>

          <div className="row">
            <div className="col-lg-12">
              <MDBBreadcrumb
                onClick={() => {
                  setCollapseSinister({ ...collapseSinister, date: !collapseSinister.date })
                }}
              >
                <MDBBreadcrumbItem>
                  {
                    (collapseSinister.date) ? <MDBIcon icon="angle-down" /> : <MDBIcon icon="angle-right" />
                  }
                  {"  Fechas"}
                </MDBBreadcrumbItem>
              </MDBBreadcrumb>
              <br />
            </div>
          </div>
          <div className="container-fluid">
            <MDBCollapse id="collapseData" isOpen={collapseSinister.date}>
              {
                paramsDate.map((prop, key) => {
                  let inputGroup = prop.map((param, keyInput) => {
                    return (
                      <div className="col-lg" key={keyInput}>
                        <MDBInput value={`${notNull(data[param.field])}`} disabled label={param.label} size="sm" />
                      </div>
                    )
                  });

                  return <div className="row" key={key}>{inputGroup}</div>;
                })
              }
            </MDBCollapse>
          </div>
          <div className="row">
            <div className="col-lg-12">
              <MDBBreadcrumb
                onClick={() => {
                  setCollapseSinister({ ...collapseSinister, amount: !collapseSinister.amount })
                }}
              >
                <MDBBreadcrumbItem>
                  {
                    (collapseSinister.amount) ? <MDBIcon icon="angle-down" /> : <MDBIcon icon="angle-right" />
                  }
                  {"  Montos"}
                </MDBBreadcrumbItem>
              </MDBBreadcrumb>
              <br />
            </div>
          </div>
          <div className="container-fluid">
            <MDBCollapse id="collapseData" isOpen={collapseSinister.amount}>
              {
                paramsAmount.map((prop, key) => {
                  let inputGroup = prop.map((param, keyInput) => {
                    return (
                      <div className="col-lg" key={keyInput}>
                        <MDBInput value={`${notNull(data[param.field])}`} disabled label={param.label} size="sm" />
                      </div>
                    )
                  });

                  return <div className="row" key={key}>{inputGroup}</div>;
                })
              }
            </MDBCollapse>
          </div>
        </DivInput>
      }
    </div>
  );
}

export default Sinister;