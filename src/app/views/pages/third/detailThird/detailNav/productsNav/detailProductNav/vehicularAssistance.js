import React from 'react'
import TableUI from '../../../../../../../components/table';

const VehicularAssistance = (props) => {
  const getColumns = [
    {
      label: "Asistencia",
      name: "asistencia",
      style: {
        textAlign: "center",
      }
    },
    {
      label: "Total Servicios",
      name: "totalServicios",
      style: {
        textAlign: "center",
      }
    },
    {
      label: "Servicios Utilizados",
      name: "serviciosUtilizados",
      style: {
        textAlign: "center",
      }
    },
    {
      label: "Servicios Disponibles",
      name: "serviciosDiponibles",
      style: {
        textAlign: "center",
      }
    }
  ];

  const getData = [];

  return (
    <div>
      <TableUI
        data={getData}
        columns={getColumns}
        options={{
          print: false,
          download: false,
          filter: false,
          filterType: "dropdown",
          responsive: "scroll",
          textLabels: {
            body: {
              noMatch: "No se encontraron registros",
            }
          }
        }}
      />
    </div>
  );
}

export default VehicularAssistance;