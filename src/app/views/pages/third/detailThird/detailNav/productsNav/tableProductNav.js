import React from 'react'

const TableProductNav = (props) => {

  const getColumns = [
    {
      Header: "Ramo",
      accessor: "ramo",
      style: {
        textAlign: "center",
      }
    },
    {
      Header: "Desc. Producto",
      accessor: "desProducto",
      style: {
        textAlign: "center",
      }
    },
    {
      Header: "Cód. Producto",
      accessor: "codProducto",
      style: {
        textAlign: "center",
      }
    },
    {
      Header: "N° Póliza",
      accessor: "numPoliza",
      style: {
        textAlign: "center",
      },
      Cell: prop => <a href="#" onClick={() => props.selectPolicy(prop.value[0])}>{prop.value[1]}</a>
    },
    {
      Header: "N° Certificado",
      accessor: "numCertificado",
      style: {
        textAlign: "center",
      }
    }
  ]
  // },
  // {
  //     Header: "Placa",
  //     accessor: "numPlaca",
  //     style: {
  //         textAlign: "center",
  //     }
  // },
  // {
  //     Header: "Plan",
  //     accessor: "desPlan",
  //     style: {
  //         textAlign: "center",
  //     }
  // },
  // {
  //     Header: "Rol",
  //     accessor: "rol",
  //     style: {
  //         textAlign: "center",
  //     }
  // },
  // {
  //     Header: "Corredor",
  //     accessor: "nomBroker",
  //     style: {
  //         textAlign: "center",
  //     }
  // },
  // {
  //     Header: "Suma Asegurada",
  //     accessor: "sumaAsegurada",
  //     style: {
  //         textAlign: "center",
  //     }
  // },
  // {
  //     Header: "Prima",
  //     accessor: "primaTotal",
  //     style: {
  //         textAlign: "center",
  //     }
  // },
  // {
  //     Header: "Ind. Deuda",
  //     accessor: "indDeuda",
  //     style: {
  //         textAlign: "center",
  //     }
  // },
  // {
  //     Header: "Estado",
  //     accessor: "estadoPoliza",
  //     style: {
  //         textAlign: "center",
  //     }
  // },
  // {
  //     Header: "Fecha Inicio",
  //     accessor: "fecIniVigencia",
  //     style: {
  //         textAlign: "center",
  //     }
  // },
  // {
  //     Header: "Fecha Venc.",
  //     accessor: "fecFinVigencia",
  //     style: {
  //         textAlign: "center",
  //     }
  // }


  let propsData = props.data;
  let data = [];

  if (propsData && propsData.length > 0) {
    propsData.map((prop, key) => {
      data.push({
        "ramo": prop.ramo,
        "desProducto": prop.desProducto,
        "codProducto": prop.codProducto,
        "numPoliza": [key, prop.numPoliza],
        "numCertificado": prop.numCertificado,
        "numPlaca": (prop.numPlaca) ? prop.numPlaca : "",
        "desPlan": (prop.desPlan) ? prop.desPlan : "",
        "rol": prop.rol,
        "nomBroker": (prop.nomBroker) ? prop.nomBroker : "",
        "sumaAsegurada": (prop.sumaAsegurada) ? prop.sumaAsegurada : "",
        "primaTotal": (prop.primaTotal) ? prop.primaTotal : "",
        "indDeuda": "",
        "estadoPoliza": (prop.estadoPoliza) ? prop.estadoPoliza : "",
        "fecIniVigencia": (prop.fecIniVigencia) ? prop.fecIniVigencia : "",
        "fecFinVigencia": (prop.fecFinVigencia) ? prop.fecFinVigencia : ""
      })
    });
  }

  return (
    <div>
      Tabla Productos
      </div>
    // <DetailTable
    //     columns={getColumns}
    //     data={data}
    // />
  );
}

export default TableProductNav;