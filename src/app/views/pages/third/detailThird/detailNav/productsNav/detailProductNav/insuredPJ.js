import React, { useState, useMemo, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { bindActionCreators } from 'redux'
import ElementTable from 'components/table/elementTable'
import { Layout } from 'element-react'
import Select from 'react-select'

import { MDBRow, MDBIcon } from 'mdbreact'
import { policyLoaderKeys } from 'state/ducks/policy'
import { Button } from 'element-react';
import { policyOperations } from 'state/ducks/policy'
import PubSub from 'state/utils/PubSub'

const COLUMNS = [
  {
    label: "Tipo Documento",
    prop: "tipoDocumento",
    minWidth: 160,
  },
  {
    label: "Número Documento",
    prop: "numDocumento",
    minWidth: 190,
  },
  {
    label: "Nombre",
    prop: "nombre",
    minWidth: 180,
  },
  {
    label: "Código Afiliado",
    prop: "codAfiliado",
    minWidth: 160,
  },
  /*
  {
    label: "Código Cliente",
    prop: "codCliente",
    minWidth: 150,
  },
  {
    label: "Número Afiliado",
    prop: "nroAfiliado",
    minWidth: 160,
  },
  */
  {
    label: "Plan",
    prop: "plan",
    minWidth: 150,
  },
  {
    label: "Estado Plan",
    prop: "descripcion",
    minWidth: 150,
  },
];

const COLUMNS_VIDA = [
  {
    label: "Tipo Documento",
    prop: "tipoDocumentoInput",
    minWidth: 160,
  },
  {
    label: "Número Documento",
    prop: "numDocumento",
    minWidth: 190,
  },
  {
    label: "Nombre",
    prop: "nombre",
    minWidth: 180,
  },
  {
    label: "Código Tercero",
    prop: "codigoTercero",
    minWidth: 160,
  },
  {
    label: "Edad",
    prop: "edad",
    minWidth: 130,
  },
  // {
  //   label: "Fecha de Ingreso",
  //   prop: "fechaNacimiento",
  //   minWidth: 160,
  // },
  {
    label: "Plan",
    prop: "desPlan",
    minWidth: 150,
  },
  {
    label: "Estado Plan",
    prop: "status",
    minWidth: 150,
  },
];

const OPTIONS = [
  { value: "1", label: "RUC" },
  { value: "2", label: "DNI" },
]

const InsuredPJ = props => {
  const dispatch = useDispatch()

  useEffect(() => {
    return () => {
      props.selectInsured(null)
    }
  }, [])


  const policyOperationAction = useMemo(() => {
    return bindActionCreators(policyOperations, dispatch)
  }, [dispatch])

  const [page, setPage] = useState(1)
  const [rowPage, setRowPage] = useState(10)
  const [callService, setCallService] = useState(true)
  const [body, setBody] = useState({
    "numDocumento": '',
    "tipoDocumento": '',
    "apeMaterno": '',
    "apePaterno": '',
    "nombre":'',
    "numCertificado":''
  })

  const select = useSelector(state => state.policy.select)
  const loaderVisible = useSelector(state => state.shared.loader.includes(policyLoaderKeys.POLICY_INSURED_LIST_DETALLE))
  
  const validateForm = () =>{
    if(body.tipoDocumento !== ''  && body.numDocumento.length === 0){
      PubSub.publish('WARNING_SEARCH_THIRD', 'Debe llenar el nùmero de documento.');
          return false;
    }
    if(body.tipoDocumento === '1' && body.numDocumento.length !== 11)
    {
      PubSub.publish('WARNING_SEARCH_THIRD', 'Debe llenar el nùmero de documento válido');
      return false;
    }
    
    if(body.tipoDocumento === '2' && body.numDocumento.length !== 8)
    {
      PubSub.publish('WARNING_SEARCH_THIRD', 'Debe llenar el nùmero de documento válido');
      return false;
    }

    return true;
  }


  useEffect(() => {
    if(validateForm()){
      let requestBody = {
        "filter": {
          "core": (select.core) ? select.core.toString() : '',
          "codProducto": (select.codProducto) ? select.codProducto.toString() : '',
          "codProdAcselx": (select.codProdAcselx) ? select.codProdAcselx.toString() : '',
          "numPoliza": (select.numPoliza) ? select.numPoliza.toString() : '',
          //"ramo": select.ramo,
          "tipoDocumento": (body.tipoDocumento && body.tipoDocumento.value) ? body.tipoDocumento.value : '',
          "numDocumento": body.numDocumento || '',
          "apeMaterno": body.apeMaterno ? body.apeMaterno.toUpperCase() : '',
          "apePaterno": body.apePaterno ? body.apePaterno.toUpperCase() : '',
          "nombre": body.nombre ? body.nombre.toUpperCase() : '',
          "numCertificado": body.numCertificado || ''
        },
        "pagination": {
          "filasPorPag": rowPage,
          "pagActual": page
        }
      }
      
      policyOperationAction.policyInsuredListDetalle(requestBody)
    }
  }, [callService])
  
  
  const {
    dataList,
    pagination
  } = useSelector(state => state.policy.asegurado)
  

  const changeRowPage = (_rowPage) => {
    setRowPage(_rowPage)
    setPage(1)
    setCallService(!callService)
  }

  const changePage = (_page) => {
    setPage(_page)
    setCallService(!callService)
  }

  const getColumns = () => {
    return select.ramo === 'VIDA' ? COLUMNS_VIDA : COLUMNS
  }

  const LimpiarFormulario = () =>{
    setBody({
      ...body,
      "numDocumento": '',
      "tipoDocumento": '',
      "apeMaterno": '',
      "apePaterno": '',
      "nombre":'',
      "numCertificado":''
    })
  }


  const handleChange = (evnt) => {
    //aca
    let cutNumDoc = false
    let long = 15;

    let {name, value} = evnt.target

    if (name === "numDocumento") {
      if (body.tipoDocumento === '1') {
        long = 11
        cutNumDoc = value.length > long
        
      } 
      else if (body.tipoDocumento === '2') {
        long = 8
        cutNumDoc = value.length > long
      }
      else if (body.tipoDocumento === '') {
        long = 0
        cutNumDoc = value.length > long
      }
      
      if (cutNumDoc) {
        value = value.slice(0, long)
      }
    }
    
    if (name === "tipoDocumento") body.numDocumento = ''

    setBody({
      ...body,
      [name]: value
    })
  }

  const selectRowTable = item => {
    props.selectInsured(item)
  }

  return (
    <div>
      <Layout.Row gutter="10" style={{ marginBottom: '15px' }}>
        <Layout.Col span="4" offset="1">
          <label className="col-form-label">Tipo documento:</label>
        </Layout.Col>
        <Layout.Col span="7">
          <Select placeholder="Seleccionar"
            name="form-field-name"
            options={OPTIONS}
            onChange={(val) => {
              handleChange({
                target: {
                  name: 'tipoDocumento',
                  value: val ? val.value:''

                }
              })
            }}
            isClearable
          />
        </Layout.Col>
        <Layout.Col span="4" offset="1">
          <label className="col-form-label">Número documento:</label>
        </Layout.Col>
        <Layout.Col span="7">
          <input
            className="form-control"
            type="number"
            name="numDocumento"
            onChange={handleChange}
            value={body.numDocumento}
          />
        </Layout.Col>
      </Layout.Row>
      
      <Layout.Row gutter="10" style={{ marginBottom: '15px' }}>
        <Layout.Col span="4" offset="1">
          <label className="col-form-label">Apellido Paterno:</label>
        </Layout.Col>
        <Layout.Col span="7">
          <input
            className="form-control"
            type="text"
            name="apePaterno"
            onChange={handleChange}
            value={body.apePaterno}
          />
        </Layout.Col>
        <Layout.Col span="4" offset="1">
          <label className="col-form-label">Apellido Materno:</label>
        </Layout.Col>
        <Layout.Col span="7">
          <input
            className="form-control"
            type="text"
            name="apeMaterno"
            onChange={handleChange}
            value={body.apeMaterno}
          />
        </Layout.Col>
      </Layout.Row>

      <Layout.Row gutter="10" style={{ marginBottom: '15px' }}>
        <Layout.Col span="4" offset="1">
          <label className="col-form-label">Nombres:</label>
        </Layout.Col>
        <Layout.Col span="7">
          <input
            className="form-control"
            type="text"
            name="nombre"
            onChange={handleChange}
            value={body.nombre}
          />
        </Layout.Col>
        <Layout.Col span="4" offset="1">
          <label className="col-form-label">Nro Certificado:</label>
        </Layout.Col>
        <Layout.Col span="7">
          <input
            className="form-control"
            type="number"
            name="numCertificado"
            onChange={handleChange}
            value={body.numCertificado}
          />
        </Layout.Col>
      </Layout.Row>


      <div className="row justify-content-md-center">
        <div className="col-2">
          <Button
            type="danger"
            onClick={() => {
              setCallService(!callService)
            }}
          >
            <MDBIcon icon="search" />
            {'  Buscar'}
          </Button>
        </div>
        <div className="col-2">
          <Button type="danger" onClick={() => {
              LimpiarFormulario()
            }}>>
            <MDBIcon icon="eraser" />
            {'  Limpiar'}
          </Button>
        </div>
      </div>
      <br />
      <div style={{"cursor":"pointer"}}>
      <ElementTable
        data={dataList ? dataList : []}
        columns={getColumns()}
        page={page}
        rowPage={rowPage}
        pagination={pagination}
        loader={loaderVisible}
        onChangeRowPage={changeRowPage}
        onChangePage={changePage}
        onChangeSelect={selectRowTable}
      />
      </div>
    </div>
  )
}

export default InsuredPJ