// @ts-check
import React, { useState, useEffect, useMemo } from 'react'
import { useSelector } from 'react-redux'
import ElementTable from 'components/table/elementTable'
import DetailProcedure from './procedureNav/detailProcedure'
import { operationLoaderKeys } from 'state/ducks/operation'

const COLUMNS = [
  {
    label: "N° Trámite",
    prop: "numTramite",
    width: 130,
  },
  {
    label: "Tipo Operación",
    prop: "tipoOperacion",
    minWidth: 150,
  },
  {
    label: "Fecha Solicitud",
    prop: "fecSolicitud",
    minWidth: 150,
  },
  {
    label: "Estado",
    prop: "estado",
    minWidth: 150,
  },
  {
    label: "Código Producto",
    prop: "codProducto",
    minWidth: 160,
  },
  {
    label: "Producto",
    prop: "desProducto",
    minWidth: 150,
  },
  {
    label: "N° Póliza",
    prop: "numPoliza",
    width: 130,
  },
];

const ProcedureNav = ({
  operation: {
    procedure: {
      dataList = [],
      pagination,
    },
  },
  getProcedureList,
}) => {

  const [selectProcedure, setSelectProcedure] = useState(null)
  const [page, setPage] = useState(1)
  const [rowPage, setRowPage] = useState(10)

  const loaderVisible = useSelector(state => state.shared.loader.includes(operationLoaderKeys.PROCEDURE_LIST))

  useEffect(() => {
    getProcedureList({ page, rowPage })
  }, [])

  useEffect(() => {
    selectProcedure &&
      document.getElementById('my_scrollable_div')
        .scrollIntoView({ behavior: 'smooth' })
  }, [selectProcedure])

  // const changeCheckProcedureNav = (id) => {
  //   if ('rbPendientes' === id) {
  //     setRbPendientes(true)
  //     setRbCerrados(false)
  //   } else if ('rbCerrados' === id) {
  //     setRbPendientes(false)
  //     setRbCerrados(true)
  //   }
  // }

  const changePageProcedure = (currentPage) => {
    setPage(currentPage)
    setSelectProcedure(null)
    // call service
    getProcedureList({ page: currentPage, rowPage })
  }

  const changeRowsPerPageProcedure = (rowsPerPage) => {
    setPage(1)
    setRowPage(rowsPerPage)
    setSelectProcedure(null)
    // call service
    getProcedureList({ page, rowPage: rowsPerPage })
  }

  //Cuando haces Click a la fila - Row
  const clickRowTable = (selectProcedure) => {
    setSelectProcedure(selectProcedure)
  }

  const getData = useMemo(() => {
    return dataList ? dataList.map(item => ({ ...item })) : []
  }, [dataList])


  return (
    <div className="row">
      {/* <div className="col-lg-12">
        <div className="row">
          <div className="form-check col-sm" style={{ margin: '5px' }}>
            <input
              className="form-check-input"
              type="radio"
              name="rbProcedure"
              id="rbPendientes"
              value="1"
              defaultChecked
              onClick={() => changeCheckProcedureNav('rbPendientes')}
            />
            <label className="form-check-label" htmlFor="rbPendientes">
              Pendientes
              </label>
          </div>
          <div className="form-check col-sm" style={{ margin: '5px' }}>
            <input
              className="form-check-input"
              type="radio"
              name="rbProcedure"
              id="rbCerrados"
              value="2"
              onClick={() => changeCheckProcedureNav('rbCerrados')}
            />
            <label className="form-check-label" htmlFor="rbCerrados">
              Cerrados
              </label>
          </div>

          <div className="col-sm">
            <div className="row">
              <div className="col-sm-3" style={{ flexDirection: 'row' }}>
                <label
                  htmlFor="estado"
                  style={{
                    fontSize: '16px',
                    paddingLeft: '0px',
                    paddingRight: '0px',
                    paddingTop: '5px',
                  }}
                >Estado:</label>
              </div>

              <div className="col-sm-6">
                <input type="text" className="form-control" id="estado" />
              </div>
            </div>
          </div>

        </div>

      </div> */}

      <div className="col-lg-12" style={{ cursor: 'pointer'}}>
        <ElementTable
          loader={loaderVisible}
          data={getData}
          onChangeSelect={clickRowTable}
          columns={COLUMNS}
          onChangePage={page => {
            changePageProcedure(page)
          }}
          onChangeRowPage={rowPage => {
            changeRowsPerPageProcedure(rowPage)
          }}
          page={page}
          rowPage={rowPage}
          pagination={pagination}
        />
      </div>

      <div className="col-lg-12">
        {selectProcedure &&
          <DetailProcedure
            procedure={selectProcedure}
          />}
      </div>

      <div
        style={{ width: '10px', height: '10px' }}
        id="my_scrollable_div"
      />

    </div>
  )
}

export default ProcedureNav