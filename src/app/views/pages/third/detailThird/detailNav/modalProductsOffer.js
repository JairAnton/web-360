import React, { Component } from 'react';

class ModalProductsOffer extends Component {


    getColumnsProductsSuggested = () => {
        const columns = [
            {
                Header: "Riesgo",
                accessor: "riesgo",
                style: {
                    textAlign: "center",
                }
            },
            {
                Header: "Categoría Producto",
                accessor: "tipoProd",
                style: {
                    textAlign: "center",
                }
            },
            {
                Header: "Indicador",
                accessor: "probabilidad",
                style: {
                    textAlign: "center",
                },
                Cell: props => <p>{props.value}</p>
            }
        ]

        return columns;
    }

    getDataProductsSuggested = () => {
        return [];
    }


    render() {
        return (
            <div>
                <button type="button" className="btn btn-primary" data-toggle="modal" data-target=".bd-modal-products-offer-xl">Productos x Ofrecer</button>

                <div className="modal fade bd-modal-products-offer-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                    <div className="modal-dialog modal-xl">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title">Productos por ofercer</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Cerrar">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                {/* <DetailTable
                                    columns={this.getColumnsProductsSuggested()}
                                    data={this.getDataProductsSuggested()}
                                /> */}
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ModalProductsOffer;