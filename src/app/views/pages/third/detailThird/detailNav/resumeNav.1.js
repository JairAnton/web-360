import React from 'react'

import DetailTable from '../detailTable'
import ModalProductsOffer from './modalProductsOffer'

const ResumeNav = ({detail}) => {
    const getColumns = () => {
        const columns = [
            {
                Header: "Ide",
                accessor: "ide",
                style: {
                    textAlign: "center",
                }
            },
            {
                Header: "N° Caso Pivotal",
                accessor: "tipo",
                style: {
                    textAlign: "center",
                }
            },
            {
                Header: "Fecha Registro",
                accessor: "numero",
                style: {
                    textAlign: "center",
                }
            },
            {
                Header: "Área",
                accessor: "anexo",
                style: {
                    textAlign: "center",
                }
            },
            {
                Header: "Tipo Caso",
                accessor: "uso",
                style: {
                    textAlign: "center",
                }
            },
            {
                Header: "Categoría",
                accessor: "estado",
                style: {
                    textAlign: "center",
                }
            },
            {
                Header: "Asunto",
                accessor: "usuarioModif",
                style: {
                    textAlign: "center",
                }
            },
            {
                Header: "Tema",
                accessor: "fechaModif",
                style: {
                    textAlign: "center",
                }
            },
            {
                Header: "N° Reclamo",
                accessor: "usuarioModif",
                style: {
                    textAlign: "center",
                }
            },
            {
                Header: "Producto",
                accessor: "fechaModif",
                style: {
                    textAlign: "center",
                }
            },
            {
                Header: "Ramo",
                accessor: "usuarioModif",
                style: {
                    textAlign: "center",
                }
            },
            {
                Header: "Canal",
                accessor: "fechaModif",
                style: {
                    textAlign: "center",
                }
            },
            {
                Header: "Responsable",
                accessor: "usuarioModif",
                style: {
                    textAlign: "center",
                }
            },
            {
                Header: "Estado",
                accessor: "fechaModif",
                style: {
                    textAlign: "center",
                }
            },
            {
                Header: "N° Siniestro",
                accessor: "fechaModif",
                style: {
                    textAlign: "center",
                }
            }
        ]

        return columns;
    }

    const getData = () => {
        const cells = null; //this.props.detail.celulares;
        let data = [];

        if(cells){
            cells.map((prop) => {
                data.push({
                    "esPrincipal": prop.esPrincipal,
                    "tipo": prop.tipo,
                    "numero": prop.numero,
                    "anexo": prop.anexo,
                    "uso": prop.uso ,
                    "estado": prop.estado   ,
                    "usuarioModif": prop.usuarioModif,
                    "fechaModif": prop.fechaModif
                });
            });
        }

        return data;
    }

    return(
        <div className="row">
            <div className="col-lg">
                <DetailTable
                    columns={getColumns()}
                    data={getData()}
                />
            </div>
            <div className="col-lg">
                <hr/>
                <div className="row">
                    <div className="col-lg-4">
                        <div className="card bg-light mb-3" style={{"max-width": "18rem;"}}>
                            <div className="card-header">Productos Vigentes</div>
                            <div className="card-body">
                                <p className="card-text">
                                    Se registran 6 Productos Vigentes.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4">
                        <div className="card bg-light mb-3" style={{"max-width": "18rem;"}}>
                            <div className="card-header">Reclamos</div>
                            <div className="card-body">
                                <p className="card-text">Se registra 1 Reclamo(s) abiertos.</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4">
                        <div className="card bg-light mb-3" style={{"max-width": "18rem;"}}>
                            <div className="card-header">Posición del Cliente</div>
                            <div className="card-body">
                                <p className="card-text">
                                    Segmento: PLATINO 2
                                    <br/>
                                    Canal Principal: FUERZA DE VENTAS
                                    <br/>
                                    Sectorista:
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4">
                        <div className="card bg-light mb-3" style={{"max-width": "18rem;"}}>
                            <div className="card-header">Siniestros</div>
                            <div className="card-body">
                                <p className="card-text">
                                    Se registra 38 Siniestro(s) Salud.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4">
                        <div className="card bg-light mb-3" style={{"max-width": "18rem;"}}>
                            <div className="card-header">Productos por ofrecer</div>
                            <div className="card-body">
                                {
                                    detail && 
                                    detail.productosSugeridos &&
                                    detail.productosSugeridos.length > 0 &&
                                    <div>
                                        <p className="card-text">
                                            Se registra {detail.productosSugeridos.length} Consulta(s) Web 
                                        </p>
                                        <ModalProductsOffer/>
                                    </div>

                                }
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4">
                        <div className="card bg-light mb-3" style={{"max-width": "18rem;"}}>
                            <div className="card-header">Consultas Web</div>
                            <div className="card-body">
                                
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4">
                        <div className="card bg-light mb-3" style={{"max-width": "18rem;"}}>
                                <div className="card-header">Tratamiento</div>
                                <div className="card-body">
                                    <p className="card-text"></p>
                                </div>
                        </div>
                    </div>
                    <div className="col-lg-4">
                        <div className="card bg-light mb-3" style={{"max-width": "18rem;"}}>
                            <div className="card-header">Documentos</div>
                            <div className="card-body">
                                <p className="card-text">
                                    No se registran Cartas de Garantía.
                                    <br/>
                                    No se registran Certificados de Supervivencia.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4">
                        <div className="card bg-light mb-3" style={{"max-width": "18rem;"}}>
                            <div className="card-header">Autorizaciones Medicas</div>
                            <div className="card-body">
                                <h5 className="card-title">Light card title</h5>
                                <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-4">
                        <div className="card border-dark bg-light mb-3" style={{"max-width": "18rem;"}}>
                                <div className="card-header">Scoring Vida</div>
                                <div className="card-body">
                                    <p className="card-text">No se registran Autorizaciones Médicas.</p>
                                </div>
                        </div>
                    </div>
                    <div className="col-lg-4">
                        <div className="card border-dark bg-light mb-3" style={{"max-width": "18rem;"}}>
                                <div className="card-header">Canales de Comunicación</div>
                                <div className="card-body">
                                    <p className="card-text">Se registra 5 canal(es) de comunicación.</p>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ResumeNav;