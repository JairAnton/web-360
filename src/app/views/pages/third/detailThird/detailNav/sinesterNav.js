// @ts-check
import React, { useState, useMemo, useEffect, useRef } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { bindActionCreators } from 'redux'

import SearchSinester from './sinesterNav/searchSinester'
import DetailSinister from './sinesterNav/detailSinister'
import ElementTable from 'components/table/elementTable'
import { sinisterOperations, sinisterLoaderKeys } from 'state/ducks/sinister'
import { changeFormatDate, removeTZ } from 'state/utils/functionUtils'

const SINESTER_COLUMNS = [
  {
    label: "Producto",
    prop: "producto",
    minWidth: 160
  },
  {
    label: "N° Póliza",
    prop: "numPoliza",
    width: 120
  },
  {
    label: "Certificado",
    prop: "nroCertificado",
    minWidth: 120
  },
  {
    label: "N° Caso BPM",
    prop: "numCasoBpm",
    minWidth: 180
  },
  {
    label: "Tipo Siniestro",
    prop: "tipoSiniestro",
    minWidth: 170
  },
  {
    label: "Fec. Recepción",
    prop: "fechaRecepcion",
    render: (row, column, index) => (<span> {changeFormatDate(removeTZ(row[column.prop]), '-', '/')} </span>),
    minWidth: 180
  },
  {
    label: "Fec. Siniestro",
    prop: "fechaSiniestro",
    render: (row, column, index) => (<span> {changeFormatDate(removeTZ(row[column.prop]), '-', '/')} </span>),
    minWidth: 200
  },
  {
    label: "Estado",
    prop: "estado",
    minWidth: 100
  }
]

const RAMOS_DETAIL_VH = ['VEHICULARES', 'RIESGOS GENERALES']

const SiniesterNav = props => {
  const dispatch = useDispatch()
  const sinisterOperationsActions = useMemo(() => {
    return bindActionCreators(sinisterOperations, dispatch)
  }, [dispatch])

  const renderDataList = useRef(true)
  const [useRender, setUseRender] = useState(false)
  const [page, setPage] = useState(1)
  const [rowPage, setRowPage] = useState(10)
  const [selectSinister, setSelectSinister] = useState(null)
  const [body, setBody] = useState({})
  const [ramo, setRamo] = useState('')
  const [dataListSinister, setDataListSinister] = useState([])

  const loaderVisible = useSelector(state => state.shared.loader.includes(sinisterLoaderKeys.SINISTER_LIST))
  const { dataList, pagination } = useSelector(store => store.sinister)

  //#region funtions  
  const changeSelect = (data) => {
    setSelectSinister(data)
    setRamo(data.tipoSiniestro)
    if (RAMOS_DETAIL_VH.includes(data.tipoSiniestro)) {
      let requestBody = {
        "nroCasoBPM": (data.numCasoBpm) ? data.numCasoBpm.toString() : ''
      }

      sinisterOperationsActions.sinisterDetail(requestBody)

      // auditoria
      let requestLogging = {
        "path": window.location.href,
        "action": "SINISTER_DETAIL"
      }
      sinisterOperationsActions.auditLogging(requestLogging)
    }
    scrollToRefSinister()
  }

  const changePage = (numPage) => {
    setPage(numPage)
  }

  const changeRowPage = (sizePage) => {
    setRowPage(sizePage)
    setPage(1)
  }

  const getListSinister = () => {
    props.getListSinister({ ...body, page, rowPage })
    setSelectSinister(null)
    scrollToRefSinister()
  }

  const changeBody = (name, value) => {
    setBody({ ...body, [name]: value })
  }

  const scrollToRefSinister = () => {
    setTimeout(() => {
      document.getElementById('my_scrollable_tercero_sinister').scrollIntoView({ behavior: 'smooth' })
    }, 500)
  }
  //#endregion

  //#region React->UseEffects
  useEffect(() => {
    const sinisterFA = props.sinisterFA

    if (sinisterFA) {
      if (sinisterFA.callService) {
        props.getListSinister({ ...body, page, rowPage })
        scrollToRefSinister()
      } else {
        setDataListSinister(sinisterFA.data && sinisterFA.data.length > 0 ? sinisterFA.data : [])
      }
    }
    setUseRender(true)
  }, [])

  useEffect(() => {
    if (renderDataList.current) {
      renderDataList.current = false
      return
    }
    setDataListSinister(dataList && dataList.length > 0 ? dataList : [])
  }, [dataList])

  useEffect(() => {
    if (!useRender) {
      return
    }

    setSelectSinister(null)
    props.getListSinister({ ...body, page, rowPage })
    scrollToRefSinister()
  }, [page])
  //#endregion

  return (
    <div className="row">
      <div className="col-lg-12">
        <SearchSinester
          getListSinister={getListSinister}
          changeBody={changeBody}
        />
      </div>
      <div className="col-lg-12" style={{ cursor: 'pointer' }}>
        <ElementTable
          columns={SINESTER_COLUMNS}
          data={dataListSinister}
          onChangeSelect={changeSelect}
          page={page}
          rowPage={rowPage}
          onChangePage={changePage}
          onChangeRowPage={changeRowPage}
          loader={loaderVisible}
          pagination={pagination}
        />
      </div>
      <div className="col-lg-12">
        <br />
        <div
          style={{ width: '10px', height: '10px' }}
          id="my_scrollable_tercero_sinister"
        />
        {selectSinister &&
          <DetailSinister
            selected={selectSinister}
            ramo={ramo}
          />
        }
      </div>
    </div>
  )
}

export default SiniesterNav