// @ts-check
import React, { useEffect } from 'react'
import { useSelector } from 'react-redux'
import ElementTable from 'components/table/elementTable'
import salesforceLoaderKeys from 'state/ducks/salesforce/loaderKeys'

const COLUMNS = [
  {
    label: "Retenible",
    prop: "retenible",
    minWidth: 150,
  },
  {
    label: "N° Caso",
    prop: "numeroCaso",
    minWidth: 150,
  },
  {
    label: "Ramo",
    prop: "ramo",
    minWidth: 150,
  },
  {
    label: "Producto",
    prop: "producto",
    minWidth: 150,
  },
  {
    label: "N° Póliza",
    prop: "numeroPoliza",
    minWidth: 150,
  },
  {
    label: "Certificado",
    prop: "numeroCertificado",
    minWidth: 150,
  },
  {
    label: "Endosado",
    prop: "endosado",
    minWidth: 150,
  },
  {
    label: "Canal",
    prop: "canal",
    minWidth: 150,
  },
]

const RetentionNav = (props) => {
  // const [dataSelected, setDataSelected] = useState(null);

  const loaderVisible = useSelector(state => state.shared.loader.includes(salesforceLoaderKeys.RETENTION));
  const { retention: { dataList } } = useSelector(state => state.salesforce);

  useEffect(() => {
    props.getRetentionListMassive()
  }, [])

  return (
    <div className="row">
      <div className="col-lg-12">
        <ElementTable
          data={dataList}
          columns={COLUMNS}
          loader={loaderVisible}
          allRow
        />
      </div>
    </div>
  )

}

export default RetentionNav