// @ts-check
import React, { useEffect, useState } from 'react'
import { Layout, Select, Input, Button } from 'element-react'
// import { useInput } from 'components/hooks/input'

const inputStyle = {
  fontSize: '13px',
  paddingLeft: '0px',
  paddingRight: '0px'
}

const formStyle = {
  marginBottom: '0.1em',
  alignItems: 'flex-end'
}

const OPTIONS = [
  {
    value: 'AX',
    label: 'Vehicular y RRGG',
  },
  {
    value: 'RS',
    label: 'Salud',
  },
  {
    value: 'AE',
    label: 'Vida',
  },
]

const SearchSinester = props => {
  const [numPoliza, setNumPoliza] = useState('')
  const [numCasoBpm, setNumCasoBpm] = useState('')
  const [core, setCore] = useState(null)

  const changeNumPoliza = val => {
    setNumPoliza(val)
  }

  const changeNumCasoBpm = val => {
    setNumCasoBpm(val)
  }

  const changeCore = (selectedCore) => {
    setCore(selectedCore)
  }

  useEffect(() => {
    props.changeBody('numPoliza', numPoliza);
  }, [numPoliza]);

  useEffect(() => {
    props.changeBody('numCasoBpm', numCasoBpm);
  }, [numCasoBpm]);

  useEffect(() => {
    props.changeBody('core', core);
  }, [core]);

  return (
    <div className="container-fluid" style={{ marginBottom: '10px' }}>
      <form>
        <Layout.Row style={formStyle} type="flex" gutter="10" justify="space-around">
          <Layout.Col span="5">
            <label style={inputStyle}>N° Póliza:</label>
            <Input onChange={changeNumPoliza} />
          </Layout.Col>

          <Layout.Col span="5">
            <label style={inputStyle}>N° Caso BPM:</label>
            <Input onChange={changeNumCasoBpm} />
          </Layout.Col>

          <Layout.Col span="5">
            <Layout.Col span="24">
              <label style={inputStyle}>Core:</label>
            </Layout.Col>
            <Select
              value={core}
              onChange={changeCore}
            >
              {OPTIONS.map(item => (
                <Select.Option
                  key={item.value}
                  label={item.label}
                  value={item.value}
                />
              ))}
            </Select>
          </Layout.Col>

          <Layout.Col span="5" style={{
            justifyContent: 'center',
            alignItems: 'center',
            height: '100%',
          }}>
            <Button
              style={{ width: '100%' }}
              type="danger"
              icon="search"
              onClick={props.getListSinister}
            >
              Buscar
            </Button>
            {/* <button
              className="btn btn-danger btn-icon-split"
              onClick={() => { props.getListSinister() }}
            >
              <span className="icon text-white-50">
                <i className="fas fa-search"></i>
              </span>
              <span className="text">Buscar</span>
            </button> */}
          </Layout.Col>

        </Layout.Row>

      </form>

      {/* <div className="row">
        <div className="input-group">
          <div className="input-group-append">
            <button className="btn btn-danger btn-icon-split"
              onClick={() => {
                props.getListSinister();
              }}
            >
              <span className="icon text-white-50">
                <i className="fas fa-search"></i>
              </span>
              <span className="text">Buscar</span>
            </button>
          </div>
        </div>
      </div> */}
    </div>
  );

}

export default SearchSinester;