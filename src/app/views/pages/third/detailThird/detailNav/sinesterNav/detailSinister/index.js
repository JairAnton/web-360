import React, { Component, useEffect, useMemo } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { bindActionCreators } from 'redux'
import Detail from './detail'
import DetailSV from './detailSV'
import { Loading } from 'element-react';

import { sinisterOperations, sinisterLoaderKeys } from 'state/ducks/sinister'

const RAMOS_DETAIL_VH = ['VEHICULARES', 'RIESGOS GENERALES']
  // && RAMOS_DETAIL.includes(ramo) 

const DetailSinister = props => {
  const visibleLoader = useSelector(store => store.shared.loader.includes(sinisterLoaderKeys.SINISTER_DETAIL))
  const {ramo, selected} = props

  const ComponentDetail = useMemo(() => {
    if(RAMOS_DETAIL_VH.includes(ramo)){
      return <Detail/>
    }

    return <DetailSV data={selected}/>
  },[ramo])


  return (
    <div>
      {
        visibleLoader ?
          <Loading>
            ComponentDetail
          </Loading>
        :
          ComponentDetail
      }
    </div>
  )
}

export default DetailSinister
