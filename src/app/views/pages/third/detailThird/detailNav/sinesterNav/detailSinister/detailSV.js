import React, { useState, useEffect, useMemo } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { bindActionCreators } from 'redux'
import _ from 'lodash'
import styled from 'styled-components'
import { MDBBreadcrumb, MDBCollapse, MDBInput, MDBBreadcrumbItem, MDBIcon } from 'mdbreact'
import policyLoaderKeys from 'state/ducks/policy/loaderKeys'
import ElementTable from 'components/table/elementTable';
import { changeFormatDate } from 'state/utils/functionUtils'

const DivInput = styled.div`
    .breadcrumb {
        cursor: pointer;
    }
    .md-form {
        margin-top: 0.5rem;
        margin-bottom: 0.5rem;
    }
`

//#region CAMPOS

const columns = [
  {
    label: "N° Siniestro",
    prop: "numSiniestroBpmAx",
    width: 130,
  },
  // {
  //   label: "N° Caso CRM",
  //   prop: "numCasoCrm",
  //   minWidth: 150,
  // },
  {
    label: "Producto",
    prop: "producto",
    minWidth: 150,
  },
  {
    label: "N° poliza",
    prop: "numPoliza",
    minWidth: 130,
  },
  {
    label: "N° Certificado",
    prop: "nroCertificado",
    minWidth: 150,
  },
  {
    label: "Tipo siniestro",
    prop: "tipoSiniestro",
    minWidth: 150,
  },
  {
    label: "Tipo servicio",
    prop: "tipoServicio",
    minWidth: 150,
  },
  {
    label: "Estado siniestro",
    prop: "estadoSiniestro",
    minWidth: 160,
  },
]

const params = [
  [
    { field: "asegurado", label: "Asegurado" },
    { field: "ejecutivoAsignado", label: "Ejecutivo asignado" },
    { field: "desSiniestro", label: "Descripción Siniestro" },
  ],
  [
    { field: "observacion", label: "Observacion" },
    { field: "lugarOcurrencia", label: "Lugar de ocurrencia" },
    { field: "comisaria", label: "Comisaria" },
  ],
  [
    { field: "agraviado", label: "Agraviado" },
    { field: "direccionTaller", label: "Dirección del taller" },
    { field: "numPlaca", label: "Placa" },
  ],
  [
    { field: "numMotor", label: "Motor" },
    { field: "clinica", label: "Clinica" },
    { field: "motivoRechazo", label: "Motivo Rechazo" },
  ],
  [
    { field: "numCasoBpm", label: "Número Caso BPM" },
    { field: "numCasoCrm", label: "Número Caso CRM" },
    { field: "causa", label: "Causa" },
  ],
  [
    { field: "consecuencia", label: "Consecuencia" },
    { field: "estadoAnalisis", label: "Estado Analisis" },
    { field: "beneficio", label: "Beneficio" },
  ],

  [
    { field: "nombreProcurador", label: "Nombre Procurador" },
    { field: "producto", label: "Producto" },
    { field: "tipoServicio", label: "Tipo Servicio" },
  ],
  [
    { field: "numSiniestroBpmAx", label: "Número Siniestro" },
    { field: "estado", label: "Estado Siniestro" },
    { field: "tipoSiniestro", label: "Tipo de siniestro" },
  ],
]

const paramsDate = [
  [
    { field: "fechaLiquidacion", label: "Fecha Liquidación" },
    { field: "fechaOcurrencia", label: "Fecha de Ocurrencia" },
    { field: "fechaHoraReporte", label: "Fecha Hora Reporte" },
  ],
  [
    { field: "fechaAtencion", label: "Fecha de Atención" },
    { field: "fechaRecepcion", label: "Fecha de Recepción" },
  ]
]

const paramsAmount = [
  [
    { field: "moneda", label: "Moneda" },
    { field: "deducible", label: "Deducible" },
    { field: "coaseguro", label: "Coasegurado" },
  ],
  [
    { field: "tipoPago", label: "Tipo de Pago" },
    { field: "totalGastosPresentados", label: "Total de Gastos Presentados" },
    { field: "totalGastosExcluidos", label: "Total de Gastos Excluidos" },
  ]
]

//#endregion

const notNull = value => (_.isNil(value) || `${value}`.trim() === '') ? '-' : value

const DetailSV = props => {
  const [collapseSinister, setCollapseSinister] = useState({ detail: true, date: true, amount: true })

  const data = useMemo(() => {
    let dataD = props.data

    dataD.fechaAtencion = (dataD.fechaAtencion) ? changeFormatDate(dataD.fechaAtencion, '-', '/') : '-';
    dataD.fechaHoraReporte = (dataD.fechaHoraReporte) ? changeFormatDate(dataD.fechaHoraReporte, '-', '/') : '-'
    dataD.fechaLiquidacion = (dataD.fechaLiquidacion) ? changeFormatDate(dataD.fechaLiquidacion, '-', '/') : '-'
    dataD.fechaOcurrencia = (dataD.fechaOcurrencia) ? changeFormatDate(dataD.fechaOcurrencia, '-', '/') : '-'
    dataD.fechaRecepcion = (dataD.fechaRecepcion) ? changeFormatDate(dataD.fechaRecepcion, '-', '/') : '-'
    dataD.numSiniestroBpmAx = (dataD.numSiniestroBpm) ? dataD.numSiniestroBpm : dataD.numSiniestroAX

    return dataD
  }, [props.data]);


  return (
    <DivInput>
      <div className="row">
        <div className="col-lg-12">
          <MDBBreadcrumb
            onClick={() => {
              setCollapseSinister({ ...collapseSinister, detail: !collapseSinister.detail })
            }}
          >
            <MDBBreadcrumbItem>
              {
                (collapseSinister.detail) ? <MDBIcon icon="angle-down" /> : <MDBIcon icon="angle-right" />
              }
              {"  Detalle de Siniestro"}
            </MDBBreadcrumbItem>
          </MDBBreadcrumb>
          <br />
        </div>
      </div>
      <div className="container-fluid">
        <MDBCollapse id="collapseData" isOpen={collapseSinister.detail}>
          {
            params.map((prop, key) => {
              let inputGroup = prop.map((param, keyInput) => {
                return (
                  <div className="col-lg" key={keyInput}>
                    <MDBInput value={`${notNull(data[param.field])}`} disabled label={param.label} size="sm" />
                  </div>
                )
              });

              return <div className="row" key={key}>{inputGroup}</div>;
            })
          }
        </MDBCollapse>
      </div>
      <div className="row">
        <div className="col-lg-12">
          <MDBBreadcrumb
            onClick={() => {
              setCollapseSinister({ ...collapseSinister, date: !collapseSinister.date })
            }}
          >
            <MDBBreadcrumbItem>
              {
                (collapseSinister.date) ? <MDBIcon icon="angle-down" /> : <MDBIcon icon="angle-right" />
              }
              {"  Fechas"}
            </MDBBreadcrumbItem>
          </MDBBreadcrumb>
          <br />
        </div>
      </div>
      <div className="container-fluid">
        <MDBCollapse id="collapseData" isOpen={collapseSinister.date}>
          {
            paramsDate.map((prop, key) => {
              let inputGroup = prop.map((param, keyInput) => {
                return (
                  <div className="col-lg" key={keyInput}>
                    <MDBInput value={`${notNull(data[param.field])}`} disabled label={param.label} size="sm" />
                  </div>
                )
              });

              return <div className="row" key={key}>{inputGroup}</div>;
            })
          }
        </MDBCollapse>
      </div>
      <div className="row">
        <div className="col-lg-12">
          <MDBBreadcrumb
            onClick={() => {
              setCollapseSinister({ ...collapseSinister, amount: !collapseSinister.amount })
            }}
          >
            <MDBBreadcrumbItem>
              {
                (collapseSinister.amount) ? <MDBIcon icon="angle-down" /> : <MDBIcon icon="angle-right" />
              }
              {"  Montos"}
            </MDBBreadcrumbItem>
          </MDBBreadcrumb>
          <br />
        </div>
      </div>
      <div className="container-fluid">
        <MDBCollapse id="collapseData" isOpen={collapseSinister.amount}>
          {
            paramsAmount.map((prop, key) => {
              let inputGroup = prop.map((param, keyInput) => {
                return (
                  <div className="col-lg" key={keyInput}>
                    <MDBInput value={`${notNull(data[param.field])}`} disabled label={param.label} size="sm" />
                  </div>
                )
              });

              return <div className="row" key={key}>{inputGroup}</div>;
            })
          }
        </MDBCollapse>
      </div>
    </DivInput>
  )
}

export default DetailSV