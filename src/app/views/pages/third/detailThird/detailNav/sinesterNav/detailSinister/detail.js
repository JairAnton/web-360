import React, { useState, useMemo, useEffect } from 'react'
import { bindActionCreators } from 'redux'
import { useSelector, useDispatch } from 'react-redux'
import {
  MDBInput, MDBCollapse, MDBBreadcrumb, MDBBreadcrumbItem, MDBIcon,
  // MDBCard, MDBCardBody, MDBCardTitle, MDBCardText, MDBCardHeader, MDBBtn, MDBContainer,
} from "mdbreact";
import styled from 'styled-components'

import ElementTable from 'components/table/elementTable'
import { sinisterOperations, sinisterLoaderKeys } from 'state/ducks/sinister'
import { changeFormatDate, removeTZ } from 'state/utils/functionUtils'

const DivInput = styled.div`
  .breadcrumb {
    cursor: pointer;
  }
  .md-form {
    margin-top: 0.5rem;
    margin-bottom: 0.5rem;
  }
`;

const OBSERVATION_COLUMNS = [
  {
    label: "Fecha y Hora de Creación",
    prop: "fechaCreacion",
    minWidth: 160
  },
  {
    label: "Usuario",
    prop: "usuarioCreadorNotas",
    minWidth: 160
  },
  {
    label: "Comentario",
    prop: "comentario",
    minWidth: 160
  },
];

const INSURANCE_DAMAGE_COLUMNS = [
  {
    label: "Detalle",
    prop: "detalle",
    minWidth: 160
  },
  {
    label: "Usuario Modificación",
    prop: "usuarioModificado",
    minWidth: 160
  },
  {
    label: "Fecha Modificación",
    prop: "fechaModificacion",
    minWidth: 160
  },
];

const CIRCUNSTANCE_SINISTER_COLUMNS = [
  {
    label: "Detalle",
    prop: "detalle",
    minWidth: 160
  },
  {
    label: "Usuario Modificación",
    prop: "usuarioModificado",
    minWidth: 160
  },
  {
    label: "Fecha Modificación",
    prop: "fechaModificacion",
    minWidth: 160
  },
];

const DEDUCTIBLE_COLUMNS = [
  {
    label: "Detalle",
    prop: "detalle",
    minWidth: 160
  },
  {
    label: "Usuario Modificación",
    prop: "usuarioModificado",
    minWidth: 160
  },
  {
    label: "Fecha Modificación",
    prop: "fechaModificacion",
    minWidth: 160
  },
];

const TECHNICAL_ADJUSTMENTS_COLUMNS = [
  {
    label: "Tecnico",
    prop: "tecnico",
    minWidth: 160
  },
  {
    label: "Taller",
    prop: "taller",
    minWidth: 160
  },
  {
    label: "Fecha Informe",
    prop: "fechaInforme",
    minWidth: 160
  },
  {
    label: "Tipo Ajuste",
    prop: "tipoAjuste",
    minWidth: 160
  },
  {
    label: "Moneda",
    prop: "moneda",
    minWidth: 160
  },
  {
    label: "Monto Ajustado",
    prop: "montoAjustado",
    minWidth: 160
  },
  {
    label: "Estado",
    prop: "estado",
    minWidth: 160
  },
];

//#region Map for Inputs detail
const datosSiniestro = [
  [
    { field: 'asegurado', label: 'Asegurado' },
    { field: 'placa', label: 'Placa' },
    { field: 'motor', label: 'Motor' },
  ],
  [
    { field: 'siniestroAx', label: 'Nro Siniestro A/X' },
    { field: 'casoBpm', label: 'Nro Caso BPM' },
    { field: 'producto', label: 'Producto' },
  ],
  [
    { field: 'consecuencia', label: 'Consecuencias' },
    { field: 'estadoSiniestro', label: 'Estado Siniestro' },
    { field: 'fechaHoraOcurrencia', label: 'Fecha Hora Ocurrencia' },
  ],
  [
    { field: 'poliza', label: 'Nro Póliza' },
    { field: 'certificado', label: 'Nro Certificado' },
    { field: 'causa', label: 'Causa' },
  ],
]

const detalleSiniestro = [
  [
    { field: 'ejecutivo', label: 'Ejecutivo asignado' },
    { field: 'lugar', label: 'Lugar' },
    { field: 'comisaria', label: 'Comisaria' },
  ],
  [
    { field: 'nroPivotal', label: 'Número Caso CRM ' },
    { field: 'departamento', label: 'Departamento' },
    { field: 'provincia', label: 'Provincia' },
  ],
  [
    { field: 'distrito', label: 'Distrito' }, 
    { field: 'referencia', label: 'Referencia ' },
    { field: 'fechaHoraDeclaracion', label: 'Fecha/Hora Declaración ' },
  ],
  [
    { field: 'dosaje', label: 'Dosaje ' },
    { field: 'terceroAfectados', label: 'Tercero afectados ' },
    { field: 'muertos', label: 'Muertos ' },
  ],
  [
    { field: 'dahnosVehAseg', label: 'Daños del vehículo asegurado' },
    { field: 'taller', label: 'Taller' },
    { field: 'telefDeclarante', label: 'Telef. Declarante' },
  ],
]

const infoContactProcurador = [
  [
    { field: 'fechaHoraContacto', label: 'Fecha y Hora de Contacto' },
    { field: 'fechaHoraOcurrInf', label: 'Fecha y Hora de Ocurrencia ' },
    { field: 'fechaCulminacion', label: 'Fecha de Culminación' },
  ],
  [
    { field: 'procurador', label: 'Procurador' },
    { field: 'contacto', label: 'Contacto ' },
    { field: 'telefono', label: 'Telefono ' },
  ],
  [
    { field: 'departamentoInf', label: 'Departamento ' },
    { field: 'provinciaInf', label: 'Provincia ' },
    { field: 'distritoInf', label: 'Distrito ' },
  ],
  [
    { field: 'comisariaInf', label: 'Comisaria' },
    { field: 'lugarInf', label: 'Lugar' },
  ],
]
//#endregion

const Detail = props => {
  const [collapseDataSinister, setCollapseDataSinister] = useState(true);
  const [collapseDetailSinister, setCollapseDetailSinister] = useState(true);
  const [collapseInformationSinister, setCollapseInformationSinister] = useState(true);
  const [collapseTechnicalAdjustments, setCollapseTechnicalAdjustments] = useState(true);
  const [collapseDeductibles, setCollapseDeductibles] = useState(true);
  const [collapseCircumstanceSinister, setCollapseCircumstanceSinister] = useState(true);
  const [collapseInsuredDamages, setCollapseInsuredDamages] = useState(true);
  const [collapseObservations, setCollapseObservations] = useState(true);

  const {
    detailSinister,
    ajusteTecnico,
    deducibles,
    circunstancias,
    danio,
    observaciones
  } = useSelector(store => store.sinister.detail)

  const nDetailSinister = useMemo(() => {
    if (!detailSinister) return detailSinister
    detailSinister.fechaHoraOcurrencia = changeFormatDate(removeTZ(detailSinister.fechaHoraOcurrencia), '-', '/')
    detailSinister.fechaHoraDeclaracion = changeFormatDate(removeTZ(detailSinister.fechaHoraDeclaracion), '-', '/')
    detailSinister.fechaHoraContacto = changeFormatDate(removeTZ(detailSinister.fechaHoraContacto), '-', '/')
    detailSinister.fechaHoraOcurrInf = changeFormatDate(removeTZ(detailSinister.fechaHoraOcurrInf), '-', '/')
    detailSinister.fechaCulminacion = changeFormatDate(removeTZ(detailSinister.fechaCulminacion), '-', '/')
    return detailSinister
  }, [detailSinister])

  const nObservaciones = useMemo(() => {
    if (!observaciones) return observaciones
    return observaciones.map((obs) => ({
      ...obs,
      'fechaCreacion': changeFormatDate(removeTZ(obs.fechaCreacion), '-', '/')
    }))
  }, [observaciones])

  return (
    <div>
      <div className="row">
        <div className="col-lg-12">
          <MDBBreadcrumb
            onClick={() => {
              setCollapseDataSinister(!collapseDataSinister);
            }}
          >
            <MDBBreadcrumbItem>
              {
                (collapseDataSinister) ? <MDBIcon icon="angle-down" /> : <MDBIcon icon="angle-right" />
              }
              {"  Datos del Siniestro"}
            </MDBBreadcrumbItem>
          </MDBBreadcrumb>
          <MDBCollapse id="collapseDireccion" isOpen={collapseDataSinister}>
            <br />
            <DivInput className="container-fluid">
              {
                datosSiniestro.map((item, index) => {
                  const inputGroup = item.map((param, keyInput) => {
                    const value = param.field in nDetailSinister
                      ? nDetailSinister[param.field]
                      : '-'
                    return (
                      <div className="col-lg-4" key={keyInput}>
                        <MDBInput value={value || '-'} disabled label={param.label} size="sm" />
                      </div>
                    )
                  })
                  return <div className="row" key={index}>{inputGroup}</div>;
                })
              }
            </DivInput>
          </MDBCollapse>
        </div>
      </div>
      <br />
      <div className="row">
        <div className="col-lg-12">
          <MDBBreadcrumb
            onClick={() => {
              setCollapseDetailSinister(!collapseDetailSinister)
            }}
          >
            <MDBBreadcrumbItem>
              {(collapseDetailSinister) ? <MDBIcon icon="angle-down" /> : <MDBIcon icon="angle-right" />}
              {"  Detalle de Siniestro"}
            </MDBBreadcrumbItem>
          </MDBBreadcrumb>
          <MDBCollapse id="collapseDireccion" isOpen={collapseDetailSinister}>
            <br />
            <DivInput className="container-fluid">
              {
                detalleSiniestro.map((item, index) => {
                  const inputGroup = item.map((param, keyInput) => {
                    const value = param.field in nDetailSinister
                      ? nDetailSinister[param.field]
                      : '-'
                    return (
                      <div className="col-lg-4" key={keyInput}>
                        <MDBInput value={value || '-'} disabled label={param.label} size="sm" />
                      </div>
                    )
                  })
                  return <div className="row" key={index}>{inputGroup}</div>;
                })
              }
            </DivInput>
          </MDBCollapse>
        </div>
      </div>
      <br />
      <div className="row">
        <div className="col-lg-12">
          <MDBBreadcrumb
            onClick={() => {
              setCollapseInformationSinister(!collapseInformationSinister)
            }}
          >
            <MDBBreadcrumbItem>
              <MDBIcon
                icon={`angle-${collapseInformationSinister ? 'down' : 'right'}`}
              />
              {"  Información del contacto con el procurador"}
            </MDBBreadcrumbItem>
          </MDBBreadcrumb>
          <MDBCollapse id="collapseInformationSinister" isOpen={collapseInformationSinister}>
            <br />
            <DivInput className="container-fluid">
              {
                infoContactProcurador.map((item, index) => {
                  const inputGroup = item.map((param, keyInput) => {
                    const value = param.field in nDetailSinister
                      ? nDetailSinister[param.field]
                      : 'Este campo no existe'
                    return (
                      <div className="col-lg-4" key={keyInput}>
                        <MDBInput value={value || '-'} disabled label={param.label} size="sm" />
                      </div>
                    )
                  })
                  return <div className="row" key={index}>{inputGroup}</div>;
                })
              }
            </DivInput>
          </MDBCollapse>
        </div>
      </div>
      <br />
      <div className="row">
        <div className="col-lg-12">
          <MDBBreadcrumb
            onClick={() => {
              setCollapseTechnicalAdjustments(!collapseTechnicalAdjustments)
            }}
          >
            <MDBBreadcrumbItem>
              {
                (collapseTechnicalAdjustments) ? <MDBIcon icon="angle-down" /> : <MDBIcon icon="angle-right" />
              }
              {"  Ajustes Tecnicos"}
            </MDBBreadcrumbItem>
          </MDBBreadcrumb>
          <MDBCollapse id="collapseTechnicalAdjustments" isOpen={collapseTechnicalAdjustments}>
            <br />
            <div className="row">
              <div className="col-lg-12">
                <ElementTable
                  columns={TECHNICAL_ADJUSTMENTS_COLUMNS}
                  data={ajusteTecnico}
                  allRow
                />
              </div>
            </div>
          </MDBCollapse>
        </div>
      </div>
      <br />
      <div className="row">
        <div className="col-lg-12">
          <MDBBreadcrumb
            onClick={() => {
              setCollapseDeductibles(!collapseDeductibles);
            }}
          >
            <MDBBreadcrumbItem>
              {
                (collapseDeductibles) ? <MDBIcon icon="angle-down" /> : <MDBIcon icon="angle-right" />
              }
              {"  Deducibles"}
            </MDBBreadcrumbItem>
          </MDBBreadcrumb>
          <MDBCollapse id="collapseDeductibles" isOpen={collapseDeductibles}>
            <br />
            <div className="row">
              <div className="col-lg-12">
                <ElementTable
                  columns={DEDUCTIBLE_COLUMNS}
                  data={deducibles}
                  allRow
                />
              </div>
            </div>
          </MDBCollapse>
        </div>
      </div>
      <br />
      <div className="row">
        <div className="col-lg-12">
          <MDBBreadcrumb
            onClick={() => {
              setCollapseCircumstanceSinister(!collapseCircumstanceSinister)
            }}
          >
            <MDBBreadcrumbItem>
              {
                (collapseCircumstanceSinister) ? <MDBIcon icon="angle-down" /> : <MDBIcon icon="angle-right" />
              }
              {"  Circunstancias del Siniestro"}
            </MDBBreadcrumbItem>
          </MDBBreadcrumb>
          <MDBCollapse id="collapseCircumstanceSinister" isOpen={collapseCircumstanceSinister}>
            <br />
            <div className="row">
              <div className="col-lg-12">
                <ElementTable
                  columns={CIRCUNSTANCE_SINISTER_COLUMNS}
                  data={circunstancias}
                  allRow
                />
              </div>
            </div>
          </MDBCollapse>
        </div>
      </div>
      <br />
      <div className="row">
        <div className="col-lg-12">
          <MDBBreadcrumb
            onClick={() => {
              setCollapseInsuredDamages(!collapseInsuredDamages);
            }}
          >
            <MDBBreadcrumbItem>
              {
                (collapseInsuredDamages) ? <MDBIcon icon="angle-down" /> : <MDBIcon icon="angle-right" />
              }
              {"  Daños del bien asegurado"}
            </MDBBreadcrumbItem>
          </MDBBreadcrumb>
          <MDBCollapse id="collapseInsuredDamages" isOpen={collapseInsuredDamages}>
            <br />
            <div className="row">
              <div className="col-lg-12">
                <ElementTable
                  columns={INSURANCE_DAMAGE_COLUMNS}
                  data={danio}
                  allRow
                />
              </div>
            </div>
          </MDBCollapse>
        </div>
      </div>
      <br />
      <div className="row">
        <div className="col-lg-12">
          <MDBBreadcrumb
            onClick={() => {
              setCollapseObservations(!collapseObservations);
            }}
          >
            <MDBBreadcrumbItem>
              {
                (collapseObservations) ? <MDBIcon icon="angle-down" /> : <MDBIcon icon="angle-right" />
              }
              {"  Observaciones"}
            </MDBBreadcrumbItem>
          </MDBBreadcrumb>
          <MDBCollapse id="collapseObservations" isOpen={collapseObservations}>
            <br />
            <div className="row">
              <div className="col-lg-12">
                <ElementTable
                  columns={OBSERVATION_COLUMNS}
                  data={nObservaciones}
                  allRow
                />
              </div>
            </div>
          </MDBCollapse>
        </div>
      </div>
    </div>
  );
}

export default Detail;