import React, { useState, useMemo, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { bindActionCreators } from 'redux'
import ElementTable from 'components/table/elementTable'

import { policyOperations, policyLoaderKeys } from 'state/ducks/policy'

const COLUMNS = [
  {
    label: "Riesgo",
    prop: "riesgo",
    minWidth: 150
  },
  {
    label: "Tipo de producto",
    prop: "tipoProducto",
    minWidth: 150
  },
  {
    label: "Color",
    prop: "color",
    minWidth: 150
  },
  {
    label: "Probabilidad",
    prop: "probabilidad",
    minWidth: 150
  },
]

const ProductsSuggestNav = () => {
  const dispatch = useDispatch()
  const policyOperationsAction = useMemo(() => {
    return bindActionCreators(policyOperations, dispatch)
  }, [dispatch])

  //ERR_POLICY_SUGGEST
  const person = useSelector(state => state.person)
  const { dataList } = useSelector(state => state.policy.sugerido)
  const loaderVisible = useSelector(state => state.shared.loader.includes(policyLoaderKeys.POLICY_SUGGEST))

  useEffect(() => {
    let requestBody = {
      "tipoDocumento": (person.select && person.select.codTipoDocumento) ? `${person.select.codTipoDocumento}`:'',
      "numDocumento": (person.select && person.select.numDocumento) ? `${person.select.numDocumento}` : '',
    }

    policyOperationsAction.policySuggest(requestBody)

    // auditoria
    let requestLogging = {
      "path": window.location.href,
      "action": "POLICY_SUGGEST"
    }
    policyOperationsAction.auditLogging(requestLogging)
  }, [])

  const dataListFormat = useMemo(()=>{
    return dataList 
      ? dataList.map(item => ({...item, probabilidad: (item.probabilidad ? (Number(item.probabilidad)*100).toString().substring(0,5).concat('%'):'')})) 
      : []
  },[dataList])

  return (
    <div>
      <ElementTable
        columns={COLUMNS}
        data={dataListFormat}
        loader={loaderVisible}
        allRow
      />
    </div>
  )
}

export default ProductsSuggestNav;
