import React, { Component, useState, useEffect, useMemo, useRef } from 'react';
import { useSelector } from 'react-redux'
import ElementTable from 'components/table/elementTable';
import operationLoaderKeys from 'state/ducks/operation/loaderKeys'
import styled from 'styled-components'
import { MDBCol, MDBInput, MDBBreadcrumb, MDBBreadcrumbItem, MDBIcon, MDBCollapse } from "mdbreact";
import _ from 'lodash'
import { changeFormatDate } from 'state/utils/functionUtils'
import { Layout } from 'element-react';

const DivMDBBreadcrumb = styled.div`
  .breadcrumb {
    cursor: pointer;
  }
`;

const DivInput = styled.div`
  .md-form {
    margin-top: 0.5rem;
    margin-bottom: 0.5rem;
  }
`;

const COLUMNS = [
  { label: 'Número de documento', prop: 'numeroDocumento', minWidth: 150 },
  { label: 'Estado', prop: 'estado', minWidth: 150 },
  { label: 'Fecha de recepción', prop: 'fechaRecepcion', minWidth: 150 },
  { label: 'Monto beneficio', prop: 'montoBeneficio', minWidth: 150 }
]

const COLUMNS_DETALLE_GASTOS_PRESENTADOS = [
  { label: 'Item', prop: 'item', width: 70 },
  { label: 'Doc. Pago', prop: 'docPago', width: 120 },
  { label: 'Excluido', prop: 'excluido', width: 100 },
  { label: 'IGV', prop: 'igv', width: 70 },
  { label: 'Motivo', prop: 'motivo', width: 320 },
  { label: 'Ruc Proveedor', prop: 'rucProveedor', width: 145 },
  { label: 'Tipo Gasto', prop: 'tipoGasto', width: 115 },
  { label: 'Total', prop: 'total', width: 80 },
  { label: 'Venta', prop: 'venta', width: 80 },
]

const DETAIL = [
  [
    { label: 'Gastos presentados', field: 'gastosPresentados' },
    { label: 'Gastos excluidos', field: 'gastosExcluidos' },
    { label: 'Deducible', field: 'deducible' },
  ],
  [
    { label: 'Co-Asegurado', field: 'coaseguro' },
    { label: 'Monto beneficio', field: 'montoBeneficio' },
    { label: 'Moneda', field: 'moneda' },
  ],
  [
    { label: 'Diagnóstico', field: 'diagnostico' },
    { label: 'Estado', field: 'estado' },
    { label: 'Observaciones', field: 'observaciones' },
  ],
  [
    { label: 'Tipo de pago', field: 'tipoPago' },
    { label: 'Fecha de liquidación', field: 'fechaLiquidacion' },
    { label: 'Tipo de atención', field: 'tipoAtencion' },
  ],
  [
    { label: 'Usuario', field: 'usuario' },
    // { label: 'Doc. pago', field: 'docPago' },
    { label: 'Fecha Atencion', field: 'fechaAtencion' },
  ],
  [
    { label: 'Asegurado', field: 'asegurado' },
    { label: 'Producto', field: 'producto' },
    { label: 'Fecha Rechazo', field: 'fechaRechazo' },
  ],
  [
    { label: 'Motivo Rechazo', field: 'motivoRechazo' },
    { label: 'Número Póliza', field: 'nroPoliza' },
    { label: 'Banco Abono', field: 'bancoAbono' },
  ],
  [
    { label: 'Número Cuenta Titular', field: 'nroCuentaTitular' },
  ]
]

const notNull = value => (_.isNil(value) || `${value}`.trim() === '') ? '-' : value

const RefundNav = (props) => {
  const renderDataList = useRef(true)
  const [dataSelected, setDataSelected] = useState(null)
  const [collapseDetail, setCollapseDetail] = useState(true)
  const [dataListRefund, setDataListRefund] = useState([])

  const loaderVisible = useSelector(store => store.shared.loader.includes(operationLoaderKeys.REFUND_LIST))
  const refund = useSelector(store => store.operation.refund)
  //#region funtions
  const changeDataSelected = (data) => {
    setDataSelected(data);
  }

  const formatDataList = (_dataList) => {
    return _dataList.map((item) => ({
      ...item,
      "fechaLiquidacion": (item.fechaLiquidacion) ? changeFormatDate(item.fechaLiquidacion, '-', '/') : '',
      "fechaAtencion": (item.fechaAtencion) ? changeFormatDate(item.fechaAtencion, '-', '/') : '',
      "fechaRecepcion": (item.fechaRecepcion) ? changeFormatDate(item.fechaRecepcion, '-', '/') : '',
    }))
  }
  //#endregion

  //#region React -> UseEffects
  useEffect(() => {
    const refundFA = props.refundFA
    if (refundFA) {
      if (refundFA.callService) {
        props.getRefundList();
      } else {
        setDataListRefund(refundFA.data && refundFA.data.length > 0 ? formatDataList(refundFA.data) : [])
      }
    } else {
      props.getRefundList();
    }
  }, [])

  useEffect(() => {
    if (renderDataList.current) {
      renderDataList.current = false
      return
    }

    setDataListRefund(refund.dataList && refund.dataList.length > 0 ? formatDataList(refund.dataList) : [])
  }, [refund.dataList]);

  useEffect(() => {
    setTimeout(() => {
      dataSelected && document.getElementById('my_scrollable_refund_div').scrollIntoView({ behavior: 'smooth' })
    }, 500)
  }, [dataSelected])

  //#endregion

  return (
    <div className="row">
      <div className="col-lg-12" style={{ cursor: 'pointer' }}>
        <ElementTable
          columns={COLUMNS}
          data={dataListRefund}
          onChangeSelect={changeDataSelected}
          allRow
          loader={loaderVisible}
        />
      </div>
      <div className="col-lg-12">
        <div
          style={{ width: '10px', height: '10px' }}
          id="my_scrollable_refund_div"
        />
        {
          dataSelected &&
          <div>
            <br />
            <DivMDBBreadcrumb>
              <MDBBreadcrumb onClick={() => setCollapseDetail(!collapseDetail)}>
                <MDBBreadcrumbItem>
                  {collapseDetail
                    ? <MDBIcon icon="angle-down" />
                    : <MDBIcon icon="angle-right" />}
                  {"  Detalle de Reeembolso"}
                </MDBBreadcrumbItem>
              </MDBBreadcrumb>
            </DivMDBBreadcrumb>
            <br />
            <MDBCollapse
              id="collapseContactInfo"
              isOpen={collapseDetail}
              style={{ paddingLeft: '10px', }}
            >
              <DivInput>
                <div className="col-lg-12">
                  {DETAIL.map((group, index) => {
                    return (
                      <div className="row" key={index}>
                        {
                          // @ts-ignore
                          group.map((item, index) => {
                            const value = item.field in dataSelected
                              ? dataSelected[item.field]
                              : 'Este campo no existe en este trámite'

                            return (
                              <div className="col-lg-4" key={index}>
                                <MDBInput
                                  value={`${notNull(value)}`}
                                  label={item.label}
                                  size="sm"
                                  disabled
                                />
                              </div>
                            )
                          })
                        }
                      </div>
                    )
                  })}
                  <br />
                  <div>
                    <Layout.Row justify='center'>
                    
                        <DivMDBBreadcrumb>
                          <MDBBreadcrumb>
                            <MDBBreadcrumbItem>
                              {"  Detalle de Gastos Presentados"}
                            </MDBBreadcrumbItem>
                          </MDBBreadcrumb>
                        </DivMDBBreadcrumb>

                    </Layout.Row>
                  </div>
                  <br />
                  <ElementTable
                    data={dataSelected.detalleGastosPresentados || []}
                    columns={COLUMNS_DETALLE_GASTOS_PRESENTADOS}
                    allRow
                  />
                </div>
              </DivInput>
            </MDBCollapse>
          </div>
        }
      </div>
    </div>
  );

}

export default RefundNav;