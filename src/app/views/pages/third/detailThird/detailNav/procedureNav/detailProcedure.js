import React from 'react'
import { MDBCol, MDBInput, MDBBreadcrumb, MDBBreadcrumbItem, MDBIcon, MDBCollapse } from "mdbreact";
import _ from 'lodash'
import styled from 'styled-components'
import { changeFormatDate } from 'state/utils/functionUtils'
import { Input } from 'element-react';
import { Layout } from 'element-react'
const FIELD_GROUPS = [
  [
    { field: 'numTramite', label: 'N° Trámite' },
    { field: 'tipoOperacion', label: 'Tipo de Operación' },
    { field: 'estado', label: 'Estado' },
  ], [
    { field: 'contratante', label: 'Contratante' },
    { field: 'fecRegistro', label: 'Fecha Registro' },
    { field: 'ejecutivoComercial', label: 'Ejecutivo Comercial' },
  ], [
    { field: 'tipoCanal', label: 'Tipo Canal' },
    { field: 'canal', label: 'Canal' },
    { field: 'emisor', label: 'Emisor' },
  ], [
    { field: 'intermediario', label: 'Intermediario' },
    { field: 'codProducto', label: 'Código de Producto' },
    { field: 'desProducto', label: 'Producto' },
  ], [
    { field: 'glosaResumen', label: 'Glosa Resumen' },
    { field: 'numPoliza', label: 'N° Póliza' },
    { field: 'ramo', label: 'Ramo' },
  ], [
    { field: 'responsable', label: 'Responsable' },
    { field: 'fecSolicitud', label: 'Fecha Solicitud' },
    { field: 'esBroker', label: 'Es Broker' },
  ], [
    { field: 'observacion', label: 'Observaciones', large: true },
  ], [
    { field: 'bitacora', label: 'Bitácora', large: true },
  ],
]

const DivInput = styled.div`
  .breadcrumb {
    cursor: pointer;
  }
  .md-form {
    margin-top: 0.5rem;
    margin-bottom: 0.5rem;
  }
`;

const DivMDBBreadcrumb = styled.div`
  .breadcrumb {
  }
`;


const notNull = value => (_.isNil(value) || `${value}`.trim() === '') ? '-' : value

const DetailProcedure = ({ procedure = {} }) => {

  return (
    <DivInput>
      <div>
        <br />
        <Layout.Row  justify='center' style={{"marginBottom":"10px"}}>
          

              <DivMDBBreadcrumb>
                          <MDBBreadcrumb>
                            <MDBBreadcrumbItem>
                              {"Detalle del Trámite"}
                            </MDBBreadcrumbItem>
                          </MDBBreadcrumb>
                        </DivMDBBreadcrumb>
            </Layout.Row>

        

        
        <br />
      </div>

      {FIELD_GROUPS.map((group, index) => {
        return (
          <div className="form-group row" key={index}>
            {
              // @ts-ignore
              group.map((item, index) => {
                const value = item.field in procedure
                  ? procedure[item.field]
                  : 'Este campo no existe en éste trámite'

                return (
                  <div className={`col-lg${item.large ? '' : '-4'}`} key={index}>
                    {
                      item.large ?
                        (
                          <div>
                            <label>{item.label}</label>
                            <Input
                              value={value}
                              type="textarea"
                              autosize={{ minRows: 2, maxRows: 4 }}
                            />
                          </div>
                        )
                        :
                        <MDBInput
                          value={`${notNull(
                            `${item.label}`.includes('Fecha')
                              ? changeFormatDate(value, '-', '/')
                              : value
                          )}`}
                          label={item.label}
                          size="sm"
                          disabled
                        />
                    }
                    {/* {<MDBInput
                      value={`${notNull(
                        `${item.label}`.includes('Fecha')
                          ? changeFormatDate(value, '-', '/')
                          : value
                      )}`}
                      label={item.label}
                      size="sm"
                      disabled
                      {...(item.large ? { type: 'textarea' } : {})}
                    />} */}
                  </div>
                )
              })}
          </div>
        )
      })}


    </DivInput>
  );
}

export default DetailProcedure