//@ts-check
import React, { useState, useEffect, useRef, useMemo } from 'react'
import { useSelector } from 'react-redux'
import { Radio, Layout, Button } from 'element-react'
import Select from 'react-select'
import _ from 'lodash'

import ElementTable from 'components/table/elementTable'
import loaderKeys from 'state/ducks/salesforce/loaderKeys'
import { changeFormatDate, removeSeconds } from 'state/utils/functionUtils'
import { CASE_TYPES } from 'state/utils/constants'
import DetailCase from './casesNav/detailCase/detailCase'

const SCROLLABLE_DIV_DETAIL_ID = 'scrollable_div_detail'

const COLUMNS = [
  {
    label: "N° Caso",
    prop: "numCasoSf",
    minWidth: 75,
  },
  {
    label: "Fecha/Hora Apertura",
    prop: "fechaHoraApertura",
    minWidth: 130,
    render: (row, column, idx) => {
      let value = null
      if (row[column.prop]) {
        value = removeSeconds(changeFormatDate(row[column.prop], '-', '/').split('.')[0])
        const x = value.split(' ')
        value = `${x[0].split('/').reverse().join('/')} ${x[1]}`
      }
      return <span>{value}</span>
    },
  },
  {
    label: "Tipo de Registro del Caso",
    prop: "descripcionTipoCasoSf",
    minWidth: 210,
    render: (row, column, idx) => (
      <span>
        {row['descripcionTipoCasoSf'] ? row['descripcionTipoCasoSf'] : row['tipoCasoSf']}
      </span>
    ),
  },
  {
    label: "Tipo",
    prop: "tipo",
    minWidth: 150,
  },
  {
    label: "Categoría",
    prop: "categoria",
    minWidth: 150,
  },
  {
    label: "Asunto",
    prop: "asunto",
    minWidth: 150,
  },
  {
    label: "Tema",
    prop: "tema",
    minWidth: 150,
  },
  {
    label: "Estado",
    prop: "estadoCaso",
    minWidth: 150,
  },
  {
    label: "Propietario del Caso",
    prop: "propietarioCaso",
    minWidth: 200,
  },
]

function CasesNav({ getCasesList, casesFA }) {
  const rendered = useRef(true)
  const renderedDatalist = useRef(true)
  const [useRender, setUseRender] = useState(false)
  const [casesByStatus, setCasesByStatus] = useState('all')
  const [pagination, setPagination] = useState({ page: 1, rowsPerPage: 10 })
  const [selectedCase, setSelectedCase] = useState(null)
  const [dataListCases, setDataListCases] = useState([])
  const [selectedCaseTypes, setSelectedCaseTypes] = useState([])
  const loaderVisible = useSelector(state => state.shared.loader.includes(loaderKeys.CASES))
  const { dataList, pagination: paginationProp } = useSelector(state => state.salesforce.cases)

  useEffect(() => {
    //  * @param { boolean } firstAction.callService - Indica si se debe llamar al servicio
    //   * @param { object } [firstAction.select] - el objeto seleccionado
    //     * @param { object[] } [firstAction.data] - el objeto seleccionado
    //       * @param { number } [firstAction.type] - el tipo de registros segun el NAV
    if (casesFA && !casesFA.callService) {
      setUseRender(true)
      setCasesByStatus(casesFA.type === 1 ? 'open' : 'all')
      setDataListCases(casesFA.data && casesFA.data.length > 0 ? casesFA.data : [])
    } else {
      getCasesList()
    }
  }, [])

  useEffect(() => {
    if (rendered.current) {
      rendered.current = false
      return
    }
    if (useRender) {
      setUseRender(false)
      return
    }
    const { rowsPerPage, page } = pagination
    const types = selectedCaseTypes.map(item => item.value)
    setSelectedCase(null)
    getCasesList(rowsPerPage, page, casesByStatus, types)
  }, [casesByStatus, pagination])

  useEffect(() => {
    const scrollableDiv = document.getElementById(SCROLLABLE_DIV_DETAIL_ID)
    selectedCase && scrollableDiv &&
      scrollableDiv.scrollIntoView({ behavior: 'smooth' })
  }, [selectedCase])


  useEffect(() => {
    if (renderedDatalist.current) {
      renderedDatalist.current = false
      return
    }
    let auxDataList = (dataList && _.isArray(dataList)) ? dataList.map(item => ({ ...item })) : []
    setDataListCases(auxDataList)
  }, [dataList])



  const changePageCase = (page) => {
    setPagination({ ...pagination, page })
  }

  const changeRowsPerPageCase = (rowsPerPage) => {
    setPagination({ page: 1, rowsPerPage })
  }

  const clickRowTable = (_selectedCase) => {
    setSelectedCase(_selectedCase)
  }

  const onRadioChange = val => {
    setPagination({ ...pagination, page: 1 })
    setCasesByStatus(val)
  }

  const onChangeCaseType = selectedItems => {
    setSelectedCaseTypes(selectedItems ? selectedItems : [])
  }

  const filterCases = () => {
    const { rowsPerPage, page } = pagination
    const types = selectedCaseTypes.map(item => item.value)
    setSelectedCase(null)
    getCasesList(rowsPerPage, page, casesByStatus, types)
  }

  return (
    <div>
      <Layout.Row
        type="flex"
        justify="center"
        style={{ justifyContent: 'center', alignItems: 'center' }}
        gutter="20"
      >
        <Layout.Col span="4">
          <Radio value="open" checked={casesByStatus === 'open'} onChange={onRadioChange}>
            Casos Abiertos
          </Radio>
        </Layout.Col>

        <Layout.Col span="4">
          <Radio value="all" checked={casesByStatus === 'all'} onChange={onRadioChange}>
            Todos los Casos
          </Radio>
        </Layout.Col>

        <Layout.Col span="13">
          <div style={{ cursor: 'pointer' }}>
            <Select
              options={CASE_TYPES}
              onChange={onChangeCaseType}
              isClearable
              placeholder="Seleccione uno o más tipos de caso"
              isMulti
              styles={{
                multiValue: styles => ({ ...styles, backgroundColor: '#bedaff' }),
                multiValueLabel: styles => ({ ...styles, fontSize: 17 }),
              }}
              value={selectedCaseTypes}
            />
          </div>
        </Layout.Col>

        <Layout.Col span="3">
          <Button size="large" type="danger" onClick={filterCases}>
            Filtrar
          </Button>
        </Layout.Col>
      </Layout.Row>
      <br />
      <div className="col-lg-12" style={{ cursor: 'pointer' }}>
        <ElementTable
          data={dataListCases}
          loader={loaderVisible}
          columns={COLUMNS}
          onChangeSelect={clickRowTable}
          onChangePage={changePageCase}
          onChangeRowPage={changeRowsPerPageCase}
          pagination={paginationProp}
          rowPage={pagination.rowsPerPage}
          page={pagination.page}
        />
      </div>

      <div id={SCROLLABLE_DIV_DETAIL_ID} className="col-lg-12">
        <br />
        {selectedCase && <DetailCase selectedCase={selectedCase} />}
      </div>
    </div>
  )
}

export default React.memo(CasesNav)