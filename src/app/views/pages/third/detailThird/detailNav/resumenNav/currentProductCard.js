import React, { Component, useState, useEffect, useMemo, useRef } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Table, Pagination, Loading, Button } from 'element-react'
import { MDBIcon } from 'mdbreact'

import { personLoaderKeys } from 'state/ducks/person'
import { policyLoaderKeys, policyOperations } from 'state/ducks/policy'
import { log } from 'util';

/**
 * @typedef { {callService: boolean, select: object}} FirstAction
 *
 */

const CurrentProductCard = props => {
  const dispatch = useDispatch()
  const policyOperationsActions = useMemo(() => {
    return bindActionCreators(policyOperations, dispatch)
  }, [dispatch])

  const initSelectNav = useRef(true)
  const [nameNav, setNameNav] = useState('products')
  const [firstAction, setFirstAction] = useState(null)

  const {
    initFetch,
    dataList
  } = useSelector(store => store.policy.resume)
  const loaderVisible = useSelector(store => store.shared.loader.includes(policyLoaderKeys.POLICY_RESUME_PV_LIST))
  const lvPersonDetail = useSelector(store => store.shared.loader.includes(personLoaderKeys.PERSON_DETAIL))

  const { select: selectedPerson } = useSelector(store => store.person)

  //#region Funtions
  /**
   * Definimos las acciones que se pueden hacer en este CARD
   * 1. Seleccionar una poliza -> No hace busqueda, y me muestra su detalle
   * 2. Ver más -> Me dirige al TAB y hace busqueda general
   * 
   * @param{object} action
   * @param{string} [action.id]
   * @param{number} [action.index]
   * @param{boolean} action.show
   */
  
  const actionResumeCard = (action) => {
    let poliza = null
    let dataSelect = null
    if (action) {
      if (action.show) {
        setFirstAction({
          callService: true
        })
      } else {
        for (let i = 0; i < dataList.length; i++) {
          poliza = dataList[i]
          if (poliza.numPoliza && (poliza.numPoliza.toString() === action.id) && (i === action.index)) {
            dataSelect = poliza
            break;
          }
        }
        setFirstAction({
          callService: false,
          select: dataSelect
        })
      }
    }
  }
  //#endregion

  //#region React-useEffect
  useEffect(() => {
    if (initSelectNav.current) {
      initSelectNav.current = false
      return
    }
    props.selectNav(nameNav, firstAction)
  }, [firstAction])

  useEffect(() => {
    if (!initFetch) {
      let requestBody = {
        filter: {
          ideTercero: (selectedPerson.ideTercero) ? selectedPerson.ideTercero.toString() : "",
          codExterno: (selectedPerson.codExterno) ? selectedPerson.codExterno.toString() : "",
          codAfiliado: (selectedPerson.codAfiliado) ? selectedPerson.codAfiliado.toString() : "",
          numPlaca: "",
          numMotor: "",
          nombreTitular: "",
          estado: 'PV'
        },
        pagination: {
          filasPorPag: 10,
          pagActual: 1,
        }
      }
      policyOperationsActions.policyResumePvList(requestBody)

      // auditoria
      let requestLogging = {
        "path": window.location.href,
        "action": "POLICY_POLICY_RESUME_PV_LIST"
      }
      policyOperationsActions.auditLogging(requestLogging)
    }
  }, [])

  useEffect(() => {
    //if (!initFetch) 
      let requestBody = {
        filter: {
          ideTercero: (selectedPerson.ideTercero) ? selectedPerson.ideTercero.toString() : "",
          codExterno: (selectedPerson.codExterno) ? selectedPerson.codExterno.toString() : "",
          codAfiliado: (selectedPerson.codAfiliado) ? selectedPerson.codAfiliado.toString() : "",
          numPlaca: "",
          numMotor: "",
          nombreTitular: "",
          estado: 'PV'
        },
        pagination: {
          filasPorPag: 10,
          pagActual: 1,
        }
      }
      policyOperationsActions.policyResumePvList(requestBody)
    //}
  }, [selectedPerson])
  //#endregion
   
  //#region React-UseMemo
  const getDataResume = useMemo(() => {
    return dataList.map((item, index) => (
      <li key={index}>
        <a
          href="#/"
          style={{
            textDecoration: 'underline',
            color: 'blue'
          }}
          onClick={() => {
            actionResumeCard({
              id: item.numPoliza ? item.numPoliza.toString() : '',
              index: index,
              show: false
            })
          }}
        >
          {item.desProducto ? item.desProducto : ' '} {item.numPoliza ? (' - ' + item.numPoliza) : ' '} {item.numCertificado ? (' - ' + item.numCertificado) : ' '}
        </a>
      </li>
    ))
  }, [dataList])
  //#endregion
  

  return (
    <div className="card resumeTabCard">
      <h5 className="card-header">Productos Vigentes</h5>
      <div className="card-body" style={{ maxHeight: '235px' }}>
        {
          loaderVisible
            ? <div>
                <br />
                <br />
                <Loading text="Cargando...">
                  <span></span>
                  <span></span>
                  <span></span>
                </Loading>
                <br />
              </div>

            : <div>
              {
                (dataList && dataList.length > 0)
                  ? (
                    <div>
                      <p
                        style={{
                          marginTop: "-10px",
                          marginBottom: '0.5rem'
                        }}
                      >Tiene <strong>{dataList.length} productos:</strong></p>
                      <div
                        style={{
                          height: '130px',
                          overflowX: 'auto'
                        }}
                      >
                        <ul
                          style={{
                            // width: '400px',
                            // lineHeight: '30px',
                            fontSize: '13px',
                            // listStyle: 'none',
                            position: 'relative',
                            right: '20px'
                          }}
                        >
                          {getDataResume}
                        </ul>
                      </div>
                      <div style={{ textAlign: 'right' }}>
                        <a href='#/' onClick={() => { actionResumeCard({ 'show': true }) }}>Ver mas...</a>
                      </div>
                    </div>
                  )
                  : (<div>
                    <Button type='danger'>
                      <MDBIcon icon="exclamation-triangle" />
                    </Button> No se registran Productos Vigentes.
                      </div>)
              }
            </div>
        }
      </div>
    </div>
  )
}

export default CurrentProductCard 