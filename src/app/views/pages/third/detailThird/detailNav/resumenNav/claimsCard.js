import React, { Component, useMemo, useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { bindActionCreators } from 'redux'
import { salesforceLoaderKeys, salesforceOperations } from 'state/ducks/salesforce'
import { Layout, Loading } from 'element-react';
import { MDBIcon } from 'mdbreact'

const STATE_CLOSE_CASES = [
  'ANULADO',
  'CERRADO',
  'CLOSED',
  'CERRADO FALLIDO',
  'NO GESTIONABLE',
  'NO CONTACTADO',
  'NO RETENIDO',
  'RETENIDO CONFIRMADO',
  'RETENIDO NO CONFIRMADO',
  'RETENIDO',
  'NO RENOVADO',
  'RENOVACION CONFIRMADA',
  'RENOVACION NO CONFIRMADA',
  'NO COBRADO',
  'COBRANZA CONFIRMADA',
  'COBRANZA NO CONFIRMADA'
]


const ClaimsCard = props => {
  const dispatch = useDispatch()
  const salesforceOperationsActions = useMemo(() => {
    return bindActionCreators(salesforceOperations, dispatch)
  }, [dispatch])

  const initSelectNav = useRef(true)
  const [nameNav, setNameNav] = useState('cases')
  const [firstAction, setFirstAction] = useState(null)

  const {
    claims: {
      initFetch: initFetchClaims,
      dataList: dataListClaims
    }
  } = useSelector(store => store.salesforce.resume)
  const lvClaims = useSelector(store => store.shared.loader.includes(salesforceLoaderKeys.RESUME_CLAIMS))
  const { select: selectedPerson } = useSelector(store => store.person)


  //#region Funtions
  /**
   * Definimos las acciones que se pueden hacer en este CARD
   * 1. Seleccionar una poliza -> No hace busqueda, y me muestra su detalle
   * 2. Ver más -> Me dirige al TAB y hace busqueda general
   * 
   * @param {object} action
   * @param {object[]} [action.data]
   * @param {boolean} action.show
   * @param {string} action.nameNav
   * @param {number} action.type
   */
  const actionResumeCard = (action) => {
    if (action) {
      setNameNav(action.nameNav)
      setFirstAction({
        callService: action.show,
        data: action.data,
        type: action.type
      })
    }
  }

  /**
   * Esta funcion sera el intermediario entre la funciona
   * 'actionResumeCard', ya que esta se encarga de modificar
   * el objeto firstAction
   * 
   * @param {string} name - hace referencia a que NAV va afectar
   * @param {number} type - 1 si es activo, 2 si son del ultimo año 
   */
  const accionIntermediaCard = (name, type) => {
    let data = null
    if (name === 'cases') {
      if (type === 1 && countClaimsActive === 0) {
        return
      }
      if (type === 2 && countClaimsTotal === 0) {
        return
      }
      data = type === 1 ? dataListClaims.filter(c => !STATE_CLOSE_CASES.includes(c.estadoCaso)) : dataListClaims
    }

    actionResumeCard({
      show: false,
      nameNav: name,
      data,
      type
    })
  }
  //#endregion

  useEffect(() => {
    if (initSelectNav.current) {
      initSelectNav.current = false
      return
    }
    props.selectNav(nameNav, firstAction)
  }, [firstAction])


  useEffect(() => {
    let date = new Date()
    date.setFullYear(date.getFullYear() - 1)
    let dateFull = date.toISOString().substring(0, 10).concat('T00:00:00')

    if (!initFetchClaims) {
      const requestClaims = {
        "filter": {
          "codExterno": selectedPerson.codExterno ? selectedPerson.codExterno.toString() : '',
          "dateFrom": dateFull,
          "types": [
            "F1_Reclamaciones_CNT",
            "F1_ReclamacionEPS",
            "F1_ReclamacionLegal",
            "F1_ReclamacionSeguros",
          ]
        },
        "pagination": {
          "filasPorPag": 100,
          "pagActual": 1
        }
      }

      salesforceOperationsActions.resumeClaims(requestClaims)

      // auditoria
      let requestLogging = {
        "path": window.location.href,
        "action": "SALESFORCE_RESUME_CLAIMS"
      }
      salesforceOperationsActions.auditLogging(requestLogging)
    }
  }, [])

  useEffect(() => {
    let date = new Date()
    date.setFullYear(date.getFullYear() - 1)
    let dateFull = date.toISOString().substring(0, 10).concat('T00:00:00')

    const requestClaims = {
      "filter": {
        "codExterno": selectedPerson.codExterno ? selectedPerson.codExterno.toString() : '',
        "dateFrom": dateFull,
        "types": [
          "F1_Reclamaciones_CNT",
          "F1_ReclamacionEPS",
          "F1_ReclamacionLegal",
          "F1_ReclamacionSeguros",
        ]
      },
      "pagination": {
        "filasPorPag": 100,
        "pagActual": 1
      }
    }

    salesforceOperationsActions.resumeClaims(requestClaims)

    // auditoria
    let requestLogging = {
      "path": window.location.href,
      "action": "SALESFORCE_RESUME_CLAIMS"
    }
    salesforceOperationsActions.auditLogging(requestLogging)
  
  }, [selectedPerson])

  const countClaimsActive = useMemo(() => {
    return dataListClaims ? dataListClaims.filter(c => !STATE_CLOSE_CASES.includes(c.estadoCaso)).length : 0
  }, [dataListClaims])
  const countClaimsTotal = useMemo(() => {
    return dataListClaims ? dataListClaims.length : 0
  }, [dataListClaims])

  return (
    <div className="card resumeTabCard">
      <h5 className="card-header">Reclamos</h5>
      <div className="card-body">
        <div
          style={{
            justifyContent: "space-around",
            display: 'flex',
            flexDirection: 'column',
            width: '100%',
            height: '100%',
          }}
        >
          {
            lvClaims
              ?
              <div>
                <br />
                <Loading text="Cargando...">
                  <span></span>
                  <span></span>
                  <span></span>
                </Loading>
                <br />
              </div>
              :
              <Layout.Row gutter='10'>
                <Layout.Col span='5'>
                  <MDBIcon icon="check-circle"
                    style={{
                      fontSize: '35px',
                      color: 'green'
                    }}
                  />
                </Layout.Col>
                <Layout.Col span='19'>
                  {
                    countClaimsActive
                      ?
                      <p>
                        Tiene <strong><a href='#/' style={{ textDecoration: 'underline' }} onClick={() => { accionIntermediaCard('cases', 1) }} >{countClaimsActive === 1 ? countClaimsActive + ' reclamo ' : countClaimsActive + ' reclamos'} </a></strong> en curso
                      </p>
                      :
                      <p>
                        No tienen <strong><a href='#/' style={{ textDecoration: 'underline' }} >reclamos</a></strong> en curso
                      </p>
                  }
                </Layout.Col>
              </Layout.Row>
          }
          {
            lvClaims
              ?
              <div>
                <br />
                <br />
                <br />
                <Loading text="Cargando...">
                  <span></span>
                  <span></span>
                  <span></span>
                </Loading>
                <br />
              </div>
              :
              <Layout.Row gutter='10'>
                <Layout.Col span='5'>
                  <MDBIcon icon="exclamation-triangle"
                    style={{
                      fontSize: '35px',
                      color: 'red'
                    }}
                  />
                </Layout.Col>
                <Layout.Col span='19'>
                  {
                    countClaimsTotal
                      ?
                      <p>
                        Ha tenido <strong><a href='#/' style={{ textDecoration: 'underline' }} onClick={() => { accionIntermediaCard('cases', 2) }} >{countClaimsTotal === 1 ? countClaimsTotal + ' reclamo ' : countClaimsTotal + ' reclamos'} </a></strong> {countClaimsTotal === 1 ? ' registrado ' : ' registrados '} en el ultimo año
                      </p>
                      :
                      <p>
                        No ha tenido <strong><a href='#/' style={{ textDecoration: 'underline' }} >reclamos</a></strong> registrados en el ultimo año
                      </p>
                  }
                </Layout.Col>
              </Layout.Row>
          }
        </div>
      </div>
    </div>
  )
}

export default ClaimsCard 