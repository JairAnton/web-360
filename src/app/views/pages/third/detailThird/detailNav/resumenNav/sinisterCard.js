import React, { Component, useState, useEffect, useMemo, useRef } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Table, Pagination, Loading, Button, Layout } from 'element-react'
import { MDBIcon } from 'mdbreact'

import { sinisterLoaderKeys, sinisterOperations } from 'state/ducks/sinister'
import { operationLoaderKeys, operationOperations } from 'state/ducks/operation'

const SinisterCard = props => {
  const dispatch = useDispatch()
  const sinisterOperationsActions = useMemo(() => {
    return bindActionCreators(sinisterOperations, dispatch)
  }, [dispatch])
  const operationOperationsActions = useMemo(() => {
    return bindActionCreators(operationOperations, dispatch)
  }, [dispatch])

  const initSelectNav = useRef(true)
  const [nameNav, setNameNav] = useState('sinister')
  const [firstAction, setFirstAction] = useState(null)

  const {
    initFetch: initFetchSinister,
    dataList
  } = useSelector(store => store.sinister.resume)
  const loaderVisibleSinister = useSelector(store => store.shared.loader.includes(sinisterLoaderKeys.SINISTER_RESUME_LIST))

  const {
    initFetch: initFetchOperation,
    dataList: dataListOperation
  } = useSelector(store => store.operation.resume)
  const loaderVisibleOperation = useSelector(store => store.shared.loader.includes(operationLoaderKeys.REFUND_RESUME_LIST))

  const { select: selectedPerson, personType } = useSelector(store => store.person)


  //#region Funtions
  /**
   * Definimos las acciones que se pueden hacer en este CARD
   * 1. Seleccionar una poliza -> No hace busqueda, y me muestra su detalle
   * 2. Ver más -> Me dirige al TAB y hace busqueda general
   * 
   * @param {object} action
   * @param {object[]} [action.data]
   * @param {boolean} action.show
   * @param {string} action.nameNav
   */
  const actionResumeCard = (action) => {
    if (action) {
      setNameNav(action.nameNav)
      setFirstAction({
        callService: action.show,
        data: action.data
      })
    }
  }

  /**
   * Esta funcion sera el intermediario entre la funciona
   * 'actionResumeCard', ya que esta se encarga de modificar
   * el objeto firstAction
   * 
   * @param {string} name - hace referencia a que NAV va afectar
   * @param {number} type - 1 si es activo, 2 si son del ultimo año 
   */
  const accionIntermediaCard = (name, type, typeSinister = 'vehiculares') => {
    let data = null
    if (name === 'sinister') {
      if (typeSinister === 'vehiculares') {
        if ((type === 1 && countDataActiveVehicular === 0) || (type === 2 && countDataTotalVehicular === 0)) {
          return
        }
        data = type === 1 ? dataList.filter(s => s.estadoSiniestro === 'Activo' && s.tipoSiniestro === "VEHICULARES") : dataList.filter(s => s.tipoSiniestro === "VEHICULARES")
      }

      if (typeSinister === 'rrgg') {
        if ((type === 1 && countDataActiveRrgg === 0) || (type === 2 && countDataTotalRrgg === 0)) {
          return
        }
        data = type === 1 ? dataList.filter(s => s.estadoSiniestro === 'Activo' && s.tipoSiniestro === "RIESGOS GENERALES") : dataList.filter(s => s.tipoSiniestro === "RIESGOS GENERALES")
      }
    }
    if (name === 'refund') {
      if ((type === 2 || type === 1) && countDataTotalOperation === 0) {
        return
      }
      data = dataListOperation
    }


    actionResumeCard({
      show: false,
      nameNav: name,
      data
    })
  }
  //#endregion

  //#region React-useEffect
  useEffect(() => {
    if (initSelectNav.current) {
      initSelectNav.current = false
      return
    }
    props.selectNav(nameNav, firstAction)
  }, [firstAction])

  useEffect(() => {
    if (!initFetchSinister) {
      let requestBody = {
        filter: {
          "codExterno": (selectedPerson.codExterno) ? selectedPerson.codExterno.toString() : "",
          "codAfiliado": (selectedPerson.codAfiliado) ? selectedPerson.codAfiliado.toString() : "",
          "casoBizagi": "",
          "codProdAcselx": "",
          "numPoliza": "",
          "numCertificado": "",
          "core": 'AX',
        },
        pagination: {
          filasPorPag: 10,
          pagActual: 1,
        }
      }

      sinisterOperationsActions.sinisterResumeList(requestBody)

      // auditoria
      let requestLogging = {
        "path": window.location.href,
        "action": "SINISTER_RESUME_LIST"
      }
      sinisterOperationsActions.auditLogging(requestLogging)
    }

    if (!initFetchOperation) {
      const date = new Date()
      date.setFullYear(date.getFullYear() - 1)
      const requestBody = {
        "codAfiliado": (selectedPerson.codAfiliado) ? selectedPerson.codAfiliado.toString() : "",
        "codProdAcselx": "",
        "numPoliza": "",
        "fecMinRegistro": `${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}`,
        "enLinea": ""
      }
      operationOperationsActions.refundResumeList(requestBody)

      // auditoria
      let requestLogging = {
        "path": window.location.href,
        "action": "OPERATIONS_REFUND_RESUME_LIST"
      }
      operationOperationsActions.auditLogging(requestLogging)
    }
  }, [])

  //#endregion

  useEffect(() => {
      let requestBody = {
        filter: {
          "codExterno": (selectedPerson.codExterno) ? selectedPerson.codExterno.toString() : "",
          "codAfiliado": (selectedPerson.codAfiliado) ? selectedPerson.codAfiliado.toString() : "",
          "casoBizagi": "",
          "codProdAcselx": "",
          "numPoliza": "",
          "numCertificado": "",
          "core": 'AX',
        },
        pagination: {
          filasPorPag: 10,
          pagActual: 1,
        }
      }
      sinisterOperationsActions.sinisterResumeList(requestBody)
      // auditoria
      let requestLoggingSinister = {
        "clientId": 'log',
      }
      sinisterOperationsActions.auditLogging(requestLoggingSinister)

      const date = new Date()
      date.setFullYear(date.getFullYear() - 1)
      const requestBody2 = {
        "codAfiliado": (selectedPerson.codAfiliado) ? selectedPerson.codAfiliado.toString() : "",
        "codProdAcselx": "",
        "numPoliza": "",
        "fecMinRegistro": `${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}`,
        "enLinea": ""
      }
      operationOperationsActions.refundResumeList(requestBody2)

      // auditoria
      let requestLogging = {
        "path": window.location.href,
        "action": "OPERATIONS_REFUND_RESUME_LIST"
      }
      operationOperationsActions.auditLogging(requestLogging)
    
  }, [selectedPerson])


  //#region React-UseMemo
  //SINIESTRO-VEHICULAR
  const countDataActiveVehicular = useMemo(() => {
    return dataList ? dataList.filter(s => s.estadoSiniestro === 'Activo' && s.tipoSiniestro === "VEHICULARES").length : 0
  }, [dataList])

  const countDataTotalVehicular = useMemo(() => {
    return dataList && dataList.length > 0 ? dataList.filter(s => s.tipoSiniestro === "VEHICULARES").length : 0
  }, [dataList])

  //SINIESTRO-RRGG
  const countDataActiveRrgg = useMemo(() => {
    return dataList ? dataList.filter(s => s.estadoSiniestro === 'Activo' && s.tipoSiniestro === "RIESGOS GENERALES").length : 0
  }, [dataList])

  const countDataTotalRrgg = useMemo(() => {
    return dataList && dataList.length > 0 ? dataList.filter(s => s.tipoSiniestro === "RIESGOS GENERALES").length : 0
  }, [dataList])


  //OPERACIONES
  const countDataTotalOperation = useMemo(() => {
    return dataListOperation && dataListOperation.length > 0 ? dataListOperation.length : 0
  }, [dataListOperation])
  //#endregion

  return (
    <div className="card resumeTabCard"    >
      <h5 className="card-header">Siniestros</h5>
      <div className="card-body">
        <div
          style={{
            justifyContent: "space-around",
            display: 'flex',
            flexDirection: 'column',
            width: '100%',
            height: '100%',
          }}
        >
          <Layout.Row gutter='10'>
            <Layout.Col span='5'>
              <MDBIcon icon="car-side"
                style={{
                  fontSize: '35px'
                }}
              />
            </Layout.Col>
            <Layout.Col span='19'>
              {
                loaderVisibleSinister
                  ?
                  <div>
                    <br />
                    <Loading text="Cargando...">
                      <span></span>
                      <span></span>
                      <span></span>
                    </Loading>
                    <br />
                  </div>
                  :
                  <p>
                    Tiene <strong><a href='#/' style={{ textDecoration: 'underline' }} onClick={() => { accionIntermediaCard('sinister', 1) }}>{countDataActiveVehicular} siniestro</a></strong> en curso y
                    ha tenido <strong><a href="#/" style={{ textDecoration: 'underline' }} onClick={() => { accionIntermediaCard('sinister', 2) }}>{countDataTotalVehicular} siniestros</a></strong> en el ultimo Año
                  </p>
              }
            </Layout.Col>
          </Layout.Row>
          {
            personType === 'PN'
              ? (
                <Layout.Row gutter='10'>
                  <Layout.Col span='5'>
                    <MDBIcon icon="briefcase-medical"
                      style={{
                        fontSize: '35px'
                      }}
                    />
                  </Layout.Col>
                  <Layout.Col span='19'>
                    {
                      loaderVisibleOperation
                        ?
                        <div>
                          <br />
                          <Loading text="Cargando...">
                            <span></span>
                            <span></span>
                            <span></span>
                          </Loading>
                          <br />
                        </div>
                        :
                        (countDataTotalOperation
                          ?
                          <p>
                            Usted a tenido <strong> <a href="#/" style={{ textDecoration: 'underline' }} onClick={() => { accionIntermediaCard('refund', 2) }}> {countDataTotalOperation} reembolsos</a></strong> en el ultimo Año
                    </p>
                          :
                          <p>
                            No ha tenido <strong> <a href="#/" style={{ textDecoration: 'underline' }} onClick={() => { accionIntermediaCard('refund', 2) }}>ningun reembolsos</a></strong> en el ultimo Año
                    </p>
                        )
                    }
                  </Layout.Col>
                </Layout.Row>
              )
              : (
                <Layout.Row gutter='10'>
                  <Layout.Col span='5'>
                    <MDBIcon icon="home"
                      style={{
                        fontSize: '35px'
                      }}
                    />
                  </Layout.Col>
                  <Layout.Col span='19'>
                    {
                      loaderVisibleSinister
                        ?
                        <div>
                          <br />
                          <Loading text="Cargando...">
                            <span></span>
                            <span></span>
                            <span></span>
                          </Loading>
                          <br />
                        </div>
                        :
                        (
                          <p>
                            Tiene <strong><a href='#/' style={{ textDecoration: 'underline' }} onClick={() => { accionIntermediaCard('sinister', 1, 'rrgg') }}>{countDataActiveRrgg} siniestro</a></strong> en curso y
                            ha tenido <strong><a href="#/" style={{ textDecoration: 'underline' }} onClick={() => { accionIntermediaCard('sinister', 2, 'rrgg') }}>{countDataTotalRrgg} siniestros</a></strong> en el ultimo Año
                          </p>
                        )
                    }
                  </Layout.Col>
                </Layout.Row>
              )
          }
        </div>
      </div>
    </div>
  )
}

export default SinisterCard 