import React, { Component, useMemo, useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { bindActionCreators } from 'redux'
import { personLoaderKeys } from 'state/ducks/person'
import { Layout, Loading } from 'element-react';
import { MDBIcon } from 'mdbreact'

//VERDE
const NSE_SBS_1 = [
  ['A1', 'A2', 'B1'],
  ['OK', 'CPP', 'Sin reporte'],
]
//LILA
const NSE_SBS_2 = [
  ['Sin reporte', 'B1', 'C1', 'C2', 'D1', 'D2', 'E'],
  ['OK', 'CPP', 'Sin reporte'],
]
const NSE_SBS_3 = [
  ['A1', 'A2', 'B1'],
  ['Deficiente', 'Dudoso', 'Perdida'],
]
//ROJO
const NSE_SBS_4 = [
  ['Sin reporte', 'B1', 'C1', 'C2', 'D1', 'D2', 'E'],
  ['Deficiente', 'Dudoso', 'Perdida'],
]

const CustomerRatingCard = props => {
  const { tratamiento } = useSelector(store => store.person.select)
  const { sbs, nse, segmento, grupoEconomico, sector } = useSelector(store => store.person.detail)

  const lvPersonDetail = useSelector(store => store.shared.loader.includes(personLoaderKeys.PERSON_DETAIL))

  const scoringVida = useMemo(() => {
    if (sbs && nse) {
      //Verde
      if (NSE_SBS_1[0].includes(nse) && NSE_SBS_1[1].includes(sbs)) {
        return 'rgb(146,208,80)'
      }
      //LILA
      if ((NSE_SBS_2[0].includes(nse) && NSE_SBS_2[1].includes(sbs)) || (NSE_SBS_3[0].includes(nse) && NSE_SBS_3[1].includes(sbs))) {
        return 'rgb(249,230,153)'
      }
      //ROJO
      if (NSE_SBS_4[0].includes(nse) && NSE_SBS_4[1].includes(sbs)) {
        return 'rgb(230,47,45)'
      }
    }

    return 'rgb(233,236,239)'
  }, [sbs, nse])

  return (
    <div className="card resumeTabCard">
      <h5 className="card-header">Calificación Cliente</h5>
      <div className="card-body">
        {
          lvPersonDetail
            ? (
              <div>
                <br />
                <br />
                <Loading text="Cargando...">
                  <span></span>
                  <span></span>
                  <span></span>
                </Loading>
                <br />
              </div>
            )
            : (
              props.personType === 'PN'
                ?
                (
                  <div className='calificacionClienteCard'>
                    <Layout.Row gutter='10'>
                      <Layout.Col span='10'>
                        <strong>Segmento:</strong>
                      </Layout.Col>
                      <Layout.Col span='14'>
                        {
                          segmento ? segmento : '-'
                        }
                      </Layout.Col>
                    </Layout.Row>
                    <Layout.Row gutter='10'>
                      <Layout.Col span='10'>
                        <strong>Tratamiento:</strong>
                      </Layout.Col>
                      <Layout.Col span='14'>
                        {
                          tratamiento ? tratamiento : '-'
                        }
                      </Layout.Col>
                    </Layout.Row>
                    <Layout.Row gutter='10'
                      style={{
                        display: 'flex',
                        alignItems: 'center'
                      }}
                    >
                      <Layout.Col span='10'>
                        <strong>Scoring Vida:</strong>
                      </Layout.Col>
                      <Layout.Col span='14'>
                        <div
                          style={{
                            width: '35px',
                            height: '35px',
                            backgroundColor: scoringVida,
                            borderRadius: '18px'
                          }}
                        >
                        </div>
                      </Layout.Col>
                    </Layout.Row>
                  </div>
                )
                : (
                  <div className='calificacionClienteCard'>
                    <Layout.Row gutter='10'>
                      <Layout.Col span='10'>
                        <strong>Segmento:</strong>
                      </Layout.Col>
                      <Layout.Col span='14'>
                        {
                          segmento ? segmento : '-'
                        }
                      </Layout.Col>
                    </Layout.Row>
                    <Layout.Row gutter='10'>
                      <Layout.Col span='10'>
                        <strong>Grupo Ecónomico:</strong>
                      </Layout.Col>
                      <Layout.Col span='14'>
                        {
                          grupoEconomico ? grupoEconomico : '-'
                        }
                      </Layout.Col>
                    </Layout.Row>
                    <Layout.Row gutter='10'>
                      <Layout.Col span='10'>
                        <strong>Sector:</strong>
                      </Layout.Col>
                      <Layout.Col span='14'>
                        {
                          sector ? sector : '-'
                        }
                      </Layout.Col>
                    </Layout.Row>
                  </div>
                )
            )
        }
      </div>
    </div >
  )
}

export default CustomerRatingCard 