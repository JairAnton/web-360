import React, { Component, useMemo } from 'react'
import { useSelector } from 'react-redux';
import { MDBIcon } from 'mdbreact'
import { Layout } from 'element-react'

const stateFace = [
  {
    key: 1, label: 'Super triste',
    value: <MDBIcon icon="tired" className="iconFontSize35" style={{ color: 'rgb(230,49,62)' }} />
  },
  {
    key: 2, label: 'Triste',
    value: <MDBIcon icon="frown" className="iconFontSize35" style={{ color: 'rgb(228,96,111)' }} />
  },
  {
    key: 3, label: 'Neutro',
    value: <MDBIcon icon="meh" className="iconFontSize35" style={{ color: 'rgb(193,193,193)' }} />
  },
  {
    key: 4, label: 'Feliz',
    value: <MDBIcon icon="grin-alt" className="iconFontSize35" style={{ color: 'green' }} />
  },
  {
    key: 5, label: 'Super feliz',
    value: <MDBIcon icon="grin-stars" className="iconFontSize35" style={{ color: 'rgb(146,208,80)' }} />
  },
]

const QuizCard = props => {
  const { detail } = useSelector(state => state.person)

  const [nps, npsState, isn, isnState] = useMemo(() => {
    let {
      nps: _nps,
      isn: _isn
    } = detail
    let _npsState = (<div>Plomo</div>)
    let _isnState = (<div>Plomo</div>)

    _nps = 5
    _isn = 3

    if (_nps) {
      if (_nps >= 1 && _nps <= 6) {
        _npsState = stateFace[1].value
      } else if (_nps >= 7 && _nps <= 8) {
        _npsState = stateFace[2].value
      } else if (_nps >= 9 && _nps <= 10) {
        _npsState = stateFace[3].value
      }
    } else {
      _nps = 0
    }

    if (_isn) {
      let auxIsnState = stateFace.filter(face => face.key === _isn)
      _isnState = auxIsnState.length > 0 ? auxIsnState[0].value : _isnState
    } else {
      _isn = 0
    }

    return [_nps, _npsState, _isn, _isnState]
  }, [detail])

  return (
    <div className="card resumeTabCard">
      <h5 className="card-header">Encuesta</h5>
      <div className="card-body">
        <Layout.Row type='flex' justify='space-around'
          style={{
            height: '100%'
          }}
        >
          <Layout.Col span='12'
            style={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >
            <div
              style={{ height: '35%', display: 'flex', alignItems: 'center', }}
            ><strong>NPS</strong></div>
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'space-around',
                height: '50%',
              }}
            >
              {npsState}

              <div>Valor: {nps}</div>
            </div>
          </Layout.Col>
          <Layout.Col span='12'
            style={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >
            <div
              style={{ height: '35%', display: 'flex', alignItems: 'center', }}
            ><strong>ISN</strong></div>
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'space-around',
                height: '50%',
              }}
            >
              {isnState}

              <div>Valor: {isn}</div>
            </div>
          </Layout.Col>
        </Layout.Row>
      </div>
    </div>
  )
}

export default QuizCard 