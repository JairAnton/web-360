import React from 'react'
import { useSelector } from 'react-redux'
import ModalProductsOffer from './modalProductsOffer'
import Carousel from 'nuka-carousel'
import styled from 'styled-components';
import {
  MDBIcon
} from 'mdbreact'
import { Layout, Card, Button } from 'element-react';

import CurrentProductCard from './resumenNav/currentProductCard';
import QuizCard from './resumenNav/quizCard';
import CustomerRatingCard from './resumenNav/customerRatingCard';
import CasesCard from './resumenNav/casesCard';
import ClaimsCard from './resumenNav/claimsCard';
import SinisterCard from './resumenNav/sinisterCard';
import ProductionCard from './resumenNav/productionCard';

/**
 * 
 * @callback FunSelectNav
 * @param {String} nameNav
 * @param {Object} [firstAction]
 * @return undefined
 */

/**
 * 
 * @typedef {ResumeNav} ResumeNav
 * @param {object} props
 * @param {FunSelectNav} props.selectNav
 * 
 */
const ResumeNav = ({ selectNav }) => {
  const person = useSelector(store => store.person)

  return (
    <div>
      <Layout.Row gutter="20">
        <Layout.Col span="8" style={{ paddingTop: '20px' }}>
          <CurrentProductCard selectNav={selectNav}  />
        </Layout.Col>
        {
          (person && person.personType === 'PN')
            ? (
              <Layout.Col span="8" style={{ paddingTop: '20px' }}>
                <QuizCard selectNav={selectNav} />
              </Layout.Col>
            )
            : null

        }
        <Layout.Col span="8" style={{ paddingTop: '20px' }}>
          <CustomerRatingCard selectNav={selectNav} personType={person.personType} />
        </Layout.Col>

        {
          (person && person.personType === 'PN')
            ? null
            : (
              <Layout.Col span="8" style={{ paddingTop: '20px' }}>
                <SinisterCard selectNav={selectNav} />
              </Layout.Col>
            )

        }

        <Layout.Col span="8" style={{ paddingTop: '20px' }}>
          <CasesCard selectNav={selectNav} />
        </Layout.Col>
        <Layout.Col span="8" style={{ paddingTop: '20px' }}>
          <ClaimsCard selectNav={selectNav} />
        </Layout.Col>
        {
          (person && person.personType === 'PN')
            ? (
              <Layout.Col span="8" style={{ paddingTop: '20px' }}>
                <SinisterCard selectNav={selectNav} />
              </Layout.Col>
            )
            : null

        }
      </Layout.Row>
    </div>
  );
}

export default ResumeNav;