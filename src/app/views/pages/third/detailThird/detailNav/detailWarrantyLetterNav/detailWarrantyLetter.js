import React, { useState } from 'react';
import { MDBCol, MDBInput, MDBBreadcrumb, MDBBreadcrumbItem, MDBIcon, MDBCollapse } from "mdbreact";
import styled from 'styled-components'
import _ from 'lodash'
import { changeFormatDate } from 'state/utils/functionUtils'

const FIELD_GROUPS = [
  [
    { field: 'monto', label: 'Monto' },
    { field: 'coAsegurado', label: 'CoAsegurado' },
    { field: 'moneda', label: 'Moneda' },
  ],
  [
    { field: 'fechaFinVigencia', label: 'Fecha Fin Vigencia' },
    { field: 'fechaIniVigencia', label: 'Fecha Inicio Vigencia' },
    { field: 'numPoliza', label: 'N° Póliza' },
  ],
  [
    { field: 'procediMedico', label: 'Procedimiento Médico', large: true },
  ],
  [
    { field: 'diagnostico', label: 'Diagnóstico', large: true },
  ],
  [
    { field: 'motivoRechazo', label: 'Motivo Rechazo', large: true },
  ],
  [
    { field: 'fechaRechazo', label: 'Fecha Rechazo' },
    { field: 'numDocAsegurado', labelValue: 'tipoDocAsegurado' },
    { field: 'parentescoPaciente', label: 'Parentesco Paciente' },
  ],
  [
    { field: 'proveedor', label: 'Proveedor' },
    { field: 'numSolicitud', label: 'N° Solicitud' },
    { field: 'tipoSolicitud', label: 'Tipo Solicitud' },
  ],
    //Fecha y hora de solicitud junto
  [
    { field: 'fechaSolicitud', label: 'Fecha Solicitud' },
    { field: 'horaSolicitud', label: 'Hora Solicitud' },
    { field: 'fechaAprobacion', label: 'Fecha Aprobado' },
  
  ],
  [
    { field: 'cobertura', label: 'Cobertura' },
    { field: 'estado', label: 'Estado' },
    { field: 'numCartaGarantia', label: 'N° Carta Garantía' },
  ],
 
]

const DivInput = styled.div`
  .breadcrumb {
    cursor: pointer;
  }
  .md-form {
    margin-top: 0.5rem;
    margin-bottom: 0.5rem;
  }
`;

const DivDetailWarrantyLetter = styled.div`
  .breadcrumb {
    cursor: pointer;
  }
`;

const notNull = value => (_.isNil(value) || `${value}`.trim() === '') ? '-' : value

const DetailWarrantyLetter = ({ warrantyLetter }) => {

  const [isOpen, setIsOpen] = useState(true)

  return (
    <DivDetailWarrantyLetter>
      <br />
      <MDBBreadcrumb onClick={() => setIsOpen(!isOpen)}>
        <MDBBreadcrumbItem>
          {isOpen
            ? <MDBIcon icon="angle-down" />
            : <MDBIcon icon="angle-right" />}
          {"  Detalle de la Carta de Garantía"}
        </MDBBreadcrumbItem>

      </MDBBreadcrumb>

      <br />
      <MDBCollapse
        id="collapseContactInfo"
        isOpen={isOpen}
        style={{ paddingLeft: '10px', }}
      >
        <DivInput>
          {FIELD_GROUPS.map((group, index) => {
            return (
              <div className="form-group row" key={index}>
                {
                  // @ts-ignore
                  group.map((item, index) => {
                    const value = item.field in warrantyLetter
                      ? warrantyLetter[item.field]
                      : 'Este campo no existe en este trámite'

                    return (
                      <div className={`col-lg${item.large ? '' : '-4'}`} key={index}>
                        <MDBInput
                          value={`${notNull(
                            `${item.label}`.includes('Fecha')
                              ? changeFormatDate(value, '-', '/')
                              : value
                          )}`}
                          // label={item.label}
                          label={item.labelValue
                            ? warrantyLetter[item.labelValue]
                            : item.label}
                          size="sm"
                          disabled
                          {...(item.large? { type: 'textarea' } : {})}
                        />
                      </div>
                    )
                  })
                }
              </div>
            )
          })}
        </DivInput>
      </MDBCollapse>
    </DivDetailWarrantyLetter>
  );
}

export default DetailWarrantyLetter;