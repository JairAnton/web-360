// @ts-check
import React, { useState } from 'react'
import { MDBInput, MDBBreadcrumb, MDBBreadcrumbItem, MDBIcon, MDBCollapse } from 'mdbreact'
import styled from 'styled-components'
import _ from 'lodash'
import './styles.css'
import { changeFormatDate, removeSeconds } from 'state/utils/functionUtils'

import {
  OPERATIONS_COLLAPSIBLE_FIELD_GROUPS,
  EPS_CLAIMS_COLLAPSIBLE_FIELD_GROUPS,
  INSURANCE_CLAIMS_COLLAPSIBLE_FIELD_GROUPS,
  MEDIC_ORIENTATION_COLLAPSIBLE_FIELD_GROUPS,
  EMERGENCY_MEDICAL_UNIT_COLLAPSIBLE_FIELD_GROUPS,
  HOME_DOCTOR_COLLAPSIBLE_FIELD_GROUPS,
  REFERENCE_COLLAPSIBLE_FIELD_GROUPS,
  GENERAL_RISK_COLLAPSIBLE_FIELD_GROUPS,
  FUNERAL_ASSISTANCE_COLLAPSIBLE_FIELD_GROUPS,
  HOME_ASSISTANCE_COLLAPSIBLE_FIELD_GROUPS,
  MEDICINE_DELIVERY_COLLAPSIBLE_FIELD_GROUPS,
  TRAVEL_ASSISTANCE_COLLAPSIBLE_FIELD_GROUPS,
  SCHEDULED_ATTENTION_COLLAPSIBLE_FIELD_GROUPS,
  LEGAL_CLAIM_COLLAPSIBLE_FIELD_GROUPS,
  REPLACEMENT_DRIVER_COLLAPSIBLE_FIELD_GROUPS,
  RENEWAL_CHARGE_COLLAPSIBLE_FIELD_GROUPS,
  VEHICULAR_EMERGENCY_COLLAPSIBLE_FIELD_GROUPS,
  CRANE_MECHANICAL_ASSISTANCE_COLLAPSIBLE_FIELD_GROUPS,
  CNT_CLAIMS_COLLAPSIBLE_FIELD_GROUPS,
  RENEWAL_COLLAPSIBLE_FIELD_GROUPS,
  CNT_OPERATIONS_COLLAPSIBLE_FIELD_GROUPS,
  RETENTION_COLLAPSIBLE_FIELD_GROUPS,
  SOAT_SINISTER_COLLAPSIBLE_FIELD_GROUPS,
} from './detailCasesFields'


const DivInput = styled.div`
  .md-form {
    margin-top: 0.5rem;
    margin-bottom: 0.5rem;
  }
`;
const DivDetailCases = styled.div`
  .breadcrumb {
    cursor: pointer;
  }
`;
const DivEstadoAns = ({ estadoAns }) => (
  <div style={{
    backgroundColor: estadoAns,
    borderRadius: '50%',
    width: '20px',
    height: '20px',
    marginTop: '1px',
  }} />
)
const notNull = value => (_.isNil(value) || `${value}`.trim() === '') ? '-' : value

/**
 * Retorna el conjunto de campos de acuerdo al tipo de caso
 * @param {object} selectedCase el objeto con la data del caso seleccionado
 * @param {string} selectedCase.tipoCasoSf string que indica el tipo de caso
 */
const getFieldsByCaseSelected = selectedCase => {
  // F1_Reclamaciones_CNT
  // F1_Renovacion
  // F1_RequerimientosOperacionesCNT
  // F1_Retencion
  // F1_SiniestroSOAT
  //console.log(selectedCase.tipoCasoSf);
  switch (selectedCase.tipoCasoSf) {
    case "F1_RequerimientoOperaciones":
      return OPERATIONS_COLLAPSIBLE_FIELD_GROUPS
    case "F1_ReclamacionEPS":
      return EPS_CLAIMS_COLLAPSIBLE_FIELD_GROUPS
    case "F1_ReclamacionSeguros":
      return INSURANCE_CLAIMS_COLLAPSIBLE_FIELD_GROUPS
    case 'F1_OrientacionMedicaTelefonicaVirtual':
      return MEDIC_ORIENTATION_COLLAPSIBLE_FIELD_GROUPS
    case 'F1_UnidadMedicaEmergencia':
      return EMERGENCY_MEDICAL_UNIT_COLLAPSIBLE_FIELD_GROUPS
    case 'F1_MedicoDomicilio':
      return HOME_DOCTOR_COLLAPSIBLE_FIELD_GROUPS
    case 'F1_Referencia':
      return REFERENCE_COLLAPSIBLE_FIELD_GROUPS
    case 'F1_EmergenciaRiesgosGeneralesAjustador':
      return GENERAL_RISK_COLLAPSIBLE_FIELD_GROUPS
    case 'F1_AsistenciaFunerariaSepelio':
      return FUNERAL_ASSISTANCE_COLLAPSIBLE_FIELD_GROUPS
    case 'F1_AsistenciaHogar':
      return HOME_ASSISTANCE_COLLAPSIBLE_FIELD_GROUPS
    case 'F1_DeliveryMedicinas':
      return MEDICINE_DELIVERY_COLLAPSIBLE_FIELD_GROUPS
    case 'F1_AsistenciaViajes':
      return TRAVEL_ASSISTANCE_COLLAPSIBLE_FIELD_GROUPS
    case 'F1_AtencionProgamada':
      return SCHEDULED_ATTENTION_COLLAPSIBLE_FIELD_GROUPS
    case 'F1_ReclamacionLegal':
      return LEGAL_CLAIM_COLLAPSIBLE_FIELD_GROUPS
    case 'F1_ChoferReemplazo':
      return REPLACEMENT_DRIVER_COLLAPSIBLE_FIELD_GROUPS
    case 'F1_CobranzaRenovacion':
      return RENEWAL_CHARGE_COLLAPSIBLE_FIELD_GROUPS
    case 'F1_EmergenciaVehicular':
      return VEHICULAR_EMERGENCY_COLLAPSIBLE_FIELD_GROUPS
    case 'F1_GruaAuxilioMecanico':
      return CRANE_MECHANICAL_ASSISTANCE_COLLAPSIBLE_FIELD_GROUPS
    case 'F1_Reclamaciones_CNT':
      return CNT_CLAIMS_COLLAPSIBLE_FIELD_GROUPS
    case 'F1_Renovacion':
      return RENEWAL_COLLAPSIBLE_FIELD_GROUPS
    case 'F1_RequerimientosOperacionesCNT':
      return CNT_OPERATIONS_COLLAPSIBLE_FIELD_GROUPS
    case 'F1_Retencion':
      return RETENTION_COLLAPSIBLE_FIELD_GROUPS
    case 'F1_SiniestroSOAT':
      return SOAT_SINISTER_COLLAPSIBLE_FIELD_GROUPS
    default:
      return []
  }
}

/**
 * Componente para el detalle del caso
 * @param {object} props props del componente
 * @param {object} props.selectedCase prop que indica el caso seleccionado
 * @param {string} props.selectedCase.tipoCasoSf string que indica el tipo de caso
 * @returns {JSX.Element}
 */
function DetailCase({ selectedCase }) {
  const [openFieldGroups, setOpenFieldGroups] = useState(
    getFieldsByCaseSelected(selectedCase).map(() => true)
  )

  /**
   * Funcion para colapsar el grupo de campos
   * @param {number} index índice del conjunto de campos a colapsar
   * @param {string} [foo] ejemplo de JSDoc para un parametro opcional 
   */
  const setOpenFieldGroupsByIndex = (index, foo) => () => {
    const _openFieldGroups = [...openFieldGroups]
    _openFieldGroups[index] = !_openFieldGroups[index]
    setOpenFieldGroups(_openFieldGroups)
  }

  /**
   * 
   * @param {{ field: string, label: string }} item 
   * @param {any} value 
   */
  const renderField = (item, value) => {
    
    if (item.field === 'estadoAns') {
      return (
        <div
          className="md-form form-sm"
          style={{ width: '100%', height: '100%' }}
        >
          <p className="estado-ans-label">
            {item.label}
          </p>
          <DivEstadoAns estadoAns={value} />
          <hr className="estado-ans-hr" />
        </div>
      )
    } else if (item.field.startsWith('fecha')) {
      let val = null
      if (value) {
        val = removeSeconds(changeFormatDate(value, '-', '/').split('.')[0])
        const x = val.split(' ')
        val = `${x[0].split('/').reverse().join('/')} ${x[1]}`
      }
      return (
        <MDBInput
          value={notNull(val)}
          label={item.label}
          size="sm"
          disabled
        />
      )
    }
    else if(item.field === 'descripcion'){
      let longitud = 1
      
      if(value){
        longitud = value.split('\n').length;
      }
      else{
        value = '-'
      }
       
      
      return( 
        <div>
          <MDBInput
            label={item.label}
            value={value}
            size="sm"
            disabled
            type="textarea"
            rows={longitud}
          />
        </div>
      )
    }
    else if(item.field === 'solucion'){
      let longitud = 1
      if(value){
        longitud = value.split('\n').length;
      }
      else{
        value = '-'
      }

      return( 
        <div>
          <MDBInput
            label={item.label}
            value={value}
            size="sm"
            disabled
            type="textarea"
            rows={longitud}
          />
        </div>
      )
    } 
    else {
      if (item.field === 'descripcionTipoCasoSf' && !value)
        value = selectedCase.tipoCasoSf
      return (
        <MDBInput
          value={notNull(value)}
          label={item.label}
          size="sm"
          disabled
        />
      )
    }
    
  }

  return (
    <div>
      {getFieldsByCaseSelected(selectedCase).map((FIELD_GROUP, index1) => {
        return (
          <DivDetailCases key={index1} >
            <div onClick={setOpenFieldGroupsByIndex(index1)}>
              <MDBBreadcrumb>
                <MDBBreadcrumbItem>
                  {openFieldGroups[index1]
                    ? <MDBIcon icon="angle-down" />
                    : <MDBIcon icon="angle-right" />}
                  {FIELD_GROUP.scrollable
                    ?
                    <p id={FIELD_GROUP.scrollable}>
                      {`   ${FIELD_GROUP.title}`}
                    </p>
                    :
                    `   ${FIELD_GROUP.title}`}
                </MDBBreadcrumbItem>
              </MDBBreadcrumb>
            </div>
            <br />
            <MDBCollapse
              id="collapseContactInfo"
              isOpen={openFieldGroups[index1]}
            >
              <div className="col-lg-12">
                <DivInput >
                  {
                    // @ts-ignore
                    FIELD_GROUP.fields.map((ROW, index2) => {
                      return (
                        <div className="row" key={`${index1}-${index2}`} >
                          
                          {ROW.map((item, index3) => {
                            const value = item.field in selectedCase
                              ? selectedCase[item.field]
                              : '-'
                            return (
                              <div
                                className={`col-lg-${item.large ? '12' : '4'}`}
                                key={`${index1}-${index2}-${index3}`}
                              >
                                {renderField(item, value)}
                              </div>
                            )
                          })}
                        </div>
                      )
                    })
                  }
                  <br />
                </DivInput>
              </div>
            </MDBCollapse>
          </DivDetailCases>
        )
      })}

    </div>
  )
}

export default DetailCase