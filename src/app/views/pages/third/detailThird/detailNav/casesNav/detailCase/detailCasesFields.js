export const OPERATIONS_COLLAPSIBLE_FIELD_GROUPS = [
  {
    title: 'Registro del Caso',
    fields: [
      [
        { field: 'numCasoSf', label: 'Numero del Caso' },
        { field: 'nombreContacto', label: 'Nombre del Contacto' },
        { field: 'nombreCuenta', label: 'Nombre de la Cuenta' },
        { field: 'estadoCaso', label: 'Estado' },
        { field: 'descripcionTipoCasoSf', label: 'Tipo de Registro del Caso' },
        { field: 'tipoContacto', label: 'Tipo de Contacto' },
        { field: 'canalRespuesta', label: 'Canal de Respuesta' },
        { field: 'contactoTieneEmail', label: '¿Contacto Tiene Email?' },
      ],
    ]
  },
  {
    title: 'Tipificación del Caso',
    fields: [
      [
        { field: 'grupo', label: 'Grupo' },
        { field: 'tipo', label: 'Tipo' },
        { field: 'categoria', label: 'Categoria' },
        { field: 'asunto', label: 'Asunto' },
        { field: 'tema', label: 'Tema' },
      ]
    ]
  },
  {
    title: 'Descripción',
    fields: [
      [
        { field: 'descripcion', label: 'Descripción', large: true }
      ],
    ]
  },
  {
    title: 'Datos de Contacto',
    fields: [
      [
        { field: 'correoElectronico', label: 'Correo Electrónico' },
        { field: 'emailSeleccionadoInfoContacto', label: 'Email Seleccionado Info. Contacto' },
        { field: 'telefonoContacto', label: 'Teléfono de Contacto' },
      ],
    ]
  },
  {
    title: 'Información del Caso',
    fields: [
      [
        { field: 'origenCaso', label: 'Origen del Caso' },
        { field: 'prioridad', label: 'Prioridad' },
        { field: 'casoPrincipal', label: 'Caso Principal' },
        { field: 'numPol', label: 'Póliza' },
      ],
    ],
  },
  {
    title: 'Responsable',
    fields: [
      [
        { field: 'propietarioCaso', label: 'Propietario del Caso' },
        { field: 'areaResponsablePropietario', label: 'Area Responsable Propietario' },
        { field: 'grupoResponsable', label: 'Grupo Responsable' },
        { field: 'responsable', label: 'Responsable' },
        { field: 'responsableTieneLicenciaSalesforce', label: 'Responsable Tiene Licencia en SalesForce' },
      ],
    ],
  },
  {
    title: 'Solución',
    fields: [
      [
        { field: 'solucion', label: 'Solución', large: true }
      ],
    ],
  },
  {
    title: 'ANS',
    fields: [
      [
        { field: 'nombreAsignacion', label: 'Nombre de Asignación' },
        { field: 'ansVencimiento', label: 'ANS Vencimiento' },
        { field: 'fechaVencimientoAns', label: 'Fecha Vencimiento ANS' },
        { field: 'estadoAns', label: 'Estado ANS' },
        { field: 'ans', label: 'ANS (Minutos)' },
      ],
    ],
  },
  {
    title: 'Plataforma',
    fields: [
      [
        { field: 'numeroTicketBmatick', label: 'N° Ticket BMatick' },
        { field: 'cantidadOperaciones', label: 'Cantidad de Operaciones' },
      ],
    ],
  },
  {
    title: 'Correo Físico',
    fields: [
      [
        { field: 'fechaHoraRecepcionCorreoFisico', label: 'Fecha/Hora Recepción Correo Físico' },
      ],
    ],
  },
  {
    title: 'Vehículo de Reemplazo',
    fields: [
      [
        { field: 'numeroSiniestro', label: 'N° de Siniestro' },
        { field: 'fechaHoraSolicitud', label: 'Fecha/Hora Solicitud' },
        { field: 'indicadorClausula', label: 'Indicador de Cláusula' },
        { field: 'nombreContactoVehiculoReemplazo', label: 'Nombre Contacto' },
        { field: 'correoElectronicoContactoVehiculoReemplazo', label: 'Correo Electrónico de Contacto' },
        { field: 'anotaciones', label: 'Anotaciones', large: true },
        { field: 'numeroDiasMaximo', label: 'N° de Días Máximo' },
        { field: 'deduciblesSimplificados', label: 'Deducibles Simplificados' },
        { field: 'telefonoContactoVehiculoReemplazo', label: 'Teléfono de Contacto' },
      ],
    ],
  },
  {
    title: 'Anulacion de una Póliza',
    fields: [
      [
        { field: 'motivoAnulacion', label: 'Motivo de Anulación' }
      ],
    ],
  },
  {
    title: 'Datos del Caso',
    fields: [
      [
        { field: 'fechaHoraApertura', label: 'Fecha/Hora Apertura' },
        { field: 'fechaHoraCierre', label: 'Fecha/Hora Cierre' },
        { field: 'fechaAnulado', label: 'Fecha de Anulado' },
        { field: 'fechaDevuelto', label: 'Fecha de Devuelto' },
        { field: 'creadoPor', label: 'Creado Por' },
        { field: 'ultimaModificacionPor', label: 'Última Modificación Por' },
      ],
    ],
  },
]


export const EPS_CLAIMS_COLLAPSIBLE_FIELD_GROUPS = [
  {
    title: 'Registro del Caso',
    fields: [
      [
        { field: 'casoPrincipal', label: 'Caso Principal' },
        { field: 'nombreContacto', label: 'Nombre del Contacto' },
        { field: 'nombreCuenta', label: 'Nombre de la Cuenta' },
        { field: 'estadoCaso', label: 'Estado' },
        { field: 'clase', label: 'Clase' },
        { field: 'prioridad', label: 'Prioridad' },
        { field: 'origenCaso', label: 'Origen del Caso' },
        { field: 'canalRespuesta', label: 'Canal de Respuesta' },
        { field: 'tipoContacto', label: 'Tipo de Contacto' },
        { field: 'quejaReclamo', label: 'Queja/Reclamo' },
        { field: 'modalidadReclamo', label: 'Modalidad de Reclamo' },
        { field: 'origenReclamo', label: 'Origen del Reclamo' },
        { field: 'proveedor', label: 'Proveedor' },
      ],
    ]
  },
  {
    title: 'Tipificación del Caso',
    fields: [
      [
        { field: 'grupo', label: 'Grupo' },
        { field: 'tipo', label: 'Tipo' },
        { field: 'categoria', label: 'Categoria' },
        { field: 'asunto', label: 'Asunto' },
        { field: 'tema', label: 'Tema' },
      ]
    ]
  },
  {
    title: 'Dirección',
    fields: [
      [
        { field: 'pais', label: 'País' },
        { field: 'departamento', label: 'Departamento' },
        { field: 'provincia', label: 'Provincia' },
        { field: 'distrito', label: 'Distrito' },
        { field: 'direccionServicio', label: 'Dirección Servicio' },
        { field: 'referencias', label: 'Referencias' },
      ],
    ]
  },
  {
    title: 'Descripción',
    fields: [
      [
        { field: 'descripcion', label: 'Descripción', large: true },
        { field: 'fechaHoraOcurrencia', label: 'Fecha/Hora Ocurrencia' },
        { field: 'requiereDevolucion', label: '¿Requiere Devolución?' },
      ],
    ]
  },
  {
    title: 'Devoluciones',
    fields: [
      [
        { field: 'montoReclamado', label: 'Monto Reclamado S/ USD' },
        { field: 'abonoCuenta', label: 'Abono en Cuenta' },
        { field: 'entidadFinanciera', label: 'Entidad financiera' },
        { field: 'numeroCuenta', label: 'Numero de Cuenta' },
        { field: 'tipoCuenta', label: 'Tipo de Cuenta' },
        { field: 'tipoMoneda', label: 'Tipo de Moneda' },
      ],
    ]
  },
  {
    title: 'Información de Contacto',
    fields: [
      [
        { field: 'correoElectronico', label: 'Correo Electrónico' },
        { field: 'emailSeleccionadoInfoContacto', label: 'Email Seleccionado Info. Contacto' },
        { field: 'autorizaRtaEmail', label: 'Autoriza rta Email' },
        { field: 'telefonoContacto', label: 'Teléfono de Contacto' },
        { field: 'autorizaRtaTelefono', label: 'Autoriza rta Teléfono' },
      ],
    ]
  },
  {
    title: 'Origen Carta Física/Email',
    fields: [
      [
        { field: 'fechaRecepcion', label: 'Fecha de Recepción' }
      ],
    ],
  },
  {
    title: 'Póliza',
    fields: [
      [
        { field: 'numPol', label: 'Póliza' },
        { field: 'numeroPolizaTexto', label: 'N° Póliza Texto' },
      ],
    ],
  },
  {
    title: 'Asegurado',
    fields: [
      [
        { field: 'numbreCompletoAsegurado', label: 'Nombre Completo del Asegurado' },
        { field: 'apellidoPaternoAsegurado', label: 'Apellido Paterno del Asegurado' },
        { field: 'apellidoMaternoAsegurado', label: 'Apellido Materno del Asegurado' },
        { field: 'parentescoTitular', label: 'Parentesco con el Titular' },
        { field: 'codigoAfiliado', label: 'Código del Afiliado' },
        { field: 'edadAsegurado', label: 'Edad del Asegurado' },
      ],
    ],
  },
  {
    title: 'Detalle del reclamo',
    fields: [
      [
        { field: 'numCasoSf', label: 'N° del Caso' },
        { field: 'numeroReclamoPadre', label: 'N° Reclamo Padre' },
        { field: 'numeroReclamo', label: 'N° Reclamo' },
        { field: 'numeroLibroReclamaciones', label: 'N° Libro Reclamaciones' },
        // // { field: 'agencia', label: 'Agencia' },
        { field: 'areaResponsablePropietario', label: 'Area Responsable Propietario' },
        { field: 'casoRelacionado', label: 'Caso Relacionado' },
        { field: 'fechaCumplimiento', label: 'Fecha de Cumplimiento' },
        { field: 'descripcionTipoCasoSf', label: 'Tipo de Registro del Caso' },
        { field: 'propietarioCaso', label: 'Propietario del Caso' },
        { field: 'tpoTransDiasUtiles', label: 'Tpo. Trans. Días Útiles' },
      ],
    ],
  },
  {
    title: 'Responsable',
    fields: [
      [
        { field: 'grupoResponsable', label: 'Grupo Responsable' },
        { field: 'responsable', label: 'Responsable' },
        { field: 'responsableTieneLicenciaSalesforce', label: 'Responsable Tiene Licencia en SalesForce' },
      ],
    ],
  },
  {
    title: 'ANS',
    fields: [
      [
        { field: 'nombreAsignacion', label: 'Nombre de Asignación' },
        { field: 'fechaVencimientoAns', label: 'Fecha Vencimiento ANS' },
        { field: 'ansVencimiento', label: 'ANS Vencimiento' },
        { field: 'estadoAns', label: 'Estado ANS' },
        { field: 'ans', label: 'ANS (Minutos)' },
      ],
    ],
  },
  {
    title: 'Atención',
    fields: [
      [
        { field: 'autoResolutor', label: 'Auto Resolutor' },
        { field: 'tipoServicioAtencion', label: 'Tipo de Servicio o Atención' },
      ],
    ],
  },
  {
    title: 'Solución',
    fields: [
      [
        { field: 'solucion', label: 'Solución', large: true },
        { field: 'fechaRespuesta', label: 'Fecha de Respuesta' },
        { field: 'fechaRecepcionRespuesta', label: 'Fecha de Recepción de Respuesta' },
      ],
    ],
  },
  {
    title: 'Evaluación del Reclamo',
    fields: [
      [
        { field: 'situacionFinal', label: 'Situación Final' },
        { field: 'calificacionCalidad', label: 'Calificación Calidad' },
        { field: 'calificacionTiempo', label: 'Calificación Tiempo' },
      ],
    ],
  },
  {
    title: 'Medida correctiva',
    fields: [
      [
        { field: 'medidaCorrectiva', label: 'Medida Correctiva' },
        { field: 'accionTomar', label: 'Acción a Tomar' },
        { field: 'responsableImplementacionMc', label: 'Responsable implementación MC' },
        { field: 'fechaCumplimientoMc', label: 'Fecha Cumplmiento MC' },
        { field: 'calificacionMedidaCorrectiva', label: 'Calificación Medida Correctiva' },
      ],
    ],
  },
  {
    title: 'Respuesta al Usuario',
    fields: [
      [
        { field: 'respuestaUsuario', label: 'Respuesta al Usuario' }
      ],
    ],
  },
  {
    title: 'Links PDFs',
    fields: [
      [
        { field: 'pdfLibroReclamacion', label: 'PDF Libro Reclamación' },
        { field: 'pdfCierreReclamo', label: 'PDF Cierre de Reclamo' },
        { field: 'pdfInformeResultados', label: 'PDF Informe Resultados' },
        { field: 'estadoLibroReclamaciones', label: 'Responsable implementación MC' },
        { field: 'estadoCartaCierre', label: 'Estado Carta de Cierre' },
        { field: 'estadoInformeResultado', label: 'Estado Informe de Resultado' },
        { field: 'fechaEmisionCartaCierre', label: 'Fecha de Emisión de Carta de Cierre' },
      ],
    ],
  },
  {
    title: 'Motivo de Anulación',
    fields: [
      [
        { field: 'motivoAnulacion', label: 'Motivo de Anulación' }
      ],
    ],
  },
  {
    title: 'Información del Caso',
    fields: [
      [
        { field: 'fechaHoraApertura', label: 'Fecha/Hora Apertura' },
        { field: 'fechaHoraCierre', label: 'Fecha/Hora Cierre' },
        { field: 'creadoPor', label: 'Creado Por' },
        { field: 'areaResponsableCreacion', label: 'Área Responsable Creación' },
        { field: 'ultimaModificacionPor', label: 'Última Modificación Por' },
        { field: 'fechaAtencion', label: 'Fecha de Atención' },
        { field: 'fechaAnulado', label: 'Fecha de Anulado' },
        { field: 'fechaDevuelto', label: 'Fecha de Devuelto' },
      ],
    ],
  },
]

export const INSURANCE_CLAIMS_COLLAPSIBLE_FIELD_GROUPS = [
  {
    title: 'Registro del Caso',
    fields: [
      [
        { field: 'casoPrincipal', label: 'Caso Principal' },
        { field: 'nombreContacto', label: 'Nombre del Contacto' },
        { field: 'nombreCuenta', label: 'Nombre de la Cuenta' },
        { field: 'estadoCaso', label: 'Estado' },
        { field: 'clase', label: 'Clase' },
        { field: 'prioridad', label: 'Prioridad' },
        { field: 'origenCaso', label: 'Origen del Caso' },
        { field: 'canalRespuesta', label: 'Canal de Respuesta' },
        { field: 'tipoContacto', label: 'Tipo de Contacto' },
        { field: 'quejaReclamo', label: 'Queja/Reclamo' },
        { field: 'modalidadReclamo', label: 'Modalidad de Reclamo' },
        { field: 'origenReclamo', label: 'Origen del Reclamo' },
        { field: 'proveedor', label: 'Proveedor' },
      ],
    ]
  },
  {
    title: 'Tipificación del Caso',
    fields: [
      [
        { field: 'grupo', label: 'Grupo' },
        { field: 'tipo', label: 'Tipo' },
        { field: 'categoria', label: 'Categoria' },
        { field: 'asunto', label: 'Asunto' },
        { field: 'tema', label: 'Tema' },
      ]
    ]
  },
  {
    title: 'Dirección',
    fields: [
      [
        { field: 'pais', label: 'País' },
        { field: 'departamento', label: 'Departamento' },
        { field: 'provincia', label: 'Provincia' },
        { field: 'distrito', label: 'Distrito' },
        { field: 'direccionServicio', label: 'Dirección Servicio' },
        { field: 'referencias', label: 'Referencias' },
      ],
    ]
  },
  {
    title: 'Descripción',
    fields: [
      [
        { field: 'descripcion', label: 'Descripción', large: true },
        { field: 'fechaHoraOcurrencia', label: 'Fecha/Hora Ocurrencia' },
        { field: 'requiereDevolucion', label: '¿Requiere Devolución?' },
      ],
    ]
  },
  {
    title: 'Devoluciones',
    fields: [
      [
        { field: 'montoReclamado', label: 'Monto Reclamado S/ USD' },
        { field: 'abonoCuenta', label: 'Abono en Cuenta' },
        { field: 'entidadFinanciera', label: 'Entidad financiera' },
        { field: 'numeroCuenta', label: 'Numero de Cuenta' },
        { field: 'tipoCuenta', label: 'Tipo de Cuenta' },
        { field: 'tipoMoneda', label: 'Tipo de Moneda' },
      ],
    ]
  },
  {
    title: 'Información de Contacto',
    fields: [
      [
        { field: 'correoElectronico', label: 'Correo Electrónico' },
        { field: 'emailSeleccionadoInfoContacto', label: 'Email Seleccionado Info. Contacto' },
        { field: 'autorizaRtaEmail', label: 'Autoriza rta Email' },
        { field: 'telefonoContacto', label: 'Teléfono de Contacto' },
        { field: 'autorizaRtaTelefono', label: 'Autoriza rta Teléfono' },
      ],
    ]
  },
  {
    title: 'Origen Carta Física/Email',
    fields: [
      [
        { field: 'fechaRecepcion', label: 'Fecha de Recepción' }
      ],
    ],
  },
  {
    title: 'Póliza',
    fields: [
      [
        { field: 'numPol', label: 'Póliza' },
        { field: 'numeroPolizaTexto', label: 'N° Póliza Texto' },
      ],
    ],
  },
  {
    title: 'Asegurado',
    fields: [
      [
        { field: 'numbreCompletoAsegurado', label: 'Nombre Completo del Asegurado' },
        { field: 'apellidoPaternoAsegurado', label: 'Apellido Paterno del Asegurado' },
        { field: 'apellidoMaternoAsegurado', label: 'Apellido Materno del Asegurado' },
        { field: 'parentescoTitular', label: 'Parentesco con el Titular' },
        { field: 'codigoAfiliado', label: 'Código del Afiliado' },
        { field: 'edadAsegurado', label: 'Edad del Asegurado' },
      ],
    ],
  },
  {
    title: 'Detalle del reclamo',
    fields: [
      [
        { field: 'numCasoSf', label: 'N° del Caso' },
        { field: 'numeroReclamoPadre', label: 'N° Reclamo Padre' },
        { field: 'numeroReclamo', label: 'N° Reclamo' },
        { field: 'numeroLibroReclamaciones', label: 'N° Libro Reclamaciones' },
        { field: 'areaResponsablePropietario', label: 'Area Responsable Propietario' },
        { field: 'casoRelacionado', label: 'Caso Relacionado' },
        { field: 'fechaCumplimiento', label: 'Fecha de Cumplimiento' },
        { field: 'descripcionTipoCasoSf', label: 'Tipo de Registro del Caso' },
        { field: 'propietarioCaso', label: 'Propietario del Caso' },
        { field: 'tpoTransDiasUtiles', label: 'Tpo. Trans. Días Útiles' },

      ],
    ],
  },
  {
    title: 'Responsable',
    fields: [
      [
        { field: 'grupoResponsable', label: 'Grupo Responsable' },
        { field: 'responsableTieneLicenciaSalesforce', label: 'Responsable Tiene Licencia en SalesForce' },
        { field: 'responsable', label: 'Responsable' },
      ],
    ],
  },
  {
    title: 'ANS',
    fields: [
      [
        { field: 'nombreAsignacion', label: 'Nombre de Asignación' },
        { field: 'fechaVencimientoAns', label: 'Fecha Vencimiento ANS' },
        { field: 'ansVencimiento', label: 'ANS Vencimiento' },
        { field: 'estadoAns', label: 'Estado ANS' },
        { field: 'ans', label: 'ANS (Minutos)' },
      ],
    ],
  },
  {
    title: 'Atención',
    fields: [
      [
        { field: 'autoResolutor', label: 'Auto Resolutor' },
        { field: 'tipoServicioAtencion', label: 'Tipo de Servicio o Atención' },
      ],
    ],
  },
  {
    title: 'Solución',
    fields: [
      [
        { field: 'solucion', label: 'Solución', large: true },
        { field: 'fechaRespuesta', label: 'Fecha de Respuesta' },
        { field: 'fechaRecepcionRespuesta', label: 'Fecha de Recepción de Respuesta' },
      ],
    ],
  },
  {
    title: 'Evaluación del Reclamo',
    fields: [
      [
        { field: 'situacionFinal', label: 'Situación Final' },
        { field: 'calificacionCalidad', label: 'Calificación Calidad' },
        { field: 'calificacionTiempo', label: 'Calificación Tiempo' },
      ],
    ],
  },
  {
    title: 'Medida correctiva',
    fields: [
      [
        { field: 'accionTomar', label: 'Acción a Tomar' },
        { field: 'medidaCorrectiva', label: 'Medida Correctiva' },
        { field: 'responsableImplementacionMc', label: 'Responsable implementación MC' },
        { field: 'fechaCumplimientoMc', label: 'Fecha Cumplmiento MC' },
        { field: 'calificacionMedidaCorrectiva', label: 'Calificación Medida Correctiva' },
      ],
    ],
  },
  {
    title: 'Respuesta al Usuario',
    fields: [
      [
        { field: 'respuestaUsuario', label: 'Respuesta al Usuario' }
      ],
    ],
  },
  {
    title: 'Links PDFs',
    fields: [
      [
        { field: 'pdfLibroReclamacion', label: 'PDF Libro Reclamación' },
        { field: 'pdfCierreReclamo', label: 'PDF Cierre de Reclamo' },
        { field: 'pdfInformeResultados', label: 'PDF Informe Resultados' },
        { field: 'estadoLibroReclamaciones', label: 'Responsable implementación MC' },
        { field: 'estadoCartaCierre', label: 'Estado Carta de Cierre' },
        { field: 'estadoInformeResultado', label: 'Estado Informe de Resultado' },
        { field: 'fechaEmisionCartaCierre', label: 'Fecha de Emisión de Carta de Cierre' },
      ],
    ],
  },
  {
    title: 'Motivo de Anulación',
    fields: [
      [
        { field: 'motivoAnulacion', label: 'Motivo de Anulación' },
      ],
    ],
  },
  {
    title: 'Información del Caso',
    fields: [
      [
        { field: 'fechaHoraApertura', label: 'Fecha/Hora Apertura' },
        { field: 'fechaHoraCierre', label: 'Fecha/Hora Cierre' },
        { field: 'creadoPor', label: 'Creado Por' },
        { field: 'areaResponsableCreacion', label: 'Área Responsable Creación' },
        { field: 'ultimaModificacionPor', label: 'Última Modificación Por' },
        { field: 'fechaAnulado', label: 'Fecha de Anulado' },
        { field: 'fechaAtencion', label: 'Fecha de Atención' },
        { field: 'fechaDevuelto', label: 'Fecha de Devuelto' },
      ],
    ],
  },
]

export const MEDIC_ORIENTATION_COLLAPSIBLE_FIELD_GROUPS = [
  {
    title: 'Información',
    fields: [
      [
        { field: 'estadoCaso', label: 'Estado' },
        { field: 'descripcionTipoCasoSf', label: 'Tipo de Registro del Caso' },
        { field: 'regularizacionCaso', label: 'Regularización del Caso' },
      ],
    ]
  },
  {
    title: 'Tipificación del Caso',
    fields: [
      [
        { field: 'grupo', label: 'Grupo' },
        { field: 'tipo', label: 'tipo' },
        { field: 'categoria', label: 'Categoria' },
        { field: 'asunto', label: 'Asunto' },
        { field: 'tema', label: 'Tema' },
      ],
    ]
  },
  {
    title: 'Descripción',
    fields: [
      [
        { field: 'descripcion', label: 'Descripción', large: true },
        { field: 'telefono', label: 'Teléfono' },
      ],
    ]
  },
  {
    title: 'Información de Contacto',
    fields: [
      [
        { field: 'nombreContacto', label: 'Nombre del Contacto' },
        { field: 'nombreCuenta', label: 'Nombre de la Cuenta' },
        { field: 'telefonoAccesoWhatsapp', label: 'Teléfono con Acceso WhatsApp' },
      ],
    ]
  },
  {
    title: 'Información del Caso',
    fields: [
      [
        { field: 'numCasoSf', label: 'Numero del Caso' },
        { field: 'fechaHoraApertura', label: 'Fecha/Hora Apertura' },
        { field: 'numPol', label: 'Póliza' },
        { field: 'fechaHoraCierre', label: 'Fecha/Hora Cierre' },
        { field: 'creadoPor', label: 'Creado Por' },
        { field: 'propietarioCaso', label: 'Propietario del Caso' },
        { field: 'ultimaModificacionPor', label: 'Última Modificación Por' },
      ],
    ],
  },
  {
    title: 'Asegurado',
    fields: [
      [
        { field: 'numbreCompletoAsegurado', label: 'Nombre Completo del Asegurado' },
        { field: 'apellidoPaternoAsegurado', label: 'Apellido Paterno del Asegurado' },
        { field: 'apellidoMaternoAsegurado', label: 'Apellido Materno del Asegurado' },
        { field: 'parentescoTitular', label: 'Parentesco con el Titular' },
        { field: 'codigoAfiliado', label: 'Código del Afiliado' },
        { field: 'edadAsegurado', label: 'Edad del Asegurado' },
      ],
    ],
  },
  {
    title: 'Datos de Cobertura',
    fields: [
      [
        { field: 'descripcionCobertura', label: 'Descripción de Cobertura', large: true },
        { field: 'coaseguro', label: 'Coaseguro' },
        { field: 'clinicasAfiliadas', label: 'Clínicas Afiliadas' },
        { field: 'montoDeducible', label: 'Monto Deducible' },
      ],
    ],
  },
  {
    title: 'Evolución',
    fields: [
      [
        { field: 'evolucion', label: 'Evolución' },
        { field: 'relevancias', label: 'Relevancias' },
      ],
    ],
  },
]

export const EMERGENCY_MEDICAL_UNIT_COLLAPSIBLE_FIELD_GROUPS = [
  {
    title: 'Información',
    fields: [
      [
        { field: 'estadoCaso', label: 'Estado' },
        { field: 'descripcionTipoCasoSf', label: 'Tipo de Registro del Caso' },
        { field: 'regularizacionCaso', label: 'Regularización del Caso' },
      ],
    ]
  },
  {
    title: 'Tipificación del Caso',
    fields: [
      [
        { field: 'grupo', label: 'Grupo' },
        { field: 'tipo', label: 'tipo' },
        { field: 'categoria', label: 'Categoria' },
        { field: 'asunto', label: 'Asunto' },
        { field: 'tema', label: 'Tema' },
      ],
    ]
  },
  {
    title: 'Dirección',
    fields: [
      [
        { field: 'pais', label: 'País' },
        { field: 'departamento', label: 'Departamento' },
        { field: 'provincia', label: 'Provincia' },
        { field: 'distrito', label: 'Distrito' },
        { field: 'direccionSiniestro', label: 'Dirección del Siniestro' },
        { field: 'referencias', label: 'Referencias' },
        { field: 'anillo', label: 'Anillo' },
      ],
    ]
  },
  {
    title: 'Descripción',
    fields: [
      [
        { field: 'descripcion', label: 'Descripción', large: true },
        { field: 'telefono', label: 'Teléfono' },
        { field: 'telefonoAccesoWhatsapp', label: 'Teléfono con Acceso WhatsApp' },
      ],
    ]
  },
  {
    title: 'Información de Contacto',
    fields: [
      [
        { field: 'nombreContacto', label: 'Nombre del Contacto' },
        { field: 'nombreCuenta', label: 'Nombre de la Cuenta' },
      ],
    ]
  },
  {
    title: 'Información del Caso',
    fields: [
      [
        { field: 'numCasoSf', label: 'Numero del Caso' },
        { field: 'fechaHoraApertura', label: 'Fecha/Hora Apertura' },
        { field: 'numPol', label: 'Póliza' },
        { field: 'fechaHoraCierre', label: 'Fecha/Hora Cierre' },
        { field: 'creadoPor', label: 'Creado Por' },
        { field: 'propietarioCaso', label: 'Propietario del Caso' },
        { field: 'ultimaModificacionPor', label: 'Última Modificación Por' },
      ],
    ],
  },
  {
    title: 'ANS',
    fields: [
      [
        { field: 'nombreAsignacion', label: 'Nombre de Asignación' },
        { field: 'ansVencimiento', label: 'ANS Vencimiento' },
        { field: 'fechaVencimientoAns', label: 'Fecha Vencimiento ANS' },
        { field: 'estadoAns', label: 'Estado ANS' },
        { field: 'ans', label: 'ANS (Minutos)' },
      ],
    ],
  },
  {
    title: 'Asegurado',
    fields: [
      [
        { field: 'numbreCompletoAsegurado', label: 'Nombre Completo del Asegurado' },
        { field: 'apellidoPaternoAsegurado', label: 'Apellido Paterno del Asegurado' },
        { field: 'apellidoMaternoAsegurado', label: 'Apellido Materno del Asegurado' },
        { field: 'parentescoTitular', label: 'Parentesco con el Titular' },
        { field: 'codigoAfiliado', label: 'Código del Afiliado' },
        { field: 'edadAsegurado', label: 'Edad del Asegurado' },
      ],
    ],
  },
  {
    title: 'Políticas',
    fields: [
      [
        { field: 'cpc', label: 'CPC' },
        { field: 'tipoPoliticaAutorizacionAplicada', label: 'Tipo de Política Autorización Aplicada' },
        { field: 'tipoAtencionBrindar', label: 'Tipo de Atencion a Brindar' },
        { field: 'aplicaPoliticaAtencionAutorizacion', label: 'Aplica Política Atención o Autorización' },
      ],
    ],
  },
  {
    title: 'Gestión con el Proveedor',
    fields: [
      [
        { field: 'responsableCoordinacionServicio', label: 'Responsable Coordinacion Servicio' },
        { field: 'proveedor', label: 'Proveedor' },
        { field: 'fechaHoraAsignacionProveedor', label: 'Fecha/Hora Asignación al Proveedor' },
        { field: 'fechaHoraProgramacion', label: 'Fecha/Hora Programación' },
      ],
    ],
  },
  {
    title: 'Datos de Culminación Proveedor',
    fields: [
      [
        { field: 'fechaHoraInicioAtencionProveedor', label: 'Fecha/Hora Inicio Atención Proveedor' },
        { field: 'fechaHoraSolicitudTrasladoProveedor', label: 'Fecha/Hora Solicitud Traslado Proveedor' },
        { field: 'fechaHoraInicioTraslado', label: 'Fecha/Hora Inicio Traslado Proveedor' },
        { field: 'fechaHoraLlegadaClinica', label: 'Fecha/Hora Llegada a Clínica' },
        { field: 'fechaLlegadaClinica', label: 'Fecha de Llegada a Clínica' },
        { field: 'fechaHoraCulminacionProveedor', label: 'Fecha/Hora Culminación Proveedor' },
        { field: 'diagnosticoCulminacionCie10', label: 'Diagnóstico Culminación cie 10' },
        { field: 'clinicaDestino', label: 'Clínica de Destino' },
        { field: 'clinicaDestinoManual', label: 'Clínica de Destino Manual' },
        { field: 'nombreMedicoProveedor', label: 'Nombre Médico Proveedor' },
        { field: 'tratamientoProveedor', label: 'Tratamiento Proveedor' },
      ],
    ],
  },
  {
    title: 'Evolución',
    fields: [
      [
        { field: 'evolucion', label: 'Evolución' },
        { field: 'relevancias', label: 'Relevancias' },
      ],
    ],
  },
]

export const HOME_DOCTOR_COLLAPSIBLE_FIELD_GROUPS = [
  {
    title: 'Información',
    fields: [
      [
        { field: 'estadoCaso', label: 'Estado' },
        { field: 'descripcionTipoCasoSf', label: 'Tipo de Registro del Caso' },
        { field: 'regularizacionCaso', label: 'Regularización del Caso' },
        { field: 'tipoAtencionBrindar', label: 'Tipo de Atención a Brindar' },
      ],
    ]
  },
  {
    title: 'Tipificación del Caso',
    fields: [
      [
        { field: 'grupo', label: 'Grupo' },
        { field: 'tipo', label: 'tipo' },
        { field: 'categoria', label: 'Categoria' },
        { field: 'asunto', label: 'Asunto' },
        { field: 'tema', label: 'Tema' },
      ],
    ]
  },
  {
    title: 'Dirección',
    fields: [
      [
        { field: 'pais', label: 'País' },
        { field: 'departamento', label: 'Departamento' },
        { field: 'provincia', label: 'Provincia' },
        { field: 'distrito', label: 'Distrito' },
        { field: 'direccionSiniestro', label: 'Dirección del Siniestro' },
        { field: 'referencias', label: 'Referencias' },
        { field: 'anillo', label: 'Anillo' },
      ],
    ]
  },
  {
    title: 'Descripción',
    fields: [
      [
        { field: 'descripcion', label: 'Descripción', large: true },
        { field: 'telefono', label: 'Teléfono' },
      ],
    ]
  },
  {
    title: 'Información del  Contacto',
    fields: [
      [
        { field: 'nombreContacto', label: 'Nombre del Contacto' },
        { field: 'nombreCuenta', label: 'Nombre de la Cuenta' },
        { field: 'telefonoContacto', label: 'Teléfono de Contacto' },
      ],
    ]
  },
  {
    title: 'Información del Caso',
    fields: [
      [
        { field: 'numCasoSf', label: 'Numero del Caso' },
        { field: 'fechaHoraApertura', label: 'Fecha/Hora Apertura' },
        { field: 'numPol', label: 'Póliza' },
        { field: 'numeroPolizaTexto', label: 'N° Póliza Texto' },
        { field: 'fechaHoraCierre', label: 'Fecha/Hora Cierre' },
        { field: 'creadoPor', label: 'Creado Por' },
        { field: 'propietarioCaso', label: 'Propietario del Caso' },
        { field: 'ultimaModificacionPor', label: 'Última Modificación Por' },
      ],
    ],
  },
  {
    title: 'ANS',
    fields: [
      [
        { field: 'ansVencimiento', label: 'ANS Vencimiento' },
        { field: 'fechaVencimientoAns', label: 'Fecha Vencimiento ANS' },
        { field: 'estadoAns', label: 'Estado ANS' },
        { field: 'ans', label: 'ANS (Minutos)' },
      ],
    ],
  },
  {
    title: 'Asegurado',
    fields: [
      [
        { field: 'numbreCompletoAsegurado', label: 'Nombre Completo del Asegurado' },
        { field: 'apellidoPaternoAsegurado', label: 'Apellido Paterno del Asegurado' },
        { field: 'apellidoMaternoAsegurado', label: 'Apellido Materno del Asegurado' },
        { field: 'parentescoTitular', label: 'Parentesco con el Titular' },
        { field: 'codigoAfiliado', label: 'Código del Afiliado' },
        { field: 'edadAsegurado', label: 'Edad del Asegurado' },
      ],
    ],
  },
  {
    title: 'Políticas',
    fields: [
      [
        { field: 'cpc', label: 'CPC' },
        { field: 'tipoPoliticaAutorizacionAplicada', label: 'Tipo de Política Autorización Aplicada' },
        { field: 'tipoAtencionBrindar', label: 'Tipo de Atencion a Brindar' },
        { field: 'aplicaPoliticaAtencionAutorizacion', label: 'Aplica Política Atención o Autorización' },
      ],
    ],
  },
  {
    title: 'Gestión con el Proveedor',
    fields: [
      [
        { field: 'responsableCoordinacionServicio', label: 'Responsable Coordinacion Servicio' },
        { field: 'proveedor', label: 'Proveedor' },
        { field: 'fechaHoraAsignacionProveedor', label: 'Fecha/Hora Asignación al Proveedor' },
        { field: 'fechaHoraProgramacion', label: 'Fecha/Hora Programación' },
        { field: 'tipoConsultaNutricional', label: 'Tipo Consulta Nutricional' },
        { field: 'fechaHoraReprogramacion', label: 'Fecha/Hora de Reprogramación' },
      ],
    ],
  },
  {
    title: 'Datos de Culminación Proveedor',
    fields: [
      [
        { field: 'fechaHoraInicioAtencionProveedor', label: 'Fecha/Hora Inicio Atención Proveedor' },
        { field: 'fechaHoraCulminacionProveedor', label: 'Fecha/Hora Culminación Proveedor' },
        { field: 'diagnosticoCulminacionCie10', label: 'Diagnóstico Culminación cie 10' },
        { field: 'analisis', label: 'Análisis' },
        { field: 'tipoEntregaMedicamentos', label: 'Tipo de entrega de medicamentos' },
        { field: "fechaHoraEntregaMedicamentos", label: "Fecha Hora Entrega Medicamentos" },
        { field: 'diagnosticoCulminacion', label: 'Diagnóstico Culminación' },
        { field: 'solicitudAnalisis', label: 'Solicitu de Análisis' },
        { field: 'tratamientoProveedor', label: 'Tratamiento Proveedor' },
        { field: 'fechaHoraEnvioMedicamentos', label: 'Fecha/Hora Envío Medicamentos' },
        { field: 'fechaHoraEntregaResultadoAnalisis', label: 'Fecha/Hora Entrega Resultado Análisis' },
      ],
    ],
  },
  {
    title: 'Evolución',
    fields: [
      [
        { field: 'evolucion', label: 'Evolución' },
        { field: 'relevancias', label: 'Relevancias' },
        { field: 'realizacionConferencia', label: 'Realización de Conferencia' },
        { field: 'ampliacionMedicamentos', label: 'Ampliación de Medicamentos' },
      ],
    ],
  },
  {
    title: 'PDF Reporte al Proveedor',
    fields: [
      [
        { field: 'pdfReporteProveedor', label: 'PDF Reporte al Proveedor' },
      ],
    ],
  },
]

export const REFERENCE_COLLAPSIBLE_FIELD_GROUPS = [
  {
    title: 'Información',
    fields: [
      [
        { field: 'estadoCaso', label: 'Estado' },
        { field: 'descripcionTipoCasoSf', label: 'Tipo de Registro del Caso' },
        { field: 'regularizacionCaso', label: 'Regularización del Caso' },
      ],
    ]
  },
  {
    title: 'Tipificación del Caso',
    fields: [
      [
        { field: 'grupo', label: 'Grupo' },
        { field: 'tipo', label: 'tipo' },
        { field: 'categoria', label: 'Categoria' },
        { field: 'asunto', label: 'Asunto' },
        { field: 'tema', label: 'Tema' },
      ],
    ]
  },
  {
    title: 'Dirección',
    fields: [
      [
        { field: 'pais', label: 'País' },
        { field: 'departamento', label: 'Departamento' },
        { field: 'provincia', label: 'Provincia' },
        { field: 'distrito', label: 'Distrito' },
        { field: 'direccionSiniestro', label: 'Dirección del Siniestro' },
        { field: 'referencias', label: 'Referencias' },
      ],
    ]
  },
  {
    title: 'Descripción',
    fields: [
      [
        { field: 'descripcion', label: 'Descripción', large: true },
        { field: 'telefono', label: 'Teléfono' },
      ],
    ]
  },
  {
    title: 'Información de Contacto',
    fields: [
      [
        { field: 'nombreContacto', label: 'Nombre del Contacto' },
        { field: 'nombreCuenta', label: 'Nombre de la Cuenta' },
        { field: 'telefonoAccesoWhatsapp', label: 'Teléfono con Acceso WhatsApp' },
      ],
    ]
  },
  {
    title: 'Información del Caso',
    fields: [
      [
        { field: 'numCasoSf', label: 'Numero del Caso' },
        { field: 'fechaHoraApertura', label: 'Fecha/Hora Apertura' },
        { field: 'numPol', label: 'Póliza' },
        { field: 'fechaHoraCierre', label: 'Fecha/Hora Cierre' },
        { field: 'creadoPor', label: 'Creado Por' },
        { field: 'propietarioCaso', label: 'Propietario del Caso' },
        { field: 'ultimaModificacionPor', label: 'Última Modificación Por' },
      ],
    ],
  },
  {
    title: 'Asegurado',
    fields: [
      [
        { field: 'numbreCompletoAsegurado', label: 'Nombre Completo del Asegurado' },
        { field: 'apellidoPaternoAsegurado', label: 'Apellido Paterno del Asegurado' },
        { field: 'apellidoMaternoAsegurado', label: 'Apellido Materno del Asegurado' },
        { field: 'parentescoTitular', label: 'Parentesco con el Titular' },
        { field: 'codigoAfiliado', label: 'Código del Afiliado' },
        { field: 'edadAsegurado', label: 'Edad del Asegurado' },
      ],
    ],
  },
  {
    title: 'Cobertura',
    fields: [
      [
        { field: 'descripcionCobertura', label: 'Descripción de Cobertura', large: true },
        { field: 'montoDeducible', label: 'Monto Deducible' },
        { field: 'clinicasAfiliadas', label: 'Clínicas Afiliadas' },
        { field: 'coaseguro', label: 'Coaseguro' },
      ],
    ],
  },
  {
    title: 'Políticas',
    fields: [
      [
        { field: 'referenciaAutorizada', label: 'Referencia Autorizada' },
        { field: 'tipoPoliticaAutorizacionAplicada', label: 'Tipo de Política Autorización Aplicada' },
        { field: 'aplicaPoliticaAtencionAutorizacion', label: 'Aplica Política Atención o Autorización' },
      ],
    ],
  },
  {
    title: 'Coordinación (Común)',
    fields: [
      [
        { field: 'responsableCoordinacionServicio', label: 'Responsable Coordinacion Servicio' },
        { field: 'tipoTransporte', label: 'Tipo de Transporte' },
        { field: 'tipoAmbulancia', label: 'Tipo de Ambulancia' },
        { field: 'clinicaOrigen', label: 'Clínica de Origen' },
        { field: 'clinicaDestino', label: 'Clínica de Destino' },
        { field: 'clinicaDestinoManual', label: 'Clínica de Destino Manual' },
        { field: 'proveedor', label: 'Proveedor' },
        { field: 'fechaHoraAutorizacionRechazo', label: 'Fecha/Hora Autorización Rechazo' },
        { field: 'fechaHoraSolicitudTrasladoMedicoTratante', label: 'Fecha/Hora Sol. traslado Med. Tratante' },
        { field: 'fechaHoraAsignacionProveedor', label: 'Fecha/Hora Asignación al Proveedor' },
        { field: 'fechaHoraProgramacion', label: 'Fecha/Hora Programación' },
        { field: 'fechaHoraReprogramacion', label: 'Fecha/Hora de Reprogramación' },
        { field: 'fechaLlegadaClinica', label: 'Fecha de Llegada a Clínica' },
        { field: 'fechaHoraLlegadaClinicaDestino', label: 'Fecha/Hora Llegada a Clínica Destino' },
        { field: 'fechaHoraInicioAtencionProveedor', label: 'Fecha/Hora Inicio Atención Proveedor' },
        { field: 'fechaHoraInicioTraslado', label: 'Fecha/Hora Inicio Traslado Proveedor' },
        { field: 'fechaHoraLlegadaClinica', label: 'Fecha/Hora Llegada a Clínica' },
        { field: 'fechaHoraPartidaAvionAmbulancia', label: 'Fecha/Hora Partida Avión Ambulancia' },
        { field: 'fechaHoraLlegadaAeropuertoPaciente', label: 'Fecha/Hora Llegada Aeropuerto Paciente' },
        { field: 'fechaHoraReservaPasajes', label: 'Fecha/Hora Reserva Pasajes' },
      ],
    ],
  },
  {
    title: 'Avión Comercial',
    fields: [
      [
        { field: 'fechaHoraEmisionPasajes', label: 'Fecha/Hora Emisión Pasajes' },
      ],
    ],
  },
  {
    title: 'Avión Ambulancia',
    fields: [
      [
        { field: 'fechaHoraContactoPaciente', label: 'Fecha/Hora Contacto Paciente' },
        { field: 'fechaHoraPartidaPaciente', label: 'Fecha/Hora de Partida con Paciente' },
        { field: 'fechaHoraLlegadaAeropuerto', label: 'Fecha/Hora de Llegada al Aeropuerto' },
      ],
    ],
  },
  {
    title: 'Datos de Culminación Proveedor',
    fields: [
      [
        { field: 'nombreMedicoProveedor', label: 'Nombre Médico Proveedor' },
        { field: 'fechaHoraCulminacionProveedor', label: 'Fecha/Hora Culminación Proveedor' },
        { field: 'tratamientoProveedor', label: 'Tratamiento Proveedor' },
        { field: 'diagnosticoCulminacionCie10', label: 'Diagnóstico Culminación cie 10' },
      ],
    ],
  },
  {
    title: 'Evolución',
    fields: [
      [
        { field: 'evolucion', label: 'Evolución' },
        { field: 'relevancias', label: 'Relevancias' },
      ],
    ],
  },
]

export const GENERAL_RISK_COLLAPSIBLE_FIELD_GROUPS = [
  {
    title: 'Descripción',
    fields: [
      [
        { field: 'descripcion', label: 'Descripción', large: true }
      ],
    ]
  },
  {
    title: 'Detalles',
    fields: [
      [
        { field: 'numCasoSf', label: 'Número del Caso' },
        { field: 'estadoCaso', label: 'Estado' },
        { field: 'descripcionTipoCasoSf', label: 'Tipo de Registro del Caso' },
        { field: 'origenCaso', label: 'Origen del Caso' },
        { field: 'prioridad', label: 'Prioridad' },
        { field: 'propietarioCaso', label: 'Propietario del Caso' },
      ],
    ]
  },
  {
    title: 'Tipificación del Caso',
    fields: [
      [
        { field: 'grupo', label: 'Grupo' },
        { field: 'tipo', label: 'Tipo' },
        { field: 'categoria', label: 'Categoria' },
        { field: 'asunto', label: 'Asunto' },
        { field: 'tema', label: 'Tema' },
      ]
    ]
  },
  {
    title: 'Contacto',
    fields: [
      [
        { field: 'nombreContacto', label: 'Nombre del Contacto' },
        { field: 'nombreCuenta', label: 'Nombre de la Cuenta' },
        { field: 'tipoContacto', label: 'Tipo de Contacto' },
        { field: 'telefonoContacto', label: 'Teléfono de Contacto' },
        { field: 'telefono', label: 'Teléfono' },
        { field: 'correoElectronicoContactoVehiculoReemplazo', label: 'Correo Electrónico de Contacto' },
      ],
    ]
  },
  {
    title: 'Dirección',
    fields: [
      [
        { field: 'pais', label: 'País' },
        { field: 'departamento', label: 'Departamento' },
        { field: 'provincia', label: 'Provincia' },
        { field: 'distrito', label: 'Distrito' },
        { field: 'direccionSiniestro', label: 'Dirección del Siniestro' },
        { field: 'referencias', label: 'Referencias' },
      ],
    ]
  },
  {
    title: 'Ocurrencia',
    fields: [
      [
        { field: 'fechaHoraOcurrencia', label: 'Fecha/Hora Ocurrencia' },
      ],
    ]
  },
  {
    title: 'Información Proveedor',
    fields: [
      [
        { field: 'proveedor', label: 'Proveedor' },
        { field: 'ternaOfrecida', label: 'Terna Ofrecida' },
        { field: 'tipoSiniestro', label: 'Tipo de Siniestro' },
        { field: 'montoAproximadoReclamo', label: 'Monto Aproximado de Reclamo' },
        { field: 'fechaHoraAsignacionProveedor', label: 'Fecha/Hora Asignación al Proveedor' },
      ],
    ]
  },
  {
    title: 'Observaciones',
    fields: [
      [
        { field: 'anotaciones', label: 'Anotaciones', large: true },
      ],
    ]
  },
  {
    title: 'Póliza',
    fields: [
      [
        { field: 'numPol', label: 'Póliza' },
      ],
    ],
  },
  {
    title: 'Información del Caso',
    fields: [
      [
        { field: 'fechaHoraApertura', label: 'Fecha/Hora Apertura' },
        { field: 'creadoPor', label: 'Creado Por' },
        { field: 'fechaHoraCierre', label: 'Fecha/Hora Cierre' },
      ],
    ],
  },
  {
    title: 'Cobertura',
    fields: [
      [
        { field: 'descripcionCobertura', label: 'Descripción de Cobertura', large: true },
        { field: 'montoDeducible', label: 'Monto Deducible' },
        { field: 'coaseguro', label: 'Coaseguro' },
      ],
    ],
  },
]


export const FUNERAL_ASSISTANCE_COLLAPSIBLE_FIELD_GROUPS = [
  {
    title: 'Descripción',
    fields: [
      [
        { field: 'descripcion', label: 'Descripción', large: true }
      ],
    ]
  },
  {
    title: 'Detalles',
    fields: [
      [
        { field: 'numCasoSf', label: 'Número del Caso' },
        { field: 'estadoCaso', label: 'Estado' },
        { field: 'descripcionTipoCasoSf', label: 'Tipo de Registro del Caso' },
        { field: 'origenCaso', label: 'Origen del Caso' },
        { field: 'prioridad', label: 'Prioridad' },
        { field: 'propietarioCaso', label: 'Propietario del Caso' },
      ],
    ]
  },
  {
    title: 'Tipificación del Caso',
    fields: [
      [
        { field: 'grupo', label: 'Grupo' },
        { field: 'tipo', label: 'Tipo' },
        { field: 'categoria', label: 'Categoria' },
        { field: 'asunto', label: 'Asunto' },
        { field: 'tema', label: 'Tema' },
      ]
    ]
  },
  {
    title: 'Contacto',
    fields: [
      [
        { field: 'nombreContacto', label: 'Nombre del Contacto' },
        { field: 'telefonoContacto', label: 'Teléfono de Contacto' },
        { field: 'correoElectronicoContactoVehiculoReemplazo', label: 'Correo Electrónico de Contacto' },
        { field: 'tipoContacto', label: 'Tipo de Contacto' },
        { field: 'nombreCuenta', label: 'Nombre de la Cuenta' },
      ],
    ],
  },
  {
    title: 'Asegurado',
    fields: [
      [
        { field: 'numbreCompletoAsegurado', label: 'Nombre Completo del Asegurado' },
        { field: 'apellidoPaternoAsegurado', label: 'Apellido Paterno del Asegurado' },
        { field: 'apellidoMaternoAsegurado', label: 'Apellido Materno del Asegurado' },
        { field: 'parentescoTitular', label: 'Parentesco con el Titular' },
        { field: 'codigoAfiliado', label: 'Código del Afiliado' },
        { field: 'edadAsegurado', label: 'Edad del Asegurado' },
      ]
    ]
  },
  {
    title: 'Causa de Fallecimiento',
    fields: [
      [
        { field: 'causaFallecimiento', label: 'Causa de Fallecimiento', large: true },
      ]
    ]
  },
  {
    title: 'Dirección',
    fields: [
      [
        { field: 'pais', label: 'País' },
        { field: 'departamento', label: 'Departamento' },
        { field: 'provincia', label: 'Provincia' },
        { field: 'distrito', label: 'Distrito' },
        { field: 'direccionSiniestro', label: 'Dirección del Siniestro' },
        { field: 'referencias', label: 'Referencias' },
      ],
    ]
  },
  {
    title: 'Ocurrencia',
    fields: [
      [
        { field: 'fechaHoraOcurrencia', label: 'Fecha/Hora Ocurrencia' },
      ],
    ]
  },
  {
    title: 'Información del Proveedor',
    fields: [
      [
        { field: 'proveedor', label: 'Proveedor' },
        { field: 'fechaHoraAsignacionProveedor', label: 'Fecha/Hora Asignación al Proveedor' },
        { field: 'fechaHoraLlegadaLugar', label: 'Fecha/Hora de llegada al lugar' },
        { field: 'correoContactoProveedor', label: 'Correo contacto proveedor' },
        { field: 'fechaHoraInicioAtencionProveedor', label: 'Fecha/Hora Inicio Atención Proveedor' },
        { field: 'fechaHoraCulminacionProveedor', label: 'Fecha/Hora Culminación Proveedor' },
      ],
    ]
  },
  {
    title: 'Observaciones',
    fields: [
      [
        { field: 'anotaciones', label: 'Anotaciones', large: true },
      ],
    ]
  },
  {
    title: 'Póliza',
    fields: [
      [
        { field: 'numPol', label: 'Póliza' },
      ],
    ],
  },
  {
    title: 'ANS',
    fields: [
      [
        { field: 'fechaVencimientoAns', label: 'Fecha Vencimiento ANS' },
        { field: 'ansVencimiento', label: 'ANS Vencimiento' },
        { field: 'estadoAns', label: 'Estado ANS' },
        { field: 'ans', label: 'ANS (Minutos)' },
      ],
    ],
  },
  {
    title: 'Información del Caso',
    fields: [
      [
        { field: 'fechaHoraApertura', label: 'Fecha/Hora Apertura' },
        { field: 'creadoPor', label: 'Creado Por' },
        { field: 'fechaHoraCierre', label: 'Fecha/Hora Cierre' },
      ],
    ],
  },
  {
    title: 'Cobertura',
    fields: [
      [
        { field: 'descripcionCobertura', label: 'Descripción de Cobertura', large: true },
      ],
    ],
  },
]


export const HOME_ASSISTANCE_COLLAPSIBLE_FIELD_GROUPS = [
  {
    title: 'Descripción',
    fields: [
      [
        { field: 'descripcion', label: 'Descripción', large: true }
      ],
    ]
  },
  {
    title: 'Detalles',
    fields: [
      [
        { field: 'numCasoSf', label: 'Número del Caso' },
        { field: 'estadoCaso', label: 'Estado' },
        { field: 'descripcionTipoCasoSf', label: 'Tipo de Registro del Caso' },
        { field: 'origenCaso', label: 'Origen del Caso' },
        { field: 'prioridad', label: 'Prioridad' },
        { field: 'propietarioCaso', label: 'Propietario del Caso' },
      ],
    ]
  },
  {
    title: 'Tipificación del Caso',
    fields: [
      [
        { field: 'grupo', label: 'Grupo' },
        { field: 'tipo', label: 'Tipo' },
        { field: 'categoria', label: 'Categoria' },
        { field: 'asunto', label: 'Asunto' },
        { field: 'tema', label: 'Tema' },
      ]
    ]
  },
  {
    title: 'Contacto',
    fields: [
      [
        { field: 'nombreContacto', label: 'Nombre del Contacto' },
        { field: 'nombreCuenta', label: 'Nombre de la Cuenta' },
        { field: 'correoElectronicoContactoVehiculoReemplazo', label: 'Correo Electrónico de Contacto' },
        { field: 'tipoContacto', label: 'Tipo de Contacto' },
        { field: 'telefonoContacto', label: 'Teléfono de Contacto' },
        { field: 'telefono', label: 'Teléfono' },
      ],
    ]
  },
  {
    title: 'Dirección',
    fields: [
      [
        { field: 'pais', label: 'País' },
        { field: 'departamento', label: 'Departamento' },
        { field: 'provincia', label: 'Provincia' },
        { field: 'distrito', label: 'Distrito' },
        { field: 'direccionSiniestro', label: 'Dirección del Siniestro' },
        { field: 'referencias', label: 'Referencias' },
      ],
    ]
  },
  {
    title: 'Ocurrencia',
    fields: [
      [
        { field: 'fechaHoraOcurrencia', label: 'Fecha/Hora Ocurrencia' },
      ],
    ]
  },
  {
    title: 'Detalles del Proveedor',
    fields: [
      [
        { field: 'proveedor', label: 'Proveedor' },
        { field: 'fechaHoraInicioAtencionProveedor', label: 'Fecha/Hora Inicio Atención Proveedor' },
        { field: 'fechaHoraAsignacionProveedor', label: 'Fecha/Hora Asignación Proveedor' },
        { field: 'medioPago', label: 'Medio de pago' },
      ],
    ]
  }, 
  {
    title: 'Observaciones',
    fields: [
      [
        { field: 'anotaciones', label: 'Anotaciones', large: true },
      ],
    ]
  },
  {
    title: 'Póliza',
    fields: [
      [
        { field: 'numPol', label: 'Póliza' },
      ],
    ],
  },
  {
    title: 'ANS',
    fields: [
      [
        { field: 'fechaVencimientoAns', label: 'Fecha Vencimiento ANS' },
        { field: 'ansVencimiento', label: 'ANS Vencimiento' },
        { field: 'estadoAns', label: 'Estado ANS' },
        { field: 'ans', label: 'ANS (Minutos)' },
      ],
    ],
  },
  {
    title: 'Información del Caso',
    fields: [
      [
        { field: 'fechaHoraApertura', label: 'Fecha/Hora de Apertura' },
        { field: 'creadoPor', label: 'Creado Por' },
        { field: 'fechaHoraCierre', label: 'Fecha/Hora de Cierre' },
      ],
    ],
  },
  {
    title: 'Cobertura',
    fields: [
      [
        { field: 'descripcionCobertura', label: 'Descripción de Cobertura', large: true },
        { field: 'montoDeducible', label: 'Monto Deducible' },
        { field: 'coaseguro', label: 'Coaseguro' },
      ],
    ],
  },
]

export const MEDICINE_DELIVERY_COLLAPSIBLE_FIELD_GROUPS = [
  {
    title: 'Descripción',
    fields: [
      [
        { field: 'descripcion', label: 'Descripción', large: true }
      ],
    ]
  },
  {
    title: 'Detalles',
    fields: [
      [
        { field: 'numCasoSf', label: 'Número del Caso' },
        { field: 'estadoCaso', label: 'Estado' },
        { field: 'descripcionTipoCasoSf', label: 'Tipo de Registro del Caso' },
        { field: 'origenCaso', label: 'Origen del Caso' },
        { field: 'prioridad', label: 'Prioridad' },
        { field: 'propietarioCaso', label: 'Propietario del Caso' },
      ],
    ]
  },
  {
    title: 'Tipificación del Caso',
    fields: [
      [
        { field: 'grupo', label: 'Grupo' },
        { field: 'tipo', label: 'Tipo' },
        { field: 'categoria', label: 'Categoria' },
        { field: 'asunto', label: 'Asunto' },
        { field: 'tema', label: 'Tema' },
      ]
    ]
  },
  {
    title: 'Contacto',
    fields: [
      [
        { field: 'nombreContacto', label: 'Nombre del Contacto' },
        { field: 'nombreCuenta', label: 'Nombre de la Cuenta' },
      ],
    ],
  },
  {
    title: 'Asegurado',
    fields: [
      [
        { field: 'numbreCompletoAsegurado', label: 'Nombre Completo del Asegurado' },
        { field: 'apellidoPaternoAsegurado', label: 'Apellido Paterno del Asegurado' },
        { field: 'apellidoMaternoAsegurado', label: 'Apellido Materno del Asegurado' },
        { field: 'parentescoTitular', label: 'Parentesco con el Titular' },
        { field: 'codigoAfiliado', label: 'Código del Afiliado' },
        { field: 'edadAsegurado', label: 'Edad del Asegurado' },
      ],
    ],
  },
  {
    title: 'Detalles del Caso',
    fields: [
      [
        { field: 'nombreClinica', label: 'Nombre de la clínica' },
        { field: 'diagnostico', label: 'Diagnóstico' },
        { field: 'cantidadMesesAutorizados', label: 'Cantidad de meses autorizados' },
      ],
    ]
  },
  {
    title: 'Medicinas, cantidades y posología',
    fields: [
      [
        { field: 'medicinasCantidadesPosologia', label: 'Medicinas, cantidades y posología', large: true, },
      ],
    ],
  },
  {
    title: 'Dirección',
    fields: [
      [
        { field: 'pais', label: 'País' },
        { field: 'departamento', label: 'Departamento' },
        { field: 'provincia', label: 'Provincia' },
        { field: 'distrito', label: 'Distrito' },
        { field: 'direccionServicio', label: 'Dirección Servicio' },
        { field: 'referencias', label: 'Referencias' },
      ],
    ]
  },
  {
    title: 'Póliza',
    fields: [
      [
        { field: 'numPol', label: 'Póliza' },
      ],
    ],
  },
  {
    title: 'Observaciones',
    fields: [
      [
        { field: 'anotaciones', label: 'Anotaciones', large: true },
      ],
    ]
  },
  {
    title: 'ANS',
    fields: [
      [
        { field: 'fechaVencimientoAns', label: 'Fecha Vencimiento ANS' },
        { field: 'ansVencimiento', label: 'ANS Vencimiento' },
        { field: 'estadoAns', label: 'Estado ANS' },
        { field: 'ans', label: 'ANS (Minutos)' },
      ],
    ],
  },
  {
    title: 'Información del Caso',
    fields: [
      [
        { field: 'fechaHoraApertura', label: 'Fecha/Hora Apertura' },
        { field: 'creadoPor', label: 'Creado Por' },
        { field: 'fechaHoraCierre', label: 'Fecha/Hora Cierre' },
      ],
    ]
  },
  {
    title: 'Coberturas',
    fields: [
      [
        { field: 'descripcionCobertura', label: 'Descripción de Cobertura', large: true },
        { field: 'montoDeducible', label: 'Monto Deducible' },
        { field: 'coaseguro', label: 'Coaseguro' },
      ],
    ],
  },
]

export const TRAVEL_ASSISTANCE_COLLAPSIBLE_FIELD_GROUPS = [
  {
    title: 'Descripción',
    fields: [
      [
        { field: 'descripcion', label: 'Descripción', large: true }
      ],
    ]
  },
  {
    title: 'Detalles',
    fields: [
      [
        { field: 'numCasoSf', label: 'Número del Caso' },
        { field: 'estadoCaso', label: 'Estado' },
        { field: 'descripcionTipoCasoSf', label: 'Tipo de Registro del Caso' },
        { field: 'origenCaso', label: 'Origen del Caso' },
        { field: 'prioridad', label: 'Prioridad' },
        { field: 'propietarioCaso', label: 'Propietario del Caso' },
      ],
    ]
  },
  {
    title: 'Tipificación del Caso',
    fields: [
      [
        { field: 'grupo', label: 'Grupo' },
        { field: 'tipo', label: 'Tipo' },
        { field: 'categoria', label: 'Categoria' },
        { field: 'asunto', label: 'Asunto' },
        { field: 'tema', label: 'Tema' },
      ]
    ]
  },
  {
    title: 'Asegurado',
    fields: [
      [
        { field: 'numbreCompletoAsegurado', label: 'Nombre Completo del Asegurado' },
        { field: 'apellidoPaternoAsegurado', label: 'Apellido Paterno del Asegurado' },
        { field: 'apellidoMaternoAsegurado', label: 'Apellido Materno del Asegurado' },
        { field: 'parentescoTitular', label: 'Parentesco con el Titular' },
        { field: 'codigoAfiliado', label: 'Código del Afiliado' },
        { field: 'edadAsegurado', label: 'Edad del Asegurado' },
      ],
    ],
  },
  {
    title: 'Condiciones del Pasajero',
    fields: [
      [
        { field: 'condicionPasajero', label: 'Condición de pasajero' },
        { field: 'fechaSalidaPais', label: 'Fecha de Salida del País' },
        { field: 'telefono', label: 'Teléfono' },
        { field: 'telefonoAccesoWhatsapp', label: 'Teléfono con Acceso WhatsApp' },
        { field: 'correoElectronicoContactoVehiculoReemplazo', label: 'Correo Electrónico de Contacto' },
      ],
    ],
  },
  {
    title: 'Dirección',
    fields: [
      [
        { field: 'pais', label: 'País' },
        { field: 'departamento', label: 'Departamento' },
        { field: 'provincia', label: 'Provincia' },
        { field: 'distrito', label: 'Distrito' },
        { field: 'direccionServicio', label: 'Dirección Servicio' },
        { field: 'referencias', label: 'Referencias' },
      ],
    ]
  },
  {
    title: 'Ocurrencias',
    fields: [
      [
        { field: 'fechaHoraOcurrencia', label: 'Fecha/Hora Ocurrencia' },
      ],
    ]
  },
  {
    title: 'Detalles del Proveedor',
    fields: [
      [
        { field: 'proveedor', label: 'Proveedor' },
        { field: 'correoContactoProveedor', label: 'Correo contacto proveedor' },
        { field: 'fechaHoraAsignacionProveedor', label: 'Fecha/Hora Asignación Proveedor' },
        { field: 'fechaHoraProgramacion', label: 'Fecha/Hora Programación' },
        { field: 'fechaHoraLlegadaLugar', label: 'Fecha/Hora de llegada al lugar' },
        { field: 'fechaHoraInicioAtencionProveedor', label: 'Fecha/Hora Inicio Atención Proveedor' },
        { field: 'fechaHoraCulminacionProveedor', label: 'Fecha/Hora Culminación Proveedor' },
      ],
    ]
  },
  {
    title: 'Observaciones',
    fields: [
      [
        { field: 'anotaciones', label: 'Anotaciones', large: true },
      ],
    ]
  },
  {
    title: 'Póliza',
    fields: [
      [
        { field: 'numPol', label: 'Póliza' },
      ],
    ],
  },
  {
    title: 'ANS',
    fields: [
      [
        { field: 'ansVencimiento', label: 'ANS Vencimiento' },
        { field: 'fechaVencimientoAns', label: 'Fecha Vencimiento ANS' },
        { field: 'estadoAns', label: 'Estado ANS' },
        { field: 'ans', label: 'ANS (Minutos)' },
      ],
    ],
  },
  {
    title: 'Información del Caso',
    fields: [
      [
        { field: 'fechaHoraApertura', label: 'Fecha/Hora Apertura' },
        { field: 'creadoPor', label: 'Creado Por' },
        { field: 'fechaHoraCierre', label: 'Fecha/Hora Cierre' },
      ],
    ],
  },
  {
    title: 'Información del Contacto',
    fields: [
      [
        { field: 'nombreContacto', label: 'Nombre del Contacto' },
        { field: 'nombreCuenta', label: 'Nombre de la Cuenta' },
        { field: 'tipoContacto', label: 'Tipo de Contacto' },
      ],
    ],
  },
  {
    title: 'Cobertura',
    fields: [
      [
        { field: 'descripcionCobertura', label: 'Descripción de Cobertura', large: true },
        { field: 'montoDeducible', label: 'Monto Deducible' },
        { field: 'coaseguro', label: 'Coaseguro' },
      ],
    ],
  },
]

export const SCHEDULED_ATTENTION_COLLAPSIBLE_FIELD_GROUPS = [
  {
    title: 'Descripción',
    fields: [
      [
        { field: 'descripcion', label: 'Descripción', large: true }
      ],
    ]
  },
  {
    title: 'Detalles',
    fields: [
      [
        { field: 'numCasoSf', label: 'Número del Caso' },
        { field: 'estadoCaso', label: 'Estado' },
        { field: 'descripcionTipoCasoSf', label: 'Tipo de Registro del Caso' },
        { field: 'origenCaso', label: 'Origen del Caso' },
        { field: 'prioridad', label: 'Prioridad' },
        { field: 'propietarioCaso', label: 'Propietario del Caso' },
      ],
    ]
  },
  {
    title: 'Tipificación del Caso',
    fields: [
      [
        { field: 'grupo', label: 'Grupo' },
        { field: 'tipo', label: 'Tipo' },
        { field: 'categoria', label: 'Categoria' },
        { field: 'asunto', label: 'Asunto' },
        { field: 'tema', label: 'Tema' },
      ]
    ]
  },
  {
    title: 'Asegurado',
    fields: [
      [
        { field: 'numbreCompletoAsegurado', label: 'Nombre Completo del Asegurado' },
        { field: 'apellidoPaternoAsegurado', label: 'Apellido Paterno del Asegurado' },
        { field: 'apellidoMaternoAsegurado', label: 'Apellido Materno del Asegurado' },
        { field: 'parentescoTitular', label: 'Parentesco con el Titular' },
        { field: 'codigoAfiliado', label: 'Código del Afiliado' },
        { field: 'edadAsegurado', label: 'Edad del Asegurado' },
      ],
    ],
  },
  {
    title: 'Información de Contacto',
    fields: [
      [
        { field: 'nombreCuenta', label: 'Nombre de la Cuenta' },
        { field: 'nombreContacto', label: 'Nombre del Contacto' },
        { field: 'tipoContacto', label: 'Tipo de Contacto' },
        { field: 'telefono', label: 'Teléfono' },
        { field: 'telefonoAccesoWhatsapp', label: 'Teléfono con Acceso WhatsApp' },
        { field: 'correoElectronicoContactoVehiculoReemplazo', label: 'Correo Electrónico de Contacto' },
      ],
    ]
  },
  {
    title: 'Dirección',
    fields: [
      [
        { field: 'pais', label: 'País' },
        { field: 'departamento', label: 'Departamento' },
        { field: 'provincia', label: 'Provincia' },
        { field: 'distrito', label: 'Distrito' },
        { field: 'direccionServicio', label: 'Dirección Servicio' },
        { field: 'referencias', label: 'Referencias' },
      ],
    ]
  },
  {
    title: 'Observaciones',
    fields: [
      [
        { field: 'anotaciones', label: 'Anotaciones', large: true },
      ],
    ]
  },
  {
    title: 'Póliza',
    fields: [
      [
        { field: 'numPol', label: 'Póliza' },
      ],
    ],
  },
  {
    title: 'Información del Caso',
    fields: [
      [
        { field: 'fechaHoraApertura', label: 'Fecha/Hora Apertura' },
        { field: 'fechaHoraCierre', label: 'Fecha/Hora Cierre' },
      ],
    ],
  },
  {
    title: 'Cobertura',
    fields: [
      [
        { field: 'montoDeducible', label: 'Monto Deducible' },
        { field: 'coaseguro', label: 'Coaseguro' },
      ],
    ],
  },
  // {
  //   title: 'Condiciones de Atención',
  //   fields: [
  //     [
  //       { field: 'pais', label: 'País' },
  //       { field: 'departamento', label: 'Departamento' },
  //       { field: 'provincia', label: 'Provincia' },
  //       { field: 'distrito', label: 'Distrito' },
  //       { field: 'direccionServicio', label: 'Dirección Servicio' },
  //       { field: 'referencias', label: 'Referencias' },
  //     ],
  //   ]
  // },

]

export const LEGAL_CLAIM_COLLAPSIBLE_FIELD_GROUPS = [
  {
    title: 'Información del Caso',
    fields: [
      [
        { field: 'nombreContacto', label: 'Nombre de Contacto' },
        { field: 'numbreCuenta', label: 'Nombre de la Cuenta' },
        { field: 'estado', label: 'Estado' },
        { field: 'origenCaso', label: 'Origen del Caso' },
        { field: 'organismo', label: 'Organismo' },
        { field: 'expediente', label: 'Expediente' },
        { field: 'descripcionTipoCasoSf', label: 'Tipo de Registro del Caso' },
        { field: 'creadoPor', label: 'Creado Por' },
        { field: 'propietarioCaso', label: 'Propietario del Caso' },
        { field: 'ultimaModificacionPor', label: 'Última Modificación Por' },
      ],
    ],
  },
  {
    title: 'Tipificación del Caso',
    fields: [
      [
        { field: 'grupo', label: 'Grupo' },
        { field: 'tipo', label: 'Tipo' },
        { field: 'categoria', label: 'Categoria' },
        { field: 'asunto', label: 'Asunto' },
        { field: 'tema', label: 'Tema' },
      ]
    ]
  },
  {
    title: 'Descripción',
    fields: [
      [
        { field: 'descripcion', label: 'Descripción', large: true }
      ],
    ]
  },
 {
    title: 'Detalles de Caso',
    fields: [
      [
        { field: 'numCasoSf', label: 'N° del Caso' },
        { field: 'fechaHoraApertura', label: 'Fecha/Hora de Apertura' },
        { field: 'numPol', label: 'Póliza' },
        { field: 'fechaHoraCierre', label: 'Fecha/Hora de Cierre' },
        { field: 'prioridad', label: 'Prioridad' },

      ],
    ]
  },
  {
    title: 'Asegurado',
    fields: [
      [
        { field: 'numbreCompletoAsegurado', label: 'Nombre Completo del Asegurado' },
        { field: 'apellidoPaternoAsegurado', label: 'Apellido Paterno del Asegurado' },
        { field: 'apellidoMaternoAsegurado', label: 'Apellido Materno del Asegurado' },
        { field: 'parentescoTitular', label: 'Parentesco con el Titular' },
        { field: 'codigoAfiliado', label: 'Código del Afiliado' },
        { field: 'edadAsegurado', label: 'Edad del Asegurado' },
      ],
    ],
  },
  {
    title: 'ANS',
    fields: [
      [
        { field: 'nombreAsignacion', label: 'Nombre de Asignación' },
        { field: 'ansVencimiento', label: 'ANS Vencimiento' },
        { field: 'fechaVencimientoAns', label: 'Fecha Vencimiento ANS' },
        { field: 'estadoAns', label: 'Estado ANS' },
        { field: 'ans', label: 'ANS (Minutos)' },
      ],
    ],
  },
  {
    title: 'Detalles del Siniestro',
    fields: [
      [
        { field: 'numeroSiniestro', label: 'N° de Siniestro' },
      ],
    ],
  },
  {
    title: 'Anotaciones',
    fields: [
      [
        { field: 'anotaciones', label: 'Anotaciones', large: true },
      ],
    ]
  },
  {
    title: 'Primera Instancia',
    fields: [
      [
        { field: 'fechaRespuesta', label: 'Fecha de Respuesta' },
        { field: 'canalRespuesta', label: 'Canal de Respuesta' },
      ],
    ]
  },
  {
    title: 'Medida Correctiva',
    fields: [
      [
        { field: 'medidaCorrectiva', label: 'Medida Correctiva' },
      ],
    ]
  },
]

export const REPLACEMENT_DRIVER_COLLAPSIBLE_FIELD_GROUPS = [
  {
    title: 'Descripción',
    fields: [
      [
        { field: 'descripcion', label: 'Descripción', large: true }
      ],
    ]
  },
  {
    title: 'Detalles',
    fields: [
      [
        { field: 'numCasoSf', label: 'Número del Caso' },
        { field: 'estadoCaso', label: 'Estado' },
        { field: 'descripcionTipoCasoSf', label: 'Tipo de Registro del Caso' },
        { field: 'origenCaso', label: 'Origen del Caso' },
        { field: 'prioridad', label: 'Prioridad' },
        { field: 'propietarioCaso', label: 'Propietario del Caso' },
      ],
    ]
  },
  {
    title: 'Tipificación del Caso',
    fields: [
      [
        { field: 'grupo', label: 'Grupo' },
        { field: 'tipo', label: 'Tipo' },
        { field: 'categoria', label: 'Categoria' },
        { field: 'asunto', label: 'Asunto' },
        { field: 'tema', label: 'Tema' },
        { field: 'subtema', label: 'Sub Tema' },
      ]
    ]
  },
  {
    title: 'Conductor',
    fields: [
      [
        { field: 'nombreConductor', label: 'Nombre del Conductor' },
        { field: 'apellidoPaternoConductor', label: 'Apellido Paterno del Conductor' },
        { field: 'apellidoMaternoConductor', label: 'Apellido Materno del Conductor' },
        { field: 'telefonoConductor', label: 'Teléfono del Conductor' },
      ],
    ]
  },
  {
    title: 'Dirección Origen',
    fields: [
      [
        { field: 'pais', label: 'País' },
        { field: 'departamento', label: 'Departamento' },
        { field: 'provincia', label: 'Provincia' },
        { field: 'distrito', label: 'Distrito' },
        { field: 'direccionSiniestro', label: 'Dirección del Siniestro' },
        { field: 'referencias', label: 'Referencias' },
      ],
    ]
  },
  {
    title: 'Dirección Destino',
    fields: [
      [
        { field: 'paisDestino', label: 'País Destino' }, // REFERENCIA, preguntar
        { field: 'departamentoDestino', label: 'Departamento Destino' }, // REFERENCIA, preguntar
        { field: 'provinciaDestino', label: 'Provincia Destino' }, // REFERENCIA, preguntar
        { field: 'distritoDestino', label: 'Distrito Destino' }, // REFERENCIA, preguntar
        { field: 'direccionDestino', label: 'Dirección Destino' },
        { field: 'referenciasDestino', label: 'Referencias Destino' },
      ],
    ]
  },
  {
    title: 'Ocurrencia',
    fields: [
      [
        { field: 'fechaHoraOcurrencia', label: 'Fecha/Hora Ocurrencia' },
      ],
    ]
  },
  {
    title: 'Datos del vehículo',
    fields: [
      [
        { field: 'cpc', label: 'CPC' },
        { field: 'placaVehiculo', label: 'Placa' },
        { field: 'motorVehiculo', label: 'Motor' },
        { field: 'claseVehiculo', label: 'Clase de Vehículo' },
        { field: 'marcaVehiculo', label: 'Marca' },
        { field: 'modeloVehiculo', label: 'Modelo' },
        { field: 'colorVehiculo', label: 'Color' },
        { field: 'anioVehiculo', label: 'Año' },
      ],
    ],
  },
  {
    title: 'Proveedor',
    fields: [
      [
        { field: 'proveedor', label: 'Proveedor' },
        { field: 'fechaHoraProgramacion', label: 'Fecha/Hora Programación' },
        { field: 'fechaHoraReprogramacion', label: 'Fecha/Hora de Reprogramación' },
        { field: 'fechaHoraAsignacionProveedor', label: 'Fecha/Hora Asignación al Proveedor' },
      ],
    ]
  },
  {
    title: 'PDF Reporte al Proveedor',
    fields: [
      [
        { field: 'pdfReporteProveedor', label: 'PDF Reporte al Proveedor' },
      ],
    ],
  },
  {
    title: 'Observaciones',
    fields: [
      [
        { field: 'anotaciones', label: 'Anotaciones', large: true },
      ],
    ]
  },
  {
    title: 'Póliza',
    fields: [
      [
        { field: 'numPol', label: 'Póliza' },
      ],
    ],
  },
  {
    title: 'Motivo de Anulación',
    fields: [
      [
        { field: 'motivoAnulacion', label: 'Motivo de Anulación' }
      ],
    ],
  },
  {
    title: 'ANS',
    fields: [
      [
        { field: 'nombreAsignacion', label: 'Nombre de Asignación' },
        { field: 'ansVencimiento', label: 'ANS Vencimiento' },
        { field: 'fechaVencimientoAns', label: 'Fecha Vencimiento ANS' },
        { field: 'estadoAns', label: 'Estado ANS' },
        { field: 'ans', label: 'ANS (Minutos)' },
      ],
    ],
  },
  {
    title: 'Datos de Culminación Proveedor',
    fields: [
      [
        { field: 'fechaHoraInicioAtencionProveedor', label: 'Fecha/Hora Inicio Atención Proveedor' },
        { field: 'fechaHoraCulminacionProveedor', label: 'Fecha/Hora Culminación Proveedor' },
      ],
    ],
  },
  {
    title: 'Información del Caso',
    fields: [
      [
        { field: 'fechaHoraApertura', label: 'Fecha/Hora Apertura' },
        { field: 'fechaHoraCierre', label: 'Fecha/Hora Cierre' },
        { field: 'creadoPor', label: 'Creado Por' },
      ],
    ],
  },
  {
    title: 'Registro del Caso',
    fields: [
      [
        { field: 'nombreContacto', label: 'Nombre del Contacto' },
        { field: 'nombreCuenta', label: 'Nombre de la Cuenta' },
        { field: 'correoElectronicoContactoVehiculoReemplazo', label: 'Correo Electrónico de Contacto' },
        { field: 'tipoContacto', label: 'Tipo de Contacto' },
        { field: 'telefonoContacto', label: 'Teléfono de Contacto' },
      ],
    ]
  },
  {
    title: 'Cobertura',
    fields: [
      [
        { field: 'descripcionCobertura', label: 'Descripción de Cobertura', large: true },
        { field: 'montoDeducible', label: 'Monto Deducible' },
        { field: 'coaseguro', label: 'Coaseguro' },
      ],
    ],
  },
  {
    title: 'Responsable',
    fields: [
      [
        { field: 'grupoResponsable', label: 'Grupo Responsable' },
        { field: 'responsable', label: 'Responsable' },
        { field: 'responsableTieneLicenciaSalesforce', label: 'Responsable Tiene Licencia en SalesForce' },
      ],
    ],
  },
]


export const RENEWAL_CHARGE_COLLAPSIBLE_FIELD_GROUPS = [
  {
    title: 'Datos de Carga - Búsqueda',
    fields: [
      [
        { field: 'polizaCargaMasiva', label: 'Póliza Carga Masiva' },
        { field: 'ramoCargaArchivo', label: 'Ramo Carga Archivo' },
        { field: 'tipoDocumentoContratante', label: 'Tipo Documento Contratante' },
        { field: 'numeroDocumentoContratante', label: 'N° Documento Contratante' },
        { field: 'tipoDocumentoTitular', label: 'Tipo Documento Titular' },
        { field: 'numeroDocumentoTitular', label: 'N° Documento Titular' },
        { field: 'placaVehiculo', label: 'Placa' },
        { field: 'motorVehiculo', label: 'Motor' },
        { field: 'masivo', label: 'Masivo' },
      ],
    ]
  },
  {
    title: 'Información de Contacto',
    fields: [
      [
        { field: 'nombreContacto', label: 'Nombre del Contacto' },
        { field: 'nombreCuenta', label: 'Nombre de la Cuenta' },
        { field: 'telefonoContacto', label: 'Teléfono de Contacto' },
        { field: 'otrosTelefonosContratante', label: 'Otros Teléfonos Contratante' },
        { field: 'correoElectronico', label: 'Correo Electrónico' },
      ],
    ]
  },
  {
    title: 'Información de Vigencia Anterior',
    fields: [
      [
        { field: 'numPol', label: 'Póliza' },
        { field: 'primaBrutaRenovada', label: 'Prima Bruta Renovada' },
        { field: 'cantidadCuotas', label: 'Cantidad de Cuotas' },
        { field: 'tipoFinanciamiento', label: 'Tipo de Financiamiento' },
      ],
    ]
  },
  {
    title: 'Pago Vencido',
    fields: [
      [
        { field: 'motivoRechazo', label: 'Motivo de rechazo' },
        { field: 'numDocPagoVigenciaActual', label: 'N° Doc. Cobranza Vigencia Actual' },
        { field: 'fechaVencVigenciaActual', label: 'Fecha Venc. Doc. Cobranza Vigencia Actual' },
      ],
    ]
  },
  {
    title: 'Tipificación del Caso',
    fields: [
      [
        { field: 'grupo', label: 'Grupo' },
        { field: 'tipo', label: 'Tipo' },
        { field: 'categoria', label: 'Categoria' },
        { field: 'asunto', label: 'Asunto' },
        { field: 'tema', label: 'Tema' },
      ]
    ]
  },
  {
    title: 'Resultados',
    fields: [
      [
        { field: 'resultado', label: 'Resultado' },
        { field: 'subResultado', label: 'Sub Resultado' },
        { field: 'infoAdicional', label: 'Info Adicional' },
        { field: 'fechaCompromisoPago', label: 'Fecha de Compromiso de Pago' },
      ]
    ]
  },
  {
    title: 'Gestión del Caso',
    fields: [
      [
        { field: 'afiliadoCargoCuenta', label: 'Se Afilió a Cargo en Cuenta' },
        { field: 'audio', label: 'Audio' },
        { field: 'autorizacionDescuento', label: 'Autorización Descuento' },
        { field: 'cantidadLlamadas', label: 'Cantidad de Llamadas' },
        { field: 'fechaCompraNuevoBien', label: 'Fecha de Compra del Nuevo Bien' },
      ]
    ]
  },
  {
    title: 'Descripción',
    fields: [
      [
        { field: 'descripcion', label: 'Descripción', large: true }
      ],
    ]
  },
  {
    title: 'Registro del Caso',
    fields: [
      [
        { field: 'origenCaso', label: 'Origen del Caso' },
        { field: 'estadoCaso', label: 'Estado' },
        { field: 'prioridad', label: 'Prioridad' },
        { field: 'descripcionTipoCasoSf', label: 'Tipo de Registro del Caso' },
        { field: 'numCasoSf', label: 'Numero del Caso' },
      ],
    ]
  },
  {
    title: 'Información Próxima Cobranza',
    fields: [
      [
        { field: 'numDocPago', label: 'N° Documento Cobranza' },
        { field: 'fechaVenc', label: 'Fecha Venc. Documento Cobranza' },
      ],
    ]
  },
  {
    title: 'ANS',
    fields: [
      [
        { field: 'nombreAsignacion', label: 'Nombre de Asignación' },
        { field: 'ansVencimiento', label: 'ANS Vencimiento' },
        { field: 'fechaVencimientoAns', label: 'Fecha Vencimiento ANS' },
        { field: 'estadoAns', label: 'Estado ANS' },
        { field: 'ans', label: 'ANS (Minutos)' },
      ],
    ],
  },
  {
    title: 'Información del Caso',
    fields: [
      [
        { field: 'fechaHoraApertura', label: 'Fecha/Hora Apertura' },
        { field: 'fechaHoraCierre', label: 'Fecha/Hora Cierre' },
        { field: 'propietarioCaso', label: 'Propietario del Caso' },
        { field: 'areaResponsablePropietario', label: 'Area Responsable Propietario' },
      ],
    ],
  },
  {
    title: 'Información del Sistema',
    fields: [
      [
        { field: 'creadoPor', label: 'Creado Por' },
        { field: 'ultimaModificacionPor', label: 'Última Modificación Por' },
        { field: 'areaResponsableCreacion', label: 'Área Responsable Creación' },
      ],
    ],
  },
]

export const VEHICULAR_EMERGENCY_COLLAPSIBLE_FIELD_GROUPS = [
  {
    title: 'Descripción',
    fields: [
      [
        { field: 'descripcion', label: 'Descripción', large: true }
      ],
    ]
  },
  {
    title: 'Detalles',
    fields: [
      [
        { field: 'numCasoSf', label: 'Número del Caso' },
        { field: 'estadoCaso', label: 'Estado' },
        { field: 'descripcionTipoCasoSf', label: 'Tipo de Registro del Caso' },
        { field: 'origenCaso', label: 'Origen del Caso' },
        { field: 'prioridad', label: 'Prioridad' },
        { field: 'campania', label: 'Campaña' },
        { field: 'indicadorBbva', label: 'Indicador BBVA' },
      ],
    ],
  },
  {
    title: 'Tipificación del Caso',
    fields: [
      [
        { field: 'grupo', label: 'Grupo' },
        { field: 'tipo', label: 'Tipo' },
        { field: 'categoria', label: 'Categoria' },
        { field: 'asunto', label: 'Asunto' },
        { field: 'tema', label: 'Tema' },
        { field: 'subtema', label: 'Sub Tema' },
        { field: 'tipoAtencion', label: 'Tipo de Atención' },
        { field: 'tipoServicio', label: 'Tipo de Servicio' },
      ]
    ]
  },
  {
    title: 'Contacto',
    fields: [
      [
        { field: 'nombreContacto', label: 'Nombre del Contacto' },
        { field: 'nombreCuenta', label: 'Nombre de la Cuenta' },
        { field: 'telefonoContacto', label: 'Teléfono de Contacto' },
      ],
    ]
  },
  {
    title: 'Conductor',
    fields: [
      [
        { field: 'nombreConductor', label: 'Nombre del Conductor' },
        { field: 'apellidoPaternoConductor', label: 'Apellido Paterno del Conductor' },
        { field: 'apellidoMaternoConductor', label: 'Apellido Materno del Conductor' },
        { field: 'tipoDocumentoConductor', label: 'Tipo Documento Conductor' },
        { field: 'numeroDocumentoConductor', label: 'N° Documento del Conductor' },
        { field: 'licenciaConductor', label: 'Licencia del Conductor' },
        { field: 'telefonoConductor', label: 'Teléfono del Conductor' },
        { field: 'emailConductor', label: 'Correo Eléctronico del Conductor' },
      ],
    ]
  },
  {
    title: 'Dirección',
    fields: [
      [
        { field: 'pais', label: 'País' },
        { field: 'departamento', label: 'Departamento' },
        { field: 'provincia', label: 'Provincia' },
        { field: 'distrito', label: 'Distrito' },
        { field: 'direccionSiniestro', label: 'Dirección del Siniestro' },
        { field: 'referencias', label: 'Referencias' },
        { field: 'anillo', label: 'Anillo' },
      ],
    ]
  },
  {
    title: 'Ocurrencia',
    fields: [
      [
        { field: 'fechaHoraOcurrencia', label: 'Fecha/Hora Ocurrencia' },
        { field: 'cantidadDiasHoraDesdeOcurrencia', label: 'Cantidad Día(s)/Hora(s) Desde Ocurrencia' },
        { field: 'semaforoOcurrencia', label: 'Semáforo de Ocurrencia' },
        { field: 'lugarOcurrencia', label: 'Lugar de Ocurrencia' },
      ],
    ]
  },
  {
    title: 'Datos de vehículo',
    fields: [
      [
        { field: 'cpc', label: 'CPC' },
        { field: 'numeroPolizaTexto', label: 'N° Póliza Texto' },
        { field: 'placaVehiculo', label: 'Placa' },
        { field: 'motorVehiculo', label: 'Motor' },
        { field: 'claseVehiculo', label: 'Clase de Vehículo' },
        { field: 'marcaVehiculo', label: 'Marca' },
        { field: 'modeloVehiculo', label: 'Modelo' },
        { field: 'colorVehiculo', label: 'Color' },
        { field: 'anioVehiculo', label: 'Año' }
        
      ],
    ],
  },
  {
    title: 'Proveedor',
    fields: [
      [
        { field: 'proveedor', label: 'Proveedor' },
        { field: 'fechaHoraLlegadaLugar', label: 'Fecha/Hora de llegada al lugar' },
        { field: 'fechaHoraContactado', label: 'Fecha/Hora de Contactado' },
        { field: 'fechaHoraCulminacionProveedor', label: 'Fecha/Hora Culminación Proveedor' },
        { field: 'fechaHoraAsignacionProveedor', label: 'Fecha/Hora Asignación al Proveedor' },
        { field: 'fechaHoraInicioAtencionProveedor', label: 'Fecha/Hora Inicio Atención Proveedor' },
        { field: 'fechaHoraContactado', label: 'Fecha/Hora de Contactado por Validar' },
      ],
    ]
  },
  {
    title: 'URLs PDF',
    fields: [
      [
        { field: 'pdfReporteProveedor', label: 'PDF Reporte al Proveedor' },
      ],
    ],
  },
  {
    title: 'Observaciones',
    fields: [
      [
        { field: 'anotaciones', label: 'Anotaciones', large: true },
      ],
    ]
  },
  {
    title: 'Póliza/Cobertura',
    fields: [
      [
        { field: 'numPol', label: 'Póliza' },
        { field: 'descripcionCobertura', label: 'Descripción de Cobertura' },
      ],
    ],
  },
  {
    title: 'Comisaría',
    fields: [
      [
        { field: 'comisaria', label: 'Comisaría', large: true },
      ],
    ],
  },
  {
    title: 'Deducibles',
    fields: [
      [
        { field: 'deduciblesSimplificados', label: 'Deducibles' },
      ],
    ],
  },
  {
    title: 'Robo Total',
    fields: [
      [
        { field: 'tieneDenunciaPolicial', label: 'Tiene Denuncia Policial' },
        { field: 'proveedorGps', label: 'Proveedor GPS' },
        { field: 'gpsObligatorio', label: 'GPS Obligatorio' },
        { field: 'gpsInstalado', label: 'GPS Instalado' },
      ],
    ],
  },
  {
    title: 'Información Adicional(Siniestro)',
    fields: [
      [
        { field: 'muertosHeridos', label: 'Tiene Denuncia Policial' },
        { field: 'hayHeridos', label: 'Hay Heridos' },
        { field: 'hayFallecidos', label: 'Hay Fallecidos' },
        { field: 'vehiculosTipoPerdida', label: 'Vehículos Tipo Pérdida' },
        { field: 'cantidadHeridos', label: 'Cantidad de Heridos' },
        { field: 'cantidadFallecidos', label: 'Cantidad de Fallecidos' },
      ],
    ],
  },
  {
    title: 'Transmisión del Caso',
    fields: [
      [
        { field: 'comentariosBizagi', label: 'Comentarios Bizagi' },
        { field: 'fechaTransmisionSiniestro', label: 'Fecha de Transmisión del Siniestro' },
        { field: 'numeroBpm', label: 'N° BPM' },
      ],
    ],
  },
  {
    title: 'ANS',
    fields: [
      [
        { field: 'fechaVencimientoAns', label: 'Fecha Vencimiento ANS' },
        { field: 'ansVencimiento', label: 'ANS Vencimiento' },
        { field: 'estadoAns', label: 'Estado ANS' },
        { field: 'ans', label: 'ANS (Minutos)' },
      ],
    ],
  },
  {
    title: 'Motivo de Anulación',
    fields: [
      [
        { field: 'motivoAnulacion', label: 'Motivo de Anulación', large: true },
      ],
    ],
  },
  {
    title: 'Información del Caso',
    fields: [
      [
        { field: 'fechaHoraApertura', label: 'Fecha/Hora Apertura' },
        { field: 'fechaHoraCierre', label: 'Fecha/Hora Cierre' },
        { field: 'propietarioCaso', label: 'Propietario del Caso' },
      ],
    ],
  },
  {
    title: 'Responsable',
    fields: [
      [
        { field: 'grupoResponsable', label: 'Grupo Responsable' },
        { field: 'responsable', label: 'Responsable' },
        { field: 'responsableTieneLicenciaSalesforce', label: 'Responsable Tiene Licencia en SalesForce' },
      ],
    ],
  },
]

export const CRANE_MECHANICAL_ASSISTANCE_COLLAPSIBLE_FIELD_GROUPS = [
  {
    title: 'Descripción',
    fields: [
      [
        { field: 'descripcion', label: 'Descripción', large: true }
      ],
    ]
  },
  {
    title: 'Detalles',
    fields: [
      [
        { field: 'casoPrincipal', label: 'Caso Principal' },
        { field: 'numCasoSf', label: 'Número del Caso' },
        { field: 'estadoCaso', label: 'Estado' },
        { field: 'descripcionTipoCasoSf', label: 'Tipo de Registro del Caso' },
        { field: 'origenCaso', label: 'Origen del Caso' },
        { field: 'prioridad', label: 'Prioridad' },
        { field: 'subtema', label: 'Sub Tema' },
      ],
    ],
  },
  {
    title: 'Tipificación del Caso',
    fields: [
      [
        { field: 'grupo', label: 'Grupo' },
        { field: 'tipo', label: 'Tipo' },
        { field: 'categoria', label: 'Categoria' },
        { field: 'asunto', label: 'Asunto' },
        { field: 'tema', label: 'Tema' },
      ]
    ]
  },
  {
    title: 'Contacto',
    fields: [
      [
        { field: 'nombreContacto', label: 'Nombre del Contacto' },
        { field: 'nombreCuenta', label: 'Nombre de la Cuenta' },
        { field: 'tipoContacto', label: 'Tipo de Contacto' },
        { field: 'telefonoContacto', label: 'Teléfono' },
      ],
    ]
  },
  {
    title: 'Conductor',
    fields: [
      [
        { field: 'nombreConductor', label: 'Nombre del Conductor' },
        { field: 'apellidoPaternoConductor', label: 'Apellido Paterno del Conductor' },
        { field: 'apellidoMaternoConductor', label: 'Apellido Materno del Conductor' },
        { field: 'tipoDocumentoConductor', label: 'Tipo de Documento del Conductor' },
        { field: 'numeroDocumentoConductor', label: 'N° Documento del Conductor' },
        { field: 'telefonoConductor', label: 'Teléfono del Conductor' },
      ],
    ]
  },
  {
    title: 'Dirección Origen',
    fields: [
      [
        { field: 'pais', label: 'País' },
        { field: 'departamento', label: 'Departamento' },
        { field: 'provincia', label: 'Provincia' },
        { field: 'distrito', label: 'Distrito' },
        { field: 'direccionSiniestro', label: 'Dirección del Siniestro' },
        { field: 'referencias', label: 'Referencias' },
        { field: 'anillo', label: 'Anillo' },
      ],
    ]
  },
  {
    title: 'Dirección Destino',
    fields: [
      [
        { field: 'paisDestino', label: 'País Destino' },
        { field: 'departamentoDestino', label: 'Departamento Destino' },
        { field: 'provinciaDestino', label: 'Provincia Destino' },
        { field: 'distritoDestino', label: 'Distrito Destino' },
        { field: 'direccionDestino', label: 'Dirección Destino' },
        { field: 'referenciasDestino', label: 'Referencias Destino' },
      ],
    ]
  },
  {
    title: 'Ocurrencia',
    fields: [
      [
        { field: 'fechaHoraOcurrencia', label: 'Fecha/Hora Ocurrencia' },
        { field: 'cantidadDiasHoraDesdeOcurrencia', label: 'Cantidad Día(s)/Hora(s) Desde Ocurrencia' },
        { field: 'semaforoOcurrencia', label: 'Semáforo Ocurrencia' },
        { field: 'lugarOcurrencia', label: 'Lugar de Ocurrencia' },
      ],
    ]
  },
  {
    title: 'Datos del vehículo',
    fields: [
      [
        { field: 'cpc', label: 'CPC' },
        { field: 'numeroPolizaTexto', label: 'N° Póliza Texto' },
        { field: 'placaVehiculo', label: 'Placa' },
        { field: 'motorVehiculo', label: 'Motor' },
        { field: 'claseVehiculo', label: 'Clase de Vehículo' },
        { field: 'marcaVehiculo', label: 'Marca' },
        { field: 'modeloVehiculo', label: 'Modelo' },
        { field: 'colorVehiculo', label: 'Color' },
        { field: 'anioVehiculo', label: 'Año' },
      ],
    ],
  },
  {
    title: 'Proveedor',
    fields: [
      [
        { field: 'proveedor', label: 'Proveedor' },
        { field: 'fechaHoraProgramacion', label: 'Fecha/Hora Programación' },
        { field: 'fechaHoraAsignacionProveedor', label: 'Fecha/Hora Asignación al Proveedor' },
        { field: 'fechaHoraLlegadaLugar', label: 'Fecha/Hora de llegada al lugar' },
        { field: 'fechaHoraInicioAtencionProveedor', label: 'Fecha/Hora Inicio Atención Proveedor' },
        { field: 'fechaHoraCulminacionProveedor', label: 'Fecha/Hora Culminación Proveedor' },
        { field: 'operadorProveedorGruaAuxilio', label: 'Operador Proveedor Grua/Auxilio' },
        { field: 'cumplioServicio', label: 'Cumplió Servicio' },
      ],
    ]
  },
  {
    title: 'URLs PDF',
    fields: [
      [
        { field: 'pdfReporteProveedor', label: 'PDF Reporte al Proveedor' },
      ],
    ],
  },
  {
    title: 'Observaciones',
    fields: [
      [
        { field: 'anotaciones', label: 'Anotaciones', large: true },
      ],
    ]
  },
  {
    title: 'Póliza/Cobertura',
    fields: [
      [
        { field: 'numPol', label: 'Póliza' },
        { field: 'descripcionCobertura', label: 'Descripción de Cobertura' },
      ],
    ],
  },
  {
    title: 'Reembolso',
    fields: [
      [
        { field: 'reembolso', label: 'Reembolso' }
      ],
    ],
  },
  {
    title: 'ANS',
    fields: [
      [
        { field: 'fechaVencimientoAns', label: 'Fecha Vencimiento ANS' },
        { field: 'estadoAns', label: 'Estado ANS' },
        { field: 'ans', label: 'ANS (Minutos)' },
      ],
    ],
  },
  {
    title: 'Motivo de Anulación',
    fields: [
      [
        { field: 'motivoAnulacion', label: 'Motivo de Anulación' }
      ],
    ],
  },
  {
    title: 'Información del Caso',
    fields: [
      [
        { field: 'fechaHoraApertura', label: 'Fecha/Hora Apertura' },
        { field: 'fechaHoraCierre', label: 'Fecha/Hora Cierre' },
        { field: 'propietarioCaso', label: 'Propietario del Caso' },
      ],
    ],
  },
  {
    title: 'Información del Sistema',
    fields: [
      [
        { field: 'creadoPor', label: 'Creado Por' },
        { field: 'ultimaModificacionPor', label: 'Última Modificación Por' },
      ],
    ],
  },
  {
    title: 'Responsable',
    fields: [
      [
        { field: 'grupoResponsable', label: 'Grupo Responsable' },
        { field: 'responsable', label: 'Responsable' },
        { field: 'responsableTieneLicenciaSalesforce', label: 'Responsable Tiene Licencia en SalesForce' },
      ],
    ],
  },
]

export const CNT_CLAIMS_COLLAPSIBLE_FIELD_GROUPS = [
  {
    title: 'Información del Caso',
    fields: [
      [
        { field: 'numCasoSf', label: 'N° del Caso' },
        { field: 'casoPrincipal', label: 'Caso Principal' },
        { field: 'nombreContacto', label: 'Nombre de Contacto' },
        { field: 'numbreCuenta', label: 'Nombre de la Cuenta' },
        { field: 'descripcionTipoCasoSf', label: 'Tipo de Registro del Caso' },
        { field: 'origenCaso', label: 'Origen del Caso' },
        { field: 'estado', label: 'Estado' },
        { field: 'prioridad', label: 'Prioridad' },
        { field: 'fechaRecepcion', label: 'Fecha de Recepción' }
      ],
    ],
  },
  {
    title: 'Datos de Contacto',
    fields: [
      [
        { field: 'correoElectronico', label: 'Correo Electrónico' },
      ],
    ]
  },
  {
    title: 'Información del Reclamo',
    fields: [
      [
        { field: 'quejaReclamo', label: 'Queja/Reclamo' },
        { field: 'modalidadReclamo', label: 'Modalidad de Reclamo' },
        { field: 'origenReclamo', label: 'Origen del Reclamo' },
        { field: 'autoResolutor', label: 'Auto Resolutor' },
        { field: 'tipoServicioAtencion', label: 'Tipo de Servicio o Atención' },
      ],
    ],
  },
  {
    title: 'Origen del Reclamo',
    fields: [
      [
        { field: 'pais', label: 'País' },
        { field: 'departamento', label: 'Departamento' },
        { field: 'provincia', label: 'Provincia' },
      ],
    ],
  },
  {
    title: 'Descripción',
    fields: [
      [
        { field: 'descripcion', label: 'Descripción', large: true }
      ],
    ]
  },
  {
    title: 'Tipificación del Caso',
    fields: [
      [
        { field: 'grupo', label: 'Grupo' },
        { field: 'tipo', label: 'Tipo' },
        { field: 'categoria', label: 'Categoria' },
        { field: 'asunto', label: 'Asunto' },
        { field: 'tema', label: 'Tema' },
        { field: 'clase', label: 'Clase' },
      ]
    ]
  },
  {
    title: 'Póliza',
    fields: [
      [
        { field: 'numPol', label: 'Póliza' },
        { field: 'numeroPolizaTexto', label: 'N° Póliza Texto' },
        { field: 'codigoProductoTexto', label: 'Código Producto Texto' },
      ]
    ]
  },
  {
    title: 'Grupo Responsable',
    fields: [
      [
        { field: 'grupoResponsable', label: 'Grupo Responsable' },
        { field: 'responsable', label: 'Responsable' },
        { field: 'responsableTieneLicenciaSalesforce', label: 'Responsable Tiene Licencia en SalesForce' },
      ],
    ],
  },
  {
    title: 'ANS',
    fields: [
      [
        { field: 'nombreAsignacion', label: 'Nombre de Asignación' },
        { field: 'ansVencimiento', label: 'ANS Vencimiento' },
        { field: 'fechaVencimientoAns', label: 'Fecha Vencimiento ANS' },
        { field: 'estadoAns', label: 'Estado ANS' },
        { field: 'ans', label: 'ANS (Minutos)' },
      ],
    ],
  },
  {
    title: 'Información del Caso',
    fields: [
      [
        { field: 'fechaHoraApertura', label: 'Fecha/Hora Apertura' },
        { field: 'fechaHoraCierre', label: 'Fecha/Hora Cierre' },
        { field: 'propietarioCaso', label: 'Propietario del Caso' },
        { field: 'areaResponsablePropietario', label: 'Area Responsable Propietario' },
      ],
    ],
  },
  {
    title: 'Evaluación del Reclamo',
    fields: [
      [
        { field: 'respuestaUsuario', label: 'Respuesta al Usuario' },
        { field: 'situacionFinal', label: 'Situación Final' },
      ],
    ],
  },
  {
    title: 'Detalles del Proveedor',
    fields: [
      [
        { field: 'proveedor', label: 'Proveedor' },
        { field: 'fechaHoraEnvioEmailProveedor', label: 'Fecha/Hora Envío Email Proveedor' },
        { field: 'preguntasProveedor', label: 'Preguntas al Proveedor' },
      ],
    ],
  },
  {
    title: 'Devoluciones',
    fields: [
      [
        { field: 'requiereDevolucion', label: '¿Requiere Devolución?' },
        { field: 'montoReclamado', label: 'Monto Reclamado S/ USD' },
        { field: 'abonoCuenta', label: 'Abono en Cuenta' },
        { field: 'entidadFinanciera', label: 'Entidad financiera' },
        { field: 'numeroCuenta', label: 'Numero de Cuenta' },
        { field: 'tipoCuenta', label: 'Tipo de Cuenta' },
        { field: 'tipoMoneda', label: 'Tipo de Moneda' },
      ],
    ]
  },
  {
    title: 'Solución',
    fields: [
      [
        { field: 'solucion', label: 'Solución', large: true }
      ],
    ],
  },
  {
    title: 'Motivo Cierre Fallido',
    fields: [
      [
        { field: 'motivoCierreFallido', label: 'Motivo Cierre Fallido' },
      ],
    ],
  },
]

export const RENEWAL_COLLAPSIBLE_FIELD_GROUPS = [
  {
    title: 'Datos de Carga - Búsqueda',
    fields: [
      [
        { field: 'polizaCargaMasiva', label: 'Póliza Carga Masiva' },
        { field: 'ramoCargaArchivo', label: 'Ramo Carga Archivo' },
        { field: 'tipoDocumentoContratante', label: 'Tipo Documento Contranate' },
        { field: 'numeroDocumentoContratante', label: 'N° Documento Contranate' },
        { field: 'tipoDocumentoTitular', label: 'Tipo Documento Titular' },
        { field: 'numeroDocumentoTitular', label: 'N° Documento Titular' },
        { field: 'numeroPolizaRenovada', label: 'N° Póliza Renovada' },
        { field: 'placaVehiculo', label: 'Placa' },
        { field: 'motorVehiculo', label: 'Motor' },
        { field: 'masivo', label: 'Masivo' },
      ],
    ]
  },
  {
    title: 'Información de Contacto',
    fields: [
      [
        { field: 'nombreContacto', label: 'Nombre del Contacto' },
        { field: 'nombreCuenta', label: 'Nombre de la Cuenta' },
        { field: 'tipoContacto', label: 'Tipo de Contacto' },
        { field: 'otrosTelefonosContratante', label: 'Otros Teléfonos Contratante' },
        { field: 'correoElectronico', label: 'Correo Eléctronico' },
      ],
    ]
  },
  {
    title: 'Información de Vigencia Anterior',
    fields: [
      [
        { field: 'numPol', label: 'Póliza' },
        { field: 'sumaAseguradaVigenciaAnterior', label: 'Suma Asegurada Vigencia Anterior' },
        { field: 'primaTotalVigenciaAnterior', label: 'Prima Total Vigencia Anterior' },
        { field: 'cantidadSiniestros', label: 'Cantidad de Siniestros' },
        { field: 'tipoFinanciamiento', label: 'Tipo de Financiamiento' },
        { field: 'tasaVigenciaAnterior', label: 'Tasa Vigencia Anterior' },
      ],
    ]
  },
  {
    title: 'Información de Póliza Renovada',
    fields: [
      [
        { field: 'planRenovado', label: 'Plan Renovado' },
        { field: 'codigoProductoRenovado', label: 'Código Producto Renovado' },
        { field: 'sumaAseguradaRenovada', label: 'Suma Asegurada Renovada' },
        { field: 'tipoFinanciamientoRenovado', label: 'Tipo de Financiamiento Renovado' },
        { field: 'pricing', label: 'Pricing' },
        { field: 'numeroTramiteRenovacion', label: 'Numero de Trámite de Renovación' },
        { field: 'tasaPolizaRenovada', label: 'Tasa de Póliza Renovada' },
        { field: 'tipoMonedaPolizaRenovada', label: 'Tipo de Moneda Póliza Renovada' },
        { field: 'primaBrutaRenovada', label: 'Prima Bruta Renovada' },
        { field: 'variacionPrimaBruta', label: 'Variación de la Prima Bruta' },
        { field: 'variacionDescuento', label: 'Variación del Descuento' },
        { field: 'cantidadCuotas', label: 'Cantidad de Cuotas' },
        { field: 'campaniaRenovacionCobranzas', label: 'Campaña Renovación-Cobranzas' },
      ],
    ]
  },
  {
    title: 'Tipificación del Caso',
    fields: [
      [
        { field: 'grupo', label: 'Grupo' },
        { field: 'tipo', label: 'Tipo' },
        { field: 'categoria', label: 'Categoria' },
        { field: 'asunto', label: 'Asunto' },
        { field: 'tema', label: 'Tema' },
      ]
    ]
  },
  {
    title: 'Resultados',
    fields: [
      [
        { field: 'resultado', label: 'Resultado' },
        { field: 'subResultado', label: 'Sub Resultado' },
        { field: 'infoAdicional', label: 'Info Adicional' },
      ]
    ]
  },
  {
    title: 'Gestión del Caso',
    fields: [
      [
        { field: 'afiliadoCargoCuenta', label: 'Se Afilió a Cargo en Cuenta' },
        { field: 'audio', label: 'Audio' },
        { field: 'autorizacionDescuento', label: 'Autorización Descuento' },
        { field: 'cantidadLlamadas', label: 'Cantidad de Llamadas' },
        { field: 'fechaCompraNuevoBien', label: 'Fecha de Compra del Nuevo Bien' },
      ]
    ]
  },
  {
    title: 'Descripción',
    fields: [
      [
        { field: 'descripcion', label: 'Descripción', large: true }
      ],
    ]
  },
  {
    title: 'Registro del Caso',
    fields: [
      [
        { field: 'origenCaso', label: 'Origen del Caso' },
        { field: 'estadoCaso', label: 'Estado' },
        { field: 'prioridad', label: 'Prioridad' },
        { field: 'numCasoSf', label: 'Numero del Caso' },
        { field: 'descripcionTipoCasoSf', label: 'Tipo de Registro del Caso' },
      ],
    ]
  },
  {
    title: 'Información Próxima Cobranza',
    fields: [
      [
        { field: 'numDocPago', label: 'N° Documento Cobranza' },
        { field: 'fechaVenc', label: 'Fecha Venc. Documento Cobranza' },
      ],
    ]
  },
  {
    title: 'ANS',
    fields: [
      [
        { field: 'nombreAsignacion', label: 'Nombre de Asignación' },
        { field: 'ansVencimiento', label: 'ANS Vencimiento' },
        { field: 'fechaVencimientoAns', label: 'Fecha Vencimiento ANS' },
        { field: 'estadoAns', label: 'Estado ANS' },
        { field: 'ans', label: 'ANS (Minutos)' },
      ],
    ],
  },
  {
    title: 'Información del Caso',
    fields: [
      [
        { field: 'fechaHoraApertura', label: 'Fecha/Hora Apertura' },
        { field: 'fechaHoraCierre', label: 'Fecha/Hora Cierre' },
        { field: 'propietarioCaso', label: 'Propietario del Caso' },
        { field: 'areaResponsablePropietario', label: 'Area Responsable Propietario' },
      ],
    ],
  },
  {
    title: 'Información del Sistema',
    fields: [
      [
        { field: 'creadoPor', label: 'Creado Por' },
        { field: 'ultimaModificacionPor', label: 'Última Modificación Por' },
        { field: 'areaResponsableCreacion', label: 'Área Responsable Creación' },
      ],
    ],
  },
]

export const CNT_OPERATIONS_COLLAPSIBLE_FIELD_GROUPS = [
  {
    title: 'Información del Caso',
    fields: [
      [
        { field: 'numCasoSf', label: 'Numero del Caso' },
        { field: 'nombreContacto', label: 'Nombre de Contacto' },
        { field: 'numbreCuenta', label: 'Nombre de la Cuenta' },
        { field: 'estado', label: 'Estado' },
        { field: 'descripcionTipoCasoSf', label: 'Tipo de Registro del Caso' },
        { field: 'origenCaso', label: 'Origen del Caso' },
        { field: 'canalRespuesta', label: 'Canal de Respuesta' },
        { field: 'prioridad', label: 'Prioridad' },
      ],
    ],
  },
  {
    title: 'Descripción',
    fields: [
      [
        { field: 'descripcion', label: 'Descripción', large: true }
      ],
    ]
  },
  {
    title: 'Tipificación del Caso',
    fields: [
      [
        { field: 'grupo', label: 'Grupo' },
        { field: 'tipo', label: 'Tipo' },
        { field: 'categoria', label: 'Categoria' },
        { field: 'asunto', label: 'Asunto' },
        { field: 'tema', label: 'Tema' },
        { field: 'clase', label: 'Clase' },
        { field: 'tipoContacto', label: 'Tipo de Contacto' },
      ]
    ]
  },
  {
    title: 'Responsable',
    fields: [
      [
        { field: 'grupoResponsable', label: 'Grupo Responsable' },
        { field: 'responsable', label: 'Responsable' },
        { field: 'responsableTieneLicenciaSalesforce', label: 'Responsable Tiene Licencia en SalesForce' },
      ],
    ],
  },
  {
    title: 'Datos de Contacto Ejecutivo del Banco',
    fields: [
      [
        { field: 'nombreEjecutivoBanco', label: 'Nombre del Ejecutivo del Banco' },
        { field: 'correoEjecutivoBanco', label: 'Correo del Ejecutivo del Banco' },
      ],
    ],
  },
  {
    title: 'Datos de Contacto del Cliente',
    fields: [
      [
        { field: 'correoElectronico', label: 'Correo Electrónico' },
        { field: 'telefonoContacto', label: 'Teléfono de Contacto' },
        { field: 'direccion', label: 'Dirección' },
      ],
    ],
  },
  {
    title: 'Proveedor',
    fields: [
      [
        { field: 'proveedor', label: 'Proveedor' },
      ],
    ],
  },
  {
    title: 'Información del Caso',
    fields: [
      [
        { field: 'fechaHoraApertura', label: 'Fecha/Hora Apertura' },
        { field: 'fechaHoraCierre', label: 'Fecha/Hora Cierre' },
        { field: 'propietarioCaso', label: 'Propietario del Caso' },
        { field: 'areaResponsablePropietario', label: 'Area Responsable Propietario' },
      ],
    ],
  },
  {
    title: 'Póliza',
    fields: [
      [
        { field: 'numPol', label: 'Póliza' },
        { field: 'numeroPolizaTexto', label: 'N° Póliza Texto' },
        { field: 'codigoProductoTexto', label: 'Código Producto Texto' },
        { field: 'placaVehiculo', label: 'Placa' },
      ]
    ]
  },
  {
    title: 'ANS',
    fields: [
      [
        { field: 'nombreAsignacion', label: 'Nombre de Asignación' },
        { field: 'ansVencimiento', label: 'ANS Vencimiento' },
        { field: 'fechaVencimientoAns', label: 'Fecha Vencimiento ANS' },
        { field: 'estadoAns', label: 'Estado ANS' },
        { field: 'ans', label: 'ANS (Minutos)' },
      ],
    ],
  },
  {
    title: 'Solución',
    fields: [
      [
        { field: 'solucion', label: 'Solución', large: true }
      ],
    ],
  },
  {
    title: 'Motivo Cierre Fallido',
    fields: [
      [
        { field: 'motivoCierreFallido', label: 'Motivo Cierre Fallido' },
      ],
    ],
  },
  {
    title: 'Información del Sistema',
    fields: [
      [
        { field: 'creadoPor', label: 'Creado Por' },
        { field: 'ultimaModificacionPor', label: 'Última Modificación Por' },
      ],
    ],
  },
]

export const RETENTION_COLLAPSIBLE_FIELD_GROUPS = [
  {
    title: 'Detalle Retención',
    fields: [
      [
        { field: 'reglaRetencion', label: 'Regla de Retención' },
        { field: 'resultadoReglaRetencion', label: 'Resultado Regla de Retención' },
        { field: 'motivoAnulacion', label: 'Motivo de Anulación' },
        { field: 'masivo', label: 'Masivo' },
      ],
    ],
  },
  {
    title: 'Registro del Caso',
    fields: [
      [
        { field: 'casoPrincipal', label: 'Caso Principal' },
        { field: 'estadoCaso', label: 'Estado' },
        { field: 'origenCaso', label: 'Origen del Caso' },
        { field: 'numCasoSf', label: 'Numero del Caso' },
        { field: 'descripcionTipoCasoSf', label: 'Tipo de Registro del Caso' },
        { field: 'prioridad', label: 'Prioridad' },
      ],
    ]
  },
  {
    title: 'Tipificación del Caso',
    fields: [
      [
        { field: 'grupo', label: 'Grupo' },
        { field: 'tipo', label: 'Tipo' },
        { field: 'categoria', label: 'Categoria' },
        { field: 'asunto', label: 'Asunto' },
        { field: 'tema', label: 'Tema' },
      ]
    ]
  },
  {
    title: 'Información de Contacto',
    fields: [
      [
        { field: 'nombreContacto', label: 'Nombre del Contacto' },
        { field: 'nombreCuenta', label: 'Nombre de la Cuenta' },
        { field: 'correoElectronico', label: 'Correo Electrónico' },
        { field: 'emailSeleccionadoInfoContacto', label: 'Email Seleccionado Info. Contacto' },
        { field: 'telefonoContacto', label: 'Teléfono de Contacto' },
      ],
    ]
  },
  {
    title: 'Descripción',
    fields: [
      [
        { field: 'descripcion', label: 'Descripción', large: true }
      ],
    ]
  },
  {
    title: 'Póliza',
    fields: [
      [
        { field: 'numPol', label: 'Póliza' },
        { field: 'polizaCargaMasiva', label: 'Póliza Carga Masiva' },
        { field: 'numeroPolizaRenovada', label: 'N° Póliza Renovada' },
      ],
    ],
  },
  {
    title: 'Responsable',
    fields: [
      [
        { field: 'grupoResponsable', label: 'Grupo Responsable' },
        { field: 'responsable', label: 'Responsable' },
        { field: 'responsableTieneLicenciaSalesforce', label: 'Responsable Tiene Licencia en SalesForce' },
      ],
    ],
  },
  {
    title: 'Detalle de la Gestión',
    fields: [
      [
        { field: 'audio', label: 'Audio' },
        { field: 'afiliadoCargoCuenta', label: 'Se Afilió a Cargo en Cuenta' },
        { field: 'cantidadLlamadas', label: 'Cantidad de Llamadas' },
        { field: 'fechaCompraNuevoBien', label: 'Fecha de Compra del Nuevo Bien' },
      ]
    ]
  },
  {
    title: 'Resultado de la Gestión',
    fields: [
      [
        { field: 'resultado', label: 'Resultado' },
        { field: 'subResultado', label: 'Sub Resultado' },
        { field: 'infoAdicional', label: 'Info Adicional' },
      ]
    ]
  },
  {
    title: 'Información Próxima Cobranza',
    fields: [
      [
        { field: 'numDocPago', label: 'N° Documento Cobranza' },
        { field: 'fechaVenc', label: 'Fecha Venc. Documento Cobranza' },
      ],
    ]
  },
  {
    title: 'ANS',
    fields: [
      [
        { field: 'nombreAsignacion', label: 'Nombre de Asignación' },
        { field: 'ansVencimiento', label: 'ANS Vencimiento' },
        { field: 'fechaVencimientoAns', label: 'Fecha Vencimiento ANS' },
        { field: 'estadoAns', label: 'Estado ANS' },
        { field: 'ans', label: 'ANS (Minutos)' },
      ],
    ],
  },
  {
    title: 'Información del Caso',
    fields: [
      [
        { field: 'fechaHoraApertura', label: 'Fecha/Hora Apertura' },
        { field: 'fechaHoraCierre', label: 'Fecha/Hora Cierre' },
        { field: 'propietarioCaso', label: 'Propietario del Caso' },
        { field: 'areaResponsablePropietario', label: 'Area Responsable Propietario' },
      ],
    ],
  },
  {
    title: 'Información del Sistema',
    fields: [
      [
        { field: 'creadoPor', label: 'Creado Por' },
        { field: 'ultimaModificacionPor', label: 'Última Modificación Por' },
        { field: 'areaResponsableCreacion', label: 'Área Responsable Creación' },
      ],
    ],
  },
]

export const SOAT_SINISTER_COLLAPSIBLE_FIELD_GROUPS = [
  {
    title: 'Descripción',
    fields: [
      [
        { field: 'descripcion', label: 'Descripción', large: true }
      ],
    ]
  },
  {
    title: 'Detalles',
    fields: [
      [
        { field: 'numCasoSf', label: 'Número del Caso' },
        { field: 'estadoCaso', label: 'Estado' },
        { field: 'descripcionTipoCasoSf', label: 'Tipo de Registro del Caso' },
        { field: 'origenCaso', label: 'Origen del Caso' },
        { field: 'prioridad', label: 'Prioridad' },
      ],
    ]
  },
  {
    title: 'Tipificación del Caso',
    fields: [
      [
        { field: 'grupo', label: 'Grupo' },
        { field: 'tipo', label: 'Tipo' },
        { field: 'categoria', label: 'Categoria' },
        { field: 'asunto', label: 'Asunto' },
        { field: 'tema', label: 'Tema' },
        { field: 'subtema', label: 'Sub Tema' },
        { field: 'tipoAtencion', label: 'Tipo de Atención' },
        { field: 'tipoServicio', label: 'Tipo de Servicio' },
      ]
    ]
  },
  {
    title: 'Contacto',
    fields: [
      [
        { field: 'nombreContacto', label: 'Nombre del Contacto' },
        { field: 'nombreCuenta', label: 'Nombre de la Cuenta' },
        { field: 'telefono', label: 'Teléfono' },
      ],
    ]
  },
  {
    title: 'Conductor',
    fields: [
      [
        { field: 'nombreConductor', label: 'Nombre del Conductor' },
        { field: 'apellidoPaternoConductor', label: 'Apellido Paterno del Conductor' },
        { field: 'apellidoMaternoConductor', label: 'Apellido Materno del Conductor' },
        { field: 'telefonoConductor', label: 'Teléfono del Conductor' },
        { field: 'tipoDocumentoConductor', label: 'Tipo de Documento del Conductor' },
        { field: 'numeroDocumentoConductor', label: 'N° Documento del Conductor' },
        { field: 'licenciaConductor', label: 'Licencia del Conductor' },
      ],
    ]
  },
  {
    title: 'Dirección',
    fields: [
      [
        { field: 'pais', label: 'País' },
        { field: 'departamento', label: 'Departamento' },
        { field: 'provincia', label: 'Provincia' },
        { field: 'distrito', label: 'Distrito' },
        { field: 'direccionSiniestro', label: 'Dirección del Siniestro' },
        { field: 'referencias', label: 'Referencias' },
        { field: 'anillo', label: 'Anillo' },
      ],
    ]
  },
  {
    title: 'Ocurrencias',
    fields: [
      [
        { field: 'fechaHoraOcurrencia', label: 'Fecha/Hora Ocurrencia' },
        { field: 'cantidadDiasHoraDesdeOcurrencia', label: 'Cantidad Día(s)/Hora(s) Desde Ocurrencia' },
        { field: 'semaforoOcurrencia', label: 'Semáforo Ocurrencia' },
        { field: 'lugarOcurrencia', label: 'Lugar de Ocurrencia' },
        { field: 'tieneDenunciaPolicial', label: 'Tiene Denuncia Policial' },
      ],
    ]
  },
  {
    title: 'Médico Auditor',
    fields: [
      [
        { field: 'proveedorAuditoriaMedica', label: 'Proveedor Auditoría Médica' },
      ],
    ]
  },
  {
    title: 'Observaciones',
    fields: [
      [
        { field: 'anotaciones', label: 'Anotaciones', large: true },
      ],
    ]
  },
  {
    title: 'Póliza',
    fields: [
      [
        { field: 'numPol', label: 'Póliza' },
        { field: 'numeroCertificadoSoat', label: 'N° de Certificado SOAT' },
      ],
    ],
  },
  {
    title: 'Comisaría',
    fields: [
      [
        { field: 'comisaria', label: 'Comisaría', large: true },
      ],
    ],
  },
  {
    title: 'Deducibles',
    fields: [
      [
        { field: 'deduciblesSimplificados', label: 'Deducibles Simplificados' },
      ],
    ],
  },
  {
    title: 'Información Adicional(Siniestro)',
    fields: [
      [
        { field: 'hayHeridos', label: 'Hay Heridos?' },
        { field: 'hayFallecidos', label: 'Hay Fallecidos?' },
        { field: 'cantidadHeridosSoat', label: 'Cantidad Heridos SOAT' },
        { field: 'cantidadFallecidosSoat', label: 'Cantidad Fallecidos SOAT' },
      ],
    ],
  },
  {
    title: 'Transmisión del Siniestro (Bizagi)',
    fields: [
      [
        { field: 'fechaTransmisionSiniestro', label: 'Fecha de Transmisión del Siniestro' },
        { field: 'numeroSiniestro', label: 'N° de Siniestro' },
      ],
    ],
  },
  {
    title: 'ANS',
    fields: [
      [
        { field: 'ansVencimiento', label: 'ANS Vencimiento' },
        { field: 'fechaVencimientoAns', label: 'Fecha Vencimiento ANS' },
        { field: 'estadoAns', label: 'Estado ANS' },
        { field: 'ans', label: 'ANS (Minutos)' },
      ],
    ],
  },
  {
    title: 'Motivo de Anulación',
    fields: [
      [
        { field: 'motivoAnulacion', label: 'Motivo de Anulación' }
      ],
    ],
  },
  {
    title: 'Información del Caso',
    fields: [
      [
        { field: 'fechaHoraApertura', label: 'Fecha/Hora Apertura' },
        { field: 'fechaHoraCierre', label: 'Fecha/Hora Cierre' },
        { field: 'propietarioCaso', label: 'Propietario del Caso' },
        { field: 'creadoPor', label: 'Creado Por' },
        { field: 'ultimaModificacionPor', label: 'Última Modificación Por' },
      ],
    ],
  },
  {
    title: 'Responsables',
    fields: [
      [
        { field: 'grupoResponsable', label: 'Grupo Responsable' },
        { field: 'responsable', label: 'Responsable' },
        { field: 'responsableTieneLicenciaSalesforce', label: 'Responsable Tiene Licencia en SalesForce' },
      ],
    ],
  },
  {
    title: 'Información del Proveedor',
    fields: [
      [
        { field: 'proveedor', label: 'Proveedor' },
        { field: 'fechaHoraAsignacionProveedor', label: 'Fecha/Hora Asignación al Proveedor' },
        { field: 'fechaHoraLlegadaLugar', label: 'Fecha/Hora de llegada al lugar' },
        { field: 'fechaHoraInicioAtencionProveedor', label: 'Fecha/Hora Inicio Atención Proveedor' },
        { field: 'fechaHoraCulminacionProveedor', label: 'Fecha/Hora Culminación Proveedor' },
      ],
    ]
  },
]

