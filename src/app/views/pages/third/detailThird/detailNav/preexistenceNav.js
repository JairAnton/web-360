import React, { Component, useState, useEffect, useMemo } from 'react';
import { useSelector } from 'react-redux'
import ElementTable from 'components/table/elementTable';
import personLoaderKeys from 'state/ducks/person/loaderKeys'
import styled from 'styled-components'
import { MDBCol, MDBInput, MDBBreadcrumb, MDBBreadcrumbItem, MDBIcon, MDBCollapse } from "mdbreact";
import _ from 'lodash'
import { changeFormatDate } from 'state/utils/functionUtils'
import { Layout } from 'element-react';

const DivMDBBreadcrumb = styled.div`
  .breadcrumb {
    cursor: pointer;
  }
`;

const DivInput = styled.div`
  .md-form {
    margin-top: 0.5rem;
    margin-bottom: 0.5rem;
  }
`;

const COLUMNS = [
  {
    label: 'Descripción del Estado',
    prop: 'estadoRegistro',
    width: 200,
  }, {
    label: 'Fecha Inicio Vigencia',
    prop: 'fechaInicioVigencia',
    minWidth: 130,
    render: (row, column) => (
      <span>
        {changeFormatDate(row[column.prop], '-', '/').split(' ')[0]}
      </span>
    ),
  }, {
    label: 'Fecha Fin Vigencia',
    prop: 'fechaFinVigencia',
    width: 170,
    render: (row, column) => (
      <span>
        {changeFormatDate(row[column.prop], '-', '/').split(' ')[0]}
      </span>
    ),
  }, {
    label: 'Codigo Grupo',
    prop: 'codGrupoPreexistencia',
    width: 150,
  }, {
    label: 'Grupo Preexistencia',
    prop: 'grupoPreexistencia',
    minWidth: 190,
  },
]

const COLUMNS_DIAG_PREEXISTENCIA = [
  {
    label: "Código Diagnóstico",
    prop: "codDiagnostico",
    width: 200,
  },
  {
    label: "Descripción Diagnóstico",
    prop: "descDiagnostico",
  }
]

const GRUPO_EXCLUSIONES = [
  {
    type: 'expand',
    expandPannel: function (data) {
      if (data.diagnosticoExclusion && data.diagnosticoExclusion.length > 0) {
        return (
          <div>
            {
              data.diagnosticoExclusion.map((item, index) => (<div key={index}>{item.codExclusion} - {item.descExclusion}</div>))
            }
          </div>
        )
      }
      return (
        <div>
        </div>
      )
    }
  },
  {
    label: "Código Diagnóstico Grupo",
    prop: "codDiagGrupo",
    width: 230,
  },
  {
    label: "Descripción Diagnóstico Grupo",
    prop: "descDiagGrupo",
  },
  {
    label: "Descripción Estado Registro Adgp",
    prop: "descEstadoRegistroAdgp",
    width: 280,
  }
]


const DETAIL = [
  [
    { label: 'Código Afiliado', field: 'codAfiliado', type: 'text' },
    { label: 'Código Cliente', field: 'codCliente', type: 'text' },
    { label: 'Tipo Diagnostico Grupo', field: 'tipoDiagGrupo', type: 'text' },
  ], [
    { label: 'Descripción Tipo Giagnostico Grupo', field: 'descTipDiagGrupo', type: 'text' },
    { label: 'Código Diagnostico Grupo', field: 'codDiagGrupo', type: 'text' },
    { label: 'Descripción Diagnostico Grupo', field: 'descDiagGrupo', type: 'text' },
  ], [
    { label: 'Fecha Inicio Vigencia', field: 'fechaInicioVigencia', type: 'text' },
    { label: 'Fecha Fin Vigencia', field: 'fechaFinVigencia', type: 'text' },
    { label: 'Estado Registro', field: 'estadoRegistro', type: 'text' },
  ], [
    { label: 'Indicador Tope', field: 'indTope', type: 'text' },
    { label: 'Monto Tope Nac.', field: 'montoTopeNac', type: 'text' },
    { label: 'Monto Tope Ext.', field: 'montoTopeExt', type: 'text' },
  ], [
    { label: 'Monto Tope Gen.', field: 'montoTopeGen', type: 'text' },
    { label: 'Importe Consumido', field: 'importeConsumido', type: 'text' },
    { label: 'Código Exclusión', field: 'codExclusion', type: 'text' },
  ], [
    { label: 'Descripción Exclusión', field: 'descExclusion', type: 'text' },
    { label: 'Indicador Aplica Afiliado', field: 'indAplicaAfil', type: 'text' },
    { label: 'Descripción Estado Registro Adgp', field: 'descEstadoRegistroAdgp', type: 'text' }
  ], [
    { label: 'Código Grupo Preexistencia', field: 'codGrupoPreexistencia', type: 'text' },
    { label: 'Grupo Preexistencia', field: 'grupoPreexistencia', type: 'text' },
  ],
]

const notNull = value => (_.isNil(value) || `${value}`.trim() === '') ? '-' : value

const PreexistenceNav = (props) => {
  const [dataSelected, setDataSelected] = useState(null)
  const [collapseDetail, setCollapseDetail] = useState(true)
  const loaderVisible = useSelector(store => store.shared.loader.includes(personLoaderKeys.PERSON_PREEXISTENCIA))
  const preexistence = useSelector(store => store.person.preexistence)
  const dataList = useMemo(() => {
    const auxDataList = (preexistence.dataList) ? preexistence.dataList : []
    let auxList = []

    auxDataList.forEach((item) => {
      auxList.push({
        ...item,
        "fechaFinVigencia": (item.fechaFinVigencia) ? changeFormatDate(item.fechaFinVigencia, '-', '/') : '',
        "fechaInicioVigencia": (item.fechaInicioVigencia) ? changeFormatDate(item.fechaInicioVigencia, '-', '/') : '',
        // "fechaRecepcion": (item.fechaRecepcion) ? changeFormatDate(item.fechaRecepcion, '-', '/') : '',
        "codDiagGrupo": (item.grupoExclusiones && item.grupoExclusiones.length > 0) ? item.grupoExclusiones[0].codDiagGrupo : '',
        "descDiagGrupo": (item.grupoExclusiones && item.grupoExclusiones.length > 0) ? item.grupoExclusiones[0].descDiagGrupo : '',
        "descEstadoRegistroAdgp": (item.grupoExclusiones && item.grupoExclusiones.length > 0) ? item.grupoExclusiones[0].descEstadoRegistroAdgp : '',
        "indAplicaAfil": (item.grupoExclusiones && item.grupoExclusiones.length > 0) ? item.grupoExclusiones[0].indAplicaAfil : '',
        "codExclusion": (item.grupoExclusiones && item.grupoExclusiones.length > 0) ? (item.grupoExclusiones[0].diagnosticoExclusion && item.grupoExclusiones[0].diagnosticoExclusion.length > 0 ? item.grupoExclusiones[0].diagnosticoExclusion[0].codExclusion : '') : '',
        "descExclusion": (item.grupoExclusiones && item.grupoExclusiones.length > 0) ? (item.grupoExclusiones[0].diagnosticoExclusion && item.grupoExclusiones[0].diagnosticoExclusion.length > 0 ? item.grupoExclusiones[0].diagnosticoExclusion[0].descExclusion : '') : '',
      })
    })
    return auxList
  }, [preexistence.dataList])

  useEffect(() => {
    props.getPreexistenceList();
  }, []);

  useEffect(() => {
    setTimeout(() => {
      dataSelected && document.getElementById('my_scrollable_preexistence_div').scrollIntoView({ behavior: 'smooth' })
    }, 500)
  }, [dataSelected])

  const changeDataSelected = (data) => {
    setDataSelected(data);
  }

  return (
    <div className="row">
      <div className="col-lg-12" style={{ cursor: 'pointer' }}>
        <ElementTable
          columns={COLUMNS}
          data={dataList}
          onChangeSelect={changeDataSelected}
          allRow
          loader={loaderVisible}
        />
      </div>
      <div className="col-lg-12">
        <div
          style={{ width: '10px', height: '10px' }}
          id="my_scrollable_preexistence_div"
        />
        {
          dataSelected &&
          <div>
            <br />
            <DivMDBBreadcrumb>
              <MDBBreadcrumb onClick={() => setCollapseDetail(!collapseDetail)}>
                <MDBBreadcrumbItem>
                  {collapseDetail
                    ? <MDBIcon icon="angle-down" />
                    : <MDBIcon icon="angle-right" />}
                  {"  Detalle de Preexistencia"}
                </MDBBreadcrumbItem>
              </MDBBreadcrumb>
            </DivMDBBreadcrumb>
            <br />
            <MDBCollapse
              id="collapsePreexistencia"
              isOpen={collapseDetail}
              style={{ paddingLeft: '10px', }}
            >
              <DivInput>
                <div className="col-lg-12">
                  {DETAIL.map((group, index) => {
                    return (
                      <div className="row" key={index}>
                        {
                          // @ts-ignore
                          group.map((item, index) => {
                            const value = item.field in dataSelected
                              ? dataSelected[item.field]
                              : '-'

                            return (
                              <div className="col-lg-4" key={index}>
                                <MDBInput
                                  value={`${notNull(value)}`}
                                  label={item.label}
                                  size="sm"
                                  disabled
                                />
                              </div>
                            )
                          })
                        }
                      </div>
                    )
                  })}
                </div>
                <div>
                  <br />
                  <Layout.Row type='flex' gutter="20" justify='center'
                    style={{
                      marginBottom: '10px'
                    }}
                  >
                    <h3>
                      Diagnóstico Preexistencia
                    </h3>
                  </Layout.Row>
                  <ElementTable
                    data={dataSelected.diagPreexistencia || []}
                    columns={COLUMNS_DIAG_PREEXISTENCIA}
                    allRow
                  />
                </div>

                <div>
                  <br />
                  <Layout.Row type='flex' gutter="20" justify='center'
                    style={{
                      marginBottom: '10px'
                    }}
                  >
                    <h3>
                      Grupo Exclusiones
                    </h3>
                  </Layout.Row>
                  <ElementTable
                    data={dataSelected.grupoExclusiones || []}
                    columns={GRUPO_EXCLUSIONES}
                    allRow
                  />
                </div>
              </DivInput>
            </MDBCollapse>
          </div>
        }
      </div>
    </div>
  );
}

export default PreexistenceNav;