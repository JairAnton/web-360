// @ts-check
import React, { useState, useEffect, useMemo, useRef } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Layout } from 'element-react'
import Select from 'react-select'

import ElementTable from 'components/table/elementTable'
import { policyOperations, policyLoaderKeys } from 'state/ducks/policy'
import { providerOperations, providerLoaderKeys } from 'state/ducks/provider'
import DetailClinicWorkshop from './detailClinicWorkshop/detailClinicWorkshop'
import ubigeoClinica from './detailClinicWorkshop/ubigeoClinica'
import ubigeoTalleres from './detailClinicWorkshop/ubigeoTalleres'
import { MDBCol, MDBInput,MDBBreadcrumb, MDBBreadcrumbItem, MDBIcon, MDBCollapse } from 'mdbreact'
import styled from 'styled-components'

const DivMDBBreadcrumb = styled.div`
  .breadcrumb {
    cursor: none;
  }
`;

const COLUMNS_POLICY = [
  {
    label: "Ramo",
    prop: "ramo",
    minWidth: 150,
  },
  {
    label: "N° Póliza",
    prop: "numPoliza",
    minWidth: 150,
  },
  {
    label: "Código Producto",
    prop: "codProdAcselx",
    minWidth: 160,
  },
  {
    label: "Producto",
    prop: "desProducto",
    minWidth: 150,
  },
  {
    label: "Ini. vigencia",
    prop: "fecIniVigencia",
    minWidth: 150,
  },
  {
    label: "Fin vigencia",
    prop: "fecFinVigencia",
    minWidth: 150,
  },
  {
    label: "Estado",
    prop: "estadoPoliza",
    minWidth: 150,
  }
]

const CLINIC_COLUMNS_PROVIDER = [
  {
    label: 'Clínica',
    prop: 'clinica',
    minWidth: 150,
  },
  {
    label: 'Beneficio',
    prop: 'beneficio',
    minWidth: 150
  },
  {
    label: 'Dirección',
    prop: 'direccion',
    minWidth: 150,
  },
  {
    label: 'Copago',
    prop: 'codPago',
    minWidth: 150,
  },
  {
    label: 'Cubierto',
    prop: 'cubierto',
    minWidth: 150,
    render: (row, column, _index) => (
      <span>{row[column.prop]} %</span>
    ),
  },
  {
    label: 'Contacto',
    prop: 'telefono', // o telefonoPrincipal
    minWidth: 150,
  },
]

const WORKSHOP_COLUMNS_PROVIDER = [
  {
    label: 'Taller',
    prop: 'taller',
    minWidth: 150,
  },
  {
    label: 'Dirección',
    prop: 'direccion',
    minWidth: 150,
  },
  {
    label: 'Tipo',
    prop: 'tipo',
    minWidth: 150,
  },
  {
    label: 'Deducible',
    prop: 'deducible',
    minWidth: 150,
  },
  {
    label: 'Contacto',
    prop: 'telefono', // o telefonoPrincipal
    minWidth: 150,
  },
]


const ClinicWorkshotsNav = props => {

  const dispatch = useDispatch()
  const rendered = useRef(true)

  const providerActionOperations = useMemo(() => {
    return bindActionCreators(providerOperations, dispatch)
  }, [dispatch])

  const policyActionOperations = useMemo(() => {
    return bindActionCreators(policyOperations, dispatch)
  }, [dispatch])

  const [selectedPolicy, setSelectedPolicy] = useState(null)
  const [filasPorPagPolicy, setFilasPorPagPolicy] = useState(10)
  const [pagActualPolicy, setPagActualPolicy] = useState(1)
  const [ideDepartamento, setIdeDepartamento] = useState(null)
  const [ideProvincia, setIdeProvincia] = useState(null)
  const [ideDistrito, setIdeDistrito] = useState(null)
  // const [nombreSucursal, setNombreSucursal] = useState('')
  const [bottomSectionVisible, setBottomSectionVisible] = useState(false)
  const [selectedProvider, setSelectedProvider] = useState(null)
  const [filasPorPagProvider, setFilasPorPagProvider] = useState(10)
  const [pagActualProvider, setPagActualProvider] = useState(1)
  const [codBeneficio, setCodBeneficio] = useState({ label: "AMBULATORIO", value: "A00" })

  const coverageTypeList = useSelector(state => state.policy.coberturas.coverageTypes)
  const loadingCoverageTypeList = useSelector(state => {
    return state.shared.loader.includes(policyLoaderKeys.POLICY_COVERAGE_TYPE_LIST)
  })

  useEffect(() => {
    // get coverage types and fill the select's
    if (rendered.current) {
      rendered.current = false
      return
    }
    if (selectedPolicy.core !== 'RS') return
    const request = {
      "codProdAcselx": (selectedPolicy.codProdAcselx) ? selectedPolicy.codProdAcselx.toString() : '',
      "numPoliza": (selectedPolicy.numPoliza) ? selectedPolicy.numPoliza.toString() : '',//Opcional
      "codAfiliado": (selectedPolicy.codAfiliado) ? selectedPolicy.codAfiliado.toString() : '',
      "codCliente": (selectedPolicy.codCliente) ? selectedPolicy.codCliente.toString() : '',
      "type": "C",
    }
    policyActionOperations.policyCoverageTypeList(request)
  }, [selectedPolicy])

  useEffect(() => {
    if (
      selectedPolicy && (
        selectedPolicy.core === 'RS' ||
        selectedPolicy.core === 'AX'
      )
    )
      filterProviders()
  }, [selectedPolicy])

  useEffect(() => {
    const scrollableProvidersDetail = document.getElementById('scrollable_providers_detail')
    setTimeout(() => {
      scrollableProvidersDetail && bottomSectionVisible &&
        scrollableProvidersDetail.scrollIntoView({ behavior: 'smooth' })
    }, 200)
  }, [selectedProvider])

  useEffect(() => {
    getPolicyList()
  }, [filasPorPagPolicy, pagActualPolicy])

  useEffect(() => {
    filterProviders()
  }, [filasPorPagProvider, pagActualProvider])

  useEffect(() => {
    const scrollableProvidersTable = document.getElementById('scrollable_providers_table')
    setTimeout(() => {
      scrollableProvidersTable && bottomSectionVisible &&
        scrollableProvidersTable.scrollIntoView({ behavior: 'smooth' })
    }, 200)
  }, [props.provider])

  const getServiceType = core => {
    switch (core) {
      case 'AX':
        return 'T'
      case 'RS':
        return 'C'
      default:
        return null
    }
  }

  const beneficios = useMemo(() => {
    return coverageTypeList && coverageTypeList.length > 0
      ? coverageTypeList.map(item => ({
        label: item.descripcion,
        value: item.codBeneficio,
      })) : [{
        label: "AMBULATORIO",
        value: "A00",
      }]
  }, [coverageTypeList])

  //#region UBIGEO
  const getOptionsDepartamento = useMemo(() => {
    if (selectedPolicy && selectedPolicy.core === 'RS')
      return ubigeoClinica.departamento.map((item) => ({
        value: item.codDepartamento,
        label: item.description
      }))
    if (selectedPolicy && selectedPolicy.core === 'AX')
      return ubigeoTalleres.departamento.map((item) => ({
        value: item.codDepartamento,
        label: item.description
      }))
    return []
  }, [selectedPolicy])

  const getOptionsProvincia = useMemo(() => {
    setIdeProvincia(null)
    if (!ideDepartamento)
      return []

    if (selectedPolicy && selectedPolicy.core === 'RS') {
      return ubigeoClinica.provincia.filter(prov => prov.codDepartamento === ideDepartamento.value).map((item) => ({
        value: item.codProvincia, label: item.description
      }))
    }

    if (selectedPolicy && selectedPolicy.core === 'AX') {
      return ubigeoTalleres.provincia.filter(prov => prov.codDepartamento === ideDepartamento.value).map((item) => ({
        value: item.codProvincia, label: item.description
      }))
    }

    return []
  }, [ideDepartamento])

  const getOptionsDistrito = useMemo(() => {
    setIdeDistrito(null)
    if (!ideProvincia)
      return []

    if (selectedPolicy && selectedPolicy.core === 'RS') {
      return ubigeoClinica.distrito.filter(distr => distr.codDepartamento === ideDepartamento.value && distr.codProvincia === ideProvincia.value).map((item) => ({
        value: item.codDistrito, label: item.description
      }))
    }

    if (selectedPolicy && selectedPolicy.core === 'AX') {
      return ubigeoTalleres.distrito.filter(distr => distr.codDepartamento === ideDepartamento.value && distr.codProvincia === ideProvincia.value).map((item) => ({
        value: item.codDistrito, label: item.description
      }))
    }

    return []
  }, [ideProvincia])
  //#endregion

  const filterProviders = () => {
    if (!selectedPolicy)
      return
    const { codAfiliado, codCliente, codProdAcselx, numPoliza, numCertificado, core, idePoliza } = selectedPolicy

    providerActionOperations.providerList({
      filter: {
        tipoServicio: getServiceType(core),
        ...(core === 'RS' ? {
          codAfiliado: `${codAfiliado}`,
          codCliente: `${codCliente}`,
          nombreSucursal: '',
          // filto por tipo de beneficio
          codBeneficio: `${codBeneficio.value}`,
        } : {
            numPoliza: `${numPoliza}`,
            numCertificado: `${numCertificado}`,
          }),
        codProdAcselx: `${codProdAcselx}`,
        idePoliza: idePoliza ? `${idePoliza}` : '',
        ideDepartamento: (ideDepartamento && ideDepartamento.value) ? ideDepartamento.value : '',
        ideProvincia: (ideProvincia && ideProvincia.value) ? ideProvincia.value : '',
        ideDistrito: (ideDistrito && ideDistrito.value) ? ideDistrito.value : '',
      },
      pagination: {
        filasPorPag: `${filasPorPagProvider}`,
        pagActual: `${pagActualProvider}`,
      },
    })
    // auditoria
    let requestLogging = {
      "path": window.location.href,
      "action": "PROVIDER_LIST"
    }
    providerActionOperations.auditLogging(requestLogging)
    setBottomSectionVisible(true)
  }

  const clickPolicyRowTable = selected => {
    setSelectedPolicy(selected)
    setSelectedProvider(null)
  }

  const getPolicyList = () => {
    props.getPolicyList(filasPorPagPolicy, pagActualPolicy)
  }

  const changePolicyPage = page => {
    setPagActualPolicy(page)
  }

  const changePolicyRowsPerPage = rowsPerPage => {
    setFilasPorPagPolicy(rowsPerPage)
  }

  const clickProviderRowTable = selected => {
    setSelectedProvider(selected)
  }

  const getProviderColumns = () => {
    if (selectedPolicy.core === 'RS')
      return CLINIC_COLUMNS_PROVIDER
    if (selectedPolicy.core === 'AX')
      return WORKSHOP_COLUMNS_PROVIDER
    return []
  }

  const changeProviderPage = page => {
    setPagActualProvider(page)
  }

  const changeProviderRowsPerPage = rowsPerPage => {
    setFilasPorPagProvider(rowsPerPage)
  }

  const {
    policy: { dataList: policyDataList },
    provider: { dataList: providerDataList, pagination: providerPagination },
  } = props
  const loaderPolicyVisible = useSelector(state => state.shared.loader.includes(policyLoaderKeys.POLICY_LIST))
  const loaderProviderVisible = useSelector(state => state.shared.loader.includes(providerLoaderKeys.PROVIDER_LIST))

  return (
    <div>
      <div>
        <p>
          <b>Productos Vigentes</b>
        </p>
      </div>
      <div style={{ cursor: 'pointer' }}>
        <ElementTable
          loader={loaderPolicyVisible}
          data={policyDataList}
          columns={COLUMNS_POLICY}
          onChangeSelect={clickPolicyRowTable}
          pagination={props.policy.pagination}
          page={pagActualPolicy}
          rowPage={filasPorPagPolicy}
          onChangePage={changePolicyPage}
          onChangeRowPage={changePolicyRowsPerPage}
        />
      </div>
      <br />
      {bottomSectionVisible &&
        <div>
          <div>
            <Layout.Row  justify='center' style={{"marginBottom":"10px"}}>
              <DivMDBBreadcrumb>
                          <MDBBreadcrumb>
                            <MDBBreadcrumbItem>
                              {"Centro de Atención"}
                            </MDBBreadcrumbItem>
                          </MDBBreadcrumb>
                        </DivMDBBreadcrumb>
            </Layout.Row>

          </div>

          <div className="col-sm-12">

            {selectedPolicy.core === 'RS' &&
              <Layout.Row gutter="10">
                <Layout.Col span="12">
                  <label>
                    Tipo de Cobertura:
                  </label>
                  <Select placeholder="Seleccionar"
                    name="beneficio"
                    options={beneficios}
                    value={codBeneficio}
                    onChange={item => setCodBeneficio(item)}
                    isLoading={loadingCoverageTypeList}
                    loadingMessage={() => "Cargando..."}
                    placeholder="Seleccione"
                  />
                </Layout.Col>
              </Layout.Row>}

            <Layout.Row gutter="10">
              <Layout.Col span="8">
                <label>
                  Departamento:
                </label>
                <Select placeholder="Seleccionar"
                  name="ideDepartamento"
                  options={getOptionsDepartamento}
                  value={ideDepartamento}
                  onChange={(val) => setIdeDepartamento(val)}
                  isClearable
                />
              </Layout.Col>
              <Layout.Col span="8">
                <label>
                  Provincia:
                </label>
                <Select placeholder="Seleccionar"
                  name="ideProvincia"
                  options={getOptionsProvincia}
                  value={ideProvincia}
                  onChange={(val) => { setIdeProvincia(val) }}
                  isClearable
                />
              </Layout.Col>
              <Layout.Col span="8">
                <label>
                  Distrito:
                </label>
                <Select placeholder="Seleccionar"
                  name="ideDistrito"
                  options={getOptionsDistrito}
                  value={ideDistrito}
                  onChange={(val) => { setIdeDistrito(val) }}
                  isClearable
                />
              </Layout.Col>
            </Layout.Row>
            <div className="row">
              <div className="col-sm-12" style={{ textAlign: "right" }}>
                <button className="btn btn-danger btn-icon-split" onClick={filterProviders}>
                  <span className="icon text-white-50">
                    <i className="fas fa-search"></i>
                  </span>
                  <span className="text">Buscar</span>
                </button>
              </div>
            </div>
          </div>

          <div style={{ cursor: 'pointer' }}>
            <ElementTable
              loader={loaderProviderVisible}
              data={providerDataList}
              columns={getProviderColumns()}
              onChangeSelect={clickProviderRowTable}
              pagination={providerPagination}
              page={pagActualProvider}
              rowPage={filasPorPagProvider}
              onChangePage={changeProviderPage}
              onChangeRowPage={changeProviderRowsPerPage}
            />
          </div>
          <div
            style={{ width: '10px', height: '10px' }}
            id="scrollable_providers_table"
          />
          <br />
          <div
            style={{ width: '10px', height: '10px' }}
            id="scrollable_providers_detail"
          />
          {selectedProvider && !loaderProviderVisible &&
            <DetailClinicWorkshop
              provider={selectedProvider}
              clinic={selectedPolicy.core === 'RS'}
              workshop={selectedPolicy.core === 'AX'}
            />
          }
        </div>}
    </div>
  )
}

export default ClinicWorkshotsNav
