import React from 'react'
import { MDBCol, MDBInput,MDBBreadcrumb, MDBBreadcrumbItem, MDBIcon, MDBCollapse } from 'mdbreact'
import { Layout } from 'element-react'
import { style } from '@material-ui/system';
import _ from 'lodash'
import styled from 'styled-components'

const DivInput = styled.div`
  .breadcrumb {
    cursor: pointer;
  }
  .md-form {
    margin-top: 0.5rem;
    margin-bottom: 0.5rem;
  }
`;

const DivMDBBreadcrumb = styled.div`
  .breadcrumb {
    cursor: pointer;
  }
`;

const FIELD_GROUPS = [
  [
    { field: 'usuarioEnvio', label: 'Usuario Envío' },
    { field: 'fechaEnvio', label: 'Fecha Envío' },
    { field: 'canalEnvio', label: 'Canal Envío' },
  ], [
    { field: 'herramienta', label: 'Herramienta' },
    { field: 'tipoComunicacion', label: 'Tipo Comunicación' },
    { field: 'estado', label: 'Estado' },
  ], [
    // { field: 'asunto', label: 'Asunto' },
    { field: 'destinatario', label: 'Destinatario' },
    { field: 'ramo', label: 'Ramo' }, // TODO: validar si el servicio lo devuelve
    { field: 'numPoliza', label: 'N° Póliza' },
  ],
  [
    { field: 'indEnviado', label: 'Indicador Enviado' },
    { field: 'indAbierto', label: 'Indicador Abierto' },
    { field: 'indRecibo', label: 'Indicador Recibido' },
  ],
  [
    { field: 'producto', label: 'Código Producto' }, // TODO: validar si el servicio lo devuelve
    { field: 'nombreComunicacion', label: 'Nombre Comunicación' },

    // { field: 'contenido', label: 'Contenido' },
    // { field: 'lugarEnvio', label: 'Lugar Envío' },
  ],
]

const notNull = value => (_.isNil(value) || !`${value}`.trim()) ? '-' : value

const DetailCommunication = ({ communication }) => {
  return (
    <div>
      <div>
        <Layout.Row  justify='center' style={{"marginBottom":"10px"}}>
          <DivMDBBreadcrumb>
                      <MDBBreadcrumb>
                        <MDBBreadcrumbItem>
                          {"Detalle de comunicación"}
                        </MDBBreadcrumbItem>
                      </MDBBreadcrumb>
                    </DivMDBBreadcrumb>
        </Layout.Row>
      </div>
      <DivInput>
        <div className="col-lg-12">
          {FIELD_GROUPS.map((group, idx) => (
            <div className="row" key={idx}>
              {group.map((item, _idx) => {
                return (
                  <div className="col-lg-4" key={_idx}>
                    <MDBInput
                      value={`${notNull(communication[item.field])}`}
                      disabled label={item.label}
                      size="sm"
                    />
                  </div>
                )
              })}
            </div>
          ))}
          <br />
          <div>
            <Layout.Row  justify='center' style={{"marginBottom":"10px"}}>
          

              <DivMDBBreadcrumb>
                          <MDBBreadcrumb>
                            <MDBBreadcrumbItem>
                              {"Contenido de Comunicación"}
                            </MDBBreadcrumbItem>
                          </MDBBreadcrumb>
                        </DivMDBBreadcrumb>
            </Layout.Row>
        
          </div>
          <div
            className="content"
            dangerouslySetInnerHTML={{ __html: communication ? communication.contenido : '' }}
            style={{
              width: '100%',
              border: '1px solid black'
            }}
          />
        </div>
      </DivInput>
    </div>
  )
}

export default DetailCommunication