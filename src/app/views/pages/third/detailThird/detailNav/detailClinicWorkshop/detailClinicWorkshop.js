import React from 'react'
import styled from 'styled-components'
import { MDBInput } from 'mdbreact'

import ubigeoClinica from './ubigeoClinica'
import ubigeoTalleres from './ubigeoTalleres'

const DivInput = styled.div`
  .breadcrumb {
    cursor: pointer;
  }
  .md-form {
    margin-top: 0.5rem;
    margin-bottom: 0.5rem;
  }
  padding-left:  15px;
`;

const CLINIC_DETAIL_FIELD_GROUPS = [
  [
    { label: 'Clinica', field: 'clinica' },
    { label: 'Dirección', field: 'direccion' },
    { label: 'Codigo Pago', field: 'codPago' },
  ],
  [
    { label: 'Cubierto', field: 'cubierto' },
    { label: 'Teléfono', field: 'telefono' },
    { label: 'Dirección Web', field: 'dirWeb' },
  ],
  [
    { label: 'Red de Clínicas', field: 'redClinicas' },
    { label: '**numPlan**', field: 'numPlan' },
    { label: '**codigoSucursal**', field: 'codigoSucursal' },
  ],
  [
    { label: 'Distrito', field: 'distrito' },
    { label: 'Provincia', field: 'provincia' },
    { label: 'Departamento', field: 'departamento' },
  ],
  [
    { label: '**codUbigeo**', field: 'codUbigeo' },
    { label: 'Beneficio', field: 'beneficio' },
    { label: '**codIPRESS**', field: 'codIPRESS' },
  ],
]

const WORKSHOP_DETAIL_FIELD_GROUPS = [
  [
    { label: 'Dirección', field: 'direccion' },
    { label: 'Teléfono', field: 'telefono' },
    { label: 'Taller', field: 'taller' },
  ],
  [
    { label: 'Dirección Web', field: 'dirWeb' },
    { label: 'Tipo', field: 'tipo' },
    { label: 'Deducible', field: 'deducible' },
  ]
]

const DetailClinicWorkshop = ({ provider, clinic, workshop }) => {

  const FIELD_GROUPS = clinic
    ? CLINIC_DETAIL_FIELD_GROUPS
    : workshop
      ? WORKSHOP_DETAIL_FIELD_GROUPS
      : []

  return (
    <DivInput>
      {FIELD_GROUPS.map((group, idx) => (
        <div className="form-group row" key={idx}>
          {group.map((item, _idx) => {
            const value = item.field in provider
              ? (provider[item.field] ? provider[item.field] : '-')
              : 'Este campo no existe';
            return (
              <div className="col-lg-4" key={_idx}>
                <MDBInput value={`${value}`} disabled label={item.label} size="sm" />
              </div>
            )
          })}
        </div>
      ))}
    </DivInput>
  )
}

export default DetailClinicWorkshop