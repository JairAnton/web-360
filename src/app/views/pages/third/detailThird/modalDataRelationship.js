import React, { Component } from 'react';

class ModalDataRelationship extends Component {


  getColumnsCompany = () => {
    const columns = [
      {
        Header: "Relacion",
        accessor: "relacion",
        style: {
          textAlign: "center",
        }
      },
      {
        Header: "Relacionado con",
        accessor: "relacionadoCon",
        style: {
          textAlign: "center",
        }
      },
      {
        Header: "CIIU",
        accessor: "ciiu",
        style: {
          textAlign: "center",
        }
      },
      {
        Header: "Estado",
        accessor: "estado",
        style: {
          textAlign: "center",
        }
      }
    ]

    return columns;
  }

  getDataCompany = () => {
    return [];
  }

  getColumnsRelationFamily = () => {
    const columns = [
      {
        Header: "Relacion",
        accessor: "relacion",
        style: {
          textAlign: "center",
        }
      },
      {
        Header: "Relacionado con",
        accessor: "relacionadoCon",
        style: {
          textAlign: "center",
        }
      },
      {
        Header: "Edad",
        accessor: "edad",
        style: {
          textAlign: "center",
        }
      },
      {
        Header: "Estado",
        accessor: "estado",
        style: {
          textAlign: "center",
        }
      }
    ]

    return columns;
  }

  getDataRelationFamily = () => {
    const personasContacto = this.props.detail.personasContacto;
    let data = [];

    if (personasContacto) {
      personasContacto.map((prop) => {
        data.push({
          "esPrincipal": prop.esPrincipal,
          "tipo": prop.tipo,
          "numero": prop.numero,
          "anexo": prop.anexo,
          "uso": prop.uso,
          "estado": prop.estado,
          "usuarioModif": prop.usuarioModif,
          "fechaModif": prop.fechaModif
        });
      });
    }

    return data;
  }

  render() {
    return (
      <div>
        <a href="#" data-toggle="modal" data-target=".bd-data-xl">
          Datos de Relacionamiento
                </a>

        <div className="modal fade bd-data-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
          <div className="modal-dialog modal-xl">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Relacionamiento</h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Cerrar">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <p>
                  Relación Empresarial
                </p>
                {/* <DetailTable
                    columns={this.getColumnsCompany()}
                    data={this.getDataCompany()}
                />
                <hr/>
                <p>
                    Relación Familiar
                </p>    
                <DetailTable
                    columns={this.getColumnsRelationFamily()}
                    data={this.getDataRelationFamily()}
                /> */}
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-secondary" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ModalDataRelationship;