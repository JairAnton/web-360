import React, { Component } from 'react'
import { connect } from 'react-redux'
import { MDBRow, MDBBreadcrumbItem, MDBIcon, MDBBreadcrumb, MDBCollapse, MDBInput } from 'mdbreact'
import styled from 'styled-components'
import _ from 'lodash'
import ModalAditionalData from './modalAditionalData'
import { personLoaderKeys } from '../../../../state/ducks/person'
import Loader from '../../../../components/loader'
import { changeFormatDate } from 'state/utils/functionUtils'

const DivContactInformation = styled.div`
  .breadcrumb {
    cursor: pointer;
  }
`;

const DivInput = styled.div`
  .md-form {
    margin-top: 0.5rem;
    margin-bottom: 0.5rem;
  }
`;

const CONTACT_INFO_FIELD_GROUPS_NATURAL = [
  [
    { field: 'tratamiento', label: 'Tratamiento' },//TODO -> Se debe diferenciar
    { field: 'numDocumento', labelValue: 'desTipoDocumento' },
    { field: 'desTipoTercero', label: 'Tipo de Persona' },
  ], [
    { field: 'nombre', label: 'Nombres' },
    { field: 'apePaterno', label: 'Apellido Paterno' },
    { field: 'apeMaterno', label: 'Apellido Materno' },
  ], [
    { field: 'fecNacimiento', label: 'Fecha de Nacimiento' },
    { field: 'estadoCivil', label: 'Estado Civil' },
    { field: 'esCliente', label: 'Es Cliente' },
  ], [
    { field: 'esBroker', label: 'Es Broker' },
    { field: 'ideTercero', label: 'Id Tercero' },
  ], [
    // { field: 'leyProteccionDatos', label: 'Ley de Protección de Datos' },
    // { field: 'sensible', label: 'Sensible' },
    { field: 'codAfiliado', label: 'Código Afiliado' },
    { field: 'codExterno', label: 'Código Externo' },
  ],
]

const CONTACT_INFO_FIELD_GROUPS_JURIDIC = [
  [
    
    { field: 'tratamiento', label: 'Tratamiento' },
    { field: 'numDocumento', labelValue: 'desTipoDocumento' },
    { field: 'desTipoTercero', label: 'Tipo de Persona' },
  ], [
    { field: 'razonSocial', label: 'Razón Social' },
    { field: 'nombre', label: 'Nombre Comercial' },
    { field: 'esCliente', label: 'Es Cliente' },
  ], [
    { field: 'esBroker', label: 'Es Broker' },
    { field: 'sensible', label: 'Sensible' },
  ], [
    //{ field: 'leyProteccionDatos', label: 'Ley de Protección de Datos' },
    { field: 'ideTercero', label: 'Id Tercero' },
    { field: 'codExterno', label: 'Código Externo' },
  ],
]

const CONTACT_DETAIL_INFO_FIELD_GROUPS_NATURAL = [
  [
    // { field: 'ideTercero', label: 'Id Tercero' },
    // { field: 'numDocumento', labelValue: 'desTipoDocumento' },
    // { field: 'nomCompleto', label: 'Nombre Completo' },
    { field: 'edad', label: 'Edad' },
    { field: 'sexo', label: 'Sexo' },
    { field: 'blackList', label: 'Black List' },
  ],
  // [
  //   // { field: 'estadoCivil', label: 'Estado Civil' },
  // ], 
  // [
  //   // { field: 'desTipoTercero', label: 'Tipo de Persona' },
  //   // { field: 'direccionPrincipal', label: 'Dirección Principal' },
  //   // { field: 'referenciaDirPrincipal', label: 'Referencia Principal' },
  // ],
  [
    // { field: 'telefPrincipal', label: 'Teléfono Principal' },
    // { field: 'emailPrincipal', label: 'Correo Principal' },
    { field: 'canalPrincipal', label: 'Canal Principal' },
    { field: 'segmento', label: 'Segmento' },
    { field: 'sectorista', label: 'Sectorista' },
  ],
  // [
  //   // { field: 'grupoEconomico', label: 'Grupo Económico' },
  //   // { field: 'sector', label: 'Sector' },
  // ],
  [
    { field: 'scoring', label: 'Scoring Vida' },
    { field: 'sbs', label: 'SBS' },
    { field: 'nse', label: 'NSE' },
  ],
  [
    { field: 'politicaCom', label: 'Aceptó Politica Comunicaciones' },
    { field: 'leyProteccionDatos', label: 'Ley de Protección de Datos' },
    { field: 'registroWeb', label: 'Registro Web' },
    //{ field: 'codigoCorredor', label: 'Código Corredor' },
    // { field: 'ciiu', label: 'CIIU' },
  ],
  // [
  //   // { field: 'giro', label: 'Giro' },
  //   // { field: 'seps', label: 'SEPS' },
  //   // { field: 'tratamiento', label: 'Tratamiento' },
  // ],
  // [
  //   // { field: 'esBroker', label: 'Es Broker' },
  //   // { field: 'sensible', label: 'Sensible' },
  // ], [
  //   // { field: 'codExterno', label: 'Código Externo' },
  //   // { field: 'codAfiliado', label: 'Código Afiliado' },
  // ], 
  //[

    //{ field: 'codigoCorredor', label: 'Código Corredor' },
    //   { field: 'facebook', label: 'Facebook' },
    //   { field: 'twitter', label: 'Twitter' },
    // ], [
    //   { field: 'instagram', label: 'Instagram' },
    //   { field: 'linkedIn', label: 'LinkedIn' },
    // { field: 'scoringWorkSite', label: 'Scoring Work Site' },
  //],
]

const CONTACT_DETAIL_INFO_FIELD_GROUPS_JURIDIC = [
  [
    // { field: 'ideTercero', label: 'Id Tercero' },
    // { field: 'numDocumento', labelValue: 'desTipoDocumento' },
    // { field: 'razonSocial', label: 'Razón Social' },
    { field: 'direccionPrincipal', label: 'Dirección Principal' },
    { field: 'referenciaDirPrincipal', label: 'Referencia Principal' },
    { field: 'telefPrincipal', label: 'Teléfono Principal' },
  ],
  [
    // { field: 'desTipoTercero', label: 'Tipo de Persona' },
  ],
  [
    { field: 'emailPrincipal', label: 'Correo Principal' },
    { field: 'canalPrincipal', label: 'Canal Principal' },
    { field: 'segmento', label: 'Segmento' },
  ],
  [
    // { field: 'sectorista', label: 'Sectorista' },
    { field: 'grupoEconomico', label: 'Grupo Económico' },
    // { field: 'registroWeb', label: 'Registro Web' },
    { field: 'ciiu', label: 'CIIU' },
    { field: 'giro', label: 'Giro' },
  ],
  [
    { field: 'sector', label: 'Sector' },
    { field: 'seps', label: 'SEPS' },
    { field: 'leyProteccionDatos', label: 'Ley de Protección de Datos' },
  ],
  [
    // { field: 'tratamiento', label: 'Tratamiento' },
   // { field: 'esBroker', label: 'Es Broker' },
    { field: 'codigoCorredor', label: 'Código Corredor' },
    { field: 'sitioWeb', label: 'Sitio  Web' },
  ],
  [
    { field: 'politicaCom', label: 'Aceptó Politica Comunicaciones' },
    { field: 'blackList', label: 'Black List' },
    //{ field: 'codExterno', label: 'Código Externo' },
  ],
]

const notNull = value => (_.isNil(value) || !`${value}`.trim()) ? '  -' : `${'  ' + value}`

class ContactInformation extends Component {
  state = {
    collapseContactInfo: true,
    collapseContactDetailInfo: true,
    showDetail: false,
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.loader.length !== this.props.loader.length && this.props) {
      this.setState({
        showDetail: true,
      })
    }
  }

  getContactInfoColumns = () => {
    if (this.props.person.personType === 'PJ')
      return CONTACT_INFO_FIELD_GROUPS_JURIDIC
    return CONTACT_INFO_FIELD_GROUPS_NATURAL
  }

  getContactInfoDetailColumns = () => {
    if (this.props.person.personType === 'PJ')
      return CONTACT_DETAIL_INFO_FIELD_GROUPS_JURIDIC
    return CONTACT_DETAIL_INFO_FIELD_GROUPS_NATURAL
  }

  getDataSelect = () => {
    const {
      person: { select }
    } = this.props;

    if (select.fecNacimiento) {
      select.fecNacimiento = (select.fecNacimiento.length > 0) ? changeFormatDate(select.fecNacimiento, '-', '/') : ''
    }

    return select;
  }

  getStyleTratamiento = (val) => {
    let backTratamiento = ''

    if (val === 'T4' || val === 'T5' || val === 'T6') {
      backTratamiento = 'rgb(64, 193, 172)'
    } else {
      backTratamiento = 'rgb(255,198,0)'
    }


    return (
      <div style={{
        position: 'absolute',
        width: '92%',
        background: backTratamiento,
        height: '46%',
        bottom: '12px',
      }} />
    )
  }

  render() {
    const { collapseContactInfo, collapseContactDetailInfo, showDetail } = this.state
    const { person: { detail }, loader } = this.props

    const select = this.getDataSelect();
    const loaderVisible = loader.includes(personLoaderKeys.PERSON_DETAIL)

    return (
      <DivContactInformation>
        <br />
        <MDBRow>
          <div className="col-sm-12">
            <MDBBreadcrumb
              onClick={() => {
                this.setState({
                  collapseContactInfo: !this.state.collapseContactInfo
                })
              }}
            >
              <MDBBreadcrumbItem>
                {collapseContactInfo
                  ? <MDBIcon icon="angle-down" />
                  : <MDBIcon icon="angle-right" />}
                {"  Información de Cliente"}
              </MDBBreadcrumbItem>

            </MDBBreadcrumb>

            <MDBCollapse id="collapseContactInfo" isOpen={this.state.collapseContactInfo}>
              <br />
              <DivInput>
                <div className="col-lg-12">
                  {this.getContactInfoColumns().map((group, index) => (
                    <div className="row" key={index}>
                      {group.map((item, index) => {
                        return (
                          <div className="col-lg-4" key={index}>
                            {item.field === 'tratamiento'
                              ? this.getStyleTratamiento(select[item.field]) : null}
                            <MDBInput
                              value={`${notNull(select[item.field])}`}
                              label={item.labelValue
                                ? select[item.labelValue]
                                : item.label}
                              size="sm"
                              style={{
                                fontWeight: item.field === 'tratamiento'
                                  ? 'bold'
                                  : 'normal',
                                color: 'black',
                              }}
                              disabled
                            />
                          </div>
                        )
                      })}
                    </div>
                  ))}
                </div>
              </DivInput>
            </MDBCollapse>
          </div>
        </MDBRow>
        <br />
        <MDBRow>
          <div className="col-sm-12">
            <MDBBreadcrumb
              onClick={() => {
                this.setState({
                  collapseContactDetailInfo: !this.state.collapseContactDetailInfo
                })
              }}
            >
              <MDBBreadcrumbItem>
                {(collapseContactDetailInfo)
                  ? <MDBIcon icon="angle-down" />
                  : <MDBIcon icon="angle-right" />}
                {"  Detalles de Cliente"}
              </MDBBreadcrumbItem>
            </MDBBreadcrumb>

            {(loaderVisible && collapseContactDetailInfo)
              ?
              <MDBRow center>
                <Loader />
              </MDBRow>
              :
              (showDetail
                ?
                <MDBCollapse id="collapseContactInfo" isOpen={this.state.collapseContactDetailInfo}>
                  <br />
                  <DivInput>
                    <div className="col-lg-12">
                      {
                        // @ts-ignore
                        this.getContactInfoDetailColumns().map((group, index) => (
                          <div className="row" key={index}>
                            {group.map((item, index) => {
                              return (
                                <div className="col-lg-4" key={index}>
                                  <MDBInput
                                    value={`${notNull(detail[item.field])}`}
                                    label={item.labelValue
                                      ? detail[item.labelValue]
                                      : item.label}
                                    size="sm"
                                    style={{
                                      color: 'black',
                                    }}
                                    disabled
                                  />
                                </div>
                              )
                            })}
                          </div>
                        ))}
                    </div>
                  </DivInput>

                  <div className="container row">
                    <div className="col-lg-12">
                      <ModalAditionalData
                        detail={detail}
                      />
                    </div>
                  </div>
                </MDBCollapse>
                :
                null
              )}
          </div>
        </MDBRow>
        <br />
      </DivContactInformation>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    loader: state.shared.loader,
    person: state.person
  }
}

export default connect(mapStateToProps, null)(ContactInformation)