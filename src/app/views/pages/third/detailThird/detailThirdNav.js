//@ts-check
import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import Carousel from 'nuka-carousel'
import { withToastManager } from 'react-toast-notifications'
import styled from 'styled-components'
import { MDBCard, MDBCardBody, MDBCardHeader, MDBIcon } from 'mdbreact'
import useMediaQuery from '@material-ui/core/useMediaQuery'
import { Button } from '@material-ui/core'

import { policyOperations } from 'state/ducks/policy'
import { personOperations } from 'state/ducks/person'
import { sinisterOperations } from 'state/ducks/sinister'
import { warrantyLetterOperations } from 'state/ducks/warrantyLetter'
import { operationOperations } from 'state/ducks/operation'
import { salesforceOperations } from 'state/ducks/salesforce'
import PubSub from 'state/utils/PubSub'
import {
  CasesNav, ClinicWorkshotsNav, CommunicationNav, ProcedureNav,
  ProductsNav, ResumenNav, RetentionNav, SinesterNav, WarrantyLetter,
  RefundNav, ClaimsNav, PreexistenceNav, ProductsSuggestNav
} from './detailNav/index'
import { rol_web360 } from 'state/utils/constants'
import _ from 'lodash'

const DivHeaderNav = styled.div`
  .navHeader .white-text {
    color: red
  }
  .nav-link.active {
    background-color: red;
    color: white;
  }
  .nav-link {
    color: black;
  }
  a {
    text-align: center;
    border-radius: 5px;
  }
  button {
    text-align: center;
    border-radius: 5px;
    width: 100%;
  }
`;

const PROFILE_TAB = [
  {
    label: 'RESUMEN',
    key: 'resume',
    access: [rol_web360.ROL_CONSULTA_PERSONA],
    post: 1
  },
  {
    label: 'POLIZAS',
    key: 'products',
    access: [rol_web360.ROL_FFVV, rol_web360.ROL_CONSULTA_PERSONA],
    post: 2
  },
  {
    label: 'CASOS',
    key: 'cases',
    access: [rol_web360.ROL_CONSULTA_PERSONA],
    post: 3
  },
  {
    label: 'TRAMITES',
    key: 'procedure',
    access: [rol_web360.ROL_FFVV, rol_web360.ROL_CONSULTA_PERSONA],
    post: 4
  },
  {
    label: 'REEMBOLSOS',
    key: 'refund',
    access: [rol_web360.ROL_CONSULTA_PERSONA],
    post: 5
  },
  {
    label: 'PREEXISTENCIA',
    key: 'preexistence',
    access: [rol_web360.ROL_PREEXISTENCIA],
    post: 6
  },
  {
    label: 'COMUNICACIONES',
    key: 'communication',
    access: [rol_web360.ROL_CONSULTA_PERSONA],
    post: 7
  },
  {
    label: 'PRODUCTOS SUGERIDOS',
    key: 'productsSuggest',
    access: [rol_web360.ROL_FFVV, rol_web360.ROL_CONSULTA_PERSONA],
    post: 8
  },
  {
    label: 'SINIESTROS',
    key: 'sinister',
    access: [rol_web360.ROL_CONSULTA_PERSONA],
    post: 9
  },
  {
    label: 'CARTA DE GARANTIA',
    key: 'warrantyLetter',
    access: [rol_web360.ROL_CARTAGARANTIA],
    post: 10
  },
  {
    label: 'CLINICAS/TALLERES',
    key: 'clinicWorkshots',
    access: [rol_web360.ROL_FFVV, rol_web360.ROL_CONSULTA_PERSONA],
    post: 11
  },
]

const CarouselHeader = props => {
  const greaterThan1000 = useMediaQuery('(min-width:1000px)')
  const greaterThan1250 = useMediaQuery('(min-width:1250px)')

  let slidesToShow = 5
  if (!greaterThan1000)
    slidesToShow = 4
  else if (greaterThan1250)
    slidesToShow = 6

  return (
    <Carousel
      {...props}
      slidesToShow={slidesToShow}
    >
      {props.children}
    </Carousel>
  )
}

class DetailNav extends Component {
  constructor(props) {
    super(props)
    this.state = {
      openNav: '',
      loaderSinisterNav: false,
      firstLoader: false
    }
  }
  componentDidUpdate(prevProps, prevState){
    if(_.isEmpty(this.props.person.detail) && this.state.openNav !== 'resume' && this.state.firstLoader){
      this.setState({'openNav':'resume'})
    }
  }
  componentDidMount() {
    const { toastManager } = this.props
    this.setState({firstLoader: true})
    PubSub.subscribe('ERR_POLICY_LIST', (message) => {
      console.log('ERR_POLICY_LIST')
      toastManager.add(message, {
        autoDismissTimeout: 8000,
        appearance: 'error',
        autoDismiss: true,
        pauseOnHover: true,
      })
    })

    PubSub.subscribe('ERR_PERSON_COMMUNICATION_LIST', (message) => {
      console.log('ERR_PERSON_COMMUNICATION_LIST')
      toastManager.add(message, {
        autoDismissTimeout: 8000,
        appearance: 'error',
        autoDismiss: true,
        pauseOnHover: true,
      })
    })

    PubSub.subscribe('ERR_POLICY_RETENTION_LIST_MASSIVE', (message) => {
      console.log('ERR_POLICY_RETENTION_LIST_MASSIVE')
      toastManager.add(message, {
        autoDismissTimeout: 8000,
        appearance: 'error',
        autoDismiss: true,
        pauseOnHover: true,
      })
    })

    PubSub.subscribe('ERR_SINISTER_LIST', (message) => {
      console.log('ERR_SINISTER_LIST')
      toastManager.add(message, {
        autoDismissTimeout: 8000,
        appearance: 'error',
        autoDismiss: true,
        pauseOnHover: true,
      })
    })

    PubSub.subscribe('ERR_WARRANTY_LETTER_LIST', (message) => {
      console.log('ERR_WARRANTY_LETTER_LIST')
      toastManager.add(message, {
        autoDismissTimeout: 8000,
        appearance: 'error',
        autoDismiss: true,
        pauseOnHover: true,
      })
    })

    PubSub.subscribe('ERR_OPERATION_PROCEDURE_LIST', (message) => {
      console.log('ERR_OPERATION_PROCEDURE_LIST')
      toastManager.add(message, {
        autoDismissTimeout: 8000,
        appearance: 'error',
        autoDismiss: true,
        pauseOnHover: true,
      })
    })

    PubSub.subscribe('ERR_POLICY_DETAIL', (message) => {
      console.log('ERR_POLICY_DETAIL')
      toastManager.add(message, {
        autoDismissTimeout: 8000,
        appearance: 'error',
        autoDismiss: true,
        pauseOnHover: true,
      })
    })
  }

  /**
   * @param {string} nameNav
   * @param {object} [firstAction]
   * @param {boolean} firstAction.callService - Indica si se debe llamar al servicio
   * @param {object} [firstAction.select] - el objeto seleccionado
   * @param {object[]} [firstAction.data] - lista de datos
   * @param {number} [firstAction.type] - el tipo de registros segun el NAV
   * 
   */
  selectNav = (nameNav, firstAction) => {
    this.setState({
      openNav: nameNav,
      [nameNav + 'FA']: firstAction
    }, () => {
      setTimeout(() => {
        const scrollableDiv = document.getElementById('my_scrollable_detailThird_div')
        scrollableDiv && scrollableDiv.scrollIntoView({ behavior: 'smooth' })
      }, 100)
    })
  }

  getListPolicy = async (filasPorPag = 10, pagActual = 1, rbProductosValue = 'PV') => {
    const { person: { select }, policyOperations, } = this.props
    let requestBody = {
      filter: {
        ideTercero: (select.ideTercero) ? select.ideTercero.toString() : "",
        codExterno: (select.codExterno) ? select.codExterno.toString() : "",
        codAfiliado: (select.codAfiliado) ? select.codAfiliado.toString() : "",
        numPlaca: "",
        numMotor: "",
        nombreTitular: "",
        estado: rbProductosValue,
        tipoDocumento: (select.codTipoDocumento) ? select.codTipoDocumento.toString() : "",
        numDocumento: (select.numDocumento) ? select.numDocumento.toString() : "",
      },
      pagination: {
        filasPorPag,
        pagActual,
      }
    }
    policyOperations.policyList(requestBody)
    
    // auditoria
    let requestLogging = {
      "path": window.location.href,
      "action": "POLICY_LIST"
    }
    policyOperations.auditLogging(requestLogging)
  }

  selectPolicy = async (selected) => {
    const { policyOperations } = this.props
    const requestBody = {
      core: '',
      codProducto: '',
      codProdAcselx: '',
      numPoliza: '',
      numCertificado: '',
      codAfiliado: '',
      codCliente: '',
      codPlan: '',
      idePoliza: '',
    }
    requestBody.core = selected.core
    requestBody.codProducto = selected.codProducto ? selected.codProducto.toString() : ""
    requestBody.codProdAcselx = selected.codProdAcselx ? selected.codProdAcselx.toString() : ""
    requestBody.numPoliza = selected.numPoliza ? selected.numPoliza.toString() : ""
    requestBody.numCertificado = selected.numCertificado ? selected.numCertificado.toString() : ""
    requestBody.codAfiliado = selected.codAfiliado ? selected.codAfiliado.toString() : ""
    requestBody.codCliente = selected.codCliente ? selected.codCliente.toString() : ""
    requestBody.codPlan = selected.codPlan ? selected.codPlan.toString() : ""
    requestBody.idePoliza = selected.idePoliza ? selected.idePoliza.toString() : ""

    policyOperations.policySelect(selected)
    {/* Enviar endpoint auditoria*/ }
    let requestLogging = {
      "path": window.location.href,
      "action": "POLICY_SELECT"
    }
    policyOperations.auditLogging(requestLogging)

    setTimeout(function () {
      policyOperations.policyDetail(requestBody)
      let requestLogging = {
        "path": window.location.href,
        "action": "POLICY_DETAIL"
      }
      policyOperations.auditLogging(requestLogging)
      {/* Enviar endpoint auditoria*/ }
    }, 500)
    // auditoria
    
  }

  getCommunicationList = async () => {
    const {
      person: { select: selectedPerson },
      personOperations
    } = this.props
    // @ts-ignore
    const { ideTercero, numDocumento, codTipoDocumento, codExterno } = selectedPerson || {}
    const requestBody = {
      "idTercero": ideTercero ? `${ideTercero}` : "",
      "numeroDocumento": numDocumento ? `${numDocumento}` : "",
      "tipoDocumento": codTipoDocumento ? `${codTipoDocumento}` : "",
      "codExterno": codExterno ? `${codExterno}` : "",
    }
    personOperations.personCommunicationList(requestBody)
    // auditoria
    let requestLogging = {
      "path": window.location.href,
      "action": "PERSON_COMMUNICATION_LIST"
    }
    personOperations.auditLogging(requestLogging)
      
    
  }

  getRetentionListMassive = () => {
    const { person, salesforceOperations } = this.props
    // @ts-ignore
    const { numDocumento, codTipoDocumento } = person.select || {}
    const payload = {
      "tipoDocumento": codTipoDocumento ? `${codTipoDocumento}` : '',
      "numeroDocumento": numDocumento ? `${numDocumento}` : ''
    }

    salesforceOperations.retentionList(payload)
    
    // auditoria
    let requestLogging = {
      "path": window.location.href,
      "action": "SALESFORCE_RETENTION_LIST"
    }
    salesforceOperations.auditLogging(requestLogging)
  }

  getCasesList = (filasPorPag = 10, pagActual = 1, status = 'all', types = []) => {
    const { person, salesforceOperations } = this.props
    // @ts-ignore
    const { codExterno } = person.select || {}
    const payload = {
      filter: {
        codExterno: codExterno ? codExterno.toString() : '',
        status,
        ...(types.length > 0 ? { types } : {})
      },
      pagination: {
        filasPorPag,
        pagActual,
      },
    }
    salesforceOperations.casesList(payload)
    
    // auditoria
    let requestLogging = {
      "path": window.location.href,
      "action": "SALESFORCE_CASE_LIST"
    }
    salesforceOperations.auditLogging(requestLogging)
  }

  getListSinister = ({
    core = "", casoBizagi, codProdAcselx, numPoliza,
    numCertificado, page = 1, rowPage = 10,
  }) => {
    const { person, sinisterOperations } = this.props
    // @ts-ignore
    const { codExterno, codAfiliado } = person.select || {}
    const requestBody = {
      "filter": {
        "codExterno": codExterno ? `${codExterno}` : "",
        "codAfiliado": codAfiliado ? `${codAfiliado}` : "",
        "casoBizagi": casoBizagi ? casoBizagi : "",
        "codProdAcselx": codProdAcselx ? codProdAcselx : "",
        "numPoliza": numPoliza ? numPoliza : "",
        "numCertificado": numCertificado ? numCertificado : "",
        core,
      },
      "pagination": {
        "filasPorPag": rowPage,
        "pagActual": page,
      },
    }
    sinisterOperations.sinisterList(requestBody)
    
    // auditoria
    let requestLogging = {
      "path": window.location.href,
      "action": "SINISTER_LIST"
    }
    sinisterOperations.auditLogging(requestLogging)
  }

  getWarrantyLetter = () => {
    const { warrantyLetterOperations, person } = this.props
    // @ts-ignore
    const { codAfiliado } = person.select || {}
    const payload = {
      "codAfiliado": codAfiliado ? codAfiliado.toString() : "",
      "filtrar": true
    }
    warrantyLetterOperations.warrantyLetterList(payload)
    // auditoria
    let requestLogging = {
      "path": window.location.href,
      "action": "WARRANTY_LETTER_LIST"
    }
    warrantyLetterOperations.auditLogging(requestLogging)
  }

  getProcedureList = ({ page = 1, rowPage = 10 }) => {
    const { person, operationOperations } = this.props
    // @ts-ignore
    const { ideTercero, codTipoDocumento, numDocumento, codExterno } = person.select || {}
    const requestBody = {
      "filter": {
        "ideTercero": ideTercero ? `${ideTercero}` : "",
        "tipoDocumento": codTipoDocumento ? `${codTipoDocumento}` : "",
        "numDocumento": numDocumento ? `${numDocumento}` : "",
        "codExterno": codExterno ? `${codExterno}` : "",
      },
      "pagination": {
        "filasPorPag": rowPage,
        "pagActual": page
      }
    }
    operationOperations.operationProcedureList(requestBody)

    // auditoria
    let requestLogging = {
      "path": window.location.href,
      "action": "OPERATIONS_PROCEDURE_LIST"
    }
    personOperations.auditLogging(requestLogging)
  }

  getRefundList = () => {
    const { operationOperations, person } = this.props
    const date = new Date()
    date.setFullYear(date.getFullYear() - 2)
    const requestBody = {
      "codAfiliado": (person.select && person.select.codAfiliado) ? person.select.codAfiliado.toString() : '',
      "codProdAcselx": "",
      "numPoliza": "",
      "fecMinRegistro": `${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}`,
      "enLinea": ""
    }
    operationOperations.operationRefundList(requestBody)
    // auditoria
    let requestLogging = {
      "path": window.location.href,
      "action": "OPERATIONS_REFUND_LIST"
    }
    personOperations.auditLogging(requestLogging)
  }

  getPreexistenceList = () => {
    const { personOperations, person } = this.props
    const requestBody = {
      "codAfiliado": (person.select && person.select.codAfiliado)
        ? person.select.codAfiliado.toString()
        : '',
      "codProdAcselx": "",
    }
    personOperations.personPreexistenceList(requestBody)
    // auditoria
    let requestLogging = {
      "path": window.location.href,
      "action": "PERSON_PREEXISTENCE_LIST"
    }
    personOperations.auditLogging(requestLogging)
  }

  renderTabs = () => {
    const { policy, provider, person, sinister, operation, warrantyLetter } = this.props
    switch (this.state.openNav) {
      case 'resume':
        return (
          <ResumenNav 
            selectNav={this.selectNav}
          /> 
        )
      case 'cases':
        return (
          <CasesNav
            getCasesList={this.getCasesList}
            casesFA={this.state.casesFA ? this.state.casesFA : null}
          />
        )
      case 'products':
        return (
          // @ts-ignore
          <ProductsNav
            policy={policy}
            selectPolicy={this.selectPolicy}
            getPolicyList={this.getListPolicy}
            personType={person.personType}
            productsFA={this.state.productsFA ? this.state.productsFA : null}
          />
        )
      case 'sinister':
        return (
          <SinesterNav
            getListSinister={this.getListSinister}
            sinister={sinister}
            sinisterFA={this.state.sinisterFA ? this.state.sinisterFA : null}
          />
        )
      case 'clinicWorkshots':
        return (
          <ClinicWorkshotsNav
            policy={policy}
            provider={provider}
            getPolicyList={this.getListPolicy}
          />
        )
      case 'procedure':
        return (
          <ProcedureNav
            operation={operation}
            getProcedureList={this.getProcedureList}
          />
        )
      case 'communication':
        return (
          <CommunicationNav
            communication={person.comunication}
          />
        )
      case 'retention':
        return (
          <RetentionNav
            getRetentionListMassive={this.getRetentionListMassive}
          />
        )
      case 'warrantyLetter':
        return (
          <WarrantyLetter warrantyLetter={warrantyLetter} />
        )
      case 'refund':
        return (
          <RefundNav
            getRefundList={this.getRefundList}
            refundFA={this.state.refundFA ? this.state.refundFA : null}
          />
        )
      case 'claims':
        return (
          <ClaimsNav />
        )
      case 'preexistence':
        return (
          <PreexistenceNav
            getPreexistenceList={this.getPreexistenceList}
          />
        )
      case 'productsSuggest':
        return <ProductsSuggestNav />
      default:
        return (
          <div>
            {this.selectNav('resume')}
          </div>
        )
    }
  }

  generateTabsForProfile = () => {
    const { auth: { cognitoGroups } } = this.props.user
    let tabsProfile = []
    let roles = Object.values(rol_web360)

    if (!cognitoGroups) return tabsProfile;

    cognitoGroups.forEach(group => {
      if (roles.includes(group)) {
        let tabs = PROFILE_TAB.filter(tab => tab.access.includes(group))
        tabs.forEach((tab) => {
          if (!tabsProfile.includes(tab.key)) {
            tabsProfile.push(tab.key)
          }
        })
      }
    })
    return tabsProfile
  }

  render() {
    const { openNav } = this.state
    const config = {
      slideIndex: 0,
      length: 6,
      wrapAround: false,
      underlineHeader: false,
      slidesToShow: 6,
      slidesToScroll: 2,
      cellAlign: 'left',
      transitionMode: 'scroll',
      heightMode: 'max',
      withoutControls: false,
    }

    const tabsProfile = this.generateTabsForProfile()

    return (
      <MDBCard>

        <MDBCardHeader>
          <CarouselHeader
            {...config}
            renderCenterLeftControls={(obj) => (
              <button onClick={obj.previousSlide}
                style={{
                  border: '0px',
                  borderRadius: '5px',
                  background: 'rgba(0, 0, 0, 0.4)',
                  color: 'white',
                  padding: '10px',
                  outline: '0px',
                  opacity: (obj.currentSlide !== 0) ? 1 : 0.3,
                  cursor: (obj.currentSlide !== 0) ? 'pointer' : 'not-allowed'
                }}
              >
                <MDBIcon
                  icon="angle-double-left"
                />
              </button>
            )}
            renderCenterRightControls={(obj) => (
              <button onClick={obj.nextSlide}
                style={{
                  border: '0px',
                  borderRadius: '5px',
                  background: 'rgba(0, 0, 0, 0.4)',
                  color: 'white',
                  padding: '10px',
                  outline: '0px',
                  opacity: ((obj.currentSlide + obj.slidesToShow) !== obj.slideCount) ? 1 : 0.3,
                  cursor: ((obj.currentSlide + obj.slidesToShow) !== obj.slideCount) ? 'pointer' : 'not-allowed'
                }}
              >
                <MDBIcon icon="angle-double-right" />
              </button>
            )}
            renderBottomCenterControls={null}
          >
            {
              PROFILE_TAB.map((tab, index) => {
                if (tabsProfile.includes(tab.key)) {
                  return (
                    <DivHeaderNav key={index}>
                      <Button 
                        className={"nav-link navHeader " + (openNav === tab.key ? 'active' : "")} aria-selected="true"
                        onClick={() => this.selectNav(tab.key)}>{tab.label}</Button> 
                    </DivHeaderNav>
                    )
                }
                return null
              })
            }
          </CarouselHeader>
        </MDBCardHeader>

        <MDBCardBody style={{ paddingTop: '0px' }}>
          <div
            style={{ width: '10px', height: '10px' }}
            id="my_scrollable_detailThird_div"
          />
          {this.renderTabs()}
        </MDBCardBody>
      </MDBCard>
    )
  }
}

const mapStateToProps = (state) => ({
  policy: state.policy,
  provider: state.provider,
  person: state.person,
  sinister: state.sinister,
  warrantyLetter: state.warrantyLetter,
  operation: state.operation,
  user: state.user
})
 
const mapDispatchToProps = (dispatch) => ({
  policyOperations: bindActionCreators(policyOperations, dispatch),
  personOperations: bindActionCreators(personOperations, dispatch),
  sinisterOperations: bindActionCreators(sinisterOperations, dispatch),
  warrantyLetterOperations: bindActionCreators(warrantyLetterOperations, dispatch),
  operationOperations: bindActionCreators(operationOperations, dispatch),
  salesforceOperations: bindActionCreators(salesforceOperations, dispatch),
})

export default connect(mapStateToProps, mapDispatchToProps)(withToastManager(React.memo(DetailNav)))

