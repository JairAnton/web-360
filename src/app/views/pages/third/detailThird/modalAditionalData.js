import React, { Component } from 'react';
import { MDBContainer, MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter } from 'mdbreact';
import { MDBNav, MDBNavItem, MDBNavLink } from "mdbreact";
import styled from 'styled-components';

import MUIDataTable from "mui-datatables";
import TableUI from '../../../../components/table';
import ElementTable from 'components/table/elementTable';
import { changeFormatDate, } from 'state/utils/functionUtils'

const DivHeaderAditionalData = styled.div`
    .nav-pills .navHeader .white-text{
        color: red
    }

    .nav-pills .nav-item .nav-link.active{
        background-color: red;
        color: white;
    }
     
    .nav-pills .nav-link{
        color: black;
    }
`
const StyledA = styled.a`
    color: red;

    &:hover{
        color: blue;
    }
`;

class ModalAditionalData extends Component {
  constructor(props) {
    super(props);

    this.state = {
      openModal: false,
      numNav: 1
    }

    this.toggleModalOpen = this.toggleModalOpen.bind(this);
  }

  getColumnsAddress = () => {
    const columns = [
      {
        label: "Principal",
        prop: "esPrincipal",
        minWidth: 120,
      },
      {
        label: "Dirección",
        prop: "direccion",
        minWidth: 160,
      },
      {
        label: "Referencia",
        prop: "referencia",
        minWidth: 160,
      },
      {
        label: "Departamento",
        prop: "departamento",
        minWidth: 160,
      },
      {
        label: "Provincia",
        prop: "provincia",
        minWidth: 140,
      },
      {
        label: "Distrito",
        prop: "districo",
        minWidth: 160,
      },
      {
        label: "Estado",
        prop: "estado",
        minWidth: 110,
      },
      {
        label: "Uso",
        prop: "uso",
        minWidth: 160,
      },
      {
        label: "Fec. Ult. Actualización",
        prop: "fechaModif",
        minWidth: 200,
      }
    ]

    return columns;
  }

  getDataAddress = () => {
    const directions = this.props.detail.direcciones
    const data = directions ? directions : []

    return data.map((addres) => ({
      ...addres,
      "fechaModif": (addres.fechaModif) ? changeFormatDate(addres.fechaModif, '-', '/') : ''
    }));
  }

  getColumnsPhones = () => {
    const columns = [
      {
        label: "Principal",
        prop: "esPrincipal",
        minWidth: 160,
      },
      {
        label: "Tipo",
        prop: "tipo",
        minWidth: 160,
      },
      {
        label: "N° Tel/Cel",
        prop: "numero",
        minWidth: 160,
      },
      {
        label: "Anexo",
        prop: "anexo",
        minWidth: 160,
      },
      {
        label: "Uso",
        prop: "uso",
        minWidth: 160,
      },
      {
        label: "Estado",
        prop: "estado",
        minWidth: 160,
      },
      {
        label: "Fec. Ult. Actualización",
        prop: "fechaModif",
        minWidth: 200,
      }
    ]

    return columns;
  }

  getDataPhones = () => {
    const cells = this.props.detail.telefonos;
    const data = cells ? cells : []

    return data;
  }

  getColumnsEmails = () => {
    const columns = [
      {
        label: "Principal",
        prop: "esPrincipal",
        minWidth: 160,
      },
      {
        label: "Correo",
        prop: "correo",
        minWidth: 160,
      },
      {
        label: "Uso",
        prop: "uso",
        minWidth: 160,
      },
      {
        label: "Estado",
        prop: "estado",
        minWidth: 160,
      },
      {
        label: "Fec. Ult. Actualización",
        prop: "fechaModif",
        minWidth: 160,
      }
    ]

    return columns;
  }

  getDataEmails = () => {
    const emails = this.props.detail.emails;
    const data = emails ? emails : [];

    return data.map((email) => ({
      ...email,
      "fechaModif": (email.fechaModif) ? changeFormatDate(email.fechaModif, '-', '/') : ''
    }));;
  }

  getColumnsDocuments = () => {
    const columns = [
      {
        label: "Principal",
        prop: "esPrincipal",
        minWidth: 160,
      },
      {
        label: "Tipo",
        prop: "tipo",
        minWidth: 160,
      },
      {
        label: "Número Documento",
        prop: "numDocumento",
        minWidth: 160,
      },
      {
        label: "Fec. Ult. Actualización",
        prop: "fechaModif",
        minWidth: 160,
      }
    ]

    return columns;
  }

  getDataDocuments = () => {
    const documentos = this.props.detail.documentos;
    const data = documentos ? documentos : [];

    return data.map((document) => ({
      ...document,
      "fechaModif": (document.fechaModif) ? changeFormatDate(document.fechaModif, '-', '/') : ''
    }));;
  }

  toggleModalOpen = () => {
    this.setState({
      openModal: !this.state.openModal
    })
  }


  selectNav = (idNav) => {
    this.setState({
      numNav: idNav
    })
  }

  render() {
    let numNav = this.state.numNav;

    return (
      <div>
        <StyledA href="#" onClick={() => this.toggleModalOpen()}>
          Datos de Contacto
        </StyledA>

        <MDBModal isOpen={this.state.openModal} toggle={() => this.toggleModalOpen()} fullHeight position="top">
          <MDBModalHeader toggle={() => this.toggleModalOpen()}>
            Datos de Contacto
                    </MDBModalHeader>
          <MDBModalBody>
            <DivHeaderAditionalData>
              <ul className="nav nav-pills mb-1" id="pills-tab" role="tablist">
                <li className="nav-item">
                  <a href="#modalAditionalData" className={"nav-link navHeader " + (numNav === 1 ? 'active' : "")}
                    onClick={() => this.selectNav(1)}
                  >Direcciones</a>
                </li>
                <li className="nav-item">
                  <a href="#modalAditionalData" className={"nav-link navHeader " + (numNav === 2 ? 'active' : "")}
                    onClick={() => this.selectNav(2)}
                  >Telefonos</a>
                </li>
                <li className="nav-item">
                  <a href="#modalAditionalData" className={"nav-link navHeader " + (numNav === 3 ? 'active' : "")}
                    onClick={() => {
                      this.selectNav(3);
                    }}
                  >Emails</a>
                </li>
                <li className="nav-item">
                  <a href="#modalAditionalData" className={"nav-link navHeader " + (numNav === 4 ? 'active' : "")}
                    onClick={() => {
                      this.selectNav(4);
                    }}
                  >Documentos</a>
                </li>
              </ul>
            </DivHeaderAditionalData>
            <div id="modalAditionalData">
            </div>
            <hr />
            {
              (
                () => {
                  switch (numNav) {
                    case 1:
                      return (
                        <ElementTable
                          key={numNav}
                          data={this.getDataAddress()}
                          columns={this.getColumnsAddress()}
                          allRow
                        />
                      );
                    case 2:
                      return (
                        <ElementTable
                          key={numNav}
                          data={this.getDataPhones()}
                          columns={this.getColumnsPhones()}
                          allRow
                        />
                      );
                    case 3:
                      return (
                        <ElementTable
                          key={numNav}
                          data={this.getDataEmails()}
                          columns={this.getColumnsEmails()}
                          allRow
                        />
                      );
                    case 4:
                      return (
                        <ElementTable
                          key={numNav}
                          data={this.getDataDocuments()}
                          columns={this.getColumnsDocuments()}
                          allRow
                        />
                      );
                    default:
                      return (
                        <div>
                          Error, llame a backOffice..
                        </div>
                      )
                  }
                }
              )()
            }
          </MDBModalBody>
          <MDBModalFooter>
            <MDBBtn color="danger" onClick={() => this.toggleModalOpen()}
              style={{
                'color': 'white'
              }}
            >Cerrar</MDBBtn>
          </MDBModalFooter>
        </MDBModal>
      </div>
    );
  }
}

export default ModalAditionalData;