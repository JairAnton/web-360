// @ts-check
import React, { Component } from 'react';

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { withToastManager } from 'react-toast-notifications'
import _ from 'lodash'

import { personOperations } from 'state/ducks/person'
import { generalOperations } from 'state/ducks/general'
import { personLoaderKeys } from 'state/ducks/person'

import DetailThirdNav from './detailThird/detailThirdNav'
import ContactInformation from './detailThird/contactInformation'
import PubSub from 'state/utils/PubSub'

import { MDBCard, MDBCardHeader, MDBContainer, MDBRow } from "mdbreact"
import Loader from 'components/loader'
import { ConsoleLogger } from '@aws-amplify/core';
import { policyOperations } from 'state/ducks/policy'

class DetailThird extends Component {

  state = {
    loaderVisible: true,
    personSelect : {},
    firstCall: false
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.loaderVisible && !_.isEmpty(this.props.person.detail)){
      this.setState({ loaderVisible: false, personSelect : this.props.person.select, firstCall: true})
    }
    if(this.state.firstCall && _.isEmpty(this.props.person.detail)){
      if(this.props.loader.filter(x=> x === 'PERSON_DETAIL').length === 0 && JSON.stringify(this.state.personSelect) !== JSON.stringify(this.props.person.select)){
        let {  person } = this.props
          let bodyDetail = {
            "tipoTercero": "",
            "tipoDocumento": "",
            "numDocumento": "",
            "ideTercero": (person.select && person.select.ideTercero) ? person.select.ideTercero.toString() : '',
            "codExterno": (person.select && person.select.codExterno) ? person.select.codExterno.toString() : ''
          }
          const {personOperations, policy} = this.props
          personOperations.personDetail(bodyDetail)  
          // auditoria
            
          let requestLogging = {
            "path": window.location.href,
            "action": "PERSON_DETAIL"
          }
          personOperations.auditLogging(requestLogging)
          this.setState({
            personSelect: this.props.person.select
          }) 
          let requestBody = {
            filter: {
              ideTercero: (this.props.person.select.ideTercero) ? this.props.person.select.ideTercero.toString() : "",
              codExterno: (this.props.person.select.codExterno) ? this.props.person.select.codExterno.toString() : "",
              codAfiliado: (this.props.person.select.codAfiliado) ? this.props.person.select.codAfiliado.toString() : "",
              numPlaca: "",
              numMotor: "",
              nombreTitular: "",
              estado: 'PV'
            },
            pagination: {
              filasPorPag: 10,
              pagActual: 1,
            }
          }
    
          policyOperations.policyResumePvList(requestBody)

  
      }
    }
  }

  componentDidMount() {
    const { personOperations, person, toastManager } = this.props

    let bodyDetail = {
      "tipoTercero": "",
      "tipoDocumento": "",
      "numDocumento": "",
      "ideTercero": (person.select && person.select.ideTercero) ? person.select.ideTercero.toString() : '',
      "codExterno": (person.select && person.select.codExterno) ? person.select.codExterno.toString() : ''
    }

    PubSub.subscribe('ERR_PERSON_DETAIL', (message) => {
      toastManager.add(message, {
        autoDismissTimeout: 8000,
        appearance: 'error',
        autoDismiss: true,
        pauseOnHover: true,
      })
    })

    setTimeout(() => {
      personOperations.personDetail(bodyDetail)
      // auditoria
      let requestLogging = {
        "path": window.location.href,
        "action": "PERSON_DETAIL"
      }
      personOperations.auditLogging(requestLogging)
    }, 1000)

  }

  render() {
    const { person: { select, detail, personType }, person, loader } = this.props
    const loaderVisible = loader.includes(personLoaderKeys.PERSON_DETAIL)

    return (
      <div>
        <MDBCard>
          <MDBCardHeader
            color="lighten-1"
            style={{ backgroundColor: '#E52E2D' }}
          >
            {select &&
              `
                ${select.nombre ? select.nombre : ''}
                ${personType === 'PN' && select.apePaterno ? select.apePaterno : ''}
                ${personType === 'PN' && select.apeMaterno ? select.apeMaterno : ''}
                ${personType === 'PJ' && !select.nombre ? (select.apePaterno ? select.apePaterno : '') : ''}
                ${personType === 'PJ' && !select.nombre ? (select.apeMaterno ? select.apeMaterno : '') : ''}
              `
            }
          </MDBCardHeader>
          <MDBContainer fluid>
            <ContactInformation
              person={person}
            />
          </MDBContainer>
        </MDBCard>
        <br />
        {/* {loaderVisible ? (
          <MDBRow center>
            <Loader />
          </MDBRow>
        ) : (
            <div>
              <DetailThirdNav />
            </div>
          )} */}
        <div>
          <DetailThirdNav />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  person: state.person,
  parameters: state.general.parameters,
  loader: state.shared.loader,
});

const mapDispatchToProps = (dispatch) => ({
  personOperations: bindActionCreators(personOperations, dispatch),
  policyOperations: bindActionCreators(policyOperations, dispatch),
  generalOperations: bindActionCreators(generalOperations, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(withToastManager(React.memo(DetailThird)));