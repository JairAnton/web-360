import React from 'react'

import SideBar from './sideBar'
import Footer from './footer'
import Header from './header'

export default (props) => {
    return (
        <div id="wrapper">
            <SideBar />
            <div id="content-wrapper" className="d-flex flex-column">
                <div id="content">
                    <br />
                    <div className="container-fluid">
                        {props.children}
                    </div>
                </div>
                <Footer />
            </div>
        </div>
    );
}