import React, { useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { bindActionCreators } from 'redux'
import { MDBIcon, MDBRow } from 'mdbreact'
import { NavLink } from 'react-router-dom'
import { HashLink as Link } from 'react-router-hash-link'
import { routes } from 'state/utils/routes'
import { userOperations } from 'state/ducks/user'
import { menu_options } from 'state/utils/constants'
import LogoV360 from '../../../assets/img/LogoV360.png'

export const SIDEBAR_WIDTH = '230px'

const SideMenu = ({ marginLeft, toggle, isOpenMenu }) => {
  const dispatch = useDispatch()
  const userActionOperations = useMemo(() => {
    return bindActionCreators(userOperations, dispatch)
  }, [dispatch])

  const { auth: { cognitoGroups } } = useSelector(state => state.user)


  return (

    <div style={{
      backgroundColor: 'rgb(229, 46, 45)',
      width: SIDEBAR_WIDTH,
      transition: 'margin .5s',
      position: 'absolute',
      marginLeft,
      height: '100vh',
    }}>
      <div
        className={isOpenMenu ? "sidebar-brand d-flex align-items-center justify-content-center" : ""}
        style={{
          backgroundColor: "black", height: isOpenMenu ? '100px' : '40px', textAlign: isOpenMenu ? "" : "right"
          , paddingRight: isOpenMenu ? 0 : 15, paddingTop: isOpenMenu ? 0 : 8
        }}
      >

        {
          isOpenMenu ?
            /*<div style={{ display: 'flex' }}>
              <MDBIcon style={{ "color": "white", "textAlign": "right" }}
                icon={'arrow-left'} onClick={() => toggle(false)} />
              <img src={LogoV360} alt="LogoV360" width="150" height="68" />
            </div>*/
            <img src={LogoV360} alt="LogoV360" width="150" height="68" />
            :
            <span style={{"textAlign": "right"}} class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Abrir menú">
            <MDBIcon style={{ "color": "white","cursor":"pointer" }}
              icon={'bars'} onClick={() => toggle(false)} />
            </span>
           
        }

      </div>


      <li className="nav-item" style={{ "textAlign": "center", "listStyleType": 'none' }} >
        {
          menu_options.filter(opt => {
            let auxOpt = false
            cognitoGroups.forEach(group => {
              if (opt.rol.includes(group)) {
                auxOpt = true
              }
            })
            return auxOpt
          }).map((option, index) => {
            return (
              <NavLink to={option.route} key={index} onClick={() => toggle(true)}>
                <div className="nav-link" style={{ "color": "white", "textAlign": "right" }} >
                  {
                    isOpenMenu ?
                      <div style={{ "textAlign": "left" }}>
                        <MDBIcon icon={option.icon} /> <span>{option.label}</span>
                      </div> :
                      <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title={option.label}>
                          <MDBIcon icon={option.icon} />
                      </span>  
                  }
                </div>
              </NavLink>
            )
          })
        }
        {  
          isOpenMenu?        
          <div className="nav-link" style={{ "color": "white", "textAlign": "right" , cursor: 'pointer'}} 
          onClick={() => toggle(true)}>
                  {
                    isOpenMenu ?
                      <div style={{ "textAlign": "left" }}>
                        <MDBIcon icon={'arrow-left'} /> <span>{'Cerrar menú'}</span>
                      </div> : 
                          <MDBIcon icon={'arrow-left'} /> 
                  }
                </div>:null
        }

      </li>
      <div style={SIDEMENU_STYLES.breakSesion}>
        {isOpenMenu ?
          <MDBRow center>
            <p className="footer-copyright mb-0 py-3 text-center">
              <Link to="#" onClick={() => {
                userActionOperations.logout()
              }}>

                {
                  <div>
                    <MDBIcon icon="sign-out-alt" /> {'Salir'}
                  </div>
                }
              </Link>
            </p>
          </MDBRow> :

          <MDBRow end>
          <span style={{"textAlign": "right","marginRight": "30px"}} class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Salir">
                <p className="footer-copyright mb-0 py-3 text-center" >
              <Link to="#" onClick={() => {
                userActionOperations.logout()
              }}>
                <MDBIcon icon="sign-out-alt" />
              </Link>
            </p>
            </span>
          </MDBRow>
        }
      </div>
    </div>
  )
}

const SIDEMENU_STYLES = {
  breakSesion: {
    "position": "absolute",
    "bottom": "0px",
    "color": "white",
    "backgroundColor": "rgb(184,35,35)",
    "width": '100%',
    "height": '53px'
  }
}

export default React.memo(SideMenu)