import React from 'react';
import 'components/layout/showAllUser.css'
import { personOperations, personLoaderKeys } from 'state/ducks/person'
import { MDBIcon } from 'mdbreact';
import { HashLink as Link } from 'react-router-hash-link'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { withToastManager } from 'react-toast-notifications'
import { generalOperations } from 'state/ducks/general'
import { policyOperations } from 'state/ducks/policy'
import { salesforceOperations } from 'state/ducks/salesforce'
import { sinisterOperations } from 'state/ducks/sinister'
import { operationOperations } from 'state/ducks/operation'
import _ from 'lodash'

class ShowAllUser extends React.Component {
    elements = []

    constructor(props){ 
        super(props)
        this.state = {
            'data': []
        }
        this.getUser();
    }
    
    getUser(){
        for(var x = 0; x < sessionStorage.length; x++){
            var keySession = sessionStorage.key(x)

            if(keySession !== "ouath_pkce_key")
            {
                var usuario = JSON.parse(sessionStorage.getItem(sessionStorage.key(x)));
                var data = [{
                    "numDocumento":usuario.data.numDocumento ,
                    "nombre":  usuario.data.tipoDocumento === "DNI            " 
                                ? usuario.data.nombre + " " +usuario.data.apePaterno + " " +usuario.data.apeMaterno
                                : usuario.data.razonSocial
                }]
                this.elements.push(data)
            }
        }
    }
    render(){
        var selectClient = (numDocument) => {
            if(window.location.pathname !== "/cliente/detalle")
            {
                var usuario = JSON.parse(sessionStorage.getItem(numDocument));
                const { personOperations } = this.props
    
                personOperations.personSearch(usuario.request);
                // auditoria
                let requestLogging = {
                    "path": window.location.href,
                    "action": "PERSON_SEARCH"
                }
                personOperations.auditLogging(requestLogging)
                
                setTimeout(()=>{
                    const { personOperations } = this.props
                    personOperations.personSelect(numDocument)
                    // auditoria
                    let requestLogging = {
                        "path": window.location.href,
                        "action": "PERSON_SELECT"
                    }
                    personOperations.auditLogging(requestLogging)
                },2000)    
            }
            else{
                if(this.props.person.select && this.props.person.select.numDocumento !== numDocument){
                    var usuario = JSON.parse(sessionStorage.getItem(numDocument));
                    const { personOperations } = this.props
        
                    personOperations.personSearch(usuario.request);
                    // auditoria
                    let requestLogging = {
                        "path": window.location.href,
                        "action": "PERSON_SEARCH"
                    }
                    personOperations.auditLogging(requestLogging)
                    
                    setTimeout(()=>{
                        const { personOperations } = this.props
                        personOperations.personSelect(numDocument)
                        // auditoria
                        let requestLogging = {
                            "path": window.location.href,
                            "action": "PERSON_SELECT"
                        }
                        personOperations.auditLogging(requestLogging)
                    },2000)    
                }
            }
        }
        return (
            <div className="contenedor" >
                <ul className="iterador">
                    {
                        //listItems
                        this.elements.map(function(itm,i){
                            return(
                                <Link to="#" 
                                    onClick={()=> selectClient(itm[0].numDocumento)} 
                                    >
                                    <li key={itm[0].numDocumento} 
                                        className="items" >
                                        <MDBIcon icon={'user'} className="icon" />
                                        {itm[0].nombre}
                                    </li>
                                </Link>
                            )
                        })
                    }
                </ul>
            </div>
        );
    }
}


const mapStateToProps = state => ({
    user: state.user,
    person: state.person,
    parameters: state.general.parameters,
    loader: state.shared.loader,
    //userFrequents:state.general.userFrequents
  });
  
  const mapDispatchToProps = (dispatch) => ({
    policyOperations: bindActionCreators(policyOperations, dispatch),
    personOperations: bindActionCreators(personOperations, dispatch),
    salesforceOperations: bindActionCreators(salesforceOperations, dispatch),
    sinisterOperations: bindActionCreators(sinisterOperations, dispatch),
    generalOperations: bindActionCreators(generalOperations, dispatch),
    operationOperations: bindActionCreators(operationOperations, dispatch)
  });

export default connect(mapStateToProps,mapDispatchToProps)(withToastManager(ShowAllUser));