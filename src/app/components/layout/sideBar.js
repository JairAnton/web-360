import React from 'react'
import { NavLink } from 'react-router-dom';
import { routes } from '../../state/utils/routes';

export default (props) => {
    return (
    <ul className="navbar-nav bg-gradient-danger sidebar sidebar-dark accordion" id="accordionSidebar">
      <a className="sidebar-brand d-flex align-items-center justify-content-center" href="javascript:void(0)">
        {/* <div className="sidebar-brand-icon rotate-n-15">
          <i className="fas fa-laugh-wink"></i>
        </div> */}
        <div className="sidebar-brand-text mx-3">RIMAC <sup>360°</sup></div>
      </a>

      <hr className="sidebar-divider my-0"/>
        
      <li className="nav-item">
        <NavLink to={routes.THIRD}>
          <a className="nav-link" href="javascript:void(0)">
            <i className="fas fa-fw fa-tachometer-alt"></i>
            <span>Búsqueda de Clientes</span>
            {/* Enviar endpoint auditoria*/ }
          </a>
        </NavLink>
        <NavLink to={routes.THIRD}>
          <a className="nav-link" href="javascript:void(0)">
            <i className="fas fa-fw fa-tachometer-alt"></i>
            <span>Consulta de Casos</span>
          </a>
        </NavLink>
      </li>

      <hr className="sidebar-divider"/> 
    </ul>
    );
}