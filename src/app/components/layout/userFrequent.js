import React,{Component, Fragment} from 'react';
import { MDBIcon } from 'mdbreact';
import ShowUser from 'components/layout/showAllUser'

class UserFrequent extends React.Component {
    elements = []

    state = {
        'active':false
    }
    
    show(){
        if(this.state.active === true){    
            this.setState({
                'active':false
            });
        }
        else{
            this.setState({
                'active':true
            });
        }
    }

    render(){
        return (
            <div>
                <div style={USER_STYLES.breakSesion} onClick={()=>{this.show()}}>
                    <MDBIcon icon={'cog'} />
                </div>
                {this.state.active 
                    ? 
                        <div onClick={()=> {this.show()}}>
                            <ShowUser   />
                        </div>  
                    : null}
            </div>
            
            
            
        );
    }
 
}





const USER_STYLES = {
    breakSesion: {
      "position": "absolute",
      "z-index":"100",
      "right": "0",
      "top":"30vh",
      "border-radius":"0px 0px 0px 40px",
      "color": "white",
      "padding-left":"8px",
      "backgroundColor": "rgb(184,35,35)",
      "width": '30px',
      "height": '30px',
      "cursor":"pointer",
      "alignItems": "left"
    }
  }
export default React.memo(UserFrequent);

