import React from 'react';
import { MDBFooter, MDBBtn, MDBIcon } from 'mdbreact';

const Footer = () => {
  return (
    <MDBFooter
      style={{
        backgroundColor: "#E52E2D",
        height: '77px',
        borderLeft: '1px solid white',
      }}
      className="text-center font-small darken-2"
    >
      <div className="pt-4" />
      <p className="footer-copyright mb-0 py-3 text-center">
        &copy; {new Date().getFullYear()} Copyright: <a href="https://www.rimac.com"> Rimac.com </a>
      </p>
    </MDBFooter>

  );
}

export default Footer;