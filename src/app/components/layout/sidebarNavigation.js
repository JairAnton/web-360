import React from 'react';
import { MDBListGroup, MDBIcon } from 'mdbreact';
import { NavLink } from 'react-router-dom';
import { routes } from '../../state/utils/routes';

const TopNavigation = () => {
  return (
    <MDBListGroup
      className="sidebar-fixed position-fixed list-group-flush"
      style={{ "backgroundColor": "#E52E2D" }}
    >
      <br />
      <div
        className="sidebar-brand d-flex align-items-center justify-content-center"
        style={{ "color": "white" }}
      >
        <h4>
          <div className="sidebar-brand-text mx-3">RIMAC <sup>360°</sup></div>
        </h4>
      </div>
      <hr className="hr-light wow fadeInLeft" style={{ "color": "white" }} />
      <li className="nav-item" style={{ "textAlign": "center" }}>

        <NavLink to={routes.THIRD}>
          <div className="nav-link" style={{ "color": "white" }}>
            <MDBIcon icon="tachometer-alt" />
            <span>Búsqueda de Clientes</span>
          </div>
        </NavLink>

        <NavLink to={routes.CASES}>
          <div className="nav-link" style={{ "color": "white" }}>
            <MDBIcon icon="tachometer-alt" />
            <span>Búsqueda de Casos</span>
          </div>
        </NavLink>

      </li>
    </MDBListGroup>
  );
}

export default TopNavigation;