import React from 'react'
import { Table, Pagination, Loading } from 'element-react'
import styled from 'styled-components'
import { height } from '@material-ui/system'

const DivTable = styled.div`
    
    td div.cell{
      color: black;
      padding-left: 3px;
      padding-right:3px;
      white-space: normal;
      word-break: break-word;

    },  
    th div.cell{
      color: black;
      padding-left: 3px;
      padding-right:3px;
      text-aling:center;
      white-space: normal;
      word-break: break-word;
    },   
`

const ElementTable = ({
  columns = [],
  data = [],
  onChangeSelect = () => { },
  page = null,
  rowPage = null,
  onChangePage = () => { },
  onChangeRowPage = () => { },
  loader = false,
  pagination = null,
  allRow = false,
  fontSize = '0.75rem',
  strong = false
}) => {
  const { totalCount = 0 } = pagination || {};

  columns = columns.map(column => ({
    ...column,
    renderHeader: (col) => 
    (<span style={{ fontSize: fontSize }}>
      {
        strong ? (<strong>{col.label}</strong>) : col.label
      }
    </span>)
  }))


  return (
    <DivTable>
      {
        loader ?
          <Loading>
            <div className="row">
              <div className="col-lg-12">
                <Table
                  style={{ width: '100%', cursor: 'pointer'}}
                  columns={columns}
                  data={data}
                  border
                  highlightCurrentRow
                  emptyText="No se encontraron registros."
                  maxHeight={450}
                  headerAlign="left"
                />
              </div>
            </div>
          </Loading>
          :
          <div>
            <div className="row">
              <div className="col-lg-12">
                <Table
                  style={{ width: '100%', zIndex: 0}}
                  columns={columns}
                  data={data}
                  border
                  highlightCurrentRow
                  onCurrentChange={onChangeSelect}
                  emptyText="No se encontraron registros."
                  maxHeight={450}
                />
              </div>
            </div>
            {
              !allRow &&
              <div className="row">
                <div className="col-lg-12">
                  <Pagination
                    layout="total, sizes, prev, pager, next"
                    total={totalCount}
                    currentPage={page}
                    pageSizes={[10, 20, 30, 40, 50]}
                    pageSize={rowPage}
                    small={true}
                    onCurrentChange={onChangePage}
                    onSizeChange={onChangeRowPage}
                  />
                </div>
              </div>
            }

          </div>
      }
    </DivTable>
  )
}

export default ElementTable;
