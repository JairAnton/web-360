import React from 'react'
import MUIDataTable from "mui-datatables";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import Loader from '../loader';


export default ({ data, columns, options, title = null, height = null, loader = null }) => {
  let dataT = (data) ? data : [];
  let columnsT = (columns) ? columns : [];
  let optionsT = (options) ? options : {};
  let titleT = (title) ? title : "";

  options.textLabels = {
    body: {
      noMatch: (loader) ? <Loader /> : 'No se encontraron registros!!!'
    }
  };

  let responsiveScroll = {
    maxHeight: (data) ? (data.length > 5) ? (height) ? height : "350px" : "100%" : "100%"
  }

  const getMuiTheme = () => {
    return createMuiTheme({
      overrides: {
        MuiPaper: {
          root: {
            zIndex: 0
          }
        },
        MUIDataTable: {
          responsiveScroll,
        },
        MUIDataTableBodyCell: {
          root: {
            padding: "0px 40px 0px 16px"
          }
        },
        MUIDataTableHeadCell: {
          root: {
            padding: "0px 40px 0px 16px"
          }
        },
        MuiTableBody: {
          root: {
            cursor: 'pointer'
          }
        }
      }
    })
  }

  return (
    <MuiThemeProvider theme={getMuiTheme()}>
      {loader ?
        <MUIDataTable
          key={1}
          title={titleT}
          data={[]}
          columns={columnsT}
          options={optionsT}
        />
        :
        <MUIDataTable
          key={2}
          title={titleT}
          data={dataT}
          columns={columnsT}
          options={optionsT}
        />
      }
    </MuiThemeProvider>
  );
}

