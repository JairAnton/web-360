import React from 'react'

const Loader = () => (
    <div className="spinner-grow text-primary" role="status">
        <span className="sr-only">Loading...</span>
    </div>
)

export default Loader;