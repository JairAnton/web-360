import React from "react"
import { connect } from "react-redux"
import { Route, Redirect } from "react-router-dom"
import {Auth} from 'aws-amplify'
const processEnv = process.env.NODE_ENV

const RefreshRoute = ({ component: Component, user, ...rest }) => {

  // const refreshWeb = (processEnv === 'development') ? false : (user.auth.jwt ? false : true);
  const refreshWeb = false
  

  return (
    <Route
      {...rest}
      render={props =>
        !refreshWeb ? (
          <Component {...props} />
        ) : (
            <Redirect
              to={{
                pathname: "/"
              }}
            />
          )
      }
    />
  )
};

const mapStateToProps = state => ({
  user: state.user,
});

export default connect(mapStateToProps)(RefreshRoute); 