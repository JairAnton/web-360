import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { BrowserRouter, Route, Switch, Router, Redirect } from 'react-router-dom'
import {
  Login
} from '../views/pages'

import Third from '../views/pages/third'
import Cases from '../views/pages/cases'

//TODO edw
import ProductsPo from '../views/pages/products'

import { routes } from '../state/utils/routes'
import history from '../state/utils/history'

import PrivateRoute from '../state/utils/PrivateRoute'
import RefreshRoute from './RefreshRoute'
import { Auth, Hub } from 'aws-amplify'
import { userOperations } from 'state/ducks/user';
import { Loading, Alert } from 'element-react'

export const rol_web360 = {
  ROL_CONSULTA_PERSONA: 'Rimac_Web360_ROL_CONSULTA_PERSONAS_Grupo',
  ROL_FFVV: 'Rimac_Web360_ROL_FFVV_Grupo',
  ROL_CONSULTA_CASOS: 'Rimac_Web360_ROL_CONSULTA_CASOS_Grupo',
  ROL_CARTAGARANTIA: 'Rimac_Web360_ROL_CARTAGARANTIA_Grupo',
  ROL_PREEXISTENCIA: 'Rimac_Web360_ROL_PREEXISTENCIA_Grupo',
  ROL_CONSULTA_POLIZAS: 'Rimac_Web360_ROL_CONSULTA_POLIZAS_Grupo'   //TODO 
}

class Routes extends Component {
  
  constructor(props){
    super(props)
    this.state = {
      call : false
    }
  }
  
  componentDidMount() {
    const { userOperations, user } = this.props

    Hub.listen("auth", ({ payload: { event, data } }) => {
      // @ts-ignore
      window._pepech = { event, data }
    })
    setTimeout(() => {
      Auth.currentSession()
        .then(data => {
          userOperations.authenticatedLogin()
          // auditoria
          let requestLogging = {
            "path": window.location.href,
            "action": "AUTHENTICATED_LOGIN"
          }
          userOperations.auditLogging(requestLogging)
          if (history.location.pathname.includes('/cliente')) {
            history.push('/cliente')
          }
          if (history.location.pathname.includes('/casos')) {
            history.push('/casos')
          }
          //TODO ed
          if (history.location.pathname.includes('/polizas')) {
            history.push('/polizas')
          }
          
          
          let countSessionOpen = []
          var sessionOpen = window.localStorage.getItem('SESIONES_ABIERTAS')
          
          if(sessionOpen === null || sessionOpen === '[]'){
            //first call
            let requestBody = {
              "clientId": user.sessionId,
              "firstCall": true
            }
            userOperations.sessionAdd(requestBody)

            countSessionOpen.push(user.sessionId)
            window.localStorage.setItem('SESIONES_ABIERTAS', JSON.stringify(countSessionOpen))
          }
          else{
            countSessionOpen = JSON.parse(sessionOpen)

            let requestBody = {
              "clientId": user.sessionId,
              "firstCall": false
            }
            userOperations.sessionAdd(requestBody)

            countSessionOpen.push(user.sessionId)
            window.localStorage.setItem('SESIONES_ABIERTAS', JSON.stringify(countSessionOpen))
          }


          window.addEventListener('beforeunload',(event) => {
            const { user } = this.props
            let countSessionOpen = []
            
            var sessionOpen = window.localStorage.getItem('SESIONES_ABIERTAS')
            
            if(sessionOpen != null ){
              countSessionOpen = JSON.parse(sessionOpen)
              var index = countSessionOpen.indexOf(user.sessionId)

              if(index > -1){
                countSessionOpen.splice(index, 1)
              }
              
              window.localStorage.setItem('SESIONES_ABIERTAS', JSON.stringify(countSessionOpen))
            }
          })
          
        })
        .catch(err => {
          history.push('/')
        })
    }, 2000)
  }
  componentDidUpdate(){
    const { userOperations, user } = this.props
    window.localStorage.setItem('SESIONES_MAXIMAS', user.maxConnection)
  }
  componentWillUnmount(){    
    const { userOperations, user } = this.props

    let countSessionOpen = []
    var sessionOpen = window.localStorage.getItem('SESIONES_ABIERTAS')
    
    if(sessionOpen != null ){
      countSessionOpen = JSON.parse(sessionOpen)
      var index = countSessionOpen.indexOf(user.sessionId)

      if(index > -1){
        countSessionOpen.splice(index, 1)
      }

      window.localStorage.setItem('SESIONES_ABIERTAS', JSON.stringify(countSessionOpen))
    }
  }

  render() {
    const { user } = this.props;
    
    return (
      <Router history={history}>    
        <Route render={({ location }) => (
          <Switch location={location} >
            {
              user.status === ''
              ?
                <Route exact path={routes.LOGIN} component={Login} />
              :
                  user.maxConnection < JSON.parse(window.localStorage.getItem('SESIONES_ABIERTAS')).length
                  ?
                    <Alert
                      title="Usuario no autorizado"
                      type="warning"
                      description="Usted ha llegado al máximo de sesiones permitidas en la WEB360, pida los permisos a su administrador."
                      showIcon={true}
                      closable={false}
                    />
                  :
                    // Validate rol to access to CLIENTE
                    location.pathname.includes(routes.THIRD)
                    ?
                      (
                        !user.auth.loader
                        ?
                          (
                            user.auth.cognitoGroups.filter(x => x.includes(rol_web360.ROL_CONSULTA_PERSONA)).length > 0 || 
                            user.auth.cognitoGroups.filter(x => x.includes(rol_web360.ROL_FFVV)).length > 0 || 
                            user.auth.cognitoGroups.filter(x => x.includes(rol_web360.ROL_CARTAGARANTIA)).length > 0 ||
                            user.auth.cognitoGroups.filter(x => x.includes(rol_web360.ROL_PREEXISTENCIA)).length > 0
                          ? 
                            <Route path={routes.THIRD} component={Third} />
                          : 
                            <Alert
                              title="Usuario no autorizado"
                              type="warning"
                              description="Usted no tiene permisos para acceder a CLIENTE en la WEB360, pida los permisos a su administrador."
                              showIcon={true}
                              closable={false}
                            />
                          )
                        : 
                          <div style={{paddingTop:'300px'}}>
                            <Loading text="Cargando..." />
                          </div>
                      )
                    : 
                      // Validate rol to access to CASOS
                      location.pathname.includes(routes.CASES)
                      ?
                        !user.auth.loader
                          ?
                            (
                              user.auth.cognitoGroups.filter(x => x.includes(rol_web360.ROL_CONSULTA_CASOS)).length > 0
                              ? 
                                <Route path={routes.CASES} component={Cases} />
                              : 
                                <Alert
                                  title="Usuario no autorizado"
                                  type="warning"
                                  description="Usted no tiene permisos para acceder a CASOS en la WEB360, pida los permisos a su administrador."
                                  showIcon={true}
                                  closable={false}
                                />
                            )
                        : 
                          <div style={{paddingTop:'300px'}}>
                            <Loading text="Cargando..." />
                          </div>
                      : 
                        // Validate rol to access to POLIZAS
                        location.pathname.includes(routes.POLIZAS)
                        ? 
                          !user.auth.loader 
                          ?
                            (
                              user.auth.cognitoGroups.filter(x => x.includes(rol_web360.ROL_CONSULTA_POLIZAS)).length > 0 ||
                              user.auth.cognitoGroups.filter(x => x.includes(rol_web360.ROL_FFVV)).length > 0 ||
                              user.auth.cognitoGroups.filter(x => x.includes(rol_web360.ROL_CONSULTA_PERSONA)).length > 0
                              ? 
                                <Route path={routes.POLIZAS} component={ProductsPo} />
                              : 
                                <Alert
                                  title="Usuario no autorizado"
                                  type="warning"
                                  description="Usted no tiene permisos para acceder a POLIZAS en la WEB360, pida los permisos a su administrador."
                                  showIcon={true}
                                  closable={false}
                                />
                            )
                          : 
                            <div style={{paddingTop:'300px'}}>
                              <Loading text="Cargando..." />
                            </div>
                        :
                          location.pathname === routes.LOGIN
                          ?
                            <Route exact path={routes.LOGIN} component={Login} />
                          :
                            <Route exact path="*" render={() => <Redirect to="/" />} />
            }
          </Switch>
        )} />
            
      </Router>
    )
  }
}
const mapStateToProps = (state) => ({
  user: state.user
});
const mapDispatchToProps = (dispatch) => ({
  userOperations: bindActionCreators(userOperations, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Routes);
