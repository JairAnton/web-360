export default class PubSub {
  static subscribers = {};

  static subscribe(topic, callback) {
    const callbacks = this.subscribers[topic];
    if (callbacks) {
      this.subscribers[topic] = callbacks.concat(callback)
    } else {
      this.subscribers[topic] = [callback]
    }
  }

  static publish(topic, message) {
    const callbacks = this.subscribers[topic];
    if (callbacks) {
      callbacks.forEach(c => c(message))
    }
  }
}