export const routes = {
    LOGIN: '/',
    HOME: '/inicio',
    DASHBOARD: '/dashboard',
    THIRD: '/cliente',
    THIRD_DETAIL: '/cliente/detalle',
    REGISTER_CASES: '/registrar-caso',
    CASES: '/casos',
    CASES_DETAIL: '/casos/detalle',
    SEARCH_LICENCE_PLATE: '/searchlicenceplate',
    REMOVE:'/remove',
    POLIZAS:'/polizas' //TODO ed
}