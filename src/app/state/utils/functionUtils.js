export const converter = (list = []) => {
  const newList = []
  let listItem = []
  list.forEach((item, index) => {
    if (index > 0 && index % 3 === 0) {
      newList.push(listItem)
      listItem = []
    }
    listItem.push(item)
  })
  newList.push(listItem)
  return newList
}

export const changeFormatDate = (date, fromFormat, toFormat) => {
  if (date && date.length <= 1)
    return date
  return date ? `${date}`.trim().replace('T', ' ').split(fromFormat).join(toFormat) : '';
}
//2011-05-25T20:03:00.000Z
export const removeSeconds = (value) => {
  return value.includes(':')
    ? `${value.split(':')[0]}:${value.split(':')[1]}`
    : value
}

export const removeTZ = (value) => {
  if (!value) return value

  let newValue = value.includes('T')
    ? value.replace('T', ' ')
    : value

  newValue = newValue.includes('.000Z')
    ? newValue.replace('.000Z', '')
    : newValue

  return newValue
}