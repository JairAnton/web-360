import axios from 'axios';

const getRequestBody = (body) => {
  return {
    "request": {
      "trace": {
        "serviceId": "SERV01",
        "consumerId": "COS01",
        "moduleId": "MOD01",
        "channelCode": "WEB",
        "traceId": "TRACEID01",
        "timestamp": "2019-01-24 10:00:00",
        "identity": {
          "userId": "USER01",
          "deviceId": "DEV01",
          "host": "HOST01"
        }
      },
      "payload": body
    }
  }
}

const httpApi = async (payload) => {
  var responseHttp = null;
  let requestBody = null;

  const {
    body,
    token,
    path
  } = payload;

  requestBody = getRequestBody(body);

  responseHttp = await axios({
    method: 'post',
    url: 'http://localhost:3010/values',
    data: {
      "endpoint": "https://3qx175lfgd.execute-api.us-east-2.amazonaws.com/DESA",
      "path": path,
      "request": requestBody,
      "token": token
    },
    timeout: 1000 * 30,
  }).then(res => {
    let dataJson = null;
    let statusCode = 0;
    let data = res.data;

    statusCode = data.statusCode;
    dataJson = JSON.parse(data.body);

    return {
      dataJson,
      statusCode
    };
  }).catch(err => {
    let statusCode = 400;

    return {
      statusCode,
      dataJson: {
        'message': 'Error al llamar al servicio.'
      }
    };
  });

  return responseHttp;
}

export {
  httpApi
}