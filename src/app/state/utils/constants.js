import KeyMirror from 'keymirror';

export const constants = KeyMirror({
  HEADERS_LOGIN_RIMAC: null,
  HEADERS_RIMAC: null,

  PN: null,
  PJ: null,
})

export const rol_web360 = {
  ROL_CONSULTA_PERSONA: 'Rimac_Web360_ROL_CONSULTA_PERSONAS_Grupo',
  ROL_FFVV: 'Rimac_Web360_ROL_FFVV_Grupo',
  ROL_CONSULTA_CASOS: 'Rimac_Web360_ROL_CONSULTA_CASOS_Grupo',
  ROL_CARTAGARANTIA: 'Rimac_Web360_ROL_CARTAGARANTIA_Grupo',
  ROL_PREEXISTENCIA: 'Rimac_Web360_ROL_PREEXISTENCIA_Grupo',
  ROL_CONSULTA_POLIZAS: 'Rimac_Web360_ROL_CONSULTA_POLIZAS_Grupo'   //TODO 
}

export const menu_options = [
  {
    label: 'Búsqueda de Clientes',
    route: '/cliente',
    icon: 'user',
    rol: [
      rol_web360.ROL_CONSULTA_PERSONA,
      rol_web360.ROL_FFVV,
      rol_web360.ROL_CARTAGARANTIA,
      rol_web360.ROL_PREEXISTENCIA
    ]
  },
  {
    label: 'Búsqueda de Casos',
    route: '/casos',
    icon: 'folder-open',
    rol: [
      rol_web360.ROL_CONSULTA_CASOS,
    ]
  },
  //TODO ed
  {
    label: 'Búsqueda de Polizas',
    route: '/polizas',
    icon: 'file',
    rol: [
      rol_web360.ROL_CONSULTA_POLIZAS,
      rol_web360.ROL_FFVV,
      rol_web360.ROL_CONSULTA_PERSONA

    ]
  }
]

export const CASE_TYPES = [
  { value: 'F1_AsistenciaHogar', label: 'Asistencia al hogar' },
  { value: 'F1_AsistenciaViajes', label: 'Asistencia en viajes' },
  { value: 'F1_AsistenciaFunerariaSepelio', label: 'Asistencia funeraria sepelio' },
  { value: 'F1_AtencionProgamada', label: 'Atención programada' },
  { value: 'F1_ChoferReemplazo', label: 'Chofer de reemplazo' },
  { value: 'F1_CobranzaRenovacion', label: 'Cobranza - Renovación' },
  { value: 'F1_DeliveryMedicinas', label: 'Delivery de medicinas' },
  { value: 'F1_EmergenciaRiesgosGeneralesAjustador', label: 'Emergencia riesgos generales - Ajustador' },
  { value: 'F1_EmergenciaVehicular', label: 'Emergencia vehicular' },
  { value: 'F1_GruaAuxilioMecanico', label: 'Grúa y / o auxilio mecánico' },
  { value: 'F1_MedicoDomicilio', label: 'Médico a domicilio' },
  { value: 'F1_OrientacionMedicaTelefonicaVirtual', label: 'Orientación médica(Telefónica / Virtual)' },
  { value: 'F1_Reclamaciones_CNT', label: 'Reclamaciones CNT' },
  { value: 'F1_ReclamacionEPS', label: 'Reclamación EPS' },
  { value: 'F1_ReclamacionLegal', label: 'Reclamación Legal' },
  { value: 'F1_ReclamacionSeguros', label: 'Reclamación de seguros' },
  { value: 'F1_Referencia', label: 'Referencia' },
  { value: 'F1_Renovacion', label: 'Renovación' },
  { value: 'F1_RequerimientoOperaciones', label: 'Requerimiento u operaciones' },
  { value: 'F1_RequerimientosOperacionesCNT', label: 'Requerimientos u operaciones CNT' },
  { value: 'F1_Retencion', label: 'Retención' },
  { value: 'F1_SiniestroSOAT', label: 'Siniestro SOAT' },
  { value: 'F1_UnidadMedicaEmergencia', label: 'Unidad médica emergencia' },
]