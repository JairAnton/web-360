import { createStore, applyMiddleware, combineReducers } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { composeWithDevTools } from 'redux-devtools-extension'
import { logger } from 'redux-logger'

import * as reducers from "../ducks"

import {
  userSagas,
  generalSagas,
  policySagas,
  personSagas,
  sinisterSagas,
  operationSagas,
  warrantyLetterSagas,
  providerSagas,
  salesforceSagas
} from '../ducks/saga'

//import GlobalMiddleware from '../ducks/middleware';

const sagaMiddleware = createSagaMiddleware();
const rootReducer = combineReducers(reducers);



const store = createStore(
  rootReducer,
  composeWithDevTools(
    process.env.NODE_ENV === 'development' ?
      applyMiddleware(sagaMiddleware, /*GlobalMiddleware,*/ logger)
      : applyMiddleware(sagaMiddleware /*GlobalMiddleware,*/)
  )
);

sagaMiddleware.run(userSagas);
sagaMiddleware.run(generalSagas);
sagaMiddleware.run(personSagas);
sagaMiddleware.run(policySagas);
sagaMiddleware.run(sinisterSagas);
sagaMiddleware.run(operationSagas);
sagaMiddleware.run(warrantyLetterSagas);
sagaMiddleware.run(providerSagas);
sagaMiddleware.run(salesforceSagas);

export default store;