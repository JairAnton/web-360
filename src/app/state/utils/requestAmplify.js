// import axios from 'axios';
import _ from 'lodash';
// import { constants as constantsHeaders } from "./constants";
// import * as headersAll from './headers';

import { API } from "aws-amplify";
import constansAws from '../utils/envAws'

const getRequestBody = (body) => {
  return {
    request: {
      trace: {
        serviceId: "SERV01",
        consumerId: "COS01",
        moduleId: "MOD01",
        channelCode: "WEB",
        traceId: "TRACEID01",
        timestamp: "2019-01-24 10:00:00",
        identity: {
          userId: "USER01",
          deviceId: "DEV01",
          host: "HOST01"
        }
      },
      payload: body
    }
  }
}

const createRequest = ({ url, data = {} }) => {

  const requestBody = getRequestBody(data);
  let responseAmplify = {
    statusCode: 500,
    dataJson: null,
  }
//
  const myInit = {
    body: requestBody,
  }

  return new Promise(async (resolve, reject) => {
    let errorDetail = ''
    try {
      const response = await API.post(constansAws.apiGateway.NAME, url, myInit)
      console.log(`${url}-RESPONSE:::`, response)
      if (response.response.status.success) {
        responseAmplify.statusCode = 200;
        responseAmplify.dataJson = response.response.payload;
      } else {
        try {
          errorDetail = response.status.error.messages.join()
        } catch (error) {
          console.log('FAILURE GETTING THE ERROR MESSAGES 1', error)
        }
        responseAmplify.dataJson = { "message": `Error al llamar al servicio. ${errorDetail}` }
      }
      resolve(responseAmplify)
    } catch (error) {
      console.log(`${url}-ERROR:::`, { error })
      try {
        errorDetail = error.response.data.response.status.error.messages.join()
      } catch (error) {
        console.log('FAILURE GETTING THE ERROR MESSAGES 2', error)
      }
      reject(`Error al llamar al servicio ${url} - ${errorDetail}`)
    }
  })
}

export const createAPI = urls =>
  Object.keys(urls).reduce(
    (acc, key) => ({ ...acc, [key]: createRequest(urls[key]) }),
    {}
  )

export default createRequest
