import request from './requestAmplify';
import _ from 'lodash';
import * as apis from "../ducks/api";

const createRequest = url => props => request({ ...props, url })

const allApis = Object.keys(apis).reduce(
    (ac, key) => _.merge(ac, apis[key]),
    {}
);

export default Object.keys(allApis).reduce(
  (acc, key) => ({ ...acc, [key]: createRequest(allApis[key]) }),
  {}
);

