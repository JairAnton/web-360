// @ts-check
// const processEnv = process.env.NODE_ENV
const USER_POOL_ID = 'us-east-2_wchT5vspc'
const APP_CLIENT_ID = '5b8s0uknaf05mjgoi651fai1hs'
const USER_NAME = 'user002-api-crm-rimac'
const PASSWORD = 'QWErty1234.-'
const IDENTITY_POOL_ID = 'us-east-2:8e215dbe-191e-4487-b5fb-aa7fac64f634' // USE WHEN THERE IS NO FEDERATION
const AWS_SERVICE = 'execute-api'
// const NAME_API_GATEWAY = 'UE1NETDESAAPICRM011'

// const FEDERATED_USER_POOL_ID = 'us-east-2_oYyBgwLfv'
// const FEDERATED_APP_CLIENT_ID = '5lbqcqtb4609md3vf9it1dokci'
// const FEDERATED_IDENTITY_POOL_ID = 'us-east-2:86ca9a85-de18-4e2c-b0c2-1acc103c958d'

// const URL = 'https://07s0h7b39d.execute-api.us-east-2.amazonaws.com/DESA'
// const URL = 'https://6uzj59czhd.execute-api.us-east-2.amazonaws.com/TEST'

const LOCAL_VARIABLES = {
  "REACT_APP_REGION": "us-east-2",
  "REACT_APP_OAUTH_DOMAIN": "rimac-com-pe-desa.auth.us-east-2.amazoncognito.com",
  "REACT_APP_OAUTH_REDIRECT_SIGN_IN": 'http://localhost:4004/cliente/',
  "REACT_APP_OAUTH_REDIRECT_SIGN_OUT": 'http://localhost:4004/',

  "REACT_APP_STORAGE_BUCKET": "ue1stgdesaas3prv001",
  "REACT_APP_STORAGE_IDENTITY_POOL_ID": "us-east-2:86ca9a85-de18-4e2c-b0c2-1acc103c958d",

  "REACT_APP_API_ENDPOINT_NAME": "UE1NETDESAAPICRM011",
  "REACT_APP_API_ENDPOINT_URL": "https://6uzj59czhd.execute-api.us-east-2.amazonaws.com/TEST",
  //"REACT_APP_API_ENDPOINT_URL": "https://07s0h7b39d.execute-api.us-east-2.amazonaws.com/DESA",
  "REACT_APP_FEDERATED_IDENTITY_POOL_ID": "us-east-2:86ca9a85-de18-4e2c-b0c2-1acc103c958d",
  "REACT_APP_FEDERATED_USER_POOL_ID": "us-east-2_oYyBgwLfv",
  "REACT_APP_FEDERATED_APP_CLIENT_ID": "5lbqcqtb4609md3vf9it1dokci"



  /*
  "REACT_APP_REGION": "us-east-2",
  "REACT_APP_OAUTH_DOMAIN": "rimac-com-pe-desa.auth.us-east-2.amazoncognito.com",
  "REACT_APP_OAUTH_REDIRECT_SIGN_IN": 'http://localhost:4004/cliente/',
  "REACT_APP_OAUTH_REDIRECT_SIGN_OUT": 'http://localhost:4004/',


  "REACT_APP_STORAGE_BUCKET": "ue1stgdesaas3prv001",
  "REACT_APP_STORAGE_IDENTITY_POOL_ID": "us-east-2:86ca9a85-de18-4e2c-b0c2-1acc103c958d",

  "REACT_APP_API_ENDPOINT_NAME": "UE1NETDESAAPICRM011",
  "REACT_APP_API_ENDPOINT_URL": "https://6uzj59czhd.execute-api.us-east-2.amazonaws.com/TEST",

  "REACT_APP_FEDERATED_IDENTITY_POOL_ID": "us-east-2:86ca9a85-de18-4e2c-b0c2-1acc103c958d",
  "REACT_APP_FEDERATED_USER_POOL_ID": "us-east-2_oYyBgwLfv",
  "REACT_APP_FEDERATED_APP_CLIENT_ID": "5lbqcqtb4609md3vf9it1dokci"

  
  /*"REACT_APP_REGION": "us-east-1",
  "REACT_APP_OAUTH_DOMAIN": "rimac-com-pe-virg.auth.us-east-1.amazoncognito.com",
  "REACT_APP_OAUTH_REDIRECT_SIGN_IN": 'http://localhost:4004/cliente/',
  "REACT_APP_OAUTH_REDIRECT_SIGN_OUT": 'http://localhost:4004/',


  "REACT_APP_STORAGE_BUCKET": "ue2stgprodas3prv001",
  "REACT_APP_STORAGE_IDENTITY_POOL_ID": "us-east-1:ad4b3733-3102-408c-bcf4-592c849054c3",

  "REACT_APP_API_ENDPOINT_NAME": "UE2NETPRODAPICRM011",
  "REACT_APP_API_ENDPOINT_URL": "https://br5drmw6e5.execute-api.us-east-1.amazonaws.com/PROD",

  "REACT_APP_FEDERATED_IDENTITY_POOL_ID": "us-east-1:ad4b3733-3102-408c-bcf4-592c849054c3",
  "REACT_APP_FEDERATED_USER_POOL_ID": "us-east-1_f5ohckbmy",
  "REACT_APP_FEDERATED_APP_CLIENT_ID": "59b9tdhuuk8ktj8lbvlapgc9g0"*/
}
const REGION = process.env.NODE_ENV === 'development' ? LOCAL_VARIABLES.REACT_APP_REGION : process.env.REACT_APP_REGION
const FEDERATED_IDENTITY_POOL_ID = process.env.NODE_ENV === 'development' ? LOCAL_VARIABLES.REACT_APP_FEDERATED_IDENTITY_POOL_ID : process.env.REACT_APP_FEDERATED_IDENTITY_POOL_ID
const FEDERATED_USER_POOL_ID = process.env.NODE_ENV === 'development' ? LOCAL_VARIABLES.REACT_APP_FEDERATED_USER_POOL_ID : process.env.REACT_APP_FEDERATED_USER_POOL_ID
const FEDERATED_APP_CLIENT_ID = process.env.NODE_ENV === 'development' ? LOCAL_VARIABLES.REACT_APP_FEDERATED_APP_CLIENT_ID : process.env.REACT_APP_FEDERATED_APP_CLIENT_ID
const BUCKET = process.env.NODE_ENV === 'development' ? LOCAL_VARIABLES.REACT_APP_STORAGE_BUCKET : process.env.REACT_APP_STORAGE_BUCKET
const NAME_API_GATEWAY = process.env.NODE_ENV === 'development' ? LOCAL_VARIABLES.REACT_APP_API_ENDPOINT_NAME : process.env.REACT_APP_API_ENDPOINT_NAME
const URL = process.env.NODE_ENV === 'development' ? LOCAL_VARIABLES.REACT_APP_API_ENDPOINT_URL : process.env.REACT_APP_API_ENDPOINT_URL

export default {
  MAX_ATTACHMENT_SIZE: 5000000,
  LOCAL_VARIABLES,
  s3: {
    REGION,
    BUCKET
  },
  apiGateway: {
    NAME: NAME_API_GATEWAY,
    REGION,
    URL,
    AWS_SERVICE
  },
  cognito: {
    REGION,
    USER_POOL_ID,
    APP_CLIENT_ID,
    IDENTITY_POOL_ID,
    FEDERATED_USER_POOL_ID,
    FEDERATED_APP_CLIENT_ID,
    FEDERATED_IDENTITY_POOL_ID
  },
  credentials: {
    USER_NAME,
    PASSWORD,
  },
};
