import { API } from "aws-amplify";
import constansAws from '../utils/envAws'

const getRequestBody = (body) => {
  return {
    "request": {
      "trace": {
        "serviceId": "SERV01",
        "consumerId": "COS01",
        "moduleId": "MOD01",
        "channelCode": "WEB",
        "traceId": "TRACEID01",
        "timestamp": "2019-01-24 10:00:00",
        "identity": {
          "userId": "USER01",
          "deviceId": "DEV01",
          "host": "HOST01"
        }
      },
      "payload": body
    }
  }
}

const httpAmplify = async ({ body, path }) => {
  const requestBody = getRequestBody(body);
  let responseAmplify = {
    statusCode: 500,
    dataJson: null
  }

  const myInit = {
    body: requestBody
  }

  return new Promise((resolve, reject) => {
    return API.post(constansAws.apiGateway.NAME, path, myInit)
      .then(response => {
        if (response.response.status.success) {
          responseAmplify.statusCode = 200;
          responseAmplify.dataJson = response.response.payload;
        } else {
          responseAmplify.dataJson = {
            "message": 'Error al llamar al servicio.'
          }
        }
        resolve(responseAmplify)
      })
      .catch(err => {
        responseAmplify.dataJson = {
          "message": 'Error al llamar al servicio.'
        }

        console.log('err -->> ', err);
        reject(err)
      })
  });
}

export {
  httpAmplify
}