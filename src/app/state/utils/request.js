import axios from 'axios';

import _ from 'lodash';

import { constants as constantsHeaders } from "./constants";

import * as headersAll from './headers';

const createRequest = ({
  url,
  data = {},
  params = {},
  paramsUrl = '',
  method = 'POST',
  onSuccess,
  onError,
  headers
}) => {
  var obj,
    objs,
    quantity;
    
  if (headers.type === constantsHeaders.HEADERS_LOGIN_RIMAC) {
    headers = _.assign(headers, headersAll[constantsHeaders.HEADERS_LOGIN_RIMAC]);
  }
  else if (headers.type === constantsHeaders.HEADERS_RIMAC) {
    headers = _.assign(headers, headersAll[constantsHeaders.HEADERS_RIMAC]);
  }

  //headers['Content-Type'] =  'application/json';

  //If exist paramsUrl, Add
  url = `${url}${paramsUrl}`;

  return axios({
    url,
    method,
    data,
    params,
    headers
  })
    .then(({ data }) => {
      /*if (!data || data.status !== 'OK') {
        throw data
      }*/

      return onSuccess(data)
    })
    .catch(onError)
  // }).catch(onSuccess);
}

export const createAPI = urls =>
  Object.keys(urls).reduce(
    (acc, key) => ({ ...acc, [key]: createRequest(urls[key]) }),
    {}
  )

export default createRequest
