import { withToastManager } from 'react-toast-notifications';
// import PubSub from './PubSub'

const success = ({ content }) => {
  alert('ingreso success')
  // @ts-ignore
  const toastManager = withToastManager.toastManager;
  toastManager.add('content', {
    autoDismissTimeout: 8000,
    appearance: 'success',
    autoDismiss: true,
    pauseOnHover: false,
  })
}

const error = ({ content, toastManager }) => (
  toastManager.add(content, {
    autoDismissTimeout: 8000,
    appearance: 'success',
    autoDismiss: true,
    pauseOnHover: false,
  })
)

const warning = ({ content, toastManager }) => (
  toastManager.add(content, {
    autoDismissTimeout: 8000,
    appearance: 'success',
    autoDismiss: true,
    pauseOnHover: false,
  })
)
export default {
  toastSuccess: success,
  toastError: withToastManager(error),
  toastWarning: withToastManager(warning),
}
