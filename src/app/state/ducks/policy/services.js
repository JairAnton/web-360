import api from 'state/utils/api';

export const policyDetail = async (body) => {
  const responsePolicy = await api.policyDetail({ data: body });
  return responsePolicy;
}

export const policyList = async (body) => {
  const responsePolicy = await api.policyList({ data: body });
  return responsePolicy;
}

export const policyListForRenovate = async (body) => {
  const responsePolicy = await api.policyListForRenovate({ data: body });
  return responsePolicy;
}

export const policyShortageList = async (body) => {
  const responsePolicy = await api.policyShortageList({ data: body });
  return responsePolicy;
}

export const policyCertificateEndorsementsList = async (body) => {
  const responsePolicy = await api.policyCertificateEndorsementsList({ data: body });
  return responsePolicy;
}

export const policySuggest = async (body) => {
  const responsePolicy = await api.policySuggest({ data: body });
  return responsePolicy;
}

export const policyOperationList = async (body) => {
  const responsePolicy = await api.policyOperationList({ data: body });
  return responsePolicy;
}

export const policyRetentionListMassive = async (body) => {
  const responsePolicy = await api.policyRetentionListMassive({ data: body });
  return responsePolicy;
}

export const policyCertificateBeneficiariesList = async (body) => {
  const responsePolicy = await api.policyCertificateBeneficiariesList({ data: body });
  return responsePolicy;
}

export const policyCertificateInsuredList = async (body) => {
  const responsePolicy = await api.policyCertificateInsuredList({ data: body });
  return responsePolicy;
}

export const particularDataList = async (body) => {
  const responsePolicy = await api.particularDataPolicy({ data: body });
  return responsePolicy;
}

export const coveragesList = async (body) => {
  const responsePolicy = await api.coveragesList({ data: body });
  return responsePolicy;
}

export const policyStateAccount = async (body) => {
  const responsePolicy = await api.policyStateAccount({ data: body });
  return responsePolicy;
}

export const policyCoverageTypeList = async (body) => {
  const responsePolicy = await api.policyCoverageTypeList({ data: body });
  return responsePolicy;
}

export const policyClauseList = async (body) => {
  const responsePolicy = await api.policyClauseList({ data: body });
  return responsePolicy;
}

export const policyCertificateList = async (body) => {
  const responsePolicy = await api.policyCertificateList({ data: body });
  return responsePolicy;
}
export const policyCertificateListDetalle = async (body) => {
  const responsePolicy = await api.policyCertificateListDetalle({ data: body });
  return responsePolicy;
}

export const policyInsuredList = async (body) => {
  const responsePolicy = await api.policyInsuredList({ data: body });
  return responsePolicy;
}
export const policyInsuredListDetalle = async (body) => {
  const responsePolicy = await api.policyInsuredListDetalle({ data: body });
  return responsePolicy;
}
export const policyListRamos = async (body) => {
  const responsePolicy = await api.policyListRamos({ data: body });
  return responsePolicy;
}

export const policyCoverageDeductibleList = async (body) => {
  const responsePolicy = await api.policyCoverageDeductibleList({ data: body });
  return responsePolicy;
}

export const riskAddresses = async (body) => {
  const responsePolicy = await api.riskAddresses({ data: body });
  return responsePolicy;
}

export const auditLogging = async (body) => {
  const responsePerson = await api.auditLogging({ data: body });
  return responsePerson;
}