import _ from 'lodash'
import { createReducer } from '../../utils';
import { constants } from "./types";

const initialState = {
  data: null,
  dataList: [],
  pagination: {},
  select: null,
  carencia: {
    data: null,
    dataList: [],
    select: null
  },
  deducible: {
    data: null,
    dataList: []
  },
  retention: {
    data: null,
    dataList: null,
    pagination: null,
    select: null,
  },
  clause: {
    data: null,
    dataList: []
  },
  sugerido: {
    data: null,
    dataList: [],
    select: null
  },
  operacion: {
    data: null,
    dataList: [],
    select: null
  },
  cartaGarantia: {
    data: null,
    dataList: [],
    select: null
  },
  certificado: {
    dataList: null,
    pagination: null,
    select: null,
  },
  asegurado: { //a nivel de PJ
    data: null,
    dataList: [],
    pagination: null,
    select: null,
  },
  certificadoAsegurado: {
    data: null,
    dataList: [],
    select: null,
    loaderCertificadoAsegurado: false,
  },
  certificadoBeneficiario: {
    data: null,
    dataList: [],
    select: null,
    loaderCertificadoBeneficiario: false,
  },
  certificadoEndosos: {
    data: null,
    dataList: [],
    select: null
  },
  detalle: {
    data: null,
    dataList: [],
    select: null
  },
  datosParticulares: {
    data: null,
    dataList: [],
    loaderDatosParticulares: false
  },
  coberturas: {
    data: null,
    dataList: [],
    pagination: null,
    loaderCoberturas: false,
    coverageTypes: [],
  },
  porRenovar: {
    data: null,
    dataList: [],
    select: null
  },
  masivo: {
    data: null,
    dataList: [],
    select: null
  },
  stateAccount: {
    data: null,
  },
  sinister: {
    data: null,
    dataList: null,
    pagination: null
  },
  resume: {
    initFetch: false,
    data: null,
    dataList: [],
    selected: null,
    pagination: null
  },
  riskAddresses: {
    dataList: [],
  },
  error: null,
  initLoaderPolicy: false,
  status: null
};

const policy = createReducer(initialState)({
  [constants.POLICY_FAILURE]: (state, action) => {
    var error = action.error;

    let {
      datosParticulares,
      stateAccount,
      certificadoBeneficiario,
      certificadoAsegurado,
      coberturas
    } = state;

    datosParticulares.loaderDatosParticulares = false;
    stateAccount.loaderStateAccount = false;
    certificadoBeneficiario.loaderCertificadoBeneficiario = false
    certificadoAsegurado.loaderCertificadoAsegurado = false;
    coberturas.loaderCoberturas = false;


    return { ...state, error, datosParticulares, stateAccount, certificadoBeneficiario, certificadoAsegurado, initLoaderPolicy: false }
  },

  [constants.SET_POLICY_SHORTAGE_LIST]: (state, action) => {
    var carencia = {
      ...state.carencia,
      dataList: action.response
    };

    return { ...state, carencia }
  },

  [constants.SET_POLICY_CERTIFICATE_INSURED_LETTERS_WARRANTY_LIST]: (state, action) => {
    var cartaGarantia = {
      ...state.cartaGarantia,
      dataList: action.response
    };

    return { ...state, cartaGarantia }
  },

  //#region CertificateInsured
  [constants.SET_POLICY_CERTIFICATE_INSURED_LIST]: (state, action) => {
    var certificadoAsegurado = {
      ...state.certificadoAsegurado,
      dataList: action.response,
      loaderCertificadoAsegurado: false
    };

    return { ...state, certificadoAsegurado }
  },
  //#endregion

  //#region CertificateBeneficiaries

  [constants.SET_POLICY_CERTIFICATE_BENEFICIARIES_LIST]: (state, action) => {
    var certificadoBeneficiario = {
      ...state.certificadoBeneficiario,
      dataList: action.response,
      loaderCertificadoBeneficiario: false
    };

    return { ...state, certificadoBeneficiario }
  },
  //#endregion

  [constants.SET_POLICY_CERTIFICATE_ENDORSEMENTS_LIST]: (state, action) => {
    var certificadoEndosos = {
      ...state.certificadoEndosos,
      dataList: action.response
    };

    return { ...state, certificadoEndosos }
  },

  [constants.SET_POLICY_DETAIL]: (state, action) => {
    var detalle = {
      ...state.detalle,
      data: action.response
    };

    return { ...state, detalle }
  },

  [constants.SET_POLICY_LIST]: (state, action) => {
    let {
      data,
      pagination
    } = action.response;

    return { ...state, dataList: data, pagination, initLoaderPolicy: false }
  },

  [constants.SET_POLICY_LIST_FOR_RENOVATE]: (state, action) => {
    var porRenovar = {
      ...state.porRenovar,
      dataList: action.response
    };

    return { ...state, porRenovar }
  },

  [constants.SET_POLICY_OPERATION_LIST]: (state, action) => {
    var operacion = {
      ...state.operacion,
      dataList: action.response
    };

    return { ...state, operacion }
  },

  [constants.SET_POLICY_RETENTION_LIST_MASSIVE]: (state, action) => {
    var masivo = {
      ...state.masivo,
      dataList: action.response
    };

    return { ...state, masivo }
  },

  [constants.SET_POLICY_SUGGEST]: (state, action) => {
    var sugerido = {
      ...state.sugerido,
      dataList: (action.response) ? action.response : []
    }

    return { ...state, sugerido }
  },

  //#region ParticularData
  [constants.SET_PARTICULAR_DATA_LIST]: (state, action) => {
    var datosParticulares = {
      ...state.datosParticulares,
      dataList: action.body,
    };

    return { ...state, datosParticulares }
  },
  //#endregion

  //#region Coverages
  [constants.SET_COVERAGES_LIST]: (state, action) => {
    let {
      data,
      pagination
    } = action.response;

    let coberturas = {
      ...state.coberturas,
      dataList: (data) ? data : [],
      pagination: (pagination) ? pagination : {},
    }

    return { ...state, coberturas }
  },

  [constants.SET_POLICY_COVERAGE_TYPE_LIST]: (state, action) => {
    const {
      coverageTypes,
      providerTypes
    } = action.response

    let coberturas = {
      ...state.coberturas,
      coverageTypes,
      providerTypes
    }

    return {
      ...state,
      coberturas
    }
  },
  //#endregion

  //#region StateAccount
  [constants.SET_POLICY_STATE_ACCOUNT]: (state, action) => {
    let stateAccount = state.stateAccount
    let data = action.response

    if (data && data.length > 0) {
      stateAccount.data = data[0]
    } else {
      stateAccount.data = null
    }

    return { ...state, stateAccount }
  },


  //#endregion
  [constants.SET_POLICY_SELECT]: (state, action) => {

    return {
      ...state,
      select: action.data,
      carencia: {
        data: null,
        dataList: [],
        select: null
      },
      retencion: {
        data: null,
        dataList: [],
        select: null
      },
      sugerido: {
        data: null,
        dataList: [],
        select: null
      },
      operacion: {
        data: null,
        dataList: [],
        select: null,
        providerTypes: [],
        coverageTypes: []
      },
      coberturas: {
        data: null,
        dataList: [],
        select: null,
      },
      cartaGarantia: {
        data: null,
        dataList: [],
        select: null
      },
      asegurado: { //a nivel de PJ
        data: null,
        dataList: [],
        pagination: null,
        select: null,
      },
      certificadoAsegurado: {
        data: null,
        dataList: [],
        select: null
      },
      certificadoBeneficiario: {
        data: null,
        dataList: [],
        select: null
      },
      certificadoEndosos: {
        data: null,
        dataList: [],
        select: null
      },
      detalle: {
        data: null,
        dataList: [],
        select: null
      },
      datosParticulares: {
        data: null,
        dataList: [],
      },
      porRenovar: {
        data: null,
        dataList: [],
        select: null
      }
    }
  },
  //#region Retention
  [constants.SET_POLICY_RETENTION_LIST_MASSIVE]: (state, action) => {
    var retention = {
      ...state.retention,
      dataList: (action.response.data) ? action.response.data : [],
      pagination: (action.response.pagination) ? action.response.pagination : {},
    }
    return {
      ...state,
      retention,
      error: null
    }
  },
  //#endregion
  [constants.SET_POLICY_SINISTER_LIST]: (state, action) => {
    var sinister = {
      ...state.sinister,
      dataList: (action.response.data) ? action.response.data : [],
      pagination: (action.response.pagination) ? action.response.pagination : {},
    }
    return {
      ...state,
      sinister,
      error: null
    }
  },

  [constants.SET_POLICY_CLAUSE_LIST]: (state, action) => {
    let clause = {
      dataList: (action.response) ? action.response : [],
    }

    return {
      ...state,
      clause
    }
  },

  [constants.SET_POLICY_CERTIFICATE_LIST]: (state, action) => {
    let certificado = {
      dataList: (action.response.data) ? action.response.data : [],
      pagination: (action.response.pagination) ? action.response.pagination : {},
      select: null,
    }

    return {
      ...state,
      certificado
    }
  },
  [constants.SET_POLICY_CERTIFICATE_LIST_DETALLE]: (state, action) => {
    let certificado = {
      dataList: (action.response.data) ? action.response.data : [],
      pagination: (action.response.pagination) ? action.response.pagination : {},
      select: null,
    }

    return {
      ...state,
      certificado
    }
  },

  [constants.SET_POLICY_INSURED_LIST]: (state, action) => {
    let asegurado = {
      dataList: (action.response.data) ? action.response.data : [],
      pagination: (action.response.pagination) ? action.response.pagination : {},
      data: null,
      select: null,
    }

    return {
      ...state,
      asegurado
    }
  },
  [constants.SET_POLICY_INSURED_LIST_DETALLE]: (state, action) => {
    let asegurado = {
      dataList: (action.response.data) ? action.response.data : [],
      pagination: (action.response.pagination) ? action.response.pagination : {},
      data: null,
      select: null,
    }

    return {
      ...state,
      asegurado
    }
  },
  [constants.SET_POLICY_LIST_RAMOS]: (state, action) => {
    let listRamos = {
      dataList: action.response
    }

    return {
      ...state,
      listRamos
    }
  },
  
  
  

  [constants.SET_POLICY_COVERAGE_DEDUCTIBLE_LIST]: (state, action) => {
    let deducible = {
      ...state.deducible,
      dataList: (action.response) ? action.response : [],
    }

    return {
      ...state,
      deducible
    }
  },

  [constants.SET_POLICY_RESUME_PV_LIST]: (state, action) => {
    const {
      data: dataList,
      pagination,
      initFetch
    } = action.response;

    const resume = {
      ...state.resume,
      dataList,
      pagination,
      initFetch: initFetch ? false : true
    }

    return {
      ...state,
      resume
    }
  },

  [constants.RESET_DATA_POLICY]: (state, action) => {
    return {
      ...initialState
    }
  },

  [constants.SET_RISK_ADDRESSES]: (state, action) => {
    const riskAddresses = {
      dataList: action.response || [],
    }
    return {
      ...state,
      riskAddresses,
    }
  },

  [constants.SELECT_CERTIFICATE]: (state, action) => {
    const certificado = {
      ...state.certificado,
      select: action.data,
    }
    const numCert = _.isNil(action.data) ? {} : { numCertificado: `${action.data.numCertificado}` }
    const select = _.isNil(state.select) ? null : {
      ...state.select,
      ...numCert
    }
    return {
      ...state,
      certificado,
      select,
    }
  },

  [constants.SELECT_INSURED]: (state, action) => {
    const asegurado = {
      ...state.asegurado,
      select: action.data,
    }
    const codAfiliado = _.isNil(action.data) ? {} : { codAfiliado: `${action.data.codAfiliado}` }
    const select = _.isNil(state.select) ? null : {
      ...state.select,
      ...codAfiliado
    }
    return {
      ...state,
      asegurado,
      select,
    }
  },
  [constants.SET_AUDIT_LOGGING]: (state, action) => {
    const {status} = action.response;
    return { ...state, status }
  }
});

export default policy;
