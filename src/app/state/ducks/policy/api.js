const urls = {
  policyDetail: '/poliza/detalle',
  policyList: '/poliza/listar',
  policyCertificateInsuredList: '/poliza/certificado/asegurados/listar',
  policyCertificateBeneficiariesList: '/poliza/certificado/beneficiarios/listar',
  policyListForRenovate: '/poliza/listarPorRenovar',
  policyShortageList: '/poliza/carencias/listar',
  policyCertificateEndorsementsList: '/poliza/certificado/endosos/listar',
  policyCretificateInsuredLettersWarrantyList: '/poliza/certificado/asegurados/cartasGarantia/listar',
  policySuggest: '/poliza/sugerir',
  policyOperationList: '/poliza/operacion/listar',
  policyRetentionListMassive: '/poliza/retencion/listarMasivo',
  particularDataPolicy: '/poliza/listarDatosParticulares',
  coveragesList: '/poliza/cobertura/listar',
  policyStateAccount: '/poliza/estadoCuenta/buscar',
  policyCoverageTypeList: '/poliza/cobertura/tipo/listar',
  policyClauseList: '/poliza/clausula/listar',
  policyCertificateList: '/poliza/certificado/listar',
  policyCertificateListDetalle: '/poliza/certificado/listarDetalle',
  policyInsuredList: '/poliza/asegurado/listar',
  policyInsuredListDetalle: '/poliza/certificado/listarDetalle',
  policyListRamos: '/poliza/listarRamos',
  policyCoverageDeductibleList: '/poliza/cobertura/deducible/listar',
  riskAddresses: '/poliza/direccionriesgo/listar',
  auditLogging: '/audit/logging/save'
};

export default urls;
