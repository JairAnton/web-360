// @ts-check
import { takeEvery, call, put } from 'redux-saga/effects'
import { constants } from './types'
import * as policyActions from './actions'
import * as sharedActions from '../shared/actions'
import * as policyServices from './services'
import PubSub from '../../utils/PubSub'
import policyLoaderKeys from './loaderKeys'

//importacion del servicio Siniestro
import * as sinisterSercices from '../sinister/services'

function* policyList(action) {
  try {
    console.log("policyList")
    console.log(action)
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.POLICY_LIST, true))
    const resolvedpolicy = yield call(policyServices.policyList, action.body)
    if (resolvedpolicy.statusCode === 200) {
      yield put(policyActions.setPolicyList(resolvedpolicy.dataJson))
    }
    else {
      console.log('ELSE -> ERR_POLICY_LIST')
      console.error('response: ', resolvedpolicy)
      PubSub.publish('ERR_POLICY_LIST', 'Error al traer la Lista de productos, reinténtelo.')
    }
  } catch (error) {
    console.error('error: ', error)
    console.log('ERROR -> ERR_POLICY_LIST')
    PubSub.publish('ERR_POLICY_LIST', error)
  } finally {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.POLICY_LIST, false))
  }
}

function* policyListForRenovate(action) {
  try {
    const resolvedpolicy = yield call(policyServices.policyListForRenovate, action.body)
    if (resolvedpolicy.statusCode === 200) {
      yield put(policyActions.setPolicyListForRenovate(resolvedpolicy.dataJson))
    } else {
      yield put(policyActions.policyFailure({ type: 2, errorMessage: "Advertencia, ocurrrio un problema al llamar al servicio." }))
    }
  } catch (error) {
    console.error('error: ', error)

    yield put(policyActions.policyFailure({ type: 1, errorMessage: "Ocurrio un problema, llame a su backOffice." }))
  }
}

function* policyShortageList(action) {
  try {
    const resolvedpolicy = yield call(policyServices.policyShortageList, action.body)
    if (resolvedpolicy.statusCode === 200) {
      yield put(policyActions.setPolicyShortageList(resolvedpolicy.dataJson))
    } else {
      yield put(policyActions.policyFailure({ type: 2, errorMessage: "Advertencia, ocurrrio un problema al llamar al servicio." }))
    }
  } catch (error) {
    console.error('error: ', error)
    yield put(policyActions.policyFailure({ type: 1, errorMessage: "Ocurrio un problema, llame a su backOffice." }))
  }
}

function* policyCertificateEndorsementsList(action) {
  try {
    const resolvedpolicy = yield call(policyServices.policyCertificateEndorsementsList, action.body)
    if (resolvedpolicy.statusCode === 200) {
      yield put(policyActions.setPolicyCertificateEndorsementsList(resolvedpolicy.dataJson))
    } else {
      yield put(policyActions.policyFailure({ type: 2, errorMessage: "Advertencia, ocurrrio un problema al llamar al servicio." }))
    }
  } catch (error) {
    console.error('error: ', error)
    yield put(policyActions.policyFailure({ type: 1, errorMessage: "Ocurrio un problema, llame a su backOffice." }))
  }
}

function* policySuggest(action) {
  try {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.POLICY_SUGGEST, true))

    const resolvedpolicy = yield call(policyServices.policySuggest, action.body)
    if (resolvedpolicy.statusCode === 200) {
      yield put(policyActions.setPolicySuggest(resolvedpolicy.dataJson))
    } else {
      PubSub.publish('ERR_POLICY_SUGGEST', 'Error al llamar al servicio de Productos Sugeridos, reinténtelo.')
    }
  } catch (error) {
    console.error('error: ', error)
    PubSub.publish('ERR_POLICY_SUGGEST', error)
  } finally {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.POLICY_SUGGEST, false))
  }
}

function* policyOperationList(action) {
  try {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.POLICY_OPERATION_LIST, true))
    const resolvedpolicy = yield call(policyServices.policyOperationList, action.body)
    if (resolvedpolicy.statusCode === 200) {
      yield put(policyActions.setPolicyOperationList(resolvedpolicy.dataJson))
    } else {
      yield put(policyActions.policyFailure({
        type: 2,
        errorMessage: "Advertencia, ocurrrio un problema al llamar al servicio."
      }))
    }
  } catch (error) {
    console.error('error: ', error)
    yield put(policyActions.policyFailure({
      type: 1,
      errorMessage: "Ocurrio un problema, llame a su backOffice."
    }))
  } finally {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.POLICY_OPERATION_LIST, false))
  }
}

function* policyRetentionListMassive(action) {
  try {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.RETENTION, true))

    const resolvedpolicy = yield call(policyServices.policyRetentionListMassive, action.body)
    if (resolvedpolicy.statusCode === 200) {
      yield put(policyActions.setPolicyRetentionListMassive(resolvedpolicy.dataJson))
    } else {
      yield put(policyActions.policyFailure({ type: 2, errorMessage: "Advertencia, ocurrrio un problema al llamar al servicio." }))
    }
  } catch (error) {
    console.error('error: ', error)
    yield put(policyActions.policyFailure({ type: 1, errorMessage: "Ocurrio un problema, llame a su backOffice." }))
    PubSub.publish('ERR_POLICY_RETENTION_LIST_MASSIVE', error)
  } finally {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.RETENTION, false))
  }
}

function* policyDetail(action) {
  try {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.POLICY_DETAIL, true))
    const resolvedpolicy = yield call(policyServices.policyDetail, action.body)
    if (resolvedpolicy.statusCode === 200) {
      let detail = (resolvedpolicy.dataJson.length > 0) ? resolvedpolicy.dataJson[0] : {}
      yield put(policyActions.setPolicyDetail(detail))
    } else {
      PubSub.publish('ERR_POLICY_DETAIL', 'Error al traer el Detalle de producto, reinténtelo.')
    }
  } catch (error) {
    console.error('error: ', error)
    PubSub.publish('ERR_POLICY_DETAIL', error)
  } finally {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.POLICY_DETAIL, false))
  }
}

function* policyCertificateBeneficiariesList(action) {
  try {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.BENEFICIARIES, true))
    const resolvedpolicy = yield call(policyServices.policyCertificateBeneficiariesList, action.body)
    if (resolvedpolicy.statusCode === 200) {
      yield put(policyActions.setPolicyCertificateBeneficiariesList(resolvedpolicy.dataJson))
    } else {
      yield put(policyActions.policyFailure({
        type: 2,
        errorMessage: "Advertencia, ocurrrio un problema al llamar al servicio."
      }))
    }
  } catch (error) {
    console.error('error: ', error)
    yield put(policyActions.policyFailure({ type: 1, errorMessage: "Ocurrio un problema, llame a su backOffice." }))
    PubSub.publish('ERR_POLICY_CERTIFICATE_BENEFICIARIES_LIST', error)
  } finally {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.BENEFICIARIES, false))
  }
}

function* policyCertificateInsuredList(action) {
  try {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.INSURED_LIST, true))
    const resolvedpolicy = yield call(policyServices.policyCertificateInsuredList, action.body)
    if (resolvedpolicy.statusCode === 200) {
      yield put(policyActions.setPolicyCertificateInsuredList(resolvedpolicy.dataJson))
    } else {
      yield put(policyActions.policyFailure({ type: 2, errorMessage: "Advertencia, ocurrrio un problema al llamar al servicio." }))
    }
  } catch (error) {
    console.error('error: ', error)
    yield put(policyActions.policyFailure({ type: 1, errorMessage: "Ocurrio un problema, llame a su backOffice." }))
    PubSub.publish('ERR_POLICY_CERTIFICATE_INSURED_LIST', error)
  } finally {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.INSURED_LIST, false))
  }
}

function* policyParticularData(action) {
  try {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.PARTICULAR_DATA, true))

    const resolvedParticularData = yield call(policyServices.particularDataList, action.body)

    yield put(policyActions.setParticularDataList(resolvedParticularData.dataJson))
  }
  catch (error) {
    console.error('error: ', error)
    yield put(policyActions.policyFailure({ type: 1, errorMessage: "Ocurrio un problema, llame a su backOffice." }))
    PubSub.publish('ERR_PARTICULAR_DATA_LIST', error)
  }
  finally {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.PARTICULAR_DATA, false))
  }
}

function* coveragesList(action) {
  try {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.COVERAGE, true))

    const resolvedCoveragesList = yield call(policyServices.coveragesList, action.body)

    if (resolvedCoveragesList.statusCode === 200) {
      yield put(policyActions.setCoveragesList(resolvedCoveragesList.dataJson))
    } else {
      yield put(policyActions.policyFailure({ type: 2, errorMessage: "Advertencia, ocurrrio un problema al llamar al servicio." }))
    }
  }
  catch (error) {
    console.error('error: ', error)
    yield put(policyActions.policyFailure({ type: 1, errorMessage: "Ocurrio un problema, llame a su backOffice." }))
    PubSub.publish('ERR_COVERAGES_LIST', error)
  } finally {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.COVERAGE, false))
  }
}

function* policyStateAccount(action) {
  try {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.ACCOUNT_STATUS, true))

    const resolvedpolicy = yield call(policyServices.policyStateAccount, action.body)
    console.log('STATE_ACCOUNT SAGA RESULT', resolvedpolicy)
    if (resolvedpolicy.statusCode === 200) {
      yield put(policyActions.setPolicyStateAccount(resolvedpolicy.dataJson))
    } else {
      yield put(policyActions.policyFailure({
        errorMessage: "Ocurrió un problema al llamar al servicio de Estado de Cuenta.",
        type: 2,
      }))
    }
  } catch (error) {
    console.error('error: ', error)
    yield put(policyActions.policyFailure({ type: 1, errorMessage: "Ocurrio un problema, llame a su backOffice." }))
    // PubSub.publish('ERR_POLICY_STATE_ACCOUNT', 'Error al traer la lista de Estados de cuenta, reinténtelo.')
    PubSub.publish('ERR_POLICY_STATE_ACCOUNT', error)
  }
  finally {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.ACCOUNT_STATUS, false))
  }
}


function* policySelect(action) {
  try {
    yield put(policyActions.setPolicySelect(action.data))
  } catch (error) {
    console.error('error: ', error)
    yield put(policyActions.policyFailure({ type: 1, errorMessage: "Ocurrio un problema, llame a su backOffice." }))
  }
}

/*
Este Saga llama al servicio Listar siniestro, el cual es el mismo que se llama
al nivel de Tercero.
*/
function* policySinisterList(action) {
  try {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.POLICY_SINISTER_LIST, true))

    const resolvedpolicy = yield call(sinisterSercices.sinisterList, action.body)
    if (resolvedpolicy.statusCode === 200) {
      yield put(policyActions.setPolicySinisterList(resolvedpolicy.dataJson))
    } else {
      yield put(policyActions.policyFailure({ type: 2, errorMessage: "Advertencia, ocurrrio un problema al llamar al servicio." }))
    }
  } catch (error) {
    console.error('error: ', error)
    yield put(policyActions.policyFailure({ type: 1, errorMessage: "Ocurrio un problema, llame a su backOffice." }))
    PubSub.publish('ERR_POLICY_SINISTER_LIST', error)
  } finally {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.POLICY_SINISTER_LIST, false))
  }
}

function* policyCertificateList(action) {
  try {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.POLICY_CERTIFICATE_LIST, true))

    const resolvedpolicy = yield call(policyServices.policyCertificateList, action.body)
    if (resolvedpolicy.statusCode === 200) {
      yield put(policyActions.setPolicyCertificateList(resolvedpolicy.dataJson))
    } else {
      PubSub.publish('ERR_POLICY_CERTIFICATE_LIST', 'Error al traer la lista de Certificados, reinténtelo.')
    }
  } catch (error) {
    console.error('error: ', error)
    PubSub.publish('ERR_POLICY_CERTIFICATE_LIST', error)
  } finally {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.POLICY_CERTIFICATE_LIST, false))
  }
}
function* policyCertificateListDetalle(action) {
  try {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.POLICY_CERTIFICATE_LIST_DETALLE, true))

    const resolvedpolicy = yield call(policyServices.policyCertificateListDetalle, action.body)
    if (resolvedpolicy.statusCode === 200) {
      yield put(policyActions.setPolicyCertificateListDetalle(resolvedpolicy.dataJson))
    } else {
      PubSub.publish('ERR_POLICY_CERTIFICATE_LIST_DETALLE', 'Error al traer la lista de Certificados, reinténtelo.')
    }
  } catch (error) {
    console.error('error: ', error)
    PubSub.publish('ERR_POLICY_CERTIFICATE_LIST_DETALLE', error)
  } finally {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.POLICY_CERTIFICATE_LIST_DETALLE, false))
  }
}


function* policyCoverageTypeList(action) {
  try {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.POLICY_COVERAGE_TYPE_LIST, true))

    const resolvedpolicy = yield call(policyServices.policyCoverageTypeList, action.body)
    if (resolvedpolicy.statusCode === 200) {
      yield put(policyActions.setPolicyCoverageTypeList(resolvedpolicy.dataJson))
    } else {
      PubSub.publish('ERR_POLICY_COVERAGE_TYPE_LIST', 'Error al traer los tipos de Cobertura, reinténtelo.')
    }
  } catch (error) {
    console.error('error: ', error)
    PubSub.publish('ERR_POLICY_COVERAGE_TYPE_LIST', error)
  } finally {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.POLICY_COVERAGE_TYPE_LIST, false))
  }
}

function* policyClauseList(action) {
  try {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.POLICY_CLAUSE, true))

    const resolvedpolicy = yield call(policyServices.policyClauseList, action.body)
    if (resolvedpolicy.statusCode === 200) {
      yield put(policyActions.setPolicyClauseList(resolvedpolicy.dataJson))
    } else {
      PubSub.publish('ERR_POLICY_CLAUSE', 'Error al traer las Clausulas, reinténtelo.')
    }
  } catch (error) {
    console.error('error: ', error)
    PubSub.publish('ERR_POLICY_CLAUSE', error)
  } finally {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.POLICY_CLAUSE, false))
  }
}

function* policyInsuredList(action) {
  try {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.POLICY_INSURED_LIST, true))

    const resolvedpolicy = yield call(policyServices.policyInsuredList, action.body)
    if (resolvedpolicy.statusCode === 200) {
      yield put(policyActions.setPolicyInsuredList(resolvedpolicy.dataJson))
    } else {
      PubSub.publish('ERR_POLICY_INSURED_LIST', 'Error al traer la lista de Asegurados, reinténtelo.')
    }
  } catch (error) {
    console.error('error: ', error)
    PubSub.publish('ERR_POLICY_INSURED_LIST', error)
  } finally {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.POLICY_INSURED_LIST, false))
  }
}

function* policyInsuredListDetalle(action) {
  try {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.POLICY_INSURED_LIST_DETALLE, true))

    const resolvedpolicy = yield call(policyServices.policyInsuredListDetalle, action.body)
    if (resolvedpolicy.statusCode === 200) {
      yield put(policyActions.setPolicyInsuredListDetalle(resolvedpolicy.dataJson))
    } else {
      PubSub.publish('ERR_POLICY_INSURED_LIST_DETALLE', 'Error al traer la lista de Asegurados, reinténtelo.')
    }
  } catch (error) {
    console.error('error: ', error)
    PubSub.publish('ERR_POLICY_INSURED_LIST_DETALLE', error)
  } finally {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.POLICY_INSURED_LIST_DETALLE, false))
  }
}
function* policyListRamos(action) {
  try {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.POLICY_LIST_RAMOS, true))

    const resolvedpolicy = yield call(policyServices.policyListRamos, action.body)
    if (resolvedpolicy.statusCode === 200) {
      yield put(policyActions.setpolicyListRamos(resolvedpolicy.dataJson))
    } else {
      PubSub.publish('ERR_POLICY_LIST_RAMOS', 'Error al traer la lista de Asegurados, reinténtelo.')
    }
  } catch (error) {
    console.error('error: ', error)
    PubSub.publish('ERR_POLICY_LIST_RAMOS', error)
  } finally {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.POLICY_LIST_RAMOS, false))
  }
}

function* policyCoverageDeductibleList(action) {
  try {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.POLICY_COVERAGE_DEDUCTIBLE_LIST, true))

    const resolvedpolicy = yield call(policyServices.policyCoverageDeductibleList, action.body)
    if (resolvedpolicy.statusCode === 200) {
      yield put(policyActions.setPolicyCoverageDeductibleList(resolvedpolicy.dataJson))
    } else {
      PubSub.publish('ERR_POLICY_COVERAGE_DEDUCTIBLE_LIST', 'Error al traer la lista de Deducibles, reinténtelo.')
    }
  } catch (error) {
    console.error('error: ', error)
    PubSub.publish('ERR_POLICY_COVERAGE_DEDUCTIBLE_LIST', error)
  } finally {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.POLICY_COVERAGE_DEDUCTIBLE_LIST, false))
  }
}

function* policyResumePvList(action) {
  try {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.POLICY_RESUME_PV_LIST, true))

    const resolvedpolicy = yield call(policyServices.policyList, action.body)
    if (resolvedpolicy.statusCode === 200) {
      yield put(policyActions.setPolicyResumePvList(resolvedpolicy.dataJson))
    } else {
      PubSub.publish('ERR_POLICY_RESUME_PV_LIST', 'Error al traer la lista de productos vigentes, reinténtelo.')
    }
  } catch (error) {
    console.error('error: ', error)
    PubSub.publish('ERR_POLICY_RESUME_PV_LIST', error)
  } finally {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.POLICY_RESUME_PV_LIST, false))
  }
}

function* riskAddresses(action) {
  try {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.RISK_ADDRESSES, true))
    const resolvedpolicy = yield call(policyServices.riskAddresses, action.body)
    if (resolvedpolicy.statusCode === 200) {
      yield put(policyActions.setRiskAddresses(resolvedpolicy.dataJson))
    } else {
      PubSub.publish('ERR_RISK_ADDRESSES', 'Error al traer la lista de direcciones de riesgo, reinténtelo.')
    }
  } catch (error) {
    console.error('error: ', error)
    PubSub.publish('ERR_RISK_ADDRESSES', error)
  } finally {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.RISK_ADDRESSES, false))
  }

}

function* auditLogging(action) {
  try {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.AUDIT_LOGGING, true))
    const resolvedAudit = yield call(policyServices.auditLogging, action.body);
    
    if (resolvedAudit.statusCode === 200) {
      yield put(policyActions.setAuditLogging(resolvedAudit.dataJson));
    } else {
      PubSub.publish('ERR_AUDIT_LOGGING')
    }
  } catch (error) {
    PubSub.publish('ERR_AUDIT_LOGGING', error)
  } finally {
    yield put(sharedActions.setLoaderVisible(policyLoaderKeys.AUDIT_LOGGING, false))
  }
}


export default function* sagas() {
  yield takeEvery(constants.POLICY_SHORTAGE_LIST, policyShortageList)
  yield takeEvery(constants.POLICY_DETAIL, policyDetail)
  yield takeEvery(constants.POLICY_LIST, policyList)
  yield takeEvery(constants.POLICY_LIST_FOR_RENOVATE, policyListForRenovate)
  yield takeEvery(constants.POLICY_CERTIFICATE_ENDORSEMENTS_LIST, policyCertificateEndorsementsList)
  yield takeEvery(constants.POLICY_SUGGEST, policySuggest)
  yield takeEvery(constants.POLICY_OPERATION_LIST, policyOperationList)
  yield takeEvery(constants.POLICY_RETENTION_LIST_MASSIVE, policyRetentionListMassive)
  yield takeEvery(constants.POLICY_CERTIFICATE_BENEFICIARIES_LIST, policyCertificateBeneficiariesList)
  yield takeEvery(constants.POLICY_CERTIFICATE_INSURED_LIST, policyCertificateInsuredList)
  yield takeEvery(constants.PARTICULAR_DATA_LIST, policyParticularData)
  yield takeEvery(constants.COVERAGES_LIST, coveragesList)
  yield takeEvery(constants.POLICY_STATE_ACCOUNT, policyStateAccount)
  yield takeEvery(constants.POLICY_SELECT, policySelect)
  yield takeEvery(constants.POLICY_SINISTER_LIST, policySinisterList)
  yield takeEvery(constants.POLICY_COVERAGE_TYPE_LIST, policyCoverageTypeList)
  yield takeEvery(constants.POLICY_CLAUSE_LIST, policyClauseList)
  yield takeEvery(constants.POLICY_CERTIFICATE_LIST, policyCertificateList)
  yield takeEvery(constants.POLICY_CERTIFICATE_LIST_DETALLE, policyCertificateListDetalle)
  yield takeEvery(constants.POLICY_INSURED_LIST, policyInsuredList)
  yield takeEvery(constants.POLICY_INSURED_LIST_DETALLE, policyInsuredListDetalle)
  yield takeEvery(constants.POLICY_LIST_RAMOS, policyListRamos)
  yield takeEvery(constants.POLICY_COVERAGE_DEDUCTIBLE_LIST, policyCoverageDeductibleList)
  yield takeEvery(constants.POLICY_RESUME_PV_LIST, policyResumePvList)
  yield takeEvery(constants.RISK_ADDRESSES, riskAddresses)
  yield takeEvery(constants.AUDIT_LOGGING, auditLogging);
}