import { constants } from "./types";

export const policyFailure = (error) => ({
  type: constants.POLICY_FAILURE,
  error
});

export const policyDetail = (body) => {
  return {
    type: constants.POLICY_DETAIL,
    body
  }
}

export const setPolicyDetail = (response) => {
  return {
    type: constants.SET_POLICY_DETAIL,
    response
  }
}

export const policyList = (body) => {
  return {
    type: constants.POLICY_LIST,
    body
  }
}

export const setPolicyList = (response) => {
  return {
    type: constants.SET_POLICY_LIST,
    response
  }
}

//#region CertificateInsured
export const policyCertificateInsuredList = (body) => {
  return {
    type: constants.POLICY_CERTIFICATE_INSURED_LIST,
    body
  }
}

export const setPolicyCertificateInsuredList = (response) => {
  return {
    type: constants.SET_POLICY_CERTIFICATE_INSURED_LIST,
    response
  }
}
//#endregion

//#region CertificateBeneficiaries
export const policyCertificateBeneficiariesList = (body) => {
  return {
    type: constants.POLICY_CERTIFICATE_BENEFICIARIES_LIST,
    body
  }
}

export const setPolicyCertificateBeneficiariesList = (response) => {
  return {
    type: constants.SET_POLICY_CERTIFICATE_BENEFICIARIES_LIST,
    response
  }
}
//#endregion

export const policyListForRenovate = (body) => {
  return {
    type: constants.POLICY_LIST_FOR_RENOVATE,
    body
  }
}

export const setPolicyListForRenovate = (response) => {
  return {
    type: constants.SET_POLICY_LIST_FOR_RENOVATE,
    response
  }
}

export const policyShortageList = (body) => {
  return {
    type: constants.POLICY_SHORTAGE_LIST,
    body
  }
}

export const setPolicyShortageList = (response) => {
  return {
    type: constants.SET_POLICY_SHORTAGE_LIST,
    response
  }
}

export const policyCertificateEndorsementsList = (body) => {
  return {
    type: constants.POLICY_CERTIFICATE_ENDORSEMENTS_LIST,
    body
  }
}

export const setPolicyCertificateEndorsementsList = (response) => {
  return {
    type: constants.SET_POLICY_CERTIFICATE_ENDORSEMENTS_LIST,
    response
  }
}

export const policyCretificateInsuredLettersWarrantyList = (body) => {
  return {
    type: constants.POLICY_CERTIFICATE_INSURED_LETTERS_WARRANTY_LIST,
    body
  }
}

export const setPolicyCretificateInsuredLettersWarrantyList = (response) => {
  return {
    type: constants.SET_POLICY_CERTIFICATE_INSURED_LETTERS_WARRANTY_LIST,
    response
  }
}

export const policySuggest = (body) => {
  return {
    type: constants.POLICY_SUGGEST,
    body
  }
}

export const setPolicySuggest = (response) => {
  return {
    type: constants.SET_POLICY_SUGGEST,
    response
  }
}

export const policyOperationList = (body) => {
  return {
    type: constants.POLICY_OPERATION_LIST,
    body
  }
}

export const setPolicyOperationList = (response) => {
  return {
    type: constants.SET_POLICY_OPERATION_LIST,
    response
  }
}

export const policyRetentionListMassive = (body) => {
  return {
    type: constants.POLICY_RETENTION_LIST_MASSIVE,
    body
  }
}

export const setPolicyRetentionListMassive = (response) => {
  return {
    type: constants.SET_POLICY_RETENTION_LIST_MASSIVE,
    response
  }
}

//#region ParticularData

export const ParticularDataList = (body) => ({
  type: constants.PARTICULAR_DATA_LIST,
  body
});

export const setParticularDataList = (body) => ({
  type: constants.SET_PARTICULAR_DATA_LIST,
  body
});

//#endregion

//#region Coverages
export const coveragesList = (body) => ({
  type: constants.COVERAGES_LIST,
  body
})

export const setCoveragesList = (response) => ({
  type: constants.SET_COVERAGES_LIST,
  response
})
//#endregion

//#region StateAccount
export const policyStateAccount = (body) => ({
  type: constants.POLICY_STATE_ACCOUNT,
  body
})

export const setPolicyStateAccount = (response) => ({
  type: constants.SET_POLICY_STATE_ACCOUNT,
  response
})

export const policySelect = (data) => ({
  type: constants.POLICY_SELECT,
  data
})

export const setPolicySelect = (data) => ({
  type: constants.SET_POLICY_SELECT,
  data
})

export const policySinisterList = (body) => ({
  type: constants.POLICY_SINISTER_LIST,
  body
})

export const setPolicySinisterList = (response) => ({
  type: constants.SET_POLICY_SINISTER_LIST,
  response
})

export const policyCoverageTypeList = (body) => ({
  type: constants.POLICY_COVERAGE_TYPE_LIST,
  body
})

export const setPolicyCoverageTypeList = (response) => ({
  type: constants.SET_POLICY_COVERAGE_TYPE_LIST,
  response
})

export const policyClauseList = (body) => ({
  type: constants.POLICY_CLAUSE_LIST,
  body
})

export const setPolicyClauseList = (response) => ({
  type: constants.SET_POLICY_CLAUSE_LIST,
  response
})

export const policyCertificateList = (body) => ({
  type: constants.POLICY_CERTIFICATE_LIST,
  body
})

export const setPolicyCertificateList = (response) => ({
  type: constants.SET_POLICY_CERTIFICATE_LIST,
  response
})

export const policyCertificateListDetalle = (body) => ({
  type: constants.POLICY_CERTIFICATE_LIST_DETALLE,
  body
})

export const setPolicyCertificateListDetalle = (response) => ({
  type: constants.SET_POLICY_CERTIFICATE_LIST_DETALLE,
  response
})


export const policyInsuredList = (body) => ({
  type: constants.POLICY_INSURED_LIST,
  body
})

export const setPolicyInsuredList = (response) => ({
  type: constants.SET_POLICY_INSURED_LIST,
  response
})
export const policyInsuredListDetalle = (body) => ({
  type: constants.POLICY_INSURED_LIST_DETALLE,
  body
})

export const setPolicyInsuredListDetalle = (response) => ({
  type: constants.SET_POLICY_INSURED_LIST_DETALLE,
  response
})

export const policyListRamos = (body) => ({
  type: constants.POLICY_LIST_RAMOS,
  body
})

export const setpolicyListRamos = (response) => ({
  type: constants.SET_POLICY_LIST_RAMOS,
  response
})






export const policyCoverageDeductibleList = (body) => ({
  type: constants.POLICY_COVERAGE_DEDUCTIBLE_LIST,
  body
})

export const setPolicyCoverageDeductibleList = (response) => ({
  type: constants.SET_POLICY_COVERAGE_DEDUCTIBLE_LIST,
  response
})

export const policyResumePvList = (body) => ({
  type: constants.POLICY_RESUME_PV_LIST,
  body
})

export const setPolicyResumePvList = (response) => ({
  type: constants.SET_POLICY_RESUME_PV_LIST,
  response
})

export const resetDataPolicy = () => ({
  type: constants.RESET_DATA_POLICY,
})

export const riskAddresses = (body) => {
  return {
    type: constants.RISK_ADDRESSES,
    body
  }
}

export const setRiskAddresses = (response) => ({
  type: constants.SET_RISK_ADDRESSES,
  response
})

export const selectCertificate = (data) => ({
  type: constants.SELECT_CERTIFICATE,
  data,
})

export const selectInsured = data => ({
  type: constants.SELECT_INSURED,
  data,
});

export const auditLogging = (body) => ({
  type: constants.AUDIT_LOGGING,
  body
});

export const setAuditLogging = (response) => ({
  type: constants.SET_AUDIT_LOGGING,
  response
});


