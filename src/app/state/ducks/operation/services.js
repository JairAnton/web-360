import api from '../../../state/utils/api';

export const operationProcedureList = async (body) => {
  const responseOperation = await api.operationProcedureList({ data: body });
  return responseOperation;
};

export const operationRefundList = async (body) => {
  const responseOperation = await api.operationRefundList({ data: body });
  return responseOperation;
};

export const auditLogging = async (body) => {
  const responsePerson = await api.auditLogging({ data: body });
  return responsePerson;
}