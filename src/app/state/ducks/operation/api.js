const urls = {
    operationProcedureList: '/operacion/tramite/listar',
    operationRefundList:'/operacion/reembolso/listar',
    auditLogging: '/audit/logging/save',
}

export default urls;    