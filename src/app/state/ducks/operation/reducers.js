import { createReducer } from '../../utils';
import { constants } from "./types";
import _ from 'lodash';

const initialState = {
  procedure: {
    data: null,
    dataList: [],
    pagination: null,
    select: null,
  },
  error: null,
  refund: {
    data: {},
    dataList: []
  },
  resume: {
    initFetch: false,
    data: null,
    dataList: [],
  },
  status: null
};

const operation = createReducer(initialState)({
  [constants.OPERATION_FAILURE]: (state, action) => {
    var error = action.error
    let procedure = state.procedure;

    return { ...state, error, procedure }
  },
  [constants.SET_OPERATION_PROCEDURE_LIST]: (state, action) => {

    const {
      data: dataList,
      pagination
    } = action.response

    const _procedure = {
      ...state.procedure,
      dataList,
      pagination
    }
    const procedure = _.clone(_procedure);
    return { ...state, procedure }
  },

  [constants.PROCEDURE_INIT_LOADER]: (state, action) => {
    let procedure = state.procedure;

    return { ...state, procedure }
  },
  [constants.OPERATION_FAILURE]: (state, action) => {
    let procedure = state.procedure;

    return { ...state, procedure }
  },
  [constants.SET_OPERATION_REFUND_LIST]: (state, action) => {
    const dataList = action.response

    const refund = {
      ...state.refund,
      dataList
    }
    return { ...state, refund, error: null }
  },
  [constants.SET_REFUND_RESUME_LIST]: (state, action) => {
    const {
      dataList,
      initFetch
    } = action.response

    const resume = {
      ...state.resume,
      dataList,
      initFetch: initFetch ? false : true
    }
    return { ...state, resume }
  },
  [constants.RESET_DATA_OPERATION]: (state, action) => {
    return {
      ...initialState
    }
  },
  [constants.SET_AUDIT_LOGGING]: (state, action) => {
    const {status} = action.response;
    return { ...state, status }
  }
});

export default operation;