// @ts-check
import { takeEvery, call, put } from 'redux-saga/effects'
import { constants } from './types'
import * as operationActions from './actions'
import * as operationServices from './services'
import * as sharedActions from '../shared/actions'
import { operationLoaderKeys } from '../operation'
import PubSub from '../../utils/PubSub'

function* operationProcedureList(action) {
  try {
    yield put(sharedActions.setLoaderVisible(operationLoaderKeys.PROCEDURE_LIST, true))
    const resolvedOperation = yield call(operationServices.operationProcedureList, action.body)

    if (resolvedOperation.statusCode === 200)
      yield put(operationActions.setOperationProcedureList(resolvedOperation.dataJson))
    else {
      yield put(operationActions.setOperationProcedureList({ data: [], pagination: {} }))
      PubSub.publish('ERR_OPERATION_PROCEDURE_LIST', 'Error al traer la Lista de Tramites, reintentelo.')
    }
  } catch (e) {
    PubSub.publish('ERR_OPERATION_PROCEDURE_LIST', e)
  } finally {
    yield put(sharedActions.setLoaderVisible(operationLoaderKeys.PROCEDURE_LIST, false))
  }
}

function* operationRefundList(action) {
  try {
    yield put(sharedActions.setLoaderVisible(operationLoaderKeys.REFUND_LIST, true))
    const resolvedOperation = yield call(operationServices.operationRefundList, action.body)

    if (resolvedOperation.statusCode === 200)
      yield put(operationActions.setOperationRefundList(resolvedOperation.dataJson))
    else
      PubSub.publish('ERR_OPERATION_REFUND_LIST', 'Error al traer la Lista de Reembolsos, reintentelo.')
  } catch (e) {
    PubSub.publish('ERR_OPERATION_REFUND_LIST', e)
  } finally {
    yield put(sharedActions.setLoaderVisible(operationLoaderKeys.REFUND_LIST, false))
  }
}

function* refundResumeList(action) {
  try {
    yield put(sharedActions.setLoaderVisible(operationLoaderKeys.REFUND_RESUME_LIST, true))
    const resolvedOperation = yield call(operationServices.operationRefundList, action.body)

    if (resolvedOperation.statusCode === 200)
      yield put(operationActions.setRefundResumeList({ dataList: resolvedOperation.dataJson ? resolvedOperation.dataJson : [] }))
    else
      PubSub.publish('ERR_REFUND_RESUME_LIST', 'Error al traer la Lista de Reembolsos, reintentelo.')
  } catch (e) {
    PubSub.publish('ERR_REFUND_RESUME_LIST', e)
  } finally {
    yield put(sharedActions.setLoaderVisible(operationLoaderKeys.REFUND_RESUME_LIST, false))
  }
}

function* auditLogging(action) {
  try {
    yield put(sharedActions.setLoaderVisible(operationLoaderKeys.AUDIT_LOGGING, true))
    const resolvedAudit = yield call(operationServices.auditLogging, action.body);
    
    if (resolvedAudit.statusCode === 200) {
      yield put(operationActions.setAuditLogging(resolvedAudit.dataJson));
    } else {
      PubSub.publish('ERR_AUDIT_LOGGING')
    }
  } catch (error) {
    PubSub.publish('ERR_AUDIT_LOGGING', error)
  } finally {
    yield put(sharedActions.setLoaderVisible(operationLoaderKeys.AUDIT_LOGGING, false))
  }
}

export default function* sagas() {
  yield takeEvery(constants.OPERATION_PROCEDURE_LIST, operationProcedureList)
  yield takeEvery(constants.OPERATION_REFUND_LIST, operationRefundList)
  yield takeEvery(constants.REFUND_RESUME_LIST, refundResumeList)
  yield takeEvery(constants.AUDIT_LOGGING, auditLogging)
}