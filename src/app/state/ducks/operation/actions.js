import { constants } from "./types";

export const operationFailure = (error) => ({
    type: constants.OPERATION_FAILURE,
    error
});
//#region Procedure
export const procedureInitLoader = () => ({
    type: constants.PROCEDURE_INIT_LOADER
})

export const procedureEndLoader = () => ({
    type: constants.PROCEDURE_END_LOADER
})

export const operationProcedureList = (body) => ({
    type: constants.OPERATION_PROCEDURE_LIST,
    body
});
//#endregion
export const setOperationProcedureList = (response) => ({
    type: constants.SET_OPERATION_PROCEDURE_LIST,
    response
});

export const operationRefundList = (body) => ({
  type: constants.OPERATION_REFUND_LIST,
  body
});

export const setOperationRefundList = (response) => ({
  type: constants.SET_OPERATION_REFUND_LIST,
  response
});

export const refundResumeList = (body) => ({
  type: constants.REFUND_RESUME_LIST,
  body
});

export const setRefundResumeList = (response) => ({
  type: constants.SET_REFUND_RESUME_LIST,
  response
});

export const resetDataOperation = () => ({
  type: constants.RESET_DATA_OPERATION,
});

export const auditLogging = (body) => ({
  type: constants.AUDIT_LOGGING,
  body
});

export const setAuditLogging = (response) => ({
  type: constants.SET_AUDIT_LOGGING,
  response
});
