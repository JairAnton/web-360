import KeyMirror from 'keymirror';

export const constants = KeyMirror({

  //#region Procedure
  PROCEDURE_INIT_LOADER: null,
  PROCEDURE_END_LOADER: null,

  OPERATION_PROCEDURE_LIST: null,
  SET_OPERATION_PROCEDURE_LIST: null,
  //#endregion

  OPERATION_FAILURE: null,

  OPERATION_REFUND_LIST: null,
  SET_OPERATION_REFUND_LIST: null,
  
  REFUND_RESUME_LIST: null,
  SET_REFUND_RESUME_LIST: null,

  RESET_DATA_OPERATION: null,

  AUDIT_LOGGING: null,
  SET_AUDIT_LOGGING: null,
});