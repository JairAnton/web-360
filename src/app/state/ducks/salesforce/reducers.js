import { createReducer } from '../../utils'
import { constants } from './types'

const initialState = {
  retention: {
    dataList: [],
  },
  cases: {
    dataList: [],
    pagination: null,
  },
  resume: {
    cases: {
      initFetch: false,
      data: null,
      dataList: []
    },
    claims: {
      initFetch: false,
      data: null,
      dataList: []
    }
  },
  status: null
}

const salesforce = createReducer(initialState)({
  [constants.SET_RETENTION_LIST]: (state, action) => {
    const retention = {
      ...state.retention,
      dataList: action.response,
    }
    return { ...state, retention }
  },
  [constants.SET_CASES_LIST]: (state, action) => {
    const cases = {
      ...state.cases,
      dataList: action.response.data,
      pagination: action.response.pagination,
    }
    return { ...state, cases }
  },
  [constants.SET_RESUME_CASES_LIST]: (state, action) => {
    const {
      data: dataList,
      pagination,
      initFetch
    } = action.response

    const cases = {
      dataList,
      pagination,
      initFetch: initFetch ? false : true
    }

    const resume = {
      ...state.resume,
      cases
    }
    return { ...state, resume }
  },
  [constants.SET_RESUME_CLAIMS]: (state, action) => {
    const {
      data: dataList,
      pagination,
      initFetch
    } = action.response

    const claims = {
      dataList,
      pagination,
      initFetch: initFetch ? false : true
    }

    const resume = {
      ...state.resume,
      claims
    }
    return { ...state, resume }
  },
  [constants.RESET_DATA_SALESFORCE]: (state, action) => {
    return {
      ...initialState
    }
  },
  [constants.SET_AUDIT_LOGGING]: (state, action) => {
    const {status} = action.response;
    return { ...state, status }
  }
});

export default salesforce
