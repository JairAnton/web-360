// @ts-check
import { takeEvery, call, put } from 'redux-saga/effects'
import { constants } from './types'
import * as salesforceActions from './actions'
import * as sharedActions from '../shared/actions'
import * as salesforceServices from './services'
import PubSub from '../../utils/PubSub'
import salesforceLoaderKeys from './loaderKeys'

function* retentionList(action) {
  try {
    yield put(sharedActions.setLoaderVisible(salesforceLoaderKeys.RETENTION, true))

    const resolvedRetention = yield call(salesforceServices.retentionList, action.body);
    if (resolvedRetention.statusCode === 200) {
      yield put(salesforceActions.setRetentionList(resolvedRetention.dataJson));
    } else {
      PubSub.publish('ERR_POLICY_RETENTION_CASES_LIST', 'Error al traer la lista de Retenciones, reintentelo.');
    }
  } catch (e) {
    PubSub.publish('ERR_POLICY_RETENTION_CASES_LIST', e);
  } finally {
    yield put(sharedActions.setLoaderVisible(salesforceLoaderKeys.RETENTION, false))
  }
}

function* casesList(action) {
  try {
    yield put(sharedActions.setLoaderVisible(salesforceLoaderKeys.CASES, true))

    const resolvedCases = yield call(salesforceServices.casesList, action.body);
    if (resolvedCases.statusCode === 200) {
      yield put(salesforceActions.setCasesList(resolvedCases.dataJson));
    } else {
      PubSub.publish('ERR_POLICY_CASES_LIST', 'Error al traer la lista de casos, reintentelo.')
    }
  } catch (e) {
    PubSub.publish('ERR_POLICY_CASES_LIST', e)
  } finally {
    yield put(sharedActions.setLoaderVisible(salesforceLoaderKeys.CASES, false))
  }
}

function* resumeCasesList(action) {
  try {
    yield put(sharedActions.setLoaderVisible(salesforceLoaderKeys.RESUME_CASES, true))

    const resolvedCases = yield call(salesforceServices.casesList, action.body);
    if (resolvedCases.statusCode === 200) {
      yield put(salesforceActions.setResumeCasesList(resolvedCases.dataJson));
    } else {
      PubSub.publish('ERR_RESUME_CASES', 'Error al traer la lista de casos, reintentelo.')
    }
  } catch (e) {
    PubSub.publish('ERR_RESUME_CASES', e)
  } finally {
    yield put(sharedActions.setLoaderVisible(salesforceLoaderKeys.RESUME_CASES, false))
  }
}

function* resumeClaims(action) {
  try {
    yield put(sharedActions.setLoaderVisible(salesforceLoaderKeys.RESUME_CLAIMS, true))

    const resolvedCases = yield call(salesforceServices.casesList, action.body);
    if (resolvedCases.statusCode === 200) {
      yield put(salesforceActions.setResumeClaims(resolvedCases.dataJson));
    } else {
      PubSub.publish('ERR_RESUME_CLAIMS', 'Error al traer la lista de casos, reintentelo.')
    }
  } catch (e) {
    PubSub.publish('ERR_RESUME_CLAIMS', e)
  } finally {
    yield put(sharedActions.setLoaderVisible(salesforceLoaderKeys.RESUME_CLAIMS, false))
  }
}
function* auditLogging(action) {
  try {
    yield put(sharedActions.setLoaderVisible(salesforceLoaderKeys.AUDIT_LOGGING, true))
    const resolvedAudit = yield call(salesforceServices.auditLogging, action.body);
    
    if (resolvedAudit.statusCode === 200) {
      yield put(salesforceActions.setAuditLogging(resolvedAudit.dataJson));
    } else {
      PubSub.publish('ERR_AUDIT_LOGGING')
    }
  } catch (error) {
    PubSub.publish('ERR_AUDIT_LOGGING', error)
  } finally {
    yield put(sharedActions.setLoaderVisible(salesforceLoaderKeys.AUDIT_LOGGING, false))
  }
}

export default function* sagas() {
  yield takeEvery(constants.RETENTION_LIST, retentionList)
  yield takeEvery(constants.CASES_LIST, casesList)
  yield takeEvery(constants.RESUME_CASES_LIST, resumeCasesList)
  yield takeEvery(constants.RESUME_CLAIMS, resumeClaims)
  yield takeEvery(constants.AUDIT_LOGGING, auditLogging);
}