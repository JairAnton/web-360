import KeyMirror from 'keymirror';

export const constants = KeyMirror({
  RETENTION_LIST: null,
  SET_RETENTION_LIST: null,
  CASES_LIST: null,
  SET_CASES_LIST: null,

  RESUME_CASES_LIST: null,
  SET_RESUME_CASES_LIST: null,

  RESUME_CLAIMS: null,
  SET_RESUME_CLAIMS: null,

  RESET_DATA_SALESFORCE: null,
  
  AUDIT_LOGGING: null,
  SET_AUDIT_LOGGING: null,
})