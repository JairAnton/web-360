import { constants } from './types'

export const retentionList = body => ({
  type: constants.RETENTION_LIST,
  body,
})

export const setRetentionList = response => ({
  type: constants.SET_RETENTION_LIST,
  response,
})

export const casesList = body => ({
  type: constants.CASES_LIST,
  body,
})

export const setCasesList = response => ({
  type: constants.SET_CASES_LIST,
  response,
})

export const resumeCasesList = body => ({
  type: constants.RESUME_CASES_LIST,
  body,
})

export const setResumeCasesList = response => ({
  type: constants.SET_RESUME_CASES_LIST,
  response,
})

export const resumeClaims = body => ({
  type: constants.RESUME_CLAIMS,
  body,
})

export const setResumeClaims = response => ({
  type: constants.SET_RESUME_CLAIMS,
  response,
})

export const resetDataSalesforce = () => ({
  type: constants.RESET_DATA_SALESFORCE,
})

export const auditLogging = (body) => ({
  type: constants.AUDIT_LOGGING,
  body
});

export const setAuditLogging = (response) => ({
  type: constants.SET_AUDIT_LOGGING,
  response
});

