import api from 'state/utils/api'

export const retentionList = async (body) => {
  const responsePolicy = await api.retentionList({ data: body })
  return responsePolicy
}

export const casesList = async (body) => {
  const responsePolicy = await api.casesList({ data: body })
  return responsePolicy
}

export const auditLogging = async (body) => {
  const responsePerson = await api.auditLogging({ data: body });
  return responsePerson;
}
