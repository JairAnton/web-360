const urls = {
  retentionList: '/caso/retencion/listar',
  casesList: '/caso/general/listar',
  auditLogging: '/audit/logging/save'
}

export default urls