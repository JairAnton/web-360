import { createReducer } from '../../utils';

import { constants } from "./types";

const initialState = {
    parameters: {
        data: null,
        dataList: [],
        pagination: null,
        select: null
    },
    error: null,
    initLoaderGeneral: false,
    sideMenuOpened: true,
    userFrequents: [],
    status: null
};

const general = createReducer(initialState)({
    [constants.GENERAL_FAILURE]: (state, action) => {
        var error = action.error
        return { ...state, error }
    },
    [constants.SET_PARAMETERS_LIST]: (state, action) => {
        var parameters = {
            ...state.parameters,
            dataList: action.response
        }
        return { ...state, parameters, error: null }
    },
    [constants.SIDEMENU_OPENED]: (state, action) => {
        return {
            ...state,
            sideMenuOpened: !state.sideMenuOpened,
        }
    },
    [constants.USER_FREQUENTS]: (state, action) => {
        return {
            ...state,
            userFrequents: state.userFrequents,
        }
    },
    [constants.SET_AUDIT_LOGGING]: (state, action) => {
        const {status} = action.response;
        return { ...state, status }
    }

});

export default general;