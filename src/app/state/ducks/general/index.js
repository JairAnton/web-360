import reducer from './reducers';
import api from './api';
import * as generalSelectors from "./selectors";
import * as generalMiddleware from './middleware';
import * as generalOperations from './operations';

export {
    api,
    generalSelectors,
    generalMiddleware,
    generalOperations
}
export default reducer;