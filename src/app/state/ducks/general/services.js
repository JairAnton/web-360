import api from '../../../state/utils/api';

// import { constants as constantsHeaders } from "../../utils/constants";

//Data fake parameters
import parameters from './parameters.json'

const getRequestBody = (body) => {
  return {
    "request": {
      "trace": {
        "serviceId": "SERV01",
        "consumerId": "COS01",
        "moduleId": "MOD01",
        "channelCode": "WEB",
        "traceId": "TRACEID01",
        "timestamp": "2019-01-24 10:00:00",
        "identity": {
          "userId": "USER01",
          "deviceId": "DEV01",
          "host": "HOST01"
        }
      },
      "payload": body
    }
  }
}

export const parametersList = (body) => {
  var requestBody = getRequestBody(body);

  return parameters;
};

export const auditLogging = async (body) => {
  const responsePerson = await api.auditLogging({ data: body });
  return responsePerson;
}