import { takeEvery, call, put } from 'redux-saga/effects';
import { constants } from './types';
import * as generalActions from './actions';
import * as generalServices from './services';


function* parametersList(action) {
  try {
    const resolvedGeneral = yield call(generalServices.parametersList, action.body);
    yield put(generalActions.setParametersList(resolvedGeneral));
  } catch (e) {
    yield put(generalActions.generalFailure({ type: 1, errorMessage: "Ocurrio un problema, llame a su backOffice." }));
  }
}
function* auditLogging(action) {
  try {
    const resolvedAudit = yield call(generalServices.auditLogging, action.body);
    
    if (resolvedAudit.statusCode === 200) {
      yield put(generalActions.setAuditLogging(resolvedAudit.dataJson));
    } else {
      console.log('ERR_AUDIT_LOGGING')
    }
  } catch (error) {
    console.log('ERR_AUDIT_LOGGING', error)
  } 
}

export default function* sagas() {
  yield takeEvery(constants.PARAMETERS_LIST, parametersList)
  yield takeEvery(constants.AUDIT_LOGGING, auditLogging);
}