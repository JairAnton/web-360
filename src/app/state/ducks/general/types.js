import KeyMirror from 'keymirror';

export const constants = KeyMirror({
    PARAMETERS_LIST: null,
    SET_PARAMETERS_LIST: null,
    GENERAL_FAILURE: null,
    SIDEMENU_OPENED: null,
    USER_FREQUENTS: null,

    AUDIT_LOGGING: null,
    SET_AUDIT_LOGGING: null
});