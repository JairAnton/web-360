import { constants } from "./types";

export const generalFailure = (error) => ({
    type: constants.GENERAL_FAILURE,
    error
});

export const parametersList = (body) => ({
    type: constants.PARAMETERS_LIST,
    body
});

export const setParametersList = (response) => ({
    type: constants.SET_PARAMETERS_LIST,
    response
});

export const setSidemenuStatus = () => {
    return {
        type: constants.SIDEMENU_OPENED
    }
}

export const setUserFrequents = () => {
    return {
        type: constants.USER_FREQUENTS
    }
}

export const auditLogging = (body) => ({
    type: constants.AUDIT_LOGGING,
    body
});
  
export const setAuditLogging = (response) => ({
    type: constants.SET_AUDIT_LOGGING,
    response
});