import { api as apiUser } from "./user";
import { api as apiPolicy } from './policy';
import { api as apiBranch } from './branch'
import { api as apiPerson } from './person'
import { api as apiDocument } from './document'
import { api as apiGeneral } from './general'
import { api as apiSinister } from './sinister'
import { api as apiOperation } from './operation'
import { api as apiWarrantyLetter } from './warrantyLetter'
import { api as apiProvider } from './provider'
import { api as apiSalesforce } from './salesforce'

export {
  apiUser,
  apiPolicy,
  apiBranch,
  apiPerson,
  apiDocument,
  apiGeneral,
  apiSinister,
  apiOperation,
  apiWarrantyLetter,
  apiProvider,
  apiSalesforce
}