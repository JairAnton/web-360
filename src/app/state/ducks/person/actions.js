import { constants } from "./types";

export const personFailure = (error) => ({
  type: constants.PERSON_FAILURE,
  error
});

export const personDetail = (body) => ({
  type: constants.PERSON_DETAIL,
  body
});

export const setPersonDetail = (response) => ({
  type: constants.SET_PERSON_DETAIL,
  response
});

export const personPhoneRegister = (body) => ({
  type: constants.PERSON_PHONE_REGISTER,
  body
});

export const setPersonPhoneRegister = (response) => ({
  type: constants.SET_PERSON_PHONE_REGISTER,
  response
});

export const personPhoneUpdate = (body) => ({
  type: constants.PERSON_PHONE_UPDATE,
  body
});

export const setPersonPhoneUpdate = (response) => ({
  type: constants.SET_PERSON_PHONE_UPDATE,
  response
});

export const personPreexistenceList = (body) => ({
  type: constants.PERSON_PREEXISTENCE_LIST,
  body
});

export const setPersonPreexistenceList = (response) => ({
  type: constants.SET_PERSON_PREEXISTENCE_LIST,
  response
});

export const personMailRegister = (body) => ({
  type: constants.PERSON_MAIL_REGISTER,
  body
});

export const setPersonMailRegister = (response) => ({
  type: constants.SET_PERSON_MAIL_REGISTER,
  response
});

export const personMailUpdate = (body) => ({
  type: constants.PERSON_MAIL_UPDATE,
  body
});

export const setPersonMailUpdate = (response) => ({
  type: constants.SET_PERSON_MAIL_UPDATE,
  response
});

export const personSearch = (body) => ({
  type: constants.PERSON_SEARCH,
  body
});

export const setPersonSearch = (response) => ({
  type: constants.SET_PERSON_SEARCH,
  response
});

export const auditLogging = (body) => ({
  type: constants.AUDIT_LOGGING,
  body
});

export const setAuditLogging = (response) => ({
  type: constants.SET_AUDIT_LOGGING,
  response
})
export const personFraudValidate = (body) => ({
  type: constants.PERSON_FRAUD_VALIDATE,
  body
});

export const setPersonFraudValidate = (response) => ({
  type: constants.SET_PERSON_FRAUD_VALIDATE,
  response
});

export const personAddressRegister = (body) => ({
  type: constants.PERSON_ADDRESS_REGISTER,
  body
});

export const setPersonAddressRegister = (response) => ({
  type: constants.SET_PERSON_ADDRESS_REGISTER,
  response,
});

export const personAddressUpdate = (body) => ({
  type: constants.PERSON_ADDRESS_REGISTER,
  body
});

export const setPersonAddressUpdate = (response) => ({
  type: constants.SET_PERSON_ADDRESS_REGISTER,
  response
});

//#region Communication
export const personCommunicationList = (body) => ({
  type: constants.PERSON_COMMUNICATION_LIST,
  body
});

export const setPersonCommunicationList = (response) => ({
  type: constants.SET_PERSON_COMMUNICATION_LIST,
  response
});
//#endregion

export const personSelect = (numDocument) => ({
  type: constants.PERSON_SELECT,
  numDocument
});

export const setPersonSelect = (response) => ({
  type: constants.SET_PERSON_SELECT,
  response
});

export const setPersonType = (personType) => ({
  type: constants.SET_PERSON_TYPE,
  personType
});
export const resetDataPerson = () => ({
  type: constants.RESET_DATA_PERSON
});


