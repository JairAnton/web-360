import api from '../../../state/utils/api';

export const personDetail = async (body) => {
  const responsePerson = await api.personDetail({ data: body });
  return responsePerson;
};

export const personPhoneRegister = async (body) => {
  const responsePerson = await api.personPhoneRegister({ data: body });
  return responsePerson;
}

export const personPhoneUpdate = async (body) => {
  const responsePerson = await api.personPhoneUpdate({ data: body });
  return responsePerson;
}

export const personPreexistenceList = async (body) => {
  const responsePerson = await api.personPreexistenceList({ data: body });
  return responsePerson;
}

export const personMailRegister = async (body) => {
  const responsePerson = await api.personMailRegister({ data: body });
  return responsePerson;
}

export const personMailUpdate = async (body) => {
  const responsePerson = await api.personMailUpdate({ data: body });
  return responsePerson;
}

export const personSearch = async (body) => {
  const responsePerson = await api.personSearch({ data: body });
  return responsePerson;
};

export const personFraudValidate = async (body) => {
  const responsePerson = await api.personFraudValidate({ data: body });
  return responsePerson;
}

export const personAddressRegister = async (body) => {
  const responsePerson = await api.personAddressRegister({ data: body });
  return responsePerson;
}

export const personAddressUpdate = async (body) => {
  const responsePerson = await api.personAddressUpdate({ data: body });
  return responsePerson;
}

export const personCommunicationList = async (body) => {
  const responsePerson = await api.personCommunicationList({ data: body });
  return responsePerson;
}

export const auditLogging = async (body) => {
  const responsePerson = await api.auditLogging({ data: body });
  return responsePerson;
}
