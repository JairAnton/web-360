//@ts-check
import {
  createReducer
} from '../../utils';

import {
  constants
} from "./types";

const initialState = {
  personType: null,
  detail: {},
  select: {},
  phone: null,
  preexistence: {
    data: null,
    dataList: null,
    pagination: null,
    select: null
  },
  mail: null,
  search: {
    data: null,
    dataList: [],
    pagination: {},
    select: null
  },
  loaderSearch: false,
  firtsRequestSearch: true,
  validarFraude: {
    data: null,
    dataList: null,
    pagination: null,
    select: null
  },
  address: null,
  comunication: {
    data: null,
    dataList: null,
    pagination: null,
    select: null,
  },
  error: null,
  status: null
};

const person = createReducer(initialState)({
  [constants.PERSON_FAILURE]: (state, action) => {
    var error = action.error;
    let { comunication } = state;
    comunication.loaderComunication = false;
    return {
      ...state,
      error,
      comunication,
      loaderSearch: false,
      firtsRequestSearch: false
    }
  },
  [constants.SET_PERSON_DETAIL]: (state, action) => {
    var detail = action.response

    return {
      ...state,
      detail,
      error: null
    }
  },
  [constants.SET_PERSON_PHONE_REGISTER]: (state, action) => {
    var phone = action.response
    return {
      ...state,
      phone,
      error: null
    }
  },
  [constants.SET_PERSON_PHONE_UPDATE]: (state, action) => {
    var phone = action.response
    return {
      ...state,
      phone,
      error: null
    }
  },
  [constants.SET_PERSON_PREEXISTENCE_LIST]: (state, action) => {
    var preexistence = {
      ...state.preexistence,
      dataList: action.response
    }
    return {
      ...state,
      preexistence,
      error: null
    }
  },
  [constants.SET_PERSON_MAIL_REGISTER]: (state, action) => {
    var mail = action.response
    return {
      ...state,
      mail,
      error: null
    }
  },
  [constants.SET_PERSON_MAIL_UPDATE]: (state, action) => {
    var mail = action.response
    return {
      ...state,
      mail,
      error: null
    }
  },
  [constants.SET_PERSON_SEARCH]: (state, action) => {
    const search = {
      ...state.search,
      dataList: action.response.data,
      pagination: (action.response.pagination) ? action.response.pagination : {},
      select: null
    }
    const { firtsRequestSearch } = action.response
    return {
      ...state,
      search,
      detail: {},
      error: null,
      loaderSearch: false,
      firtsRequestSearch: !!firtsRequestSearch
    }
  },
  [constants.PERSON_INIT_LOADER]: (state, action) => {
    return {
      ...state,
      error: null,
      loaderSearch: true
    }
  },
  [constants.SET_PERSON_FRAUD_VALIDATE]: (state, action) => {
    var validarFraude = {
      ...state.validarFraude,
      dataList: action.response
    }
    return { ...state, validarFraude, error: null }
  },
  [constants.SET_PERSON_ADDRESS_REGISTER]: (state, action) => {
    var address = action.response
    return { ...state, address, error: null }
  },
  [constants.SET_PERSON_ADDRESS_UPDATE]: (state, action) => {
    var address = action.response
    return { ...state, address, error: null }
  },
  [constants.SET_PERSON_COMMUNICATION_LIST]: (state, action) => {
    var comunication = {
      ...state.comunication,
      dataList: action.response,
    }
    return { ...state, comunication }
  },
  [constants.SET_PERSON_SELECT]: (state, action) => {
    const select = action.response;
    return { ...state, select, error: null }
  },
  [constants.SET_PERSON_TYPE]: (state, action) => {
    const { personType } = action
    return { ...state, personType }
  },
  [constants.RESET_DATA_PERSON]: (state, action) => {
    return { ...initialState }
  },
  [constants.SET_AUDIT_LOGGING]: (state, action) => {
    const {status} = action.response;
    return { ...state, status }
  }
});

export default person