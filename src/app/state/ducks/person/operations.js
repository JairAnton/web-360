export {
  personFailure,
  personDetail,
  personPhoneRegister,
  personPhoneUpdate,
  personPreexistenceList,
  personMailRegister,
  personMailUpdate,
  personSearch,
  personFraudValidate,
  personAddressRegister,
  personAddressUpdate,
  personCommunicationList,
  personSelect,
  resetDataPerson,
  auditLogging
} from "./actions"