import { takeEvery, call, select, put } from 'redux-saga/effects'
import _ from 'lodash'

import { constants } from './types'
import * as personActions from './actions'
import * as personServices from './services'
import * as sharedActions from '../shared/actions'
import history from '../../utils/history'
import { routes } from '../../utils/routes'

import PubSub from '../../utils/PubSub'
import personLoaderKeys from './loaderKeys'


function* personDetail(action) {
  try {
    yield put(sharedActions.setLoaderVisible(personLoaderKeys.PERSON_DETAIL, true))
    const resolvedPerson = yield call(personServices.personDetail, action.body)

    if (resolvedPerson.statusCode === 200) {
      yield put(personActions.setPersonDetail(resolvedPerson.dataJson));
    } else {
      PubSub.publish('ERR_PERSON_DETAIL', 'Error al llamar el servicio detalle, reintentelo.');
    }
  } catch (error) {
    PubSub.publish('ERR_PERSON_DETAIL', error)
  } finally {
    yield put(sharedActions.setLoaderVisible(personLoaderKeys.PERSON_DETAIL, false))
  }
}

function* personPhoneRegister(action) {
  try {
    const resolvedPerson = yield call(personServices.personPhoneRegister, action.body);

    if (resolvedPerson.statusCode === 200) {
      var phone = action.body.payload
      yield put(personActions.setPersonPhoneRegister(phone));
    } else {
      yield put(personActions.personFailure({
        type: 2,
        errorMessage: "Advertencia, ocurrrio un problema al llamar al servicio."
      }));
    }
  } catch (e) {
    yield put(personActions.personFailure({
      type: 1,
      errorMessage: "Ocurrio un problema, llame a su backOffice."
    }))
  }
}

function* personPhoneUpdate(action) {
  try {
    const resolvedPerson = yield call(personServices.personPhoneUpdate, action.body)

    if (resolvedPerson.statusCode === 200) {
      var phone = action.body.payload
      yield put(personActions.setPersonPhoneRegister(phone));
    } else {
      yield put(personActions.personFailure({
        type: 2,
        errorMessage: "Advertencia, ocurrrio un problema al llamar al servicio."
      }));
    }
  } catch (e) {
    yield put(personActions.personFailure({
      type: 1,
      errorMessage: "Ocurrio un problema, llame a su backOffice."
    }));
  }
}

function* personPreexistenceList(action) {
  try {
    yield put(sharedActions.setLoaderVisible(personLoaderKeys.PERSON_PREEXISTENCIA, true))
    const resolvedPerson = yield call(personServices.personPreexistenceList, action.body);

    if (resolvedPerson.statusCode === 200) {
      yield put(personActions.setPersonPreexistenceList(resolvedPerson.dataJson));
    } else {
      yield put(personActions.personFailure({
        type: 2,
        errorMessage: "Advertencia, ocurrrio un problema al llamar al servicio."
      }));
    }
  } catch (error) {
    PubSub.publish('ERR_PERSON_PREEXISTENCIA', error);
  } finally {
    yield put(sharedActions.setLoaderVisible(personLoaderKeys.PERSON_PREEXISTENCIA, false))
  }
}

function* personMailRegister(action) {
  try {
    const resolvedPerson = yield call(personServices.personMailRegister, action.body);

    if (resolvedPerson.statusCode === 200) {
      var mail = action.body.payload
      yield put(personActions.setPersonMailRegister(mail));
    } else {
      yield put(personActions.personFailure({
        type: 2,
        errorMessage: "Advertencia, ocurrrio un problema al llamar al servicio."
      }));
    }
  } catch (e) {
    yield put(personActions.personFailure({
      type: 1,
      errorMessage: "Ocurrio un problema, llame a su backOffice."
    }));
  }
}

function* personMailUpdate(action) {
  try {
    const resolvedPerson = yield call(personServices.personMailUpdate, action.body);

    if (resolvedPerson.statusCode === 200) {
      var mail = action.body.payload
      yield put(personActions.setPersonMailUpdate(mail));
    } else {
      yield put(personActions.personFailure({
        type: 2,
        errorMessage: "Advertencia, ocurrrio un problema al llamar al servicio."
      }));
    }
  } catch (e) {
    yield put(personActions.personFailure({
      type: 1,
      errorMessage: "Ocurrio un problema, llame a su backOffice."
    }));
  }
}

function* personSearch(action) {
  try {
    yield put(sharedActions.setLoaderVisible(personLoaderKeys.PERSON_SEARCH, true))
    const resolvedPerson = yield call(personServices.personSearch, action.body);
    if (resolvedPerson.statusCode === 200) {
      yield put(personActions.setPersonSearch(resolvedPerson.dataJson));
    } else {
      yield put(personActions.setPersonSearch({ data: [], pagination: null }));
      yield put(personActions.personFailure({
        type: 2,
        errorMessage: "Advertencia, ocurrrio un problema al llamar al servicio."
      }));
    }
  } catch (error) {
    yield put(personActions.personFailure({
      type: 1,
      errorMessage: "Ocurrio un problema, llame a su backOffice."
    }));
    PubSub.publish('ERR_PERSON_SEARCH', error)
  } finally {
    yield put(sharedActions.setLoaderVisible(personLoaderKeys.PERSON_SEARCH, false))
  }
}

function* auditLogging(action) {
  try {
    yield put(sharedActions.setLoaderVisible(personLoaderKeys.AUDIT_LOGGING, true))
    const resolvedAudit = yield call(personServices.auditLogging, action.body);
  
    if (resolvedAudit.statusCode === 200) {
      yield put(personActions.setAuditLogging(resolvedAudit.dataJson));
    } else {
      PubSub.publish('ERR_AUDIT_LOGGING')
    }
  } catch (error) {
    PubSub.publish('ERR_PERSON_SEARCH', error)
  } finally {
    yield put(sharedActions.setLoaderVisible(personLoaderKeys.AUDIT_LOGGING, false))
  }
}

function* personFraudValidate(action) {
  try {
    const resolvedPerson = yield call(personServices.personFraudValidate, action.body);
    if (resolvedPerson.statusCode === 200) {
      yield put(personActions.setPersonFraudValidate(resolvedPerson.dataJson));
    } else {
      yield put(personActions.personFailure({
        type: 2,
        errorMessage: "Advertencia, ocurrrio un problema al llamar al servicio."
      }));
    }
  } catch (e) {
    yield put(personActions.personFailure({
      type: 1,
      errorMessage: "Ocurrio un problema, llame a su backOffice."
    }));
  }
}

function* personAddressRegister(action) {
  try {
    const resolvedPerson = yield call(personServices.personAddressRegister, action.body);
    if (resolvedPerson.statusCode === 200) {
      var address = action.body.payload
      yield put(personActions.setPersonAddressRegister(address));
    } else {
      yield put(personActions.personFailure({
        type: 2,
        errorMessage: "Advertencia, ocurrrio un problema al llamar al servicio."
      }));
    }
  } catch (e) {
    yield put(personActions.personFailure({
      type: 1,
      errorMessage: "Ocurrio un problema, llame a su backOffice."
    }));
  }
}

function* personAddressUpdate(action) {
  try {
    const resolvedPerson = yield call(personServices.personAddressUpdate, action.body);
    if (resolvedPerson.statusCode === 200) {
      var address = action.body.payload
      yield put(personActions.setPersonAddressUpdate(address));
    } else {
      yield put(personActions.personFailure({
        type: 2,
        errorMessage: "Advertencia, ocurrrio un problema al llamar al servicio."
      }));
    }
  } catch (e) {
    yield put(personActions.personFailure({
      type: 1,
      errorMessage: "Ocurrio un problema, llame a su backOffice."
    }));
  }
}

function* personCommunicationList(action) {
  try {
    yield put(sharedActions.setLoaderVisible(personLoaderKeys.COMMUNICATION, true))
    const resolvedPerson = yield call(personServices.personCommunicationList, action.body)
    if (resolvedPerson.statusCode === 200) {
      yield put(personActions.setPersonCommunicationList(resolvedPerson.dataJson))
    } else {
      yield put(personActions.setPersonCommunicationList([]))
      PubSub.publish('ERR_PERSON_COMMUNICATION_LIST', 'Error al llamar servicio de Comunicaciones, reintentelo.')
    }
  } catch (error) {
    console.log('error en saga [personCommunicationList]', error)
    PubSub.publish('ERR_PERSON_COMMUNICATION_LIST', error)
  } finally {
    yield put(sharedActions.setLoaderVisible(personLoaderKeys.COMMUNICATION, false))
  }
}

function* personSelect(action) {
  try {
    const { numDocument: numDocumento } = action;
    const state = yield select();
    const search = state.person.search;
    // let selectPerson;

    const selectedPerson = _.find(search.dataList, { numDocumento })

    if (selectedPerson) {
      const personType = getPersonType(selectedPerson)
      yield put(sharedActions.setLoaderVisible(personLoaderKeys.PERSON_SELECT, true))
      yield put(personActions.setPersonSelect(selectedPerson))
      yield put(personActions.setPersonType(personType))
    
      yield put(personActions.setPersonSearch({ data: [], pagination: null, firtsRequestSearch: true }))

      history.push(routes.THIRD_DETAIL)
    } else {
      yield put(personActions.personFailure({
        type: 2,
        errorMessage: "Ocurrió un problema al seleccionar."
      }));
    }
  } catch (e) {
    yield put(personActions.personFailure({
      type: 1,
      errorMessage: "Ocurrio un problema, llame a su backOffice."
    }));
  }
  finally {
    yield put(sharedActions.setLoaderVisible(personLoaderKeys.PERSON_SELECT, false))
  }
}

export default function* sagas() {
  yield takeEvery(constants.PERSON_DETAIL, personDetail);
  yield takeEvery(constants.PERSON_PHONE_REGISTER, personPhoneRegister);
  yield takeEvery(constants.PERSON_PHONE_UPDATE, personPhoneUpdate);
  yield takeEvery(constants.PERSON_PREEXISTENCE_LIST, personPreexistenceList);
  yield takeEvery(constants.PERSON_MAIL_REGISTER, personMailRegister);
  yield takeEvery(constants.PERSON_MAIL_UPDATE, personMailUpdate);
  yield takeEvery(constants.PERSON_SEARCH, personSearch);
  yield takeEvery(constants.PERSON_FRAUD_VALIDATE, personFraudValidate);
  yield takeEvery(constants.PERSON_ADDRESS_REGISTER, personAddressRegister);
  yield takeEvery(constants.PERSON_ADDRESS_UPDATE, personAddressUpdate);
  yield takeEvery(constants.PERSON_COMMUNICATION_LIST, personCommunicationList);
  yield takeEvery(constants.PERSON_SELECT, personSelect);
  yield takeEvery(constants.AUDIT_LOGGING, auditLogging);
}



const getPersonType = (person) => {
  if (!person.desTipoTercero)
    return 'PN'
  switch (person.desTipoTercero.toUpperCase()) {
    case 'PERSONA NATURAL':
      return 'PN'
    case 'PERSONA JURIDICA':
      return 'PJ'
    case 'PERSONA JURÍDICA':
      return 'PJ'
    default:
      return 'PN'
  }
}