const urls = {
    personDetail: '/persona/detalle',
    personPhoneRegister: '/persona/telefono/registrar',
    personPhoneUpdate: '/persona/telefono/actualizar',
    personPreexistenceList: '/persona/preexistencia/listar',
    personMailRegister: '/persona/correo/registrar',
    personMailUpdate: '/persona/correo/actualizar',
    personSearch: '/persona/buscar',
    personFraudValidate: '/persona/fraude/validar',
    personAddressRegister: '/persona/direccion/registrar',
    personAddressUpdate: '/persona/direccion/actualizar',
    personCommunicationList: '/persona/comunicacion/listar',
    auditLogging: '/audit/logging/save'
};

export default urls;    