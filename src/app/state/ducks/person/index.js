import reducer from './reducers';
import api from './api';
import * as personSelectors from "./selectors";
import * as personMiddleware from './middleware';
import * as personOperations from './operations';
import personLoaderKeys from './loaderKeys'

export {
    api,
    personSelectors,
    personMiddleware,
    personOperations,
    personLoaderKeys,
}
export default reducer;