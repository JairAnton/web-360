import { default as user } from './user';
import { default as policy } from './policy';
import { default as person } from './person';
import { default as document } from './document';
import { default as branch } from './branch';
import { default as general } from './general';
import { default as sinister } from './sinister';
import { default as warrantyLetter } from './warrantyLetter';
import { default as operation } from './operation';
import { default as provider } from './provider';
import shared from './shared';
import salesforce from './salesforce'


export {
  user,
  policy,
  person,
  document,
  branch,
  general,
  sinister,
  warrantyLetter,
  operation,
  shared,
  provider,
  salesforce
}