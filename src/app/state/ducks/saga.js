import userSagas from './user/sagas'
import generalSagas from './general/sagas'
import policySagas from './policy/sagas'
import personSagas from './person/sagas'
import sinisterSagas from './sinister/sagas'
import operationSagas from './operation/sagas'
import warrantyLetterSagas from './warrantyLetter/sagas'
import providerSagas from './provider/sagas'
import salesforceSagas from './salesforce/sagas'

export {
  userSagas,
  generalSagas,
  policySagas,
  personSagas,
  sinisterSagas,
  operationSagas,
  warrantyLetterSagas,
  providerSagas,
  salesforceSagas
}   