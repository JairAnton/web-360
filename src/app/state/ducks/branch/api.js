const urls = {
    branchDeductiblesList: '/ramo/deducibles/listar',
    branchCoverageList: '/ramo/cobertura/listar',
    branchProviderList: '/ramo/proveedor/listar',
    branchTypeCoverageList: '/ramo/tipoCobertura/listar',
    auditlogging: '/audit/logging/save'
  };
  
  export default urls;
  