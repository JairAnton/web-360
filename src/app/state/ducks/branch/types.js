import KeyMirror from 'keymirror';

export const constants = KeyMirror({
    BRANCH_DEDUCTIBLES_LIST: null,
    SET_BRANCH_DEDUCTIBLES_LIST: null,

    BRANCH_COVERAGE_LIST: null,
    SET_BRANCH_COVERAGE_LIST: null,

    BRANCH_PROVIDER_LIST: null,
    SET_BRANCH_PROVIDER_LIST: null,

    BRANCH_TYPE_COVERAGE_LIST: null,
    SET_BRANCH_TYPE_COVERAGE_LIST: null,

    BRANCH_FAILURE: null,
    
    AUDIT_LOGGING: null,
    SET_AUDIT_LOGGING: null
});