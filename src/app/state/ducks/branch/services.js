import api from '../../../state/utils/api';

import { constants as constantsHeaders } from "../../utils/constants";

const getRequestBody = (body) => {
    return {
        "request": {
            "trace": {
                "serviceId": "SERV01",
                "consumerId": "COS01",
                "moduleId": "MOD01",
                "channelCode": "WEB",
                "traceId": "TRACEID01",
                "timestamp": "2019-01-24 10:00:00",
                "identity": {
                    "userId": "USER01",
                    "deviceId": "DEV01",
                    "host": "HOST01"
                }
            },
            "payload": body
        }
    }
}

export const branchDeductiblesList = (body) => {
    var requestBody = getRequestBody(body);

    return api.branchDeductiblesList({
        data: requestBody,
        //headers: { type: constantsHeaders.HEADERS_LOGIN_RIMAC },
        onSuccess: (res) => {
            return res;
        },
        onError: err => {
            console.log(`error: ${err}`);
            return 'ERROOOR';
        }
    });
}

export const branchCoverageList = (body) => {
    var requestBody = getRequestBody(body);

    return api.branchCoverageList({
        data: requestBody,
        //headers: { type: constantsHeaders.HEADERS_LOGIN_RIMAC },
        onSuccess: (res) => {
            return res;
        },
        onError: err => {
            console.log(`error: ${err}`);
            return 'ERROOOR';
        }
    });
}

export const branchProviderList = (body) => {
    var requestBody = getRequestBody(body);

    return api.branchProviderList({
        data: requestBody,
        //headers: { type: constantsHeaders.HEADERS_LOGIN_RIMAC },
        onSuccess: (res) => {
            return res;
        },
        onError: err => {
            console.log(`error: ${err}`);
            return 'ERROOOR';
        }
    });
}

export const branchTypeCoverageList = (body) => {
    var requestBody = getRequestBody(body);

    return api.branchTypeCoverageList({
        data: requestBody,
        //headers: { type: constantsHeaders.HEADERS_LOGIN_RIMAC },
        onSuccess: (res) => {
            return res;
        },
        onError: err => {
            console.log(`error: ${err}`);
            return 'ERROOOR';
        }
    });
}

export const auditLogging = async (body) => {
    const responsePerson = await api.auditLogging({ data: body });
    return responsePerson;
  }
  