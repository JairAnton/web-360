import { takeEvery, call, put } from 'redux-saga/effects'
import { constants } from './types'
import * as branchActions from './actions'
import * as branchServices from './services'


function* branchDeductiblesList(action) {
  try {
    const resolvedBranch = yield call(branchServices.branchDeductiblesList, action.body);
    if (resolvedBranch.response.status.success) {
      yield put(branchActions.setBranchDeductiblesList(resolvedBranch.response.payload));
    } else {
      yield put(branchActions.branchFailure({ type: 2, errorMessage: "Advertencia, ocurrrio un problema al llamar al servicio." }));
    }
  } catch (e) {
    yield put(branchActions.branchFailure({ type: 1, errorMessage: "Ocurrio un problema, llame a su backOffice." }));
  }
}

function* branchCoverageList(action) {
  try {
    const resolvedBranch = yield call(branchServices.branchCoverageList, action.body);
    if (resolvedBranch.response.status.success) {
      yield put(branchActions.setBranchCoverageList(resolvedBranch.response.payload));
    } else {
      yield put(branchActions.branchFailure({ type: 2, errorMessage: "Advertencia, ocurrrio un problema al llamar al servicio." }));
    }
  } catch (e) {
    yield put(branchActions.branchFailure({ type: 1, errorMessage: "Ocurrio un problema, llame a su backOffice." }));
  }
}

function* branchProviderList(action) {
  try {
    const resolvedBranch = yield call(branchServices.branchProviderList, action.body);
    if (resolvedBranch.response.status.success) {
      yield put(branchActions.setBranchProviderList(resolvedBranch.response.payload));
    } else {
      yield put(branchActions.branchFailure({ type: 2, errorMessage: "Advertencia, ocurrrio un problema al llamar al servicio." }));
    }
  } catch (e) {
    yield put(branchActions.branchFailure({ type: 1, errorMessage: "Ocurrio un problema, llame a su backOffice." }));
  }
}

function* branchTypeCoverageList(action) {
  try {
    const resolvedBranch = yield call(branchServices.branchTypeCoverageList, action.body);
    if (resolvedBranch.response.status.success) {
      yield put(branchActions.setBranchTypeCoverageList(resolvedBranch.response.payload));
    } else {
      yield put(branchActions.branchFailure({ type: 2, errorMessage: "Advertencia, ocurrrio un problema al llamar al servicio." }));
    }
  } catch (e) {
    yield put(branchActions.branchFailure({ type: 1, errorMessage: "Ocurrio un problema, llame a su backOffice." }));
  }
}

function* auditLogging(action) {
  try {
    const resolvedAudit = yield call(branchServices.auditLogging, action.body);
    
    if (resolvedAudit.statusCode === 200) {
      yield put(branchActions.setAuditLogging(resolvedAudit.dataJson));
    } else {
      console.log('ERR_AUDIT_LOGGING')
    }
  } catch (error) {
    console.log('ERR_AUDIT_LOGGING', error)
  } 
}


export default function* sagas() {
  yield takeEvery(constants.BRANCH_DEDUCTIBLES_LIST, branchDeductiblesList);
  yield takeEvery(constants.BRANCH_COVERAGE_LIST, branchCoverageList);
  yield takeEvery(constants.BRANCH_PROVIDER_LIST, branchProviderList);
  yield takeEvery(constants.BRANCH_TYPE_COVERAGE_LIST, branchTypeCoverageList);
  yield takeEvery(constants.AUDIT_LOGGING, auditLogging)
}