import { constants } from "./types";

export const branchFailure = (error) => ({
    type: constants.BRANCH_FAILURE,
    error
});

export const branchDeductiblesList = (body) => {
    return {
        type: constants.BRANCH_DEDUCTIBLES_LIST,
        body
    }
}

export const setBranchDeductiblesList = (dataList) => {
    return {
        type: constants.SET_BRANCH_DEDUCTIBLES_LIST,
        dataList
    }
}

export const branchCoverageList = (body) => {
    return {
        type: constants.BRANCH_COVERAGE_LIST,
        body
    }
}

export const setBranchCoverageList = (dataList) => {
    return {
        type: constants.SET_BRANCH_COVERAGE_LIST,
        dataList
    }
}

export const branchProviderList = (body) => {
    return {
        type: constants.BRANCH_PROVIDER_LIST,
        body
    }
}

export const setBranchProviderList = (dataList) => {
    return {
        type: constants.SET_BRANCH_PROVIDER_LIST,
        dataList
    }
}

export const branchTypeCoverageList = (body) => {
    return {
        type: constants.BRANCH_TYPE_COVERAGE_LIST,
        body
    }
}

export const setBranchTypeCoverageList = (dataList) => {
    return {
        type: constants.SET_BRANCH_TYPE_COVERAGE_LIST,
        dataList
    }
}
export const auditLogging = (body) => ({
    type: constants.AUDIT_LOGGING,
    body
});

export const setAuditLogging = (response) => ({
    type: constants.SET_AUDIT_LOGGING,
    response
})