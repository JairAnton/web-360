import { createReducer } from '../../utils';

import { constants } from "./types";

const initialState = {
    coverage: {
        data: null,
        dataList: null,
        select: null
    },
    deductibles: {
        data: null,
        dataList: null,
        select: null
    },
    provider: {
        data: null,
        dataList: null,
        select: null
    },
    typeCoverage: {
        data: null,
        dataList: null,
        select: null
    },
    error: null,
    status: null
}

const branch = createReducer(initialState)({
    [constants.BRANCH_FAILURE]: (state, action) => {
        var error = action.error
        return { ...state, error }
    },
    [constants.SET_BRANCH_COVERAGE_LIST]: (state, action) => {
        var coverage = {
            ...state.coverage,
            dataList: action.dataList
        }

        return { ...state, coverage }
    },
    [constants.SET_BRANCH_DEDUCTIBLES_LIST]: (state, action) => {
        var deductibles = {
            ...state.deductibles,
            dataList: action.dataList
        }

        return { ...state, deductibles }
    },
    [constants.SET_BRANCH_PROVIDER_LIST]: (state, action) => {
        var provider = {
            ...state.provider,
            dataList: action.dataList
        }

        return { ...state, provider }
    },
    [constants.SET_BRANCH_TYPE_COVERAGE_LIST]: (state, action) => {
        var typeCoverage = {
            ...state.typeCoverage,
            dataList: action.dataList
        }

        return { ...state, typeCoverage }
    },
    [constants.SET_AUDIT_LOGGING]: (state, action) => {
        const {status} = action.response;
        return { ...state, status }
    }
        
});

export default branch;
