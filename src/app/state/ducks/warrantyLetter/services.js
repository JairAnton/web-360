import api from '../../../state/utils/api';

export const warrantyLetterService = async (body) => {
  const responseWarrantyLetter = await api.getWarrantyLetterList({ data: body });
  return responseWarrantyLetter;
}


export const auditLogging = async (body) => {
  const responsePerson = await api.auditLogging({ data: body });
  return responsePerson;
}