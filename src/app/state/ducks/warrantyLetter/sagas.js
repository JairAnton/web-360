import { takeEvery, call, select, put } from 'redux-saga/effects';
import { constants } from './types';
import * as warrantyLetterActions from './actions';
import * as warrantyLetterServices from './services';
import PubSub from '../../utils/PubSub'
import warrantyLoaderKeys from './loaderKeys'
import * as sharedActions from '../shared/actions'


function* warrantyLetterList(action) {
  try {
    yield put(sharedActions.setLoaderVisible(warrantyLoaderKeys.WARRANTY_LOADER, true))
    const resolvedWarrantyLetterList = yield call(
      warrantyLetterServices.warrantyLetterService,
      action.body
    )
    yield put(warrantyLetterActions.setWarrantyLetterList(resolvedWarrantyLetterList.dataJson));
  }
  catch (e) {
    yield put(warrantyLetterActions.warrantyLetterFailure({ type: 1, errorMessage: "Ocurrio un problema, llame a su backOffice." }));
    PubSub.publish('ERR_WARRANTY_LETTER_LIST', 'Error al traer la Lista de Cartas de Garantia, reintentelo.');
  }
  finally {
    yield put(sharedActions.setLoaderVisible(warrantyLoaderKeys.WARRANTY_LOADER, false))
  }
}
function* auditLogging(action) {
  try {
    yield put(sharedActions.setLoaderVisible(warrantyLoaderKeys.AUDIT_LOGGING, true))
    const resolvedAudit = yield call(warrantyLetterServices.auditLogging, action.body);
    
    if (resolvedAudit.statusCode === 200) {
      yield put(warrantyLetterActions.setAuditLogging(resolvedAudit.dataJson));
    } else {
      PubSub.publish('ERR_AUDIT_LOGGING')
    }
  } catch (error) {
    PubSub.publish('ERR_AUDIT_LOGGING', error)
  } finally {
    yield put(sharedActions.setLoaderVisible(warrantyLoaderKeys.AUDIT_LOGGING, false))
  }
}


export default function* sagas() {
  yield takeEvery(constants.WARRANTY_LETTER_LIST, warrantyLetterList)
  yield takeEvery(constants.AUDIT_LOGGING, auditLogging);
}