import { createReducer } from '../../utils';

import { constants } from "./types";

const initialState = {
    warrantyLetterList: [],
    error: null,
    loaderWarrantyLetter: false,
    cartasGarantia: [],
    afiliados: [],
    status:null,
};

const warrantyLetter = createReducer(initialState)({

    [constants.WARRANTY_LETTER_FAILURE]: (state, action) => {
        return { ...state, error: action.error, loaderWarrantyLetter: false };
    },

    [constants.WARRANTY_LETTER_INIT_LOADER]: (state, action) => {
        return { ...state, loaderWarrantyLetter: true };
    },

    [constants.WARRANTY_LETTER_END_LOADER]: (state, action) => {
        return { ...state, loaderWarrantyLetter: false };
    },

    [constants.SET_WARRANTY_LETTER_LIST]: (state, action) => {
        let cartasGarantia = (action.body) ? action.body : [];
        return { ...state, cartasGarantia, loaderWarrantyLetter: false };
    },
    [constants.SET_AUDIT_LOGGING]: (state, action) => {
        const {status} = action.response;
        return { ...state, status }
    }
    

});

export default warrantyLetter;
