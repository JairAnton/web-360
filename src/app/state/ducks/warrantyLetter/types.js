import KeyMirror from 'keymirror';

export const constants = KeyMirror({
    WARRANTY_LETTER_LIST: null,
    SET_WARRANTY_LETTER_LIST: null,

    WARRANTY_LETTER_INIT_LOADER: null,
    WARRANTY_LETTER_END_LOADER: null,

    WARRANTY_LETTER_FAILURE: null,
    
    AUDIT_LOGGING: null,
    SET_AUDIT_LOGGING: null,
});