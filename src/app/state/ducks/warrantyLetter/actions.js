import { constants } from "./types";

export const setWarrantyLetterList = (body) => ({
    type: constants.SET_WARRANTY_LETTER_LIST,
    body
});

export const warrantyLetterList = (body) => ({
    type: constants.WARRANTY_LETTER_LIST,
    body
});

export const warrantyLetterFailure = (error) => ({
    type: constants.WARRANTY_LETTER_FAILURE,
    error
});

export const warrantyLetterInitLoader = () => ({
    type: constants.WARRANTY_LETTER_INIT_LOADER
})

export const warrantyLetterEndLoader = () => ({
    type: constants.WARRANTY_LETTER_END_LOADER
})

export const auditLogging = (body) => ({
    type: constants.AUDIT_LOGGING,
    body
});
  
export const setAuditLogging = (response) => ({
    type: constants.SET_AUDIT_LOGGING,
    response
});