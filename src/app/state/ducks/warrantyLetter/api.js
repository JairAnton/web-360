const urls = {
    getWarrantyLetterList: '/poliza/certificado/asegurados/cartasGarantia/listar',
    auditLogging: '/audit/logging/save'
  };
  
  export default urls;
  
  