const urls = {
  providerList: '/ramo/proveedor/listar',
  auditLogging: '/audit/logging/save',
};

export default urls;