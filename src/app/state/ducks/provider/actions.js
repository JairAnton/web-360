import { constants } from "./types";

// export const policyFailure = (error) => ({
//   type: constants.POLICY_FAILURE,
//   error
// });

export const providerList = body => {
  return {
    type: constants.PROVIDER_LIST,
    body,
  }
}

export const setProviderList = response => {
  return {
    type: constants.SET_PROVIDER_LIST,
    response,
  }
}
export const auditLogging = (body) => ({
  type: constants.AUDIT_LOGGING,
  body
});

export const setAuditLogging = (response) => ({
  type: constants.SET_AUDIT_LOGGING,
  response
});


