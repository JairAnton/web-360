import { createReducer } from '../../utils';
import { constants } from "./types";

const initialState = {
  dataList: [],
  pagination: {},
  status: null
};

const provider = createReducer(initialState)({
  [constants.SET_PROVIDER_LIST]: (state, action) => {
    const {
      data: dataList,
      pagination
    } = action.response

    return {
      ...state,
      dataList,
      pagination,
    }
  },
  [constants.SET_AUDIT_LOGGING]: (state, action) => {
    const {status} = action.response;
    return { ...state, status }
  }


});


export default provider;
