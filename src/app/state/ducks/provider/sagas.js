// @ts-check
import { takeEvery, call, put } from 'redux-saga/effects'
import { constants } from './types'
import * as providerActions from './actions'
import * as sharedActions from '../shared/actions'
import * as providerServices from './services'
import PubSub from '../../utils/PubSub'
import providerLoaderKeys from './loaderKeys'

/*
Este Saga llama al servicio Listar siniestro, el cual es el mismo que se llama
al nivel de Tercero.
*/
function* providerList(action) {
  try {
    yield put(sharedActions.setLoaderVisible(providerLoaderKeys.PROVIDER_LIST, true))

    const resolvedProvider = yield call(providerServices.providerList, action.body);
    if (resolvedProvider.statusCode === 200) {
      yield put(providerActions.setProviderList(resolvedProvider.dataJson));
    } else {
      PubSub.publish('ERR_PROVIDER_LIST', 'Error al traer la lista de Proveedores, reintentelo.');
    }
  } catch (e) {
    PubSub.publish('ERR_PROVIDER_LIST', e);
  } finally {
    yield put(sharedActions.setLoaderVisible(providerLoaderKeys.PROVIDER_LIST, false))
  }
}
function* auditLogging(action) {
  try {
    yield put(sharedActions.setLoaderVisible(providerLoaderKeys.AUDIT_LOGGING, true))
    const resolvedAudit = yield call(providerServices.auditLogging, action.body);
    
    if (resolvedAudit.statusCode === 200) {
      yield put(providerActions.setAuditLogging(resolvedAudit.dataJson));
    } else {
      PubSub.publish('ERR_AUDIT_LOGGING')
    }
  } catch (error) {
    PubSub.publish('ERR_AUDIT_LOGGING', error)
  } finally {
    yield put(sharedActions.setLoaderVisible(providerLoaderKeys.AUDIT_LOGGING, false))
  }
}

export default function* sagas() {
  yield takeEvery(constants.PROVIDER_LIST, providerList);
  yield takeEvery(constants.AUDIT_LOGGING, auditLogging);
}