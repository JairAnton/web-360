import KeyMirror from 'keymirror';

export const constants = KeyMirror({
    PROVIDER_LIST: null,
    SET_PROVIDER_LIST: null,
    
    AUDIT_LOGGING: null,
    SET_AUDIT_LOGGING: null,
});