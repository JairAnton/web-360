import api from '../../../state/utils/api';


export const providerList = async body => {
  const responseProvider = await api.providerList({ data: body })
  return responseProvider
}

export const auditLogging = async (body) => {
  const responsePerson = await api.auditLogging({ data: body });
  return responsePerson;
}