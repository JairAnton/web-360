import api from '../../../state/utils/api';

export const sinisterList = async (body) => {
    const responseSinister = await api.sinisterList({ data: body });
    return responseSinister;
};

export const sinisterDetail = async (body) => {
    const responseSinister = await api.sinisterDetail({ data: body });
    return responseSinister;
};

export const auditLogging = async (body) => {
    const responsePerson = await api.auditLogging({ data: body });
    return responsePerson;
  }