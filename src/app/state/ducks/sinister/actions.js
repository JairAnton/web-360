import { constants } from "./types"

export const sinisterFailure = (error) => ({
  type: constants.SINISTER_FAILURE,
  error,
})

export const sinisterList = (body) => ({
  type: constants.SINISTER_LIST,
  body,
})

export const setSinisterList = (response) => ({
  type: constants.SET_SINISTER_LIST,
  response,
})

export const sinisterSelectData = (data) => ({
  type: constants.SINISTER_SELECT_DATA,
  data,
})

export const sinisterDetail = (body) => ({
  type: constants.SINISTER_DETAIL,
  body,
})

export const setSinisterDetail = (response) => ({
  type: constants.SET_SINISTER_DETAIL,
  response,
})

export const sinisterResumeList = (body) => ({
  type: constants.SINISTER_RESUME_LIST,
  body,
})

export const setSinisterResumeList = (response) => ({
  type: constants.SET_SINISTER_RESUME_LIST,
  response,
})

export const resetDataSinister = () => ({
  type: constants.RESET_DATA_SINISTER
})

export const auditLogging = (body) => ({
  type: constants.AUDIT_LOGGING,
  body
});

export const setAuditLogging = (response) => ({
  type: constants.SET_AUDIT_LOGGING,
  response
});

