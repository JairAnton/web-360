import { takeEvery, call, put } from 'redux-saga/effects';

import { constants } from './types';

import * as sinisterActions from './actions';

import * as sinisterServices from './services';
import * as sharedActions from '../shared/actions'
import sinisterLoaderKeys from './loaderKeys'

import PubSub from '../../utils/PubSub'

function* sinisterList(action) {
  try {
    yield put(sharedActions.setLoaderVisible(sinisterLoaderKeys.SINISTER_LIST, true))
    const resolvedSinister = yield call(sinisterServices.sinisterList, action.body);

    if (resolvedSinister.statusCode === 200) {
      yield put(sinisterActions.setSinisterList(resolvedSinister.dataJson));
    } else {
      yield put(sinisterActions.sinisterFailure({ type: 2, errorMessage: "Advertencia, ocurrrio un problema al llamar al servicio." }));
    }
  } catch (e) {
    yield put(sinisterActions.sinisterFailure({ type: 1, errorMessage: "Ocurrio un problema, llame a su backOffice." }));
    PubSub.publish('ERR_SINISTER_LIST', e);
  } finally {
    yield put(sharedActions.setLoaderVisible(sinisterLoaderKeys.SINISTER_LIST, false))
  }
}

function* sinisterDetail(action) {
  try {
    yield put(sharedActions.setLoaderVisible(sinisterLoaderKeys.SINISTER_DETAIL, true))
    const resolvedSinister = yield call(sinisterServices.sinisterDetail, action.body);

    if (resolvedSinister.statusCode === 200) {
      yield put(sinisterActions.setSinisterDetail(resolvedSinister.dataJson));
    } else {
      PubSub.publish('ERR_SINISTER_DETAIL', 'Error al traer la Lista de Siniestros, reintentelo.');
    }
  } catch (e) {
    PubSub.publish('ERR_SINISTER_DETAIL', e);
  } finally {
    yield put(sharedActions.setLoaderVisible(sinisterLoaderKeys.SINISTER_DETAIL, false))
  }
}

function* sinisterResumeList(action) {
  try {
    yield put(sharedActions.setLoaderVisible(sinisterLoaderKeys.SINISTER_RESUME_LIST, true))
    const resolvedSinister = yield call(sinisterServices.sinisterList, action.body);

    if (resolvedSinister.statusCode === 200) {
      yield put(sinisterActions.setSinisterResumeList(resolvedSinister.dataJson));
    } else {
      PubSub.publish('ERR_SINISTER_RESUME_LIST', 'Error al traer la Lista de Siniestros, reintentelo.');
    }
  } catch (e) {
    PubSub.publish('ERR_SINISTER_RESUME_LIST', e);
  } finally {
    yield put(sharedActions.setLoaderVisible(sinisterLoaderKeys.SINISTER_RESUME_LIST, false))
  }
}
function* auditLogging(action) {
  try {
    yield put(sharedActions.setLoaderVisible(sinisterLoaderKeys.AUDIT_LOGGING, true))
    const resolvedAudit = yield call(sinisterServices.auditLogging, action.body);
    
    if (resolvedAudit.statusCode === 200) {
      yield put(sinisterActions.setAuditLogging(resolvedAudit.dataJson));
    } else {
      PubSub.publish('ERR_AUDIT_LOGGING')
    }
  } catch (error) {
    PubSub.publish('ERR_AUDIT_LOGGING', error)
  } finally {
    yield put(sharedActions.setLoaderVisible(sinisterLoaderKeys.AUDIT_LOGGING, false))
  }
}

export default function* sagas() {
  yield takeEvery(constants.SINISTER_LIST, sinisterList)
  yield takeEvery(constants.SINISTER_DETAIL, sinisterDetail)
  yield takeEvery(constants.SINISTER_RESUME_LIST, sinisterResumeList)
  yield takeEvery(constants.AUDIT_LOGGING, auditLogging);
}