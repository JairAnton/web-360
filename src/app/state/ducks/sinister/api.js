const urls = {
  sinisterList: '/siniestro/listar',
  sinisterDetail: '/siniestro/detalle',
  auditLogging: '/audit/logging/save'
};

export default urls;    