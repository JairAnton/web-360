import KeyMirror from 'keymirror';

export const constants = KeyMirror({
    SINISTER_FAILURE: null,

    SINISTER_INIT_LOADER: null,
    SINISTER_END_LOADER: null,

    SINISTER_LIST: null,
    SET_SINISTER_LIST: null,
    
    SINISTER_DETAIL: null,
    SET_SINISTER_DETAIL: null,
    
    SINISTER_SELECT_DATA: null,

    SINISTER_RESUME_LIST: null,
    SET_SINISTER_RESUME_LIST: null,

    RESET_DATA_SINISTER: null,

    AUDIT_LOGGING: null,
    SET_AUDIT_LOGGING: null,
});