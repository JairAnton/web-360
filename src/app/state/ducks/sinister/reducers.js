import { createReducer } from '../../utils';
import { constants } from "./types";

const initialState = {
  dataList: [],
  pagination: {},
  data: {

  },
  detail: {
    "detailSinister": {},
    "ajusteTecnico": [],
    "deducibles": [],
    "circunstancias": [],
    "danio": [],
    "observaciones": []
  },
  resume: {
    initFetch: false,
    data: null,
    dataList: [],
    pagination: null
  },
  error: null,
  initLoaderSinister: false,
  status: null,
};

const sinister = createReducer(initialState)({

  [constants.SINISTER_FAILURE]: (state, action) => {
    var error = action.error
    return { ...state, error, initLoaderSinister: false }
  },

  [constants.SET_SINISTER_LIST]: (state, action) => {
    const { data: dataList, pagination } = action.response;
    return { ...state, dataList, pagination, error: null, initLoaderSinister: false }
  },

  [constants.SET_SINISTER_DETAIL]: (state, action) => {
    let detail = action.response
    return {
      ...state,
      detail
    }
  },

  [constants.SINISTER_SELECT_DATA]: (state, action) => {
    let data = action.data;
    return {
      ...state,
      data,
      error: null
    }
  },

  [constants.SET_SINISTER_RESUME_LIST]: (state, action) => {
    const { data: dataList, pagination, initFetch } = action.response;
    const resume = {
      ...state.resume,
      dataList,
      pagination,
      initFetch: initFetch ? false : true
    }
    return { ...state, resume }
  },

  [constants.RESET_DATA_SINISTER]: (state, action) => {
    return { ...initialState }
  },
  [constants.SET_AUDIT_LOGGING]: (state, action) => {
    const {status} = action.response;
    return { ...state, status }
  }
});

export default sinister;