const urls = {
    personDetail: '/persona/detalle',
    personPhoneRegister: '/persona/telefono/registrar',
};
  
export default urls;    