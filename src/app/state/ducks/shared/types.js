import KeyMirror from 'keymirror';

export const constants = KeyMirror({
    LOADER_ADD: null,
    LOADER_REMOVE: null,
    SET_LOADER_VISIBLE: null,
});