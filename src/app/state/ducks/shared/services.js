import api from '../../../state/utils/api';

export const personDetail = async (body) => {
  const responsePerson = await api.personDetail({ data: body });
  return responsePerson;
};

export const personPhoneRegister = async (body) => {
  const responsePerson = await api.personPhoneRegister({ data: body });
  return responsePerson;
}

