//@ts-check
import _ from 'lodash';
import { createReducer } from '../../utils';
import { constants } from "./types";

const initialState = {
  loader: [],
};

const share = createReducer(initialState)({
  [constants.SET_LOADER_VISIBLE]: (state, action) => {
    let { loader } = state
    const { name, visible } = action.payload
    if (visible)
      loader = loader.concat([name])
    else
      loader = _.filter(loader, key => key !== name)
    return {
      ...state,
      loader: _.clone(loader),
    }
  },
});

export default share;