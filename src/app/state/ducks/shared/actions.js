import { constants } from "./types";

export const setLoaderVisible = (name, visible) => ({
  type: constants.SET_LOADER_VISIBLE,
  payload: {
    name,
    visible,
  },
});
