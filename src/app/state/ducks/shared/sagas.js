import { takeEvery, call, put } from 'redux-saga/effects';
import { constants } from './types';
import * as personActions from './actions';
import * as personServices from './services';


function* personDetail(action) {
  try {
    const resolvedPerson = yield call(personServices.personDetail, action.body);
    if (resolvedPerson.statusCode === 200) {
      yield put(personActions.setPersonDetail(resolvedPerson.dataJson));
    } else {
      yield put(personActions.personFailure({ type: 2, errorMessage: "Advertencia, ocurrrio un problema al llamar al servicio." }));
    }
  } catch (e) {
    yield put(personActions.personFailure({ type: 1, errorMessage: "Ocurrio un problema, llame a su backOffice." }));
  }
}

function* personPhoneRegister(action) {
  try {
    const resolvedPerson = yield call(personServices.personPhoneRegister, action.body);
    if (resolvedPerson.statusCode === 200) {
      var phone = action.body.payload
      yield put(personActions.setPersonPhoneRegister(phone));
    } else {
      yield put(personActions.personFailure({ type: 2, errorMessage: "Advertencia, ocurrrio un problema al llamar al servicio." }));
    }
  } catch (e) {
    yield put(personActions.personFailure({ type: 1, errorMessage: "Ocurrio un problema, llame a su backOffice." }));
  }
}


export default function* sagas() {
  yield takeEvery(constants.PERSON_DETAIL, personDetail);
  yield takeEvery(constants.PERSON_PHONE_REGISTER, personPhoneRegister);
}