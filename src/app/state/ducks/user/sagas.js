import { takeEvery, call, select, put } from 'redux-saga/effects';
import { constants } from './types';
import * as userActions from './actions';
import * as userServices from './services';
import * as sharedActions from '../shared/actions'

import userLoaderKeys from './loaderKeys'
import history from '../../utils/history';
import { routes } from '../../utils/routes';
import PubSub from '../../utils/PubSub'
import { Auth } from 'aws-amplify';
import { rol_web360 } from 'state/utils/constants'

function* login(action) {
  try {
    yield call(userServices.login)
  } catch (e) {
    console.log('login ERROR', e)
    PubSub.publish('ERR_LOGIN', 'Error al iniciar Sesión, reintentelo.');
  }
}

function* dataAuthenticated(action) {
  try {
    const dataAuthenticated = yield Auth.currentAuthenticatedUser()
    const loader = false
    let alert = true
    const {
      signInUserSession: {
        accessToken:
        {
          payload: {
            'cognito:groups': cognitoGroups
          }
        }
      }
    } = dataAuthenticated

    for (let rol in rol_web360) {
      if (cognitoGroups.includes(rol_web360[rol])) {
        alert = false
      }
    }


    Auth.currentSession()
      .then(data => console.log('SESION -> ', { data }))
      .catch(err => console.log('ERROR -> ', err))

    yield put(userActions.setAuthenticatedLogin({ cognitoGroups, loader, alert }))
  } catch (e) {

  }
}

function* logout(action) {
  try {
    Auth.signOut()
  } catch (e) {
    console.error('logout error: ', e)
  }
}

function* getKeySession(action) {
  let store = yield select();
  const keySession = yield call(userServices.getKeySession, store.user.jwtToken);
  yield put(userActions.setSecretKeys(keySession));
}

function* sessionAdd(action){
  try{
    const resolvedAdd = yield call(userServices.sessionAdd, action.body)
    if (resolvedAdd.statusCode === 200) {
      yield put(userActions.setSessionAdd(resolvedAdd.dataJson))
    }
    else {
      console.log('ELSE -> ERR_SESSION_ADD')
      console.error('response: ', resolvedAdd)
      PubSub.publish('ERR_SESSION_ADD', 'Error al agregar la sessión a la Lista, reinténtelo.')
    }
  }
  catch(error){
    console.log('ELSE -> ERR_SESSION_ADD')
    console.error('response: ', error)
    PubSub.publish('ERR_SESSION_ADD', 'Error al agregar la sessión a la Lista, reinténtelo.')
  }
}
function* sessionRemove(action){
  try{
    const resolvedRemove = yield call(userServices.sessionRemove, action.body)
    if (resolvedRemove.statusCode === 200) {
      console.log('IF -> SESSION_REMOVE')
      //yield put(userActions.setSessionRemove(resolvedRemove.dataJson))
    }
    else {
      console.log('ELSE -> ERR_SESSION_REMOVE')
      console.error('response: ', resolvedRemove)
      PubSub.publish('ERR_SESSION_ADD', 'Error al eliminar la sessión a la Lista, reinténtelo.')
    }
  }
  catch(error){
    console.log('ELSE -> ERR_SESSION_REMOVE')
    console.error('response: ', error)
    PubSub.publish('ERR_SESSION_ADD', 'Error al eliminar la sessión a la Lista, reinténtelo.')
  }
}
function* auditLogging(action) {
  try {
    yield put(sharedActions.setLoaderVisible(userLoaderKeys.AUDIT_LOGGING, true))
    const resolvedAudit = yield call(userServices.auditLogging, action.body);

    if (resolvedAudit.statusCode === 200) {
      yield put(userActions.setAuditLogging(resolvedAudit.dataJson));
    } else {
      PubSub.publish('ERR_AUDIT_LOGGING')
    }
  } catch (error) {
    PubSub.publish('ERR_AUDIT_LOGGING', error)
  } finally {
    yield put(sharedActions.setLoaderVisible(userLoaderKeys.AUDIT_LOGGING, false))
  }
}



export default function* sagas() {
  //yield takeEvery('CLICK_BUTTON', sagaClickButton);

  yield takeEvery(constants.LOGIN, login);
  yield takeEvery('GET_SECRET_KEYS', getKeySession);
  yield takeEvery('DATA_AUTHENTICATED', dataAuthenticated);
  yield takeEvery('LOGOUT', logout);
  yield takeEvery(constants.SESSION_ADD,sessionAdd);
  yield takeEvery(constants.SESSION_REMOVE,sessionRemove);
  yield takeEvery(constants.AUDIT_LOGGING, auditLogging);
}