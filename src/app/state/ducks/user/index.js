import reducer from './reducers'
import api from './api'
import * as userSelectors from "./selectors"
import * as userMiddleware from './middleware'
import * as userOperations from './operations'
import userLoaderKeys from './loaderKeys'

export {
  api,
  userSelectors,
  userMiddleware,
  userOperations,
  userLoaderKeys
}
export default reducer