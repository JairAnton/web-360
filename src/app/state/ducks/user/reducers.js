import { createReducer } from '../../utils';

import { constants } from "./types";

const initialState = {
  isAuthenticated: false,
  initLoaderUser: false,
  sessionId: Date.now().toString(),
  auth: {
    cognitoGroups: [],
    loader: true,
    alert: false
  },
  status: '',
  maxConnection: '',
  statusLog: null,
};

const user = createReducer(initialState)({

  FINISH: (state, action) => {
    return { ...state, status: action.status }
  },

  SET_JWTTOKEN: (state, action) => {
    return { ...state, jwtToken: action.jwtToken }
  },

  SET_SECRET_KEYS: (state, action) => {
    return { ...state, secretKeys: action.keys }
  },

  [constants.SET_DATA_AUTHENTICATED]: (state, action) => {
    const {
      cognitoGroups,
      loader,
      alert
    } = action.dataAuth
    const auth = { cognitoGroups, loader, alert }
    return { ...state, auth }
  },
  [constants.SET_SESSION_ADD]: (state,action) => {
    const {
      status, maxConnection
    } = action.response
    return {
      ...state,
      status, maxConnection
    }
  },
  [constants.SET_SESSION_REMOVE]: (state,action) => {
    const {
      status
    } = action.response
    console.log(action.response)

    return {
      ...state,
      statusRemove: status
    }
  },
  [constants.SET_AUDIT_LOGGING]: (state, action) => {
    const {status} = action.response;
    return { ...state, statusLog: status }
  }

  // [constants.LOGOUT]: (state, action) => {
  //   return { ...state, jwtToken: null }
  // },
});

export default user;
