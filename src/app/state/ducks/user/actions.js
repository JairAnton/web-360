import { constants } from "./types"

export const finish = () => {
  return {
    type: 'FINISH',
    status: 2
  }
}

export const setJwtToken = (jwtToken) => ({
  type: 'SET_JWTTOKEN',
  jwtToken
})

export const getSecretKeys = () => ({
  type: 'GET_SECRET_KEYS'
})

export const setSecretKeys = (keys) => ({
  type: 'SET_SECRET_KEYS',
  keys
})

export const login = () => ({
  type: constants.LOGIN
})

export const logout = () => ({
  type: constants.LOGOUT,
})

export const authenticatedLogin = () => ({
  type: constants.DATA_AUTHENTICATED
})

export const setAuthenticatedLogin = (dataAuth) => ({
  type: constants.SET_DATA_AUTHENTICATED,
  dataAuth
})

export const sessionAdd = (body) => {
  return {
    type: constants.SESSION_ADD,
    body
  }
}
export const setSessionAdd = (response) => {
  return {
    type: constants.SET_SESSION_ADD,
    response
  }
}

export const sessionRemove = (body) => {
  return {
    type: constants.SESSION_REMOVE,
    body
  }
}

export const setSessionRemove = (body) => {
  return {
    type: constants.SET_SESSION_REMOVE,
    body
  }
}
export const auditLogging = (body) => ({
  type: constants.AUDIT_LOGGING,
  body
});

export const setAuditLogging = (response) => ({
  type: constants.SET_AUDIT_LOGGING,
  response
});