const urls = {
    login: 'https://domain001-api-crm-rimac.auth.us-east-2.amazoncognito.com/oauth2/token',
    sessionAdd: '/audit/connection/add',
    sessionRemove:'/audit/connection/remove',
    auditLogging: '/audit/logging/save'
  };
  
  export default urls;
  