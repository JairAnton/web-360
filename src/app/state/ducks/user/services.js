import { Auth } from 'aws-amplify'
import api from 'state/utils/api'
// import api from '../../../state/utils/api';
// import { constants as constantsHeaders } from "../../utils/constants";

export const login = async () => {
    // const { email, password } = credentials
    // return Auth.signIn(email, password)
    return Auth.federatedSignIn({
      customProvider:'AzureADProvider'
    }) // TODO: FOR FEDERATED SIGN IN
}

export const getKeySession = async jwtToken => {
    return {}
}

export const sessionAdd = async (body) => {
  const responseSessionAdd = await api.sessionAdd({ data: body});
  return responseSessionAdd;
}


export const sessionRemove = async (body) => {
  const responseSessionRemove = await api.sessionRemove({ data: body});
  return responseSessionRemove;
}


export const auditLogging = async (body) => {
  const responsePerson = await api.auditLogging({ data: body });
  return responsePerson;
}