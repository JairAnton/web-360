import KeyMirror from 'keymirror';

export const constants = KeyMirror({
  LOGIN: null,
  DATA_AUTHENTICATED: null,
  SET_DATA_AUTHENTICATED: null,
  LOGOUT: null,

  SESSION_ADD: null,
  SET_SESSION_ADD: null,

  SESSION_REMOVE: null,
  SET_SESSION_REMOVE: null,
  
  AUDIT_LOGGING: null,
  SET_AUDIT_LOGGING: null,
});