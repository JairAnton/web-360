import reducer from './reducers';
import api from './api';
import * as documentSelectors from "./selectors";
import * as documentMiddleware from './middleware';
import * as documentOperations from './operations';

export {
    api,
    documentSelectors,
    documentMiddleware,
    documentOperations
}
export default reducer;