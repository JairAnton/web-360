import { takeEvery, call, select, put } from 'redux-saga/effects';

import { constants } from './types';

import * as documentActions from './actions';

import * as documentServices from './services';

import history from '../../utils/history';

import { routes } from '../../utils/routes';

function* documentReportResults(action) {
  try {
    const resolvedDocument = yield call(documentServices.documentReportResults, action.body);
    if (resolvedDocument.statusCode === 200) {
      yield put(documentActions.setDocumentReportResults(resolvedDocument.dataJson));
    } else {
      yield put(documentActions.documentFailure({ type: 2, errorMessage: "Advertencia, ocurrrio un problema al llamar al servicio." }));
    }
  } catch (e) {
    yield put(documentActions.documentFailure({ type: 1, errorMessage: "Ocurrio un problema, llame a su backOffice." }));
  }
}

function* documentCarteClosingClaim(action) {
  try {
    const resolvedDocument = yield call(documentServices.documentCarteClosingClaim, action.body);
    if (resolvedDocument.statusCode === 200) {
      yield put(documentActions.setDocumentCarteClosingClaim(resolvedDocument.dataJson));
    } else {
      yield put(documentActions.documentFailure({ type: 2, errorMessage: "Advertencia, ocurrrio un problema al llamar al servicio." }));
    }
  } catch (e) {
    yield put(documentActions.documentFailure({ type: 1, errorMessage: "Ocurrio un problema, llame a su backOffice." }));
  }
}

function* documentExtensionTerms(action) {
  try {
    const resolvedDocument = yield call(documentServices.documentExtensionTerms, action.body);
    if (resolvedDocument.statusCode === 200) {
      yield put(documentActions.setDocumentExtensionTerms(resolvedDocument.dataJson));
    } else {
      yield put(documentActions.documentFailure({ type: 2, errorMessage: "Advertencia, ocurrrio un problema al llamar al servicio." }));
    }
  } catch (e) {
    yield put(documentActions.documentFailure({ type: 1, errorMessage: "Ocurrio un problema, llame a su backOffice." }));
  }
}

function* documentBookClaims(action) {
  try {
    const resolvedDocument = yield call(documentServices.documentBookClaims, action.body);
    if (resolvedDocument.statusCode === 200) {
      yield put(documentActions.setDocumentBookClaims(resolvedDocument.dataJson));
    } else {
      yield put(documentActions.documentFailure({ type: 2, errorMessage: "Advertencia, ocurrrio un problema al llamar al servicio." }));
    }
  } catch (e) {
    yield put(documentActions.documentFailure({ type: 1, errorMessage: "Ocurrio un problema, llame a su backOffice." }));
  }
}

function* documentNotificationProvider(action) {
  try {
    const resolvedDocument = yield call(documentServices.documentNotificationProvider, action.body);
    if (resolvedDocument.statusCode === 200) {
      yield put(documentActions.setDocumentNotificationProvider(resolvedDocument.dataJson));
    } else {
      yield put(documentActions.documentFailure({ type: 2, errorMessage: "Advertencia, ocurrrio un problema al llamar al servicio." }));
    }
  } catch (e) {
    yield put(documentActions.documentFailure({ type: 1, errorMessage: "Ocurrio un problema, llame a su backOffice." }));
  }
}

function* documentNotificationBroker(action) {
  try {
    const resolvedDocument = yield call(documentServices.documentNotificationBroker, action.body);
    if (resolvedDocument.statusCode === 200) {
      yield put(documentActions.setDocumentNotificationBroker(resolvedDocument.dataJson));
    } else {
      yield put(documentActions.documentFailure({ type: 2, errorMessage: "Advertencia, ocurrrio un problema al llamar al servicio." }));
    }
  } catch (e) {
    yield put(documentActions.documentFailure({ type: 1, errorMessage: "Ocurrio un problema, llame a su backOffice." }));
  }
}
function* auditLogging(action) {
  try {
    const resolvedAudit = yield call(documentServices.auditLogging, action.body);
  
    if (resolvedAudit.statusCode === 200) {
      yield put(documentActions.setAuditLogging(resolvedAudit.dataJson));
    } else {
      console.log('ERR_AUDIT_LOGGING')
    }
  } catch (error) {
    console.log('ERR_AUDIT_LOGGING', error)
  } 
}

export default function* sagas() {
  yield takeEvery(constants.DOCUMENT_REPORT_RESULTS, documentReportResults);
  yield takeEvery(constants.DOCUMENT_CARTE_CLOSING_CLAIM, documentCarteClosingClaim);
  yield takeEvery(constants.DOCUMENT_EXTENSION_TERMS, documentExtensionTerms);
  yield takeEvery(constants.DOCUMENT_BOOK_CLAIMS, documentBookClaims);
  yield takeEvery(constants.DOCUMENT_NOTIFICATION_PROVIDER, documentNotificationProvider);
  yield takeEvery(constants.DOCUMENT_NOTIFICATION_BROKER, documentNotificationBroker);
  yield takeEvery(constants.AUDIT_LOGGING, auditLogging)
}