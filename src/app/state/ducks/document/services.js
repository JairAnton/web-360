import api from '../../../state/utils/api';

import { constants as constantsHeaders } from "../../utils/constants";

export const documentReportResults = async (body) => {

    const responseDocument = await api.documentReportResults({ data: body });
    return responseDocument;
}

export const documentCarteClosingClaim = async (body) => {

    const responseDocument = await api.documentCarteClosingClaim({ data: body });
    return responseDocument;
}

export const documentExtensionTerms = async (body) => {

    const responseDocument = await api.documentExtensionTerms({ data: body });
    return responseDocument;
}

export const documentBookClaims = async (body) => {

    const responseDocument = await api.documentBookClaims({ data: body });
    return responseDocument;
}

export const documentNotificationProvider = async (body) => {

    const responseDocument = await api.documentNotificationProvider({ data: body });
    return responseDocument;
}

export const documentNotificationBroker = async (body) => {

    const responseDocument = await api.documentNotificationBroker({ data: body });
    return responseDocument;
}

export const auditLogging = async (body) => {
    const responsePerson = await api.auditLogging({ data: body });
    return responsePerson;
  }