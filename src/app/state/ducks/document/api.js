const urls = {
    documentReportResults: '/documento/pdf/informeResultados',
    documentCarteClosingClaim: '/documento/pdf/cartaCierreReclamo',
    documentExtensionTerms: '/documento/pdf/ampliacionTerminos',
    documentBookClaims: '/documento/pdf/libroReclamaciones',
    documentNotificationProvider: '/documento/pdf/notificacionProveedor',
    documentNotificationBroker: '/documento/pdf/notificacionBroker',
    auditLogging: '/audit/logging/save',
  };
  
  export default urls;
  