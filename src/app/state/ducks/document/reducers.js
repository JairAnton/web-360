import { createReducer } from '../../utils';

import { constants } from "./types";

const initialState = {
    report: {
        file: null,
        nroOperacion: null
    },
    carte: {
        file: null,
        nroOperacion: null
    },
    extension: {
        file: null,
        nroOperacion: null
    },
    claims: {
        file: null,
        nroOperacion: null
    },
    provider: {
        file: null,
        nroOperacion: null
    },
    broker: {
        file: null,
        nroOperacion: null
    },
    error: null,
    status: null
};

const document = createReducer(initialState)({
    [constants.DOCUMENTO_FAILURE]: (state, action) => {
        var error = action.error
        return { ...state, error }
    },
    [constants.SET_DOCUMENT_REPORT_RESULTS]: (state, action) => {
        var report = {
            file: action.document.file,
            nroOperacion: action.document.nroOperacion
        };

        return { ...state, report, error: null }
    },
    [constants.SET_DOCUMENT_CARTE_CLOSING_CLAIM]: (state, action) => {
        var carte = {
            file: action.document.file,
            nroOperacion: action.document.nroOperacion
        };

        return { ...state, carte, error: null }
    },
    [constants.SET_DOCUMENT_EXTENSION_TERMS]: (state, action) => {
        var extension = {
            file: action.document.file,
            nroOperacion: action.document.nroOperacion
        };

        return { ...state, extension, error: null }
    },
    [constants.SET_DOCUMENT_BOOK_CLAIMS]: (state, action) => {
        var claims = {
            file: action.document.file,
            nroOperacion: action.document.nroOperacion
        };

        return { ...state, claims, error: null }
    },
    [constants.SET_DOCUMENT_NOTIFICATION_PROVIDER]: (state, action) => {
        var provider = {
            file: action.document.file,
            nroOperacion: action.document.nroOperacion
        };

        return { ...state, provider, error: null }
    },
    [constants.SET_DOCUMENT_NOTIFICATION_BROKER]: (state, action) => {
        var broker = {
            file: action.document.file,
            nroOperacion: action.document.nroOperacion
        };

        return { ...state, broker, error: null }
    },
    [constants.SET_AUDIT_LOGGING]: (state, action) => {
        const {status} = action.response;
        return { ...state, status }
    }
});

export default document;
