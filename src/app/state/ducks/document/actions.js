import { constants } from "./types";

export const documentFailure = (error) => ({
    type: constants.DOCUMENT_FAILURE,
    error
});

export const documentReportResults = (body) => {
    return {
        type: constants.DOCUMENT_REPORT_RESULTS,
        body
    }
}

export const setDocumentReportResults = (document) => {
    return {
        type: constants.SET_DOCUMENT_REPORT_RESULTS,
        document
    }
}

export const documentCarteClosingClaim = (body) => {
    return {
        type: constants.DOCUMENT_CARTE_CLOSING_CLAIM,
        body
    }
}

export const setDocumentCarteClosingClaim = (document) => {
    return {
        type: constants.SET_DOCUMENT_CARTE_CLOSING_CLAIM,
        document
    }
}

export const documentExtensionTerms = (body) => {
    return {
        type: constants.DOCUMENT_EXTENSION_TERMS,
        body
    }
}

export const setDocumentExtensionTerms = (document) => {
    return {
        type: constants.SET_DOCUMENT_EXTENSION_TERMS,
        document
    }
}

export const documentBookClaims = (body) => {
    return {
        type: constants.DOCUMENT_BOOK_CLAIMS,
        body
    }
}

export const setDocumentBookClaims = (document) => {
    return {
        type: constants.SET_DOCUMENT_BOOK_CLAIMS,
        document
    }
}

export const documentNotificationProvider = (body) => {
    return {
        type: constants.DOCUMENT_NOTIFICATION_PROVIDER,
        body
    }
}

export const setDocumentNotificationProvider = (document) => {
    return {
        type: constants.SET_DOCUMENT_NOTIFICATION_PROVIDER,
        document
    }
}

export const documentNotificationBroker = (body) => {
    return {
        type: constants.DOCUMENT_NOTIFICATION_BROKER,
        body
    }
}

export const setDocumentNotificationBroker = (document) => {
    return {
        type: constants.SET_DOCUMENT_NOTIFICATION_BROKER,
        document
    }
}

export const auditLogging = (body) => ({
    type: constants.AUDIT_LOGGING,
    body
});
  
export const setAuditLogging = (response) => ({
    type: constants.SET_AUDIT_LOGGING,
    response
})
  
